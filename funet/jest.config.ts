module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    setupFiles: ['./setupJest.ts'],
    testMatch: ['<rootDir>/src/**/*.test.ts'],
    transform: {
        '^.+\\.ts$': 'ts-jest',
    },
};