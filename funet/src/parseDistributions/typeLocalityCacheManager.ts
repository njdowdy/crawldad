import { uuidv7 } from "uuidv7";

/**
 * Example type for a type locality item.
 */
export interface Locality {
    id: string;
    name: string;
}

/**
 * Internal Map keyed by name.toLowerCase().
 */
const localityCache: Map<string, Locality> = new Map();

/**
 * Retrieves a Locality by a case-insensitive (lowercased) name.
 * Returns undefined if no matching entry.
 */
export function getLocalityByName(locName: string): Locality | undefined {
    return localityCache.get(locName.toLowerCase());
}

/**
 * Creates a new Locality entry for the given name, if it does not already exist.
 * • If an entry with that case-insensitive name is already in the cache,
 *   that existing Locality is returned instead.
 * • Otherwise, a new Locality is created and inserted into the cache.
 */
export function createLocalityByName(locName: string): Locality {
    const key = locName.toLowerCase();
    if (localityCache.has(key)) {
        return localityCache.get(key)!; // Return existing if found
    }
    const newLocality: Locality = {
        id: uuidv7(),
        name: locName
    };
    localityCache.set(key, newLocality);
    return newLocality;
}

/**
 * Retrieves or creates a Locality by name. This is convenient if you are
 * processing textual place names (like "North Carolina") and want either an
 * existing Locality or a new one if not found.
 *
 * Usage:
 *   const locId = getOrCreateLocality("North Carolina").id;
 */
export function getOrCreateLocality(locName: string): Locality {
    const existing = getLocalityByName(locName);
    return existing ? existing : createLocalityByName(locName);
}

/**
 * (Optional) Retrieves a Locality by its UUIDv7 id, rather than its name.
 * This prevents accidentally passing an ID to the name-based cache functions.
 */
export function getLocalityById(locId: string): Locality | undefined {
    for (const loc of localityCache.values()) {
        if (loc.id === locId) {
            return loc;
        }
    }
    return undefined;
}

/**
 * Clears all data from the locality cache.
 */
export function clearLocalityCache(): void {
    localityCache.clear();
}

/**
 * Returns all localities in the cache as an array.
 */
export function getAllLocalitiesFromCache(): Locality[] {
    return Array.from(localityCache.values());
}

/**
 * Returns the count of all localities in the cache.
 */
export function getLocalityCacheSize(): number {
    return localityCache.size;
}
