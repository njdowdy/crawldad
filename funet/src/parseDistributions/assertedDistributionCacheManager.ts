import { uuidv7 } from "uuidv7";

/**
 * Example type for a distribution item.
 */
export interface Distribution {
    id: string;
    name: string;
}

/**
 * Internal Map keyed by name.toLowerCase().
 */
const distributionCache: Map<string, Distribution> = new Map();

/**
 * Retrieves a Distribution by a case-insensitive (lowercased) name.
 */
export function getDistributionFromCache(distName: string): Distribution | undefined {
    return distributionCache.get(distName.toLowerCase());
}

/**
 * Adds a new Distribution to the cache, keyed by distName.toLowerCase().
 * Returns the newly created and cached item.
 */
export function addDistributionToCache(distName: string): Distribution {
    const key = distName.toLowerCase();
    if (distributionCache.has(key)) {
        // Already in the cache, just return it
        return distributionCache.get(key)!;
    }

    const newDistribution: Distribution = {
        id: uuidv7(),
        name: distName
    };
    distributionCache.set(key, newDistribution);
    return newDistribution;
}

/**
 * Clears all data from the distribution cache.
 */
export function clearDistributionCache(): void {
    distributionCache.clear();
}

/**
 * Returns all distributions in the cache as an array.
 */
export function getAllDistributionsFromCache(): Distribution[] {
    return Array.from(distributionCache.values());
}

/**
 * Returns the count of all distributions in the cache.
 */
export function getDistributionCacheSize(): number {
    return distributionCache.size;
}
