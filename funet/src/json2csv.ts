import { clearHigherTaxonCache, getAllHigherTaxaFromCache } from "./parseHigherLevelTaxa/higherTaxonCacheManager";
import { clearHostCache, getAllHostsFromCache } from "./parseHosts/hostCacheManager";
import { clearTaxonCache, getAllTaxonFromCache } from "./parseNames/taxonCacheManager";
import { clearArticleCache, getAllArticlesFromCache } from "./parseRelatedLiterature/articleCacheManager";
import { clearIssueCache, getAllIssuesFromCache } from "./parseRelatedLiterature/issueCacheManager";
import { clearJournalCache, getAllJournalsFromCache } from "./parseRelatedLiterature/journalCacheManager";
import { clearPageCache, getAllPagesFromCache } from "./parseRelatedLiterature/pageCacheManager";
import { clearPersonCache, getAllPersonsFromCache } from "./parseRelatedLiterature/personCacheManager";
import { clearVolumeCache, getAllVolumesFromCache } from "./parseRelatedLiterature/volumeCacheManager";
import { clearWebResourceCache, getAllWebResourcesFromCache } from "./parseWebResource/webResourceCacheManager";
import { Article, HigherLevelTaxon, Host, Issue, Journal, Page, Person, TaxonRecord, Volume, WebResource } from "./types";
import {
    clearLocalityCache,
    getAllLocalitiesFromCache
} from "./parseDistributions/typeLocalityCacheManager";
import {
    clearDistributionCache,
    getAllDistributionsFromCache
} from "./parseDistributions/assertedDistributionCacheManager";
import { promises as fs } from "fs";
import path from "path";
import { loadCacheData } from "./fileUtilities";

// This regex checks for typical v7 UUID formatting (the version nibble is '7')
const uuidv7Regex = /^[0-9a-f]{8}-[0-9a-f]{4}-7[0-9a-f]{3}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

function isValidUUIDv7(id: string): boolean {
    return uuidv7Regex.test(id);
}

function isValidUUIDv7OrBlank(id: string): boolean {
    if (id === "") return true
    return uuidv7Regex.test(id);
}

function isValidRank(rank: string): boolean {
    const validRanks = [
        "clade",
        "order",
        "suborder",
        "superfamily",
        "family",
        "subfamily",
        "tribe",
        "subtribe",
    ];
    return validRanks.includes(rank.toLowerCase());
}

function isValidName(name: string): boolean {
    // string with no [0-9] or special characters
    // allowing only alphabetical letters (case-insensitive)
    return /^[A-Za-z]+$/.test(name);
}

export async function tsvFromHigherTaxaDataCache(data: HigherLevelTaxon[]): Promise<void> {
    // EXAMPLE HigherTaxaDataCache OBJECT
    // {
    //   "id": "0194057a-7cdb-77fe-b981-a9acc0b6b938",
    //   "rank": "subfamily",
    //   "name": "ludiinae",
    //   "parentId": "01940579-5949-7b9b-998f-e710a9f15a5d"
    // }
    // ---------------
    // TABLE 1 NAME: higher_taxonomy.tsv
    // TABLE 1 HEADERS: id,rank,name,parent_id
    // TABLE 1 DATA: data.id, data.rank, data.name, data.parentId
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION rank: string in the set of ["clade", "order", "suborder", "superfamily", "family", "subfamily", "tribe", "subtribe"]
    // TABLE 1 VALIDATION name: string with no [0-9] or special characters
    // TABLE 1 VALIDATION parentId: string in the form of a uuidv7 or null
    // ---------------

    const validRows: string[] = ["id\trank\tname\tparent_id"];
    const invalidRows: string[] = ["id\trank\tname\tparent_id"];

    for (const item of data) {
        const { id, rank, name, parentId } = item;
        const idOk = isValidUUIDv7(id);
        const rankOk = isValidRank(rank);
        const nameOk = isValidName(name);
        const parentIdOk = parentId !== null ? isValidUUIDv7(parentId) : true;

        if (!idOk) console.warn(`higher_taxonomy.tsv validation failed for id=${id}: invalid UUID`);
        if (!rankOk) console.warn(`higher_taxonomy.tsv validation failed for id=${id}: rank "${rank}" is invalid`);
        if (!nameOk) console.warn(`higher_taxonomy.tsv validation failed for id=${id}: name "${name}" is invalid`);
        if (!parentIdOk) console.warn(`higher_taxonomy.tsv validation failed for id=${id}: parentId "${parentId}" is invalid`);

        const passesValidation = idOk && rankOk && nameOk && parentIdOk;
        const row = [id, rank, name, parentId].join("\t");

        if (passesValidation) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    const validFilePath = path.join("./data/tsv_data", "higher_taxonomy.tsv");
    const invalidFilePath = path.join("./data/tsv_data", "higher_taxonomy_INVALID.tsv");

    await fs.writeFile(validFilePath, validRows.join("\n"), "utf8");
    await fs.writeFile(invalidFilePath, invalidRows.join("\n"), "utf8");
}

// Make the 'www.' portion optional, so URLs like http://something.com or http://www.something.com are valid.
const urlRegex = /^(https?:\/\/(www\.)?[\w\-._~:/?#\[\]@!$&'()*+,;=\S]+|)$/;

export async function tsvFromWebCache(data: WebResource[]): Promise<void> {
    // EXAMPLE WebCache OBJECT
    // {
    //     "id": "0194055f-147f-700f-8a63-795dc0e8da37",
    //     "abbreviation": "AfroMoths",
    //     "resourceName": "Afromoths, online database of Afrotropical moth species (Lepidoptera)",
    //     "externalLink": "http://www.afromoths.net",
    //     "webSubelement": [
    //       {
    //         "id": "0194055f-148b-7344-903e-87aa4c01f775",
    //         "externalLink": "https://www.afromoths.net/species_by_code/AGRICAPE",
    //         "subelementType": "web_page"
    //       },
    //       {
    //         "id": "0194055f-148b-7344-903e-87aa4c01f772",
    //         "externalLink": "https://www.afromoths.net/species_by_code/AGRICAPE",
    //         "subelementType": "web_page"
    //       },
    //     ]
    // }
    // ---------------
    // TABLE 1 NAME: repositories.tsv
    // TABLE 1 HEADERS: id,abbreviation,name,link
    // TABLE 1 DATA: data.id, data.abbreviation, data.resourceName, data.externalLink
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION abbreviation: string
    // TABLE 1 VALIDATION name: string
    // TABLE 1 VALIDATION link: string in the form of a URL or ""
    // ---------------
    // TABLE 2 NAME: resources.tsv
    // TABLE 2 HEADERS: id,repository_id,link,content_type
    // TABLE 2 DATA: data.webSubelement[j].id, data.id, data.webSubelement[j].externalLink, data.webSubelement[j].subelementType
    // TABLE 2 VALIDATION id: string in the form of a uuidv7
    // TABLE 2 VALIDATION repository_id: string in the form of a uuidv7
    // TABLE 2 VALIDATION link: string in the form of a URL or ""
    // TABLE 2 VALIDATION content_type: string in the set of ["web_page","pdf","image","video","audio","other"]
    // ---------------

    const repositoriesValid: string[] = ["id\tabbreviation\tname\tlink"];
    const repositoriesInvalid: string[] = ["id\tabbreviation\tname\tlink"];

    const resourcesValid: string[] = ["id\trepository_id\tlink\tcontent_type"];
    const resourcesInvalid: string[] = ["id\trepository_id\tlink\tcontent_type"];

    const validContentTypes = ["web_page", "pdf", "image", "video", "audio", "other"];

    for (const item of data) {
        const { id, abbreviation, resourceName, externalLink, webSubelement } = item;

        const idOk = isValidUUIDv7(id!);
        const abbreviationOk = typeof abbreviation === "string";
        const resourceNameOk = typeof resourceName === "string";
        const linkOk = urlRegex.test(externalLink || "");

        if (!idOk) console.warn(`repositories.tsv validation failed for id=${id}: invalid UUID`);
        if (!abbreviationOk) console.warn(`repositories.tsv validation failed for id=${id}: abbreviation invalid`);
        if (!resourceNameOk) console.warn(`repositories.tsv validation failed for id=${id}: resourceName invalid`);
        if (!linkOk) console.warn(`repositories.tsv validation failed for id=${id}: link "${externalLink}" invalid`);

        const repoPasses = idOk && abbreviationOk && resourceNameOk && linkOk;
        const repoRow = [id, abbreviation, resourceName, externalLink || ""].join("\t");

        if (repoPasses) {
            repositoriesValid.push(repoRow);
        } else {
            repositoriesInvalid.push(repoRow);
        }

        // Now handle webSubelement
        if (Array.isArray(webSubelement)) {
            for (const sub of webSubelement) {
                const { id: subId, externalLink: subLink, subelementType } = sub;
                const subIdOk = isValidUUIDv7(subId!);
                const mainIdOk = isValidUUIDv7(item.id!);
                const subLinkOk = urlRegex.test(subLink || "");
                const typeOk = validContentTypes.includes(subelementType);

                if (!subIdOk) console.warn(`resources.tsv validation failed for subId=${subId}: invalid UUID`);
                if (!mainIdOk) console.warn(`resources.tsv validation failed for repositoryId=${item.id}: invalid UUID`);
                if (!subLinkOk) console.warn(`resources.tsv validation failed for subId=${subId}: link "${subLink}" invalid`);
                if (!typeOk) console.warn(`resources.tsv validation failed for subId=${subId}: content_type "${subelementType}" invalid`);

                const subPasses = subIdOk && mainIdOk && subLinkOk && typeOk;
                const subRow = [subId, item.id, subLink || "", subelementType].join("\t");

                if (subPasses) {
                    resourcesValid.push(subRow);
                } else {
                    resourcesInvalid.push(subRow);
                }
            }
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "repositories.tsv"), repositoriesValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "repositories_INVALID.tsv"), repositoriesInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "resources.tsv"), resourcesValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "resources_INVALID.tsv"), resourcesInvalid.join("\n"), "utf8");
}

function isSingleCapitalLetterOrEmpty(str: string): boolean {
    // single capital letter or ""
    return str === "" || /^[\p{L}]$/u.test(str);
}

function isValidSurname(str: string): boolean {
    // Allow letters including diacritical marks, hyphens, spaces, periods, and apostrophes
    // This covers cases like "St. Germain", "García-López", "O'Brien", "von Linné"
    return /^[\p{L}\s\-\.']+$/u.test(str);
}

export async function tsvFromPersonCache(data: Person[]): Promise<void> {
    // EXAMPLE PersonCache OBJECT
    // {
    //     "id": "0194055f-2a83-777c-9134-0208c6214776",
    //     "easternNameOrder": false,
    //     "givenName": "A",
    //     "surname": "Lastuvka"
    // }
    // ---------------
    // TABLE 1 NAME: people.tsv
    // TABLE 1 HEADERS: id,given_name,nicknames,surname,birth_names,middle_names,suffixes,salutations,eastern_name_order
    // TABLE 1 DATA: data.id,data.givenName,null,data.surname,null,null,null,null,data.easternNameOrder
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION given_name: single capital letter or ""
    // TABLE 1 VALIDATION nicknames: null
    // TABLE 1 VALIDATION surname: string with no [0-9] characters
    // TABLE 1 VALIDATION birth_names: null
    // TABLE 1 VALIDATION middle_names: null
    // TABLE 1 VALIDATION suffixes: null
    // TABLE 1 VALIDATION salutations: null
    // TABLE 1 VALIDATION eastern_name_order: boolean false
    // ---------------

    const validRows: string[] = ["id\tgiven_name\tnicknames\tsurname\tbirth_names\tmiddle_names\tsuffixes\tsalutations\teastern_name_order"];
    const invalidRows: string[] = ["id\tgiven_name\tnicknames\tsurname\tbirth_names\tmiddle_names\tsuffixes\tsalutations\teastern_name_order"];

    for (const item of data) {
        const { id, givenName, surname, easternNameOrder } = item;
        const idOk = isValidUUIDv7(id!);
        const givenOk = isSingleCapitalLetterOrEmpty(givenName || "");
        const surnameOk = isValidSurname(surname || "");
        const easternOk = easternNameOrder === false; // must be boolean false

        if (!idOk) console.warn(`people.tsv validation failed for id=${id}: invalid UUID`);
        if (!givenOk) console.warn(`people.tsv validation failed for id=${id}: givenName "${givenName}" invalid (must be single capital letter or "")`);
        if (!surnameOk) console.warn(`people.tsv validation failed for id=${id}: surname "${surname}" invalid (no digits allowed)`);
        if (!easternOk) console.warn(`people.tsv validation failed for id=${id}: easternNameOrder "${easternNameOrder}" invalid (must be false)`);

        const passes = idOk && givenOk && surnameOk && easternOk;

        const row = [
            id,
            givenName || "",
            "null",
            surname || "",
            "null",
            "null",
            "null",
            "null",
            easternNameOrder === false ? "false" : String(easternNameOrder),
        ].join("\t");

        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "people.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "people_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

export async function tsvFromHostCache(data: Host[]): Promise<void> {
    // EXAMPLE HostCache OBJECT
    // {
    //     "id": "01940596-9a72-7e7e-8c48-e32f24ee77d4",
    //     "name": "Pteridium caudatum"
    // }
    // ---------------
    // TABLE 1 NAME: interactions.tsv
    // TABLE 1 HEADERS: id,canonical_name
    // TABLE 1 DATA: data.id, data.name
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION canonical_name: string with no [0-9]
    // ---------------

    const validRows: string[] = ["id\tcanonical_name"];
    const invalidRows: string[] = ["id\tcanonical_name"];

    for (const item of data) {
        const { id, name } = item;
        const idOk = isValidUUIDv7(id);
        const nameOk = isValidAnyString(name || "");

        if (!idOk) console.warn(`interactions.tsv validation failed for id=${id}: invalid UUID`);
        if (!nameOk) console.warn(`interactions.tsv validation failed for id=${id}: name "${name}" is invalid`);

        const passes = idOk && nameOk;
        const row = [id, name || ""].join("\t");
        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "interactions.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "interactions_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

export async function tsvFromJournalCache(data: Journal[]): Promise<void> {
    // EXAMPLE JournalCache OBJECT
    // {
    //     "id": "01940561-4a89-7146-bf01-2eef0596ae6e",
    //     "title": "",
    //     "abbreviation": "Bresl. ent. Z."
    // }
    // ---------------
    // TABLE 1 NAME: journals.tsv
    // TABLE 1 HEADERS: id,title,abbreviation
    // TABLE 1 DATA: data.id, data.title, data.abbreviation
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION title: string
    // TABLE 1 VALIDATION abbreviation: string
    // ---------------

    const validRows: string[] = ["id\ttitle\tabbreviation"];
    const invalidRows: string[] = ["id\ttitle\tabbreviation"];

    for (const item of data) {
        const { id, title, abbreviation } = item;
        const idOk = isValidUUIDv7(id!);
        const titleOk = typeof title === "string";
        const abbrevOk = typeof abbreviation === "string";

        if (!idOk) console.warn(`journals.tsv validation failed for id=${id}: invalid UUID`);
        if (!titleOk) console.warn(`journals.tsv validation failed for id=${id}: title invalid (must be string)`);
        if (!abbrevOk) console.warn(`journals.tsv validation failed for id=${id}: abbreviation invalid (must be string)`);

        const passes = idOk && titleOk && abbrevOk;
        const row = [id, title || "", abbreviation || ""].join("\t");
        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "journals.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "journals_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

// string with no special characters except possibly spaces or letters
function isValidLabel(label: string): boolean {
    if (label === '') return true
    return /^[A-Za-z0-9\-\/\\\.♂♀]+$/.test(label);
}

export async function tsvFromPageCache(data: Page[], articleCache?: any[]): Promise<void> {
    // The extra articleCache? parameter is optional in case you want to look up page->article relationship.
    // For demonstration, we'll skip the dynamic "QUERY" since we don't have the full environment.
    // Instead, we'll simply use null (or empty string) for article_id.
    //
    // EXAMPLE PageCache OBJECT
    // {
    //     "id": "0194055f-14cd-7f56-a4fe-d8ab9443bbb5",
    //     "label": "12",
    //     "type": "plate",
    //     "link": "https://archive.org/stream/proceedingsofgen80zool#page/n143/mode/1up",
    //     "subelements": [
    //       {
    //         "id": "01940560-0312-79b6-a358-2024be89c4a2",
    //         "type": "illustration",
    //         "label": "7"
    //       },
    //       {
    //         "id": "01940560-033b-755b-a4d4-936252a45c56",
    //         "type": "illustration",
    //         "label": "6"
    //       },
    //     ]
    // }
    // ---------------
    // TABLE 1 NAME: pages.tsv
    // TABLE 1 HEADERS: id,label,type,link,article_id,content
    // TABLE 1 DATA: data.id, data.label, data.type, data.link, {{ QUERY (articleCache[i].pages[j].id === data.id) and return matching article.id }}, null
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION label: string with no special characters
    // TABLE 1 VALIDATION type: string in the set of ["page","plate"]
    // TABLE 1 VALIDATION link: string in the form of a URL or ""
    // TABLE 1 VALIDATION content: null
    // ---------------
    // TABLE 2 NAME: page_elements.tsv
    // TABLE 2 HEADERS: id,pages_id,label,type,link,content,label_text
    // TABLE 2 DATA: data.subelement[j].id, data.id, data.subelement[j].label, data.subelement[j].type, data.subelement[j].link, null, null
    // TABLE 2 VALIDATION id: string in the form of a uuidv7
    // TABLE 2 VALIDATION pages_id: string in the form of a uuidv7
    // TABLE 2 VALIDATION label: string with no special characters
    // TABLE 2 VALIDATION type: string in the set of ["text","photograph","illustration","table","chart","multimedia","other"]
    // TABLE 2 VALIDATION link: string in the form of a URL or ""
    // TABLE 2 VALIDATION content: null
    // TABLE 2 VALIDATION label_text: null
    // ---------------

    const validPages: string[] = ["id\tlabel\ttype\tlink\tarticle_id\tcontent"];
    const invalidPages: string[] = ["id\tlabel\ttype\tlink\tarticle_id\tcontent"];

    const validElements: string[] = ["id\tpages_id\tlabel\ttype\tlink\tcontent\tlabel_text"];
    const invalidElements: string[] = ["id\tpages_id\tlabel\ttype\tlink\tcontent\tlabel_text"];

    const pageTypes = ["page", "plate"];
    const elementTypes = ["text", "photograph", "illustration", "table", "chart", "multimedia", "other"];

    for (const p of data) {
        const { id, label, type, link, subelements } = p;
        const idOk = isValidUUIDv7(id!);
        const labelOk = isValidLabel(label || "");
        const typeOk = pageTypes.includes(type);
        const linkOk = urlRegex.test(link || "");

        if (!idOk) console.warn(`pages.tsv validation failed for id=${id}: invalid UUID`);
        if (!labelOk) console.warn(`pages.tsv validation failed for id=${id}: label "${label}" invalid`);
        if (!typeOk) console.warn(`pages.tsv validation failed for id=${id}: type "${type}" invalid`);
        if (!linkOk) console.warn(`pages.tsv validation failed for id=${id}: link "${link}" invalid`);

        const pageValid = idOk && labelOk && typeOk && linkOk;
        // for demonstration, set article_id to "" or null if we can't actually query
        const article_id = "";
        const row = [id, label || "", type, link || "", article_id, "null"].join("\t");

        if (pageValid) {
            validPages.push(row);
        } else {
            invalidPages.push(row);
        }

        if (Array.isArray(subelements)) {
            for (const sub of subelements) {
                const { id: elId, label: elLabel, type: elType, link: elLink } = sub;
                const elIdOk = isValidUUIDv7(elId!);
                const pagesIdOk = isValidUUIDv7(p.id!);
                const elLabelOk = isValidLabel(elLabel || "");
                const elTypeOk = elementTypes.includes(elType!);
                const elLinkOk = urlRegex.test(elLink || "");

                if (!elIdOk) console.warn(`page_elements.tsv validation failed for id=${elId}: invalid UUID`);
                if (!pagesIdOk) console.warn(`page_elements.tsv validation failed for pages_id=${p.id}: invalid UUID`);
                if (!elLabelOk) console.warn(`page_elements.tsv validation failed for id=${elId}: label "${elLabel}" invalid`);
                if (!elTypeOk) console.warn(`page_elements.tsv validation failed for id=${elId}: type "${elType}" invalid`);
                if (!elLinkOk) console.warn(`page_elements.tsv validation failed for id=${elId}: link "${elLink}" invalid`);

                const elValid = elIdOk && pagesIdOk && elLabelOk && elTypeOk && elLinkOk;
                const elRow = [elId, p.id, elLabel || "", elType, elLink || "", "null", "null"].join("\t");

                if (elValid) {
                    validElements.push(elRow);
                } else {
                    invalidElements.push(elRow);
                }
            }
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "pages.tsv"), validPages.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "pages_INVALID.tsv"), invalidPages.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "page_elements.tsv"), validElements.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "page_elements_INVALID.tsv"), invalidElements.join("\n"), "utf8");
}


export async function tsvFromVolumeCache(data: Volume[]): Promise<void> {
    // EXAMPLE VolumeCache OBJECT
    // {
    //     "id": "019405ab-9fea-7ef6-9a2a-35bd039859cd",
    //     "label": "",
    //     "year": 1985,
    //     "bracketedYear": false,
    //     "journal": {
    //       "id": "019405ab-9fea-7ef6-9a2a-35bca47cb4fa",
    //       "title": "Zametki po sistematike medvedits (Lepidoptera, Arctiidae) gruppy rodov Diacrisia Hb. - Rhyparia Hb.",
    //       "abbreviation": ""
    //     }
    // }
    // ---------------
    // TABLE 1 NAME: volumes.tsv
    // TABLE 1 HEADERS: id,label,year,bracketed_year,link,journal_id
    // TABLE 1 DATA: data.id, data.label, data.year, data.bracketedYear, null, data.journal.id
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION label: string with no special characters
    // TABLE 1 VALIDATION year: number in the form of \d{4}
    // TABLE 1 VALIDATION bracketed_year: boolean true or false
    // TABLE 1 VALIDATION link: null
    // TABLE 1 VALIDATION journal_id: string in the form of a uuidv7
    // ---------------

    const validRows: string[] = ["id\tlabel\tyear\tbracketed_year\tlink\tjournal_id"];
    const invalidRows: string[] = ["id\tlabel\tyear\tbracketed_year\tlink\tjournal_id"];

    for (const v of data) {
        const { id, label, year, bracketedYear, journal } = v;

        const idOk = isValidUUIDv7(id!);
        const labelOk = isValidLabel(label || "");
        const yearOk = /^\d{4}$/.test(String(year));
        const bracketOk = typeof bracketedYear === "boolean";
        const journalOk = journal && isValidUUIDv7(journal.id!);

        if (!idOk) console.warn(`volumes.tsv validation failed for id=${id}: invalid UUID`);
        if (!labelOk) console.warn(`volumes.tsv validation failed for id=${id}: label "${label}" invalid`);
        if (!yearOk) console.warn(`volumes.tsv validation failed for id=${id}: year "${year}" invalid (must be \\d{4})`);
        if (!bracketOk) console.warn(`volumes.tsv validation failed for id=${id}: bracketedYear "${bracketedYear}" invalid (must be boolean)`);
        if (!journalOk) console.warn(`volumes.tsv validation failed for id=${id}: journalId invalid or missing`);

        const passes = idOk && labelOk && yearOk && bracketOk && journalOk;

        const row = [
            id,
            label || "",
            String(year || ""),
            String(bracketedYear),
            "null",
            journal ? journal.id : "",
        ].join("\t");

        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "volumes.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "volumes_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

export async function tsvFromIssueCache(data: Issue[]): Promise<void> {
    // EXAMPLE IssueCache OBJECT
    // {
    //     "id": "0194055f-1482-7501-9e2f-00c4f5a12c24",
    //     "label": "2",
    //     "externalLink": "http://herba.msu.ru/shipunov/school/books/annotir_katalog_nasek_daln_vostoka_2016_2.pdf",
    //     "volume": {
    //       "id": "0194055f-1481-7e73-b51a-285af4d7bca7",
    //       "label": "8",
    //       "year": 1921,
    //       "bracketedYear": false,
    //       "journal": {
    //         "id": "0194055f-1481-7e73-b51a-285980be4a81",
    //         "title": "",
    //         "abbreviation": "Ann. Transv. Mus."
    //       }
    //     }
    // }
    // ---------------
    // TABLE 1 NAME: issues.tsv
    // TABLE 1 HEADERS: id,label,link,volume_id
    // TABLE 1 DATA: data.id, data.label, data.externalLink, data.volume.id
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION label: string with no special characters
    // TABLE 1 VALIDATION link: string in the form of a URL or ""
    // TABLE 1 VALIDATION volume_id: string in the form of a uuidv7
    // ---------------

    const validRows: string[] = ["id\tlabel\tlink\tvolume_id"];
    const invalidRows: string[] = ["id\tlabel\tlink\tvolume_id"];

    for (const issue of data) {
        const { id, label, externalLink, volume } = issue;
        const idOk = isValidUUIDv7(id!);
        const labelOk = isValidLabel(label || "");
        const linkOk = urlRegex.test(externalLink || "");
        const volumeOk = volume && isValidUUIDv7(volume.id!);

        if (!idOk) console.warn(`issues.tsv validation failed for id=${id}: invalid UUID`);
        if (!labelOk) console.warn(`issues.tsv validation failed for id=${id}: label "${label}" invalid`);
        if (!linkOk) console.warn(`issues.tsv validation failed for id=${id}: link "${externalLink}" invalid`);
        if (!volumeOk) console.warn(`issues.tsv validation failed for id=${id}: volumeId invalid or missing`);

        const passes = idOk && labelOk && linkOk && volumeOk;
        const row = [id, label || "", externalLink || "", volume ? volume.id : ""].join("\t");

        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "issues.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "issues_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

// "string with no special characters or ''" – let's allow letters, digits, underscore, minus, basically minimal check:
function isValidAbbreviation(abbrev: string): boolean {
    // Some references want "no special chars," but the example includes periods or parentheses.
    // We'll do a minimal check that it's not harmful. Adjust as needed.
    return /^[A-Za-z0-9\s\.\(\)\-]*$/.test(abbrev);
}

// "string comprised of [0-9]+":
function isFunetId(str: string): boolean {
    return /^[0-9]+$/.test(str);
}

export async function tsvFromArticleCache(data: Article[]): Promise<void> {
    // EXAMPLE ArticleCache OBJECT
    // {
    //     "id": "0194055f-1482-7501-9e2f-0129f6845082",
    //     "citedAs": [
    //       "Meyrick & Hamilton, 1921","Meyrick, 1920"
    //     ],
    //     "title": "Descriptions of South African Micro-Lepidoptera. Part 1",
    //     "issue": {
    //       "id": "0194055f-1482-7501-9e2f-00c4f5a12c24",
    //       "label": "2",
    //       "volume": {
    //         "id": "0194055f-1481-7e73-b51a-285af4d7bca7",
    //         "label": "8",
    //         "year": 1921,
    //         "bracketedYear": false,
    //         "journal": {
    //           "id": "0194055f-1481-7e73-b51a-285980be4a81",
    //           "title": "",
    //           "abbreviation": "Ann. Transv. Mus."
    //         }
    //       }
    //     },
    //     "tags": [],
    //     "pages": [
    //       {
    //         "id": "0194055f-1482-7501-9e2f-00c5e551561b",
    //         "type": "page",
    //         "label": "49",
    //         "link": "https://archive.org/stream/annalsoftran819211922tran#page/n122/mode/1up"
    //       },
    //       {
    //         "id": "0194055f-1482-7501-9e2f-00c63ec2ef4d",
    //         "type": "page",
    //         "label": "50",
    //         "link": "https://archive.org/stream/annalsoftran819211922tran#page/n123/mode/1up"
    //       }
    //     ],
    //     "authors": [
    //       {
    //         "author": {
    //           "id": "0194055f-1481-7e73-b51a-2858d9c83900",
    //           "easternNameOrder": false,
    //           "surname": "Meyrick"
    //         },
    //         "position": 0
    //       },    {
    //         "author": {
    //           "id": "0194055f-1481-7e73-b51a-2858d9c83911",
    //           "easternNameOrder": false,
    //           "surname": "Hamilton"
    //         },
    //         "position": 1
    //       }
    //     ],
    //     "abbreviation": "AUCL",
    //     "externalLink": "https://www.zobodat.at/pdf/Veroeff-Uebersee-Mus-Bremen_1_0223-0277.pdf",
    //     "funetRefLink": "20128",
    //     "doi": null,
    //     "notes": [
    //       "in Brohmer"
    //     ]
    // }
    // ---------------
    // TABLE 1 NAME: articles.tsv
    // TABLE 1 HEADERS: id,cited_as,title,issue_id,abbreviation,link,funet_id,doi,notes
    // TABLE 1 DATA: data.id, data.citedAs, data.title, data.issue.id, data.abbreviation, data.externalLink, data.funetRefLink, null, data.notes
    // TABLE 1 VALIDATION id: string in the form of a uuidv7
    // TABLE 1 VALIDATION cited_as: string array
    // TABLE 1 VALIDATION title: string or ""
    // TABLE 1 VALIDATION issue_id: string in the form of a uuidv7
    // TABLE 1 VALIDATION abbreviation: string with no special characters or ""
    // TABLE 1 VALIDATION link: string in the form of a URL or ""
    // TABLE 1 VALIDATION funet_id: string comprised of [0-9]+
    // TABLE 1 VALIDATION doi: null
    // TABLE 1 VALIDATION notes: string array or null
    // ---------------
    // TABLE 2 NAME: articles_authors.tsv
    // TABLE 2 HEADERS: articles_id,person_id,position
    // TABLE 2 DATA: data.id, data.authors[j].author.id, data.authors[j].position
    // TABLE 2 VALIDATION articles_id: string in the form of a uuidv7
    // TABLE 2 VALIDATION person_id: string in the form of a uuidv7
    // TABLE 2 VALIDATION position: number
    // ---------------

    const articlesValid: string[] = ["id\tcited_as\ttitle\tissue_id\tabbreviation\tlink\tfunet_id\tdoi\tnotes"];
    const articlesInvalid: string[] = ["id\tcited_as\ttitle\tissue_id\tabbreviation\tlink\tfunet_id\tdoi\tnotes"];

    const authorsValid: string[] = ["articles_id\tperson_id\tposition"];
    const authorsInvalid: string[] = ["articles_id\tperson_id\tposition"];

    for (const article of data) {
        let {
            id,
            citedAs,
            title,
            issue,
            abbreviation,
            externalLink,
            funetRefLink,
            notes,
            authors,
        } = article;

        if (notes === undefined) {
            notes = []
        }

        // We expect: id is valid v7, citedAs is array of strings, issue.id is v7, abbreviation is valid,
        // link is valid URL or '', funetRefLink is numeric, doi is null, notes is array or null
        if (abbreviation === undefined) abbreviation = ""
        const isIdOK = isValidUUIDv7(id!);
        const isCitedAsOK = Array.isArray(citedAs) && citedAs.every(c => typeof c === "string");
        const isTitleOK = typeof title === "string";
        let isIssueOK = false;
        if (issue && isValidUUIDv7(issue.id!)) {
            isIssueOK = true;
        }
        if (issue === undefined) {
            isIssueOK = true;
        }
        const isAbbreviationOK = abbreviation === "" || isValidAbbreviation(abbreviation);
        const isLinkOK = urlRegex.test(externalLink || "");
        const isFunetIdOK = funetRefLink === undefined ? true : (funetRefLink ? isFunetId(funetRefLink) : false);
        const isNotesOK =
            notes === null ||
            (Array.isArray(notes) && notes.every(n => typeof n === "string"));

        const passesArticle =
            isIdOK && isCitedAsOK && isTitleOK && isIssueOK &&
            isAbbreviationOK && isLinkOK && isFunetIdOK && isNotesOK;

        if (!isIdOK) console.warn(`Article validation failed for ${id}: Invalid UUID`);
        if (!isCitedAsOK) console.warn(`Article validation failed for ${id}: Invalid isCitedAsOK ${isCitedAsOK}`);
        if (!isTitleOK) console.warn(`Article validation failed for ${id}: Invalid title ${title}`);
        if (!isIssueOK) console.warn(`Article validation failed for ${id}: Invalid issue ${issue}`);
        if (!isAbbreviationOK) console.warn(`Article validation failed for ${id}: Invalid abbreviation ${abbreviation}`);
        if (!isLinkOK) console.warn(`Article validation failed for ${id}: Invalid externalLink ${externalLink}`);
        if (!isFunetIdOK) console.warn(`Article validation failed for ${id}: Invalid funetRefLink ${funetRefLink}`);
        if (!isNotesOK) console.warn(`Article validation failed for ${id}: Invalid notes ${notes}`);


        const row = [
            id,
            // convert citedAs array to a pipe-joined or comma-joined string:
            (citedAs || []).join("|"),
            title || "",
            issue ? issue.id : "",
            abbreviation || "",
            externalLink || "",
            funetRefLink || "",
            "null", // doi
            notes ? notes.join("|") : "" // flatten notes array
        ].join("\t");

        if (passesArticle) {
            articlesValid.push(row);
        } else {
            articlesInvalid.push(row);
        }

        // Now the authors
        if (Array.isArray(authors)) {
            for (const a of authors) {
                const { author: person, position } = a;
                const validAuthor = (
                    isIdOK &&
                    person &&
                    isValidUUIDv7(person.id!) &&
                    typeof position === "number"
                );

                if (!validAuthor) {
                    if (!isIdOK) console.warn(`articles_authors.tsv validation failed for article_id=${id}: invalid UUID`);
                    if (!person || !isValidUUIDv7(person.id!)) console.warn(`articles_authors.tsv validation failed for person_id=${person?.id}: invalid UUID`);
                    if (typeof position !== "number") console.warn(`articles_authors.tsv validation failed for article_id=${id}: position "${position}" invalid`);
                }

                const authorRow = [id, person ? person.id : "", String(position)].join("\t");
                if (validAuthor) {
                    authorsValid.push(authorRow);
                } else {
                    authorsInvalid.push(authorRow);
                }
            }
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "articles.tsv"), articlesValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "articles_INVALID.tsv"), articlesInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "articles_authors.tsv"), authorsValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "articles_authors_INVALID.tsv"), authorsInvalid.join("\n"), "utf8");
}

/**
 * This helper only checks the format. Real Version 7 generation is not native to Node.js yet.
 * If you rely on real v7, consider a specialized library or adapt this method.
 */
function generateUUIDv7(): string {
    // We'll generate a random v4 and tweak a nibble to "7" for demonstration ONLY.
    const v4 = crypto.randomUUID();
    // By RFC, positions 14-15 in a v4 are "4," we can pretend it's "7."
    return v4.substring(0, 14) + "7" + v4.substring(15);
}


// "string with no [0-9]"
function isValidNameNoDigits(str: string | undefined | null): boolean {
    if (!str) return false;
    return /^[A-Za-z\s\-]+$/.test(str.trim());
}

function isValidAnyString(str: unknown): boolean {
    return typeof str === "string";
}


function isValidTaxonomicStatus(s: string): boolean {
    // allowed: ["accepted","misapplied","junior_synonym","objective_synonym","subjective_synonym","senior_synonym"]
    const valid = [
        "accepted",
        "misapplied",
        "junior_synonym",
        "objective_synonym",
        "subjective_synonym",
        "senior_synonym",
    ];
    return valid.includes(s);
}

function isValidNomenclaturalStatus(status: string | null | undefined, valid: boolean): boolean {
    const set = new Set([
        "nomen_alternativum", "nomen_conservandum", "nomen_dubium", "nomen_ambiguum", "nomen_confusum", "nomen_perplexum",
        "nomen_periculosum", "nomen_erratum", "nomen_illegitimum", "nomen_invalidum", "nomen_manuscriptum", "nomen_monstrositatum",
        "nomen_novum", "nomen_nudum", "nomen_oblitum", "nomen_protectum", "nomen_rejiciendum", "nomen_suppressum", "nomen_vanum",
        "nomina_circumscribentia", "species_inquirenda"
    ]);

    if (valid) {
        // if valid is true, nomenclatural_status must be null or undefined
        if (status == null) {
            return true;
        }
        // if valid is true and status is a string (any string), return false
        if (typeof status === "string") {
            return false;
        }
    } else {
        // if valid is false and status is null or undefined, return true
        if (status == null) {
            return true;
        }
        // if valid is false and status is a string, check if it is in the set
        if (typeof status === "string") {
            if (set.has(status)) {
                return true;
            } else {
                return false;
            }
        }
    }

    return false;
}


function isValidYearString(str: string | undefined): boolean {
    // name_published_in_year: string in the form of [0-9]+ or null
    // for example "1921"
    if (!str) return false;
    return /^[0-9]+$/.test(str);
}

/**
 * If rank is subgenus/species/infraspecies => need a name with no digits or null.
 */
function isValidOptionalEpithet(epithet: string | undefined, rank: string, relevantRanks: string[]): boolean {
    // if rank is in relevantRanks => check no digits or null
    if (!relevantRanks.includes(rank)) {
        // must be null or undefined
        return epithet == null;
    } else {
        // must be no digits or empty
        // we'll allow an empty string as "null"
        if (epithet && isValidAnyString(epithet)) {
            return true
        }
        return epithet == null || /^\(?\??[A-Za-z\-]+\)?$/.test(epithet.trim());
    }
}

function isValidBoolean(val: unknown): boolean {
    return val === true || val === false;
}

function isValidInfraspeciesClass(cls: string | undefined, rank: string): boolean {
    // if data.taxonRank === "infraspecies" => must be in set
    // ["subspecies","variant","form","aberration","subvariety","forma_specialis","cultivar"]
    const set = new Set(["subspecies", "variant", "form", "aberration", "subvariety", "forma_specialis", "cultivar"]);
    if (rank !== "infraspecies") {
        return cls == null;
    } else {
        return typeof cls === "string" && set.has(cls);
    }
}

function isValidOriginalRank(rank: string | undefined): boolean {
    if (!rank) return false;
    const set = new Set(["genus", "subgenus", "species", "infraspecies"]);
    return set.has(rank);
}

/**
 * If originalRank === 'genus' => originalGenus must be string with no [0-9], else null
 */
function isValidOriginalGenus(genus: string | undefined, originalRank: string): boolean {
    if (originalRank !== "genus" && genus === undefined) {
        return true;
    } else {
        // Validates that genus is not null and contains only letters/hyphens after removing optional leading '?' 
        // For example: "Metarctia" or "?Metarctia" would be valid, but "123" or null would be invalid
        return genus !== null && isValidNameNoDigits(genus?.replace(/^\?/, ''));
    }
}

/**
 * If originalRank === 'infraspecies' => must be in the same set as infraspecies_class
 */
function isValidOriginalInfraspeciesClass(cls: string | undefined, originalRank: string): boolean {
    const set = new Set(["subspecies", "variant", "form", "aberration", "subvariety", "forma_specialis", "cultivar"]);
    if (originalRank !== "infraspecies") {
        return cls == null;
    } else {
        return typeof cls === "string" && set.has(cls);
    }
}

/**
 * We'll store transliterations as JSON if present. 
 * Table spec says "transliterations: array of { id: string, letter: string, location: number, replacementCharacter: string } objects or null"
 */
function isValidTransliterations(
    translits: any
): boolean {
    if (translits == null) return true; // can be null
    if (!Array.isArray(translits)) return false;
    return translits.every((t) => {
        if (typeof t !== "object" || t == null) return false;
        const hasId = typeof t.id === "string" && isValidUUIDv7(t.id);
        const hasLetter = typeof t.letter === "string";
        const hasLoc = typeof t.location === "number";
        const hasRepl = typeof t.replacementCharacter === "string";
        return hasId && hasLetter && hasLoc && hasRepl;
    });
}

/**
 * New function to export localities from a separate cache, since typeLocality is now a direct uuidv7.
 * Table 4: localities.tsv
 * Columns: id, name
 * Validation:
 *  • id: valid v7
 *  • name: string
 */
export async function tsvFromLocalityCache(data: { id: string; name: string; }[]): Promise<void> {
    const validRows: string[] = ["id\tname"];
    const invalidRows: string[] = [...validRows];

    for (const loc of data) {
        const { id, name } = loc;
        const idOk = isValidUUIDv7(id);
        const nameOk = typeof name === "string";

        if (!idOk) console.warn(`localities.tsv validation failed for id=${id}: invalid UUID`);
        if (!nameOk) console.warn(`localities.tsv validation failed for id=${id}: name "${name}" invalid`);

        const passes = idOk && nameOk;
        const row = [id, name].join("\t");
        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "localities.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "localities_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

function transformDataConnectionType(dataConnectionType: string | undefined, mentionedAs: any) {
    // This condition is true if:
    //   1) dataConnectionType is one of ["original_description", "namePublishedIn", "treated_as_type", "treated_as_synonym"]
    //      (in which case mentionedAs can be missing),
    // OR
    //   2) dataConnectionType is one of ["applied_name", "unresolved_name", "misidentification", "misapplication"]
    //      AND mentionedAs is a string (i.e., mentionedAs must be present and of type string here).
    const isValidConnection =
        ["original_description", "namePublishedIn", "treated_as_type", "treated_as_synonym"].includes(dataConnectionType || "") ||
        (
            typeof mentionedAs === "string" &&
            ["applied_name", "unresolved_name", "misidentification", "misapplication"].includes(dataConnectionType || "")
        );

    if (isValidConnection) {
        // Perform your logic for valid connections here
        return true;
    } else {
        // Handle invalid cases here
        return false;
    }
}

/**
 * New function to export asserted distributions from a separate cache.
 * Table 12: asserted_distributions.tsv
 * Columns: id, name
 * Validation:
 *   • id: valid v7
 *   • name: string
 */
export async function tsvFromAssertedDistributionCache(
    data: { id: string; name: string; }[]
): Promise<void> {
    const validRows: string[] = ["id\tname"];
    const invalidRows: string[] = [...validRows];

    for (const dist of data) {
        const { id, name } = dist;
        const idOk = isValidUUIDv7(id);
        const nameOk = typeof name === "string";

        if (!idOk) console.warn(`asserted_distributions.tsv validation failed for id=${id}: invalid UUID`);
        if (!nameOk) console.warn(`asserted_distributions.tsv validation failed for id=${id}: name "${name}" invalid`);

        const passes = idOk && nameOk;
        const row = [id, name].join("\t");
        if (passes) {
            validRows.push(row);
        } else {
            invalidRows.push(row);
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "asserted_distributions.tsv"), validRows.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "asserted_distributions_INVALID.tsv"), invalidRows.join("\n"), "utf8");
}

/**
 * Refactored version of tsvFromTaxonCache.
 * Now:
 *   • typeLocality is already a uuidv7 in the record (rather than a string needing a new ID).
 *   • assertedDistributions is an array of uuidv7 in each taxon record
 *     (rather than an array of names).
 * We no longer create new localities or distribution entries here; 
 * we only reference them if valid v7 in bridging tables.
 */
export async function tsvFromTaxonCache(data: TaxonRecord[]): Promise<void> {
    // Table 1: taxa.tsv
    const taxaValid: string[] = [
        "id\tparent_id\taccepted_name_usage\taccepted_name_id\tcanonical_name\tscientific_name\tscientific_name_authorship\tspecific_epithet\tinfrageneric_epithet\tinfraspecific_epithet\tname_published_in_year\tvalid\texplicit_higher_taxon\texplicit_specific_epithet\ttaxon_rank\ttaxonomic_status\tnomenclatural_status\ttype_taxon_id\ttype_locality_id\tinfraspecies_class\ttransliterations\toriginal_rank\toriginal_genus\toriginal_specific_epithet\toriginal_infraspecific_epithet\toriginal_infrageneric_epithet\toriginal_infraspecies_class"
    ];
    const taxaInvalid: string[] = [...taxaValid];

    // Table 2: vernacular_names.tsv
    const vernacularNamesValid: string[] = ["id\tname\tlanguage"];
    const vernacularNamesInvalid: string[] = [...vernacularNamesValid];

    // Table 3: taxa_vernacular_names.tsv
    const taxaVernacularNamesValid: string[] = ["taxa_id\tvernacular_names_id\tregion"];
    const taxaVernacularNamesInvalid: string[] = [...taxaVernacularNamesValid];

    // Table 5: taxa_interactions.tsv
    const taxaInteractionsValid: string[] = ["taxa_id\tinteractions_id\tinteraction"];
    const taxaInteractionsInvalid: string[] = [...taxaInteractionsValid];

    // Table 6: unresolved_names.tsv
    const unresolvedNamesValid: string[] = ["id\ttaxa_id\tname\tconnection_class"];
    const unresolvedNamesInvalid: string[] = [...unresolvedNamesValid];

    // Table 7: citations1.tsv
    const citations1Valid: string[] = ["id\tresource_id\tresource_class"];
    const citations1Invalid: string[] = [...citations1Valid];

    // Table 8: unresolved_names_citations1.tsv
    const unresolvedNamesCitations1Valid: string[] = ["unresolved_names_id\tcitations1_id"];
    const unresolvedNamesCitations1Invalid: string[] = [...unresolvedNamesCitations1Valid];

    // Table 9: citations2.tsv
    const citations2Valid: string[] = ["id\tresource_id\tresource_class"];
    const citations2Invalid: string[] = [...citations2Valid];

    // Table 10: data_connections.tsv
    const dataConnectionsValid: string[] = ["id\ttaxa_id\tname\tconnection_class"];
    const dataConnectionsInvalid: string[] = [...dataConnectionsValid];

    // Table 11: data_connections_citations2.tsv
    const dataConnectionsCitations2Valid: string[] = ["data_connections_id\tcitations2_id"];
    const dataConnectionsCitations2Invalid: string[] = [...dataConnectionsCitations2Valid];

    // Table 13: taxa_asserted_distributions.tsv
    const taxaAssertedDistributionsValid: string[] = ["taxa_id\tasserted_distributions_id"];
    const taxaAssertedDistributionsInvalid: string[] = [...taxaAssertedDistributionsValid];

    for (const record of data) {
        const {
            id,
            parentId,
            acceptedNameUsage,
            acceptedNameId,
            canonicalName,
            scientificName,
            scientificNameAuthorship,
            specificEpithet,
            infragenericEpithet,
            infraspecificEpithet,
            namePublishedInYear,
            valid,
            explicitHigherTaxon,
            explicitSpecificEpithet,
            taxonRank,
            taxonomicStatus,
            nomenclaturalStatus,
            typeTaxonId,
            typeLocality, // now a uuidv7, or null
            infraspeciesType,
            transliterated,
            originalRank,
            originalGenus,
            originalSpecificEpithet,
            originalInfraspeciesType,
            originalInfragenericEpithet,
            originalInfraspecificEpithet,
            vernacularNames,
            unresolvedChildren,
            dataConnections,
            assertedDistribution,
            interactions
        } = record;

        // validation for accepted usage & acceptedNameId depends on valid
        const acceptedUsageOk = isValidAnyString(acceptedNameUsage);
        const acceptedIdOk = acceptedNameId === '' ? true : isValidUUIDv7(acceptedNameId);

        const canonicalNameOK = isValidAnyString(canonicalName);
        const scientificNameOK = isValidAnyString(scientificName);
        const authorshipOK = (scientificNameAuthorship === null || typeof scientificNameAuthorship === "string");
        const yearOK = (namePublishedInYear === null || isValidYearString(namePublishedInYear));
        const validOK = isValidBoolean(valid);
        const explicit1OK = isValidBoolean(explicitHigherTaxon);
        const explicit2OK = isValidBoolean(explicitSpecificEpithet);
        const rankOK = taxonRank && ["genus", "subgenus", "species", "infraspecies"].includes(taxonRank);
        const statusOK = taxonomicStatus && isValidTaxonomicStatus(taxonomicStatus);
        const nomenStatusOK = isValidNomenclaturalStatus(nomenclaturalStatus, Boolean(valid));
        const typeTaxonIdOK = (typeTaxonId == null) || isValidUUIDv7(typeTaxonId);
        const infraspeciesClassOK = isValidInfraspeciesClass(infraspeciesType, taxonRank || "");
        const translitsOK = isValidTransliterations(transliterated);
        const originalRankOK = isValidOriginalRank(originalRank);
        const originalGenusOK = isValidOriginalGenus(originalGenus, originalRank || "");
        const originalSpecificOK = originalSpecificEpithet ? isValidAnyString(originalSpecificEpithet) : true;
        const originalInfraspTypeOK = isValidOriginalInfraspeciesClass(originalInfraspeciesType, originalRank || "");
        const originalInfragenericEpithetOK = originalInfragenericEpithet ? isValidAnyString(originalInfragenericEpithet) : true;
        const originalInfraspecificEpithetOK = originalInfraspecificEpithet ? isValidAnyString(originalInfraspecificEpithet) : true;

        const infragenericOK = isValidOptionalEpithet(infragenericEpithet, taxonRank || "", ["subgenus", "species", "infraspecies"]);
        const specificOK = isValidOptionalEpithet(specificEpithet, taxonRank || "", ["species", "infraspecies"]);
        const infraspecificOK = isValidOptionalEpithet(infraspecificEpithet, taxonRank || "", ["infraspecies"]);

        const parentOk = parentId ? isValidUUIDv7(parentId) : true;
        const idOk = isValidUUIDv7(id!);

        // Check the typeLocality as a direct uuidv7 or null
        let typeLocalityId = "";
        let typeLocalityOK = true;
        if (typeLocality) {
            typeLocalityOK = isValidUUIDv7(typeLocality);
            typeLocalityId = typeLocality;
        }

        const passesTaxa =
            idOk &&
            parentOk &&
            acceptedUsageOk &&
            acceptedIdOk &&
            canonicalNameOK &&
            scientificNameOK &&
            authorshipOK &&
            infragenericOK &&
            specificOK &&
            infraspecificOK &&
            (yearOK || namePublishedInYear === null) &&
            validOK &&
            explicit1OK &&
            explicit2OK &&
            rankOK &&
            statusOK &&
            nomenStatusOK &&
            typeTaxonIdOK &&
            infraspeciesClassOK &&
            translitsOK &&
            originalRankOK &&
            originalGenusOK &&
            originalSpecificOK &&
            originalInfraspTypeOK &&
            originalInfragenericEpithetOK &&
            originalInfraspecificEpithetOK &&
            typeLocalityOK;

        if (!idOk) console.warn(`Taxa validation failed for ${id}: Invalid UUID`);
        if (!parentOk) console.warn(`Taxa validation failed for ${id}: Invalid parentId ${parentId}`);
        if (!acceptedUsageOk) console.warn(`Taxa validation failed for ${id}: Invalid acceptedNameUsage ${acceptedNameUsage}`);
        if (!acceptedIdOk) console.warn(`Taxa validation failed for ${id}: Invalid acceptedNameId ${acceptedNameId}`);
        if (!canonicalNameOK) console.warn(`Taxa validation failed for ${id}: Invalid canonicalName ${canonicalName}`);
        if (!scientificNameOK) console.warn(`Taxa validation failed for ${id}: Invalid scientificName ${scientificName}`);
        if (!authorshipOK) console.warn(`Taxa validation failed for ${id}: Invalid authorship ${scientificNameAuthorship}`);
        if (!infragenericOK) console.warn(`Taxa validation failed for ${id}: Invalid infragenericEpithet ${infragenericEpithet} for rank ${taxonRank}`);
        if (!specificOK) console.warn(`Taxa validation failed for ${id}: Invalid specificEpithet ${specificEpithet} for rank ${taxonRank}`);
        if (!infraspecificOK) console.warn(`Taxa validation failed for ${id}: Invalid infraspecificEpithet ${infraspecificEpithet} for rank ${taxonRank}`);
        if (!(yearOK || namePublishedInYear === null)) console.warn(`Taxa validation failed for ${id}: Invalid year ${namePublishedInYear}`);
        if (!validOK) console.warn(`Taxa validation failed for ${id}: Invalid valid flag ${valid}`);
        if (!explicit1OK) console.warn(`Taxa validation failed for ${id}: Invalid explicitHigherTaxon flag ${explicitHigherTaxon}`);
        if (!explicit2OK) console.warn(`Taxa validation failed for ${id}: Invalid explicitSpecificEpithetTaxon flag ${explicitSpecificEpithet}`);
        if (!rankOK) console.warn(`Taxa validation failed for ${id}: Invalid rank ${taxonRank}`);
        if (!statusOK) console.warn(`Taxa validation failed for ${id}: Invalid taxonomicStatus ${taxonomicStatus}`);
        if (!nomenStatusOK) console.warn(`Taxa validation failed for ${id}: Invalid nomenclaturalStatus ${nomenclaturalStatus} for valid=${valid}`);
        if (!typeTaxonIdOK) console.warn(`Taxa validation failed for ${id}: Invalid typeTaxonId ${typeTaxonId}`);
        if (!infraspeciesClassOK) console.warn(`Taxa validation failed for ${id}: Invalid infraspeciesType ${infraspeciesType} for rank ${taxonRank}`);
        if (!translitsOK) console.warn(`Taxa validation failed for ${id}: Invalid transliterations ${JSON.stringify(transliterated)}`);
        if (!originalRankOK) console.warn(`Taxa validation failed for ${id}: Invalid originalRank ${originalRank}`);
        if (!originalGenusOK) console.warn(`Taxa validation failed for ${id}: Invalid originalGenus ${originalGenus} for originalRank ${originalRank}`);
        if (!originalSpecificOK) console.warn(`Taxa validation failed for ${id}: Invalid originalSpecificEpithet ${originalSpecificEpithet} for originalRank ${originalRank}`);
        if (!originalInfragenericEpithetOK) console.warn(`Taxa validation failed for ${id}: Invalid originalInfragenericEpithet ${originalInfragenericEpithet} for originalRank ${originalRank}`);
        if (!originalInfraspecificEpithetOK) console.warn(`Taxa validation failed for ${id}: Invalid originalInfraspecificEpithet ${originalInfraspecificEpithet} for originalRank ${originalRank}`);
        if (!originalInfraspTypeOK) console.warn(`Taxa validation failed for ${id}: Invalid originalInfraspeciesType ${originalInfraspeciesType} for originalRank ${originalRank}`);
        if (!typeLocalityOK) console.warn(`Taxa validation failed for ${id}: Invalid typeLocality ${typeLocality}`);

        const somethingWasInvalid = !idOk || !parentOk || !acceptedUsageOk || !acceptedIdOk ||
            !canonicalNameOK || !scientificNameOK || !authorshipOK || !infragenericOK ||
            !specificOK || !infraspecificOK || !(yearOK || namePublishedInYear === null) ||
            !validOK || !explicit1OK || !explicit2OK || !rankOK || !statusOK || !nomenStatusOK ||
            !typeTaxonIdOK || !infraspeciesClassOK || !translitsOK || !originalRankOK ||
            !originalGenusOK || !originalInfraspTypeOK || !originalSpecificOK || !originalInfraspecificEpithetOK ||
            !originalInfragenericEpithetOK || !typeLocalityOK;

        if (somethingWasInvalid) {
            console.warn(`Some information failed validation for ${data[0].scientificName}`)
        }

        let transliterationsValue = "null";
        if (transliterated) {
            transliterationsValue = JSON.stringify(transliterated);
        }

        const mainRow = [
            id,
            parentId || "",
            acceptedNameUsage || "",
            acceptedNameId || "",
            canonicalName || "",
            scientificName || "",
            scientificNameAuthorship || "",
            specificEpithet || "",
            infragenericEpithet || "",
            infraspecificEpithet || "",
            namePublishedInYear || "",
            String(valid),
            String(explicitHigherTaxon),
            String(explicitSpecificEpithet),
            taxonRank || "",
            taxonomicStatus || "",
            nomenclaturalStatus || "",
            typeTaxonId || "",
            typeLocalityId || "",
            infraspeciesType || "",
            transliterationsValue,
            originalRank || "",
            originalGenus || "",
            originalSpecificEpithet || "",
            originalInfraspecificEpithet || "",
            originalInfragenericEpithet || "",
            originalInfraspeciesType || ""
        ].join("\t");

        if (passesTaxa) {
            taxaValid.push(mainRow);
        } else {
            taxaInvalid.push(mainRow);
        }

        // Table 2 & 3: vernacular_names & taxa_vernacular_names
        if (Array.isArray(vernacularNames)) {
            for (const vGroup of vernacularNames) {
                const lang = vGroup.language;
                if (!Array.isArray(vGroup.vernacularNames)) continue;
                for (const vName of vGroup.vernacularNames) {
                    const vID = generateUUIDv7();
                    const vIDOk = isValidUUIDv7(vID);
                    const vNameOk = typeof vName === "string";

                    if (!vIDOk) console.warn(`vernacular_names.tsv validation failed for generatedId=${vID}: invalid UUID`);
                    if (!vNameOk) console.warn(`vernacular_names.tsv validation failed for name=${vName}: invalid type, must be string`);

                    const rowVN = [vID, vName, lang].join("\t");
                    if (vIDOk && vNameOk) {
                        vernacularNamesValid.push(rowVN);
                    } else {
                        vernacularNamesInvalid.push(rowVN);
                    }

                    const bridgingRow = [id, vID || "", "null"].join("\t");
                    const bridgingOk = isValidUUIDv7(id!) && vIDOk;
                    if (!bridgingOk) {
                        console.warn(`taxa_vernacular_names.tsv validation failed for taxaId=${id}, vId=${vID}`);
                    }
                    if (bridgingOk) {
                        taxaVernacularNamesValid.push(bridgingRow);
                    } else {
                        taxaVernacularNamesInvalid.push(bridgingRow);
                    }
                }
            }
        }

        // Table 5: taxa_interactions
        if (Array.isArray(interactions)) {
            for (const iObj of interactions) {
                const verb = iObj.verb;
                if (!Array.isArray(iObj.subjects)) continue;
                for (const subj of iObj.subjects) {
                    const bridgingOk = isValidUUIDv7(id!) && isValidUUIDv7(subj) &&
                        (verb === "consumes" || verb === "parasitized_by");
                    if (!bridgingOk) {
                        console.warn(`taxa_interactions.tsv validation failed for taxaId=${id}, interactionsId=${subj}, verb=${verb}`);
                    }
                    const rowTI = [id, subj, verb].join("\t");
                    if (bridgingOk) {
                        taxaInteractionsValid.push(rowTI);
                    } else {
                        taxaInteractionsInvalid.push(rowTI);
                    }
                }
            }
        }

        // Table 6..8: unresolvedChildren
        if (Array.isArray(unresolvedChildren)) {
            for (const uc of unresolvedChildren) {
                const { id: ucId, dataConnectionType, mentionedAs, linkedResources } = uc;
                const ucIdOk = isValidUUIDv7(ucId);
                const recordIdOk = isValidUUIDv7(id!);
                const mentionedOk = typeof mentionedAs === "string";
                const classOk = [
                    "original_description", "namePublishedIn", "treated_as_type", "treated_as_synonym",
                    "applied_name", "unresolved_name", "misidentification", "misapplication"
                ].includes(dataConnectionType || "");

                if (!ucIdOk) console.warn(`unresolved_names.tsv validation failed for id=${ucId}: invalid UUID`);
                if (!recordIdOk) console.warn(`unresolved_names.tsv validation failed for recordId=${id}: invalid UUID`);
                if (!mentionedOk) console.warn(`unresolved_names.tsv validation failed for id=${ucId}: "mentionedAs" must be string`);
                if (!classOk) console.warn(`unresolved_names.tsv validation failed for id=${ucId}: dataConnectionType="${dataConnectionType}" invalid`);

                const validUc = ucIdOk && recordIdOk && mentionedOk && classOk;
                const rowUN = [ucId, id, mentionedAs || "", dataConnectionType || ""].join("\t");

                if (validUc) {
                    unresolvedNamesValid.push(rowUN);
                } else {
                    unresolvedNamesInvalid.push(rowUN);
                }

                if (Array.isArray(linkedResources)) {
                    for (const lr of linkedResources) {
                        const { id: lrId, source, resource_type } = lr;
                        const lrIdOk = isValidUUIDv7(lrId);
                        const sourceOk = isValidUUIDv7(source || "");
                        const rtOk = ["web", "literature"].includes(resource_type || "");

                        if (!lrIdOk) console.warn(`citations1.tsv validation failed for id=${lrId}: invalid UUID`);
                        if (!sourceOk) console.warn(`citations1.tsv validation failed for source=${source}: invalid UUID`);
                        if (!rtOk) console.warn(`citations1.tsv validation failed for id=${lrId}: resource_type="${resource_type}" invalid`);

                        const rowC1 = [lrId, source || "", resource_type || ""].join("\t");
                        if (lrIdOk && sourceOk && rtOk) {
                            citations1Valid.push(rowC1);
                        } else {
                            citations1Invalid.push(rowC1);
                        }

                        const bridgingOk2 = ucIdOk && lrIdOk;
                        const rowUNC1 = [ucId, lrId].join("\t");
                        if (!bridgingOk2) {
                            console.warn(`unresolved_names_citations1.tsv validation failed for unresolvedId=${ucId}, citationId=${lrId}`);
                        }
                        if (bridgingOk2) {
                            unresolvedNamesCitations1Valid.push(rowUNC1);
                        } else {
                            unresolvedNamesCitations1Invalid.push(rowUNC1);
                        }
                    }
                }
            }
        }

        // Table 9..11: dataConnections
        if (Array.isArray(dataConnections)) {
            for (const dc of dataConnections) {
                const { id: dcId, mentionedAs, dataConnectionType, linkedResources } = dc;
                const dcIdOk = isValidUUIDv7(dcId!);
                const recordIdOk = isValidUUIDv7(id!);
                const cxnTypeOk = transformDataConnectionType(dataConnectionType, mentionedAs);

                if (!dcIdOk) console.warn(`data_connections.tsv validation failed for dataConnectionId=${dcId}: invalid UUID`);
                if (!recordIdOk) console.warn(`data_connections.tsv validation failed for taxaId=${id}: invalid UUID`);
                if (!cxnTypeOk) console.warn(`data_connections.tsv validation failed for dataConnectionId=${dcId}: dataConnectionType="${dataConnectionType}" invalid`);

                const rowDC = [dcId, id, mentionedAs || "", dataConnectionType || ""].join("\t");
                if (dcIdOk && recordIdOk && cxnTypeOk) {
                    dataConnectionsValid.push(rowDC);
                } else {
                    dataConnectionsInvalid.push(rowDC);
                }

                if (Array.isArray(linkedResources)) {
                    for (const lr of linkedResources) {
                        const { id: lrId, source, resource_type } = lr;
                        const lrIdOk = isValidUUIDv7(lrId!);
                        const sourceOk = isValidUUIDv7(source || "");
                        const resourceOk = ["web", "literature"].includes(resource_type || "");

                        if (!lrIdOk) console.warn(`citations2.tsv validation failed for citationId=${lrId}: invalid UUID`);
                        if (!sourceOk) console.warn(`citations2.tsv validation failed for source=${source}`);
                        if (!resourceOk) console.warn(`citations2.tsv validation failed for resource_type="${resource_type}" invalid`);

                        const rowC2 = [lrId, source || "", resource_type || ""].join("\t");
                        if (lrIdOk && sourceOk && resourceOk) {
                            citations2Valid.push(rowC2);
                        } else {
                            citations2Invalid.push(rowC2);
                        }

                        const bridgingOk2 = dcIdOk && lrIdOk;
                        const rowDCC2 = [dcId, lrId].join("\t");
                        if (!bridgingOk2) {
                            console.warn(`data_connections_citations2.tsv validation failed for dataConnectionId=${dcId}, citationId=${lrId}`);
                        }
                        if (bridgingOk2) {
                            dataConnectionsCitations2Valid.push(rowDCC2);
                        } else {
                            dataConnectionsCitations2Invalid.push(rowDCC2);
                        }
                    }
                }
            }
        }

        // Table 13: taxa_asserted_distributions
        if (Array.isArray(assertedDistribution)) {
            for (const distId of assertedDistribution) {
                const bridgingOk = isValidUUIDv7(id!) && isValidUUIDv7(distId);
                if (!bridgingOk) {
                    console.warn(`taxa_asserted_distributions.tsv validation failed for taxaId=${id}, distId=${distId}`);
                }
                const rowTAD = [id, distId].join("\t");
                if (bridgingOk) {
                    taxaAssertedDistributionsValid.push(rowTAD);
                } else {
                    taxaAssertedDistributionsInvalid.push(rowTAD);
                }
            }
        }
    }

    await fs.writeFile(path.join("./data/tsv_data", "taxa.tsv"), taxaValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "taxa_INVALID.tsv"), taxaInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "vernacular_names.tsv"), vernacularNamesValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "vernacular_names_INVALID.tsv"), vernacularNamesInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "taxa_vernacular_names.tsv"), taxaVernacularNamesValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "taxa_vernacular_names_INVALID.tsv"), taxaVernacularNamesInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "taxa_interactions.tsv"), taxaInteractionsValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "taxa_interactions_INVALID.tsv"), taxaInteractionsInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "unresolved_names.tsv"), unresolvedNamesValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "unresolved_names_INVALID.tsv"), unresolvedNamesInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "citations1.tsv"), citations1Valid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "citations1_INVALID.tsv"), citations1Invalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "unresolved_names_citations1.tsv"), unresolvedNamesCitations1Valid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "unresolved_names_citations1_INVALID.tsv"), unresolvedNamesCitations1Invalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "citations2.tsv"), citations2Valid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "citations2_INVALID.tsv"), citations2Invalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "data_connections.tsv"), dataConnectionsValid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "data_connections_INVALID.tsv"), dataConnectionsInvalid.join("\n"), "utf8");

    await fs.writeFile(path.join("./data/tsv_data", "data_connections_citations2.tsv"), dataConnectionsCitations2Valid.join("\n"), "utf8");
    await fs.writeFile(path.join("./data/tsv_data", "data_connections_citations2_INVALID.tsv"), dataConnectionsCitations2Invalid.join("\n"), "utf8");

    await fs.writeFile(
        path.join("./data/tsv_data", "taxa_asserted_distributions.tsv"),
        taxaAssertedDistributionsValid.join("\n"),
        "utf8"
    );
    await fs.writeFile(
        path.join("./data/tsv_data", "taxa_asserted_distributions_INVALID.tsv"),
        taxaAssertedDistributionsInvalid.join("\n"),
        "utf8"
    );
}

/**
 * Example main() usage, as requested.
 */
export async function convert(): Promise<void> {
    // Load existing cache data before we begin
    await loadCacheData();

    // The following calls assume you have these helper functions returning arrays from some in-memory caches:
    const higherTaxaData = getAllHigherTaxaFromCache();
    await tsvFromHigherTaxaDataCache(higherTaxaData);
    clearHigherTaxonCache();

    const webCacheData = getAllWebResourcesFromCache();
    await tsvFromWebCache(webCacheData);
    clearWebResourceCache();

    const personCacheData = getAllPersonsFromCache();
    await tsvFromPersonCache(personCacheData);
    clearPersonCache();

    const hostCacheData = getAllHostsFromCache();
    await tsvFromHostCache(hostCacheData);
    clearHostCache();

    const journalCacheData = getAllJournalsFromCache();
    await tsvFromJournalCache(journalCacheData);
    clearJournalCache();

    const pageCacheData = getAllPagesFromCache();
    await tsvFromPageCache(pageCacheData);
    clearPageCache();

    const volumeCacheData = getAllVolumesFromCache();
    await tsvFromVolumeCache(volumeCacheData);
    clearVolumeCache();

    const issueCacheData = getAllIssuesFromCache();
    await tsvFromIssueCache(issueCacheData);
    clearIssueCache();

    const articleCacheData = getAllArticlesFromCache();
    await tsvFromArticleCache(articleCacheData);
    clearArticleCache();

    const assertedDistributionCacheData = getAllDistributionsFromCache();
    await tsvFromAssertedDistributionCache(assertedDistributionCacheData);
    clearDistributionCache();

    const localityCacheData = getAllLocalitiesFromCache();
    await tsvFromLocalityCache(localityCacheData);
    clearLocalityCache();

    const taxonCacheData = getAllTaxonFromCache();
    await tsvFromTaxonCache(taxonCacheData);
    clearTaxonCache();
}

convert().catch(console.error);