import { uuidv7 } from "uuidv7";
import { ParsedBlock } from "../parseBlocks";
import { DataConnectionType, LinkedResource, Misapplication, TaxonRecord, WebSubelement } from "../types";
import { addOrUpdateWebResource, getWebResourceByFunetRefLink } from "../parseWebResource/webResourceCacheManager";
import { getArticleByFunetRefLink } from "../parseRelatedLiterature/articleCacheManager";
import { extractIssueInfo } from "../parseCitation";

export function parseMisapplicationBlock(element: Element): Misapplication | undefined {
    let mention = element.textContent?.trim();
    if (!mention) return;

    // --------------------------------------------------
    // 1. Extract the portion before the first semicolon 
    //    (this is typically the main taxon line)
    // --------------------------------------------------
    const firstPart = mention.split(';')[0]?.trim() || '';

    // --------------------------------------------------
    // 2. Detect if there is a "(= ...)" portion
    //    and capture it in otherMisappliedText. We then
    //    remove it from firstPart so it doesn't cause
    //    further parsing problems.
    // --------------------------------------------------
    let otherMisappliedText: string | undefined;
    const eqPattern = /\(\s*=(.*?)\)\s*;\s*/;
    const eqMatch = firstPart.match(eqPattern);
    if (eqMatch) {
        otherMisappliedText = eqMatch[1].trim();
    }

    // Remove that "(= ...)" segment from the main name
    const misappliedTo = firstPart.replace(eqPattern, '').replace(/,\s\s+/, '; ').trim();

    // --------------------------------------------------
    // 3. Parse out the references from the remainder of
    //    the mention (i.e., split(';').slice(1))
    // --------------------------------------------------
    const mentionDetailList: LinkedResource[] = [];
    mention
        .split(';')
        .slice(1)
        .map((refPart) => {
            const refDetails = refPart.trim();
            if (refDetails !== '') {
                // Handle different citation formats
                const linkElements = element.querySelectorAll('a');
                let funetRefLink: string | undefined;

                // Find matching link for the current reference
                for (const link of linkElements) {
                    const linkText = link.textContent?.trim() || '';
                    const href = link.getAttribute('href');

                    // Match either [CODE] format or full citation format
                    if (href?.startsWith('#') &&
                        (refDetails.includes(linkText) ||
                            refDetails.startsWith(linkText.replace(/[\[\]]/g, '')))) {
                        funetRefLink = href.substring(1); // Remove the # prefix
                        break;
                    }
                }

                if (funetRefLink) {
                    if (funetRefLink.startsWith("R")) {
                        // Add debug logging
                        // console.log('Processing mention with funetRefLink:', {
                        //     funetRefLink,
                        //     cacheSize: getWebResourceCacheSize(),
                        //     resourceExists: !!getWebResourceByFunetRefLink(funetRefLink)
                        // });

                        // HERE WE ARE DEALING WITH A WEB MENTION
                        // console.log(`Processing web resource with funetRefLink: ${funetRefLink}`);

                        // Step 1 & 3: Get web resource from cache using funetRefLink
                        const webResource = getWebResourceByFunetRefLink(funetRefLink);
                        if (!webResource?.id) {
                            console.warn(`No webResource found for funetRefLink: ${funetRefLink}`);
                            return;
                        }

                        // Step 2: Find the external resource link from within the same span.ext element
                        let resourceLink: string | undefined;
                        const refLinkElement = Array.from(element.querySelectorAll('a')).find(link =>
                            link.getAttribute('href')?.substring(1) === funetRefLink
                        );

                        if (refLinkElement) {
                            // Find the closest parent span.ext
                            const extSpan = refLinkElement.closest('.ext');
                            if (extSpan) {
                                // Look for the external link within this same span.ext
                                const links = extSpan.querySelectorAll('a');
                                links.forEach(link => {
                                    if (!link.href.includes("#")) {
                                        if (link?.getAttribute('href')?.startsWith('http')) {
                                            resourceLink = link?.getAttribute('href') || undefined
                                        }
                                    }
                                });
                            }
                        }

                        // Step 4: Check if a matching subelement already exists
                        let idToLinkTo: string;
                        const existingSubelement = webResource.webSubelement?.find(sub =>
                            sub.externalLink === resourceLink
                        );

                        if (existingSubelement) {
                            // Use existing subelement's ID
                            idToLinkTo = existingSubelement.id!;
                        } else {
                            // Step 5: Create new subelement if none exists
                            const newSubelement: WebSubelement = {
                                id: uuidv7(),
                                subelementType: "web_page",
                                externalLink: resourceLink
                            };
                            idToLinkTo = newSubelement.id!;

                            // Step 6: Update the webResource in cache with new subelement
                            webResource.webSubelement.push(newSubelement);
                            addOrUpdateWebResource(webResource);
                        }

                        // Step 7 & 8: Create and add the linkedResource
                        const linkedResource: LinkedResource = {
                            id: uuidv7(),
                            source: idToLinkTo,
                            resource_type: "web"
                        };
                        mentionDetailList.push(linkedResource);

                        // // ! DEBUGGING
                        // if (resourceLink?.includes("inaturalist")) {
                        //     const logData = {
                        //         iNatRef: funetRefLink,
                        //         iNatLink: resourceLink,
                        //         webResourceFound: webResource,
                        //         webResourceReCached: webResource
                        //     };

                        //     // Write to file synchronously
                        //     const fs = require('fs');
                        //     fs.appendFileSync(
                        //         './data/inat-debug.log',
                        //         JSON.stringify(logData, null, 2) + '\n---\n',
                        //         'utf8'
                        //     );
                        // }

                    } else {
                        // HERE WE ARE DEALING WITH A LITERATURE MENTION
                        // Get article from cache using funetRefLink
                        const { issueLabel, externalLink } = extractIssueInfo(refDetails, element)
                        const article = getArticleByFunetRefLink(funetRefLink, issueLabel)
                        // get the id of the pages that are referenced
                        const pagesText = refDetails.substring(refDetails.indexOf(':') + 1).trim();

                        pagesText.split(',').forEach(pageRef => {
                            // Clean and extract the page number and check for list annotation
                            const listAnnotation = pageRef.match(/\((list)\)/i);
                            const pageNumber = pageRef.replace(/\(.*?\)/g, '').trim().split(/\s+/)[0];

                            // Skip if not a valid page number
                            if (!/^\d+$/.test(pageNumber)) return;

                            // Find matching page
                            const page = article?.pages?.find(p => p.label === pageNumber);

                            if (listAnnotation) {
                                if (page?.subelements?.some(s => s.type === 'table')) {
                                    // If list annotation exists and we have table subelements, use those
                                    const tableSubelements = page.subelements.filter(s => s.type === 'table');
                                    tableSubelements.forEach(sub => {
                                        if (sub.id) {
                                            const linkedResource: LinkedResource = {
                                                id: uuidv7(),
                                                source: sub.id,
                                                resource_type: "literature"
                                            }
                                            mentionDetailList.push(linkedResource);
                                            // console.log("PUSHED: ", sub.id)
                                        }
                                    });
                                } else if (page?.id) {
                                    // Fallback to page ID if exists
                                    const linkedResource: LinkedResource = {
                                        id: uuidv7(),
                                        source: page.id,
                                        resource_type: "literature"
                                    }
                                    mentionDetailList.push(linkedResource);
                                    // console.log("PUSHED: ", page.id)
                                } else if (article?.id) {
                                    const linkedResource: LinkedResource = {
                                        id: uuidv7(),
                                        source: article.id,
                                        resource_type: "literature"
                                    }
                                    mentionDetailList.push(linkedResource);
                                    // console.log("PUSHED ARTICLE ID AS FALLBACK: ", article.id)
                                }
                            } else if (page?.id) {
                                // Use page ID if exists
                                const linkedResource: LinkedResource = {
                                    id: uuidv7(),
                                    source: page.id,
                                    resource_type: "literature"
                                }
                                mentionDetailList.push(linkedResource);
                                // console.log("PUSHED: ", page.id)
                            } else if (article?.id) {
                                // Fallback to article ID
                                const linkedResource: LinkedResource = {
                                    id: uuidv7(),
                                    source: article.id,
                                    resource_type: "literature"
                                }
                                mentionDetailList.push(linkedResource);
                                // console.log("PAGE MISSING, PUSHED ARTICLE ID AS FALLBACK: ", article.id)
                                // console.log(`SIBLING TEXT: "${siblingText}"`)
                                // console.log(`PAGES TEXT: "${pagesText}"`)
                            }
                        })

                        // Fallback: If no pages were found, use the article ID
                        if (mentionDetailList.length === 0 && article?.id) {
                            const linkedResource: LinkedResource = {
                                id: uuidv7(),
                                source: article.id,
                                resource_type: "literature"
                            }
                            mentionDetailList.push(linkedResource);
                            // console.log("NO PAGE GIVEN, PUSHED ARTICLE ID AS FALLBACK: ", article.id)
                            // console.log(`SIBLING TEXT: "${siblingText}"`)
                            // console.log(`PAGES TEXT: "${pagesText}"`)
                        }
                    }
                } else {
                    const linkedResource: LinkedResource = {
                        id: uuidv7(),
                        source: funetRefLink,
                        resource_type: "web"
                    }
                    mentionDetailList.push(linkedResource);
                }
            }
        });

    // --------------------------------------------------
    // 4. Return the Misapplication object, now including
    //    otherMisappliedText
    // --------------------------------------------------
    return {
        id: uuidv7(),
        dataConnectionType: "misapplication" as DataConnectionType,
        misappliedTo: misappliedTo,
        otherMisappliedText,
        linkedResources: mentionDetailList,
    };
}

export function parseMisapplications(parsedBlocks: ParsedBlock[], taxa: TaxonRecord[]): TaxonRecord[] {
    // Deep copy of taxa array - all modifications will be made to this copy
    const taxaWithInjectedMisapplications = structuredClone(taxa);

    // Create a map of all blocks by their blockName for easy parent lookup
    const blocksByName = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        // console.log(block.parsedBlockData)
        if (block.blockName) {
            blocksByName.set(block.blockName, block);
        }
    });

    // First pass: collect only the blocks we want to process
    const misapplicationsBlocks = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        if (block.blockType === "MisapplicationBlock") {
            misapplicationsBlocks.set(block.blockName, block);
        }
    });

    // Second pass: process only the collected valid blocks
    misapplicationsBlocks.forEach(misapplicationsBlock => {
        // Find parent block's id
        // blockType NameBlock and SynonymBlock need to go up 2 levels to get parent info
        let parentData: TaxonRecord | null
        const validParentBlock = blocksByName.get(`${misapplicationsBlock.parentBlock}_NameBlock_1`);
        // console.log("Parent: ", validParentBlock)
        parentData = validParentBlock?.parsedBlockData

        if (parentData?.id) {
            // Find the matching taxon in our deep copy
            const taxonToUpdate = taxaWithInjectedMisapplications.find(taxon => taxon.id === parentData?.id);

            if (taxonToUpdate && misapplicationsBlock.parsedBlockData) {
                // These modifications are being made to our deep copy
                if (!taxonToUpdate.dataConnections) {
                    taxonToUpdate.dataConnections = [];
                }
                taxonToUpdate.dataConnections.push(misapplicationsBlock.parsedBlockData);
            }
        }
    });

    // Return the modified deep copy
    return taxaWithInjectedMisapplications;
}
