import { uuidv7 } from "uuidv7";
import { DataConnectionType, LinkedResource, Mention, TaxonRecord, WebSubelement } from "../types";
import { ParsedBlock } from "../parseBlocks";
import { isWebMention, parseAsWebMention, parseAsLiteratureMention } from "../parseMentions/parse";
import { getWebResourceById, addOrUpdateWebResource } from "../parseWebResource/webResourceCacheManager";

/**
 * Parse a single "Unresolved Name" element, returning a Mention object if successful.
 * We assume the pattern is something like:
 *   <li>GenusName ; [R3] SomeRef ; AnotherRef ; Possibly More</li>
 * Where everything after the first semicolon is parsed as references (web or literature).
 */
export function parseUnresolvedName(element: Element): Mention | void {
    const rawMention = element.textContent?.trim();
    if (!rawMention) {
        console.error("Could not parse name from 'Unmatched external taxa' list item!");
        return;
    }

    // The <i>-tag content = the "mentionedAs" property if present
    const mentionedAs = element.querySelector('i')?.textContent?.trim() || "";

    // Split the text on semicolon
    const allParts = rawMention.split(";").map(part => part.trim()).filter(Boolean);
    if (!allParts.length) {
        return;
    }

    // The first portion is the name itself, but we prefer the italic <i> text if present
    const firstPart = allParts[0];
    const usedName = mentionedAs || firstPart;

    const mentionDetailList: LinkedResource[] = [];
    const leftoverMentions = allParts.slice(1); // everything after the name

    for (const mentionItem of leftoverMentions) {
        if (isWebMention(mentionItem, element)) {
            const webResource = parseAsWebMention(mentionItem, element);
            mentionDetailList.push(webResource);
        } else {
            // If not bracketed web mention, parse as literature
            const litResources = parseAsLiteratureMention(mentionItem, element);
            mentionDetailList.push(...litResources);
        }
    }

    // Return the Mention object
    const unresolvedName: Mention = {
        id: uuidv7(),
        mentionedAs: usedName,
        dataConnectionType: "unresolved_name",
        linkedResources: mentionDetailList
    };

    return unresolvedName;
}

/**
 * Processes all "UnresolvedNameBlock" blocks in parsedBlocks, extracting the Mention data,
 * and reassigning it as a new dataConnection of type "unresolved_name" for each parent taxon.
 */
export function resolveUnresolvedNames(parsedBlocks: ParsedBlock[]): Mention[] {
    const resolvedItemsList: Mention[] = [];

    parsedBlocks.forEach(block => {
        if (block.blockType === "UnresolvedNameBlock" && block.parentBlock !== null) {
            const data: Mention = block.parsedBlockData;
            const linkedItemsList: LinkedResource[] = [];
            const mentionedAs = data.mentionedAs;

            data.linkedResources?.forEach(resource => {
                if (resource.source) {
                    // Attempt to find a web resource with this ID
                    const resolvedSource = getWebResourceById(resource.source as string);
                    if (resolvedSource) {
                        // Ensure external link is persisted in a subelement if resource.description is present
                        if (resource.description) {
                            // Look for an existing subelement with this externalLink
                            let subelementId: string;
                            const existingSubelement = resolvedSource.webSubelement?.find(
                                sub => sub.externalLink === resource.description
                            );
                            if (existingSubelement) {
                                subelementId = existingSubelement.id!;
                            } else {
                                // Create new subelement
                                const newSubelement: WebSubelement = {
                                    id: uuidv7(),
                                    subelementType: "web_page",
                                    externalLink: encodeURI(resource.description.trim())
                                };
                                resolvedSource.webSubelement.push(newSubelement);
                                addOrUpdateWebResource(resolvedSource);
                                subelementId = newSubelement.id!;
                            }
                            // Link to the subelement ID
                            linkedItemsList.push({
                                id: uuidv7(),
                                source: subelementId,
                                resource_type: "web"
                            });
                        } else {
                            // No external link => link directly to the resource
                            linkedItemsList.push({
                                id: uuidv7(),
                                source: resolvedSource.id,
                                resource_type: "web"
                            });
                        }
                    } else {
                        // Possibly a literature resource or unknown
                        linkedItemsList.push(resource);
                    }
                }
            });

            resolvedItemsList.push({
                id: uuidv7(),
                dataConnectionType: "unresolved_name" as DataConnectionType,
                mentionedAs: mentionedAs,
                linkedResources: linkedItemsList
            });
        }
    });

    return resolvedItemsList;
}

/**
 * Inject the unresolved names mention array as "unresolvedChildren"
 * into any genus-level TaxonRecords, or adapt as needed.
 */
export function injectResolveUnresolvedNames(unresolvedNames: Mention[], taxa: TaxonRecord[]): TaxonRecord[] {
    const injectedTaxa = structuredClone(taxa);
    injectedTaxa.forEach(taxon => {
        if (taxon.taxonRank === 'genus' && taxon.valid === true) {
            taxon.unresolvedChildren = unresolvedNames;
        }
    });
    return injectedTaxa;
}