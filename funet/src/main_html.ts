import { downloadPagesUntilTarget } from "./mainUtilities";

async function main() {
    console.log("Starting HTML download...");

    try {
        const downloadedUrls = await downloadPagesUntilTarget();
        console.log(`Successfully downloaded ${downloadedUrls.length} pages`);
        console.log("Downloaded URLs:", downloadedUrls);
    } catch (error) {
        console.error("Error in main:", error);
    }
}

main();