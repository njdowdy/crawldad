import { Journal } from "../types";

const journalCache: Map<string, Journal> = new Map();

export function getJournalFromCache(cacheKey: string): Journal | undefined {
    const normalizedKey = cacheKey.toLowerCase().trim();
    return journalCache.get(normalizedKey);
}

export function addJournalToCache(cacheKey: string, journal: Journal): void {
    if (!journal.id) return;

    const normalizedKey = cacheKey.toLowerCase().trim();
    const existingJournal = journalCache.get(normalizedKey);

    if (existingJournal) {
        Object.assign(existingJournal, journal);
        return;
    }

    journalCache.set(normalizedKey, journal);
}

export function clearJournalCache(): void {
    journalCache.clear();
}

export function getJournalCacheSize(): number {
    return journalCache.size;
}

export function getAllJournalsFromCache(): Journal[] {
    return Array.from(journalCache.values());
}

export function getAllJournalCacheKeys(): string[] {
    return Array.from(journalCache.keys());
}