import { Volume } from "../types";

const volumeCache: Map<string, Volume> = new Map();

export function getVolumeFromCache(cacheKey: string): Volume | undefined {
    const normalizedKey = cacheKey.toLowerCase().trim();
    return volumeCache.get(normalizedKey);
}

export function addVolumeToCache(cacheKey: string, volume: Volume): void {
    if (!volume.id) return;

    const normalizedKey = cacheKey.toLowerCase().trim();
    const existingVolume = volumeCache.get(normalizedKey);

    if (existingVolume) {
        Object.assign(existingVolume, volume);
        return;
    }

    volumeCache.set(normalizedKey, volume);
}

export function clearVolumeCache(): void {
    volumeCache.clear();
}

export function getVolumeCacheSize(): number {
    return volumeCache.size;
}

export function getAllVolumesFromCache(): Volume[] {
    return Array.from(volumeCache.values());
}

export function getAllVolumeCacheKeys(): string[] {
    return Array.from(volumeCache.keys());
}