import { uuidv7 } from "uuidv7";
import { Article, Authorship, Issue, Page, Tag, Journal, Volume } from "../types";
import { getJournalFromCache, addJournalToCache } from "./journalCacheManager";
import { getVolumeFromCache, addVolumeToCache } from "./volumeCacheManager";
import { getIssueFromCache, addIssueToCache } from "./issueCacheManager";
import { generateArticleCacheKey, upsertArticle } from "./articleCacheManager";

export function parseTitleString(blockDataText: string): string {
    // "Hampson, 1901\nCatalogue of the Arctiadae (Arctianae) and Agaristidae in the collection of the British Museum (Nat. Hist.)\nCat. Lepid. Phalaenae Br. Mus. 3\n: 1-690,  pl. 36-54, \n: i-xix\n"
    return blockDataText.split('\n')[1]
}

export function parseJournalName(blockDataText: string, element: Element): { journalAbbreviation: string, journalTitle: string } {
    let journalText = blockDataText.split('\n')[2].trim();
    let journalName: string | undefined;
    const italicElements = element.getElementsByTagName('i');

    while (journalText !== '') {
        for (const italicElement of italicElements) {
            if (italicElement.textContent?.trim() === journalText) {
                journalName = journalText;
                if (journalName.includes('.')) {
                    return { journalAbbreviation: journalName, journalTitle: '' }
                }
                return { journalAbbreviation: '', journalTitle: journalName }
            }
        }

        const words = journalText.split(' ');
        words.pop(); // Remove the last word
        journalText = words.join(' ').trim();
    }

    return { journalAbbreviation: '', journalTitle: '' };
}

export function findExternalLink(word: string, element: Element, fullMatch: boolean = false): string[] {
    if (word === '') return [];
    const anchorElements = element.getElementsByTagName('a');
    const matches: string[] = [];

    for (const anchorElement of anchorElements) {
        const anchorText = anchorElement.textContent?.trim() || '';

        const match = fullMatch ? anchorText === word : anchorText.includes(word);

        if (match) {
            const href = anchorElement.getAttribute('href');
            if (href && href.startsWith('http')) matches.push(href);
        }
    }

    return matches;
}

export function parseVolumeNumber(blockDataText: string, element: Element): string | undefined {
    let journalText = blockDataText.split('\n')[2].trim();
    let volumeNumber: string | undefined;

    const boldElements = element.getElementsByTagName('b');

    // First check for volume number in bold
    for (const boldElement of boldElements) {
        volumeNumber = boldElement.textContent?.trim();
        if (volumeNumber) break;
    }

    // If no volume number found in bold, try parsing from text
    if (!volumeNumber) {
        // Updated regex to exclude numbers within parentheses or square brackets
        // and ensure number appears before any colon
        const volumeMatch = journalText.match(/(?<!\[|\()(?<![\[\(]\d*)\b(\d+)\b(?![\]\)])(?=.*?:)/);
        if (volumeMatch) {
            volumeNumber = volumeMatch[1];
        }
    }
    return volumeNumber;
}

export function parseIssueNumber(blockDataText: string, journalTitle: string, element?: Element): string | undefined {
    // First check for supplementary text node after journal title
    if (element) {
        const italicEl = Array.from(element.getElementsByTagName('i'))
            .find(el => el.textContent?.includes(journalTitle));

        if (italicEl) {
            // Get next text node sibling (3 is the value for TEXT_NODE)
            let nextNode = italicEl.nextSibling;
            while (nextNode && nextNode.nodeType === 3) {
                const text = nextNode.textContent?.trim();
                if (text?.match(/\(Suppl\.\)/i)) {
                    return 'supplement';
                }
                nextNode = nextNode.nextSibling;
            }
        }
    }

    // Original issue number parsing logic
    const lines = blockDataText.split('\n');
    const journalLine = lines.find(line => line.includes(journalTitle));

    if (!journalLine) return undefined;

    const textBeforeColon = journalLine.split(':')[0];
    const match = textBeforeColon.match(/\((?:\[)?([A-Za-z0-9\-/]+)(?:\])?\)/);

    if (match && match[1]) {
        return match[1].trim();
    }

    return undefined;
}

export function composeIssueObject(
    issueNumber: string,
    volumeNumber: string,
    journalTitle: string,
    journalAbbreviation: string,
    bracketedYear: boolean | undefined,
    year: number | undefined,
    externalLink: string | undefined,
    useCache: boolean = true
): Issue {
    if (useCache) {
        const journalCacheKey = `${journalTitle}|${journalAbbreviation}`;
        let journal = getJournalFromCache(journalCacheKey);

        if (!journal) {
            journal = {
                id: uuidv7(),
                title: journalTitle,
                abbreviation: journalAbbreviation
            };
            addJournalToCache(journalCacheKey, journal);
        }

        const volumeCacheKey = `${volumeNumber}|${year}|${journalCacheKey}`;
        let volume = getVolumeFromCache(volumeCacheKey);

        if (!volume) {
            volume = {
                id: uuidv7(),
                label: volumeNumber,
                year,
                bracketedYear,
                journal: journal
            };
            addVolumeToCache(volumeCacheKey, volume);
        }

        // Modified issue cache key to include year for better uniqueness
        const issueCacheKey = `${issueNumber}|${year}|${volumeCacheKey}`;
        let issue = getIssueFromCache(issueCacheKey);

        if (!issue || !useCache) {
            issue = {
                id: uuidv7(), // Always generate new ID for new issues
                label: issueNumber,
                volume: volume,
                externalLink
            };
            addIssueToCache(issueCacheKey, issue);
        }

        return issue;
    } else {
        // Create a new Issue object without using the cache
        return {
            id: uuidv7(),
            label: issueNumber,
            volume: {
                id: uuidv7(),
                label: volumeNumber,
                year,
                bracketedYear,
                journal: {
                    id: uuidv7(),
                    title: journalTitle,
                    abbreviation: journalAbbreviation,
                },
            },
            externalLink,
        };
    }
}

export function composeArticleObject(
    citedAs: string[],
    title?: string,
    issue?: Issue,
    tags?: Tag[],
    pages?: Page[],
    authors?: Authorship[],
    abbreviation?: string,
    funetRefLink?: string,
    externalLink?: string,
    doi?: string,
    notes?: string[],
    cacheKeyOverride?: string
): Article | undefined {
    if (!issue) {
        return undefined;
    }

    // Create new article
    const newArticle: Article = {
        citedAs,
        title,
        issue,
        tags: tags ?? [],
        pages,
        authors,
        abbreviation,
        funetRefLink,
        externalLink,
        doi,
        notes
    };

    // if (citedAs[0] === 'Joicey & Talbot, 1923') {
    //     console.log("CL4")
    //     console.log(pages)
    // }

    const upsertedArticle = upsertArticle(newArticle, generateArticleCacheKey(newArticle))

    return upsertedArticle;
}

export function parseDOI(href: string): string | undefined {
    const doiRegex = /10\.\d{4,9}\/[-._;()/:A-Z0-9]+/i;
    const match = href.match(doiRegex);
    return match ? match[0] : undefined;
}
