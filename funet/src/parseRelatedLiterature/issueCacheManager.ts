import { Issue } from "../types";

const issueCache: Map<string, Issue> = new Map();

export function getIssueFromCache(cacheKey: string): Issue | undefined {
    const normalizedKey = cacheKey.toLowerCase().trim();
    return issueCache.get(normalizedKey);
}

export function addIssueToCache(cacheKey: string, issue: Issue): void {
    if (!issue.id) return;

    const normalizedKey = cacheKey.toLowerCase().trim();
    const existingIssue = issueCache.get(normalizedKey);

    if (existingIssue) {
        Object.assign(existingIssue, issue);
        return;
    }

    issueCache.set(normalizedKey, issue);
}

export function clearIssueCache(): void {
    issueCache.clear();
}

export function getIssueCacheSize(): number {
    return issueCache.size;
}

export function getAllIssuesFromCache(): Issue[] {
    return Array.from(issueCache.values());
}

export function getAllIssueCacheKeys(): string[] {
    return Array.from(issueCache.keys());
}