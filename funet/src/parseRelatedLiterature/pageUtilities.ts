import { uuidv7 } from "uuidv7";
import { Issue, Page } from "../types";
import { findExternalLink } from "./publicationUtilities";

export function extractPageParameters(blockDataText: string): Array<{
    pageRangeIssue: string | undefined,
    pageRangeVolume: string | undefined,
    pageRangeStart: string | undefined,
    pageRangeEnd: string | undefined,
    plateRangeStart: string | undefined,
    plateRangeEnd: string | undefined,
    pageRangeYear: number | undefined,
    pageRangeYearBracketed: boolean | undefined,
    alternativeJournalTitle: string | undefined,
    pageNotes: string[] | undefined,
}> {
    const results: Array<{
        pageRangeIssue: string | undefined,
        pageRangeVolume: string | undefined,
        pageRangeStart: string | undefined,
        pageRangeEnd: string | undefined,
        plateRangeStart: string | undefined,
        plateRangeEnd: string | undefined,
        pageRangeYear: number | undefined,
        pageRangeYearBracketed: boolean | undefined,
        alternativeJournalTitle: string | undefined,
        pageNotes: string[] | undefined,
    }> = [];

    // Clean up blockDataText
    let cleanedBlockDataText = blockDataText.replace(/\s+(\d+)\s*pls\s+/g, ` pl. 1-$1 `).trim();

    // Extract journal title if present
    const titleMatch = blockDataText.match(/^(.*?)\s*\(\d+\):/);
    const alternativeJournalTitle = titleMatch ? titleMatch[1].trim() : undefined;

    // Split on issue boundaries with optional brackets around plates
    // const oldIssuePattern = /\((\d+)\):\s*(\[?\d+\]?(?:-\[?\d+\]?)?),\s*pl\.\s*(\d+(?:-\d+)?)\s*\((\d{4})\)/g;
    // const issuePattern = /(?:\(([\d\/]+)\))?\s*:\s*(?:\[?(\d+\]?(?:-\[?\d+)?)\]?,?\s*)?(?:pl\.\s*\[?(\d+(?:-\d+)?)\]?)?\s*(?:\((\[?\d{4}\]?)\))?/g;
    const issuePattern = /(?:\(([\d\/]+)\))?\s*:\s*(?:\[?(\d+\]?(?:-\[?\d+)?)\]?,?\s*)?(?:pl\.\s*\[?(\d+(?:-\d+)?)\]?)?\s*(?:\(([^\)]+)\))?\s*(?:\(([^\)]+)\))?\s*(?:,\s*|$)/g;

    // Instead of modifying cleanedBlockDataText in a loop, gather matches in one go
    const allMatches = [...cleanedBlockDataText.matchAll(issuePattern)];

    // if (blockDataText.includes("Fruhstorfer, 1910")) {
    //     console.log("------------------");
    //     console.log("cleanedBlockDataText:\n", cleanedBlockDataText);
    // }

    for (const match of allMatches) {
        let notes: string[] = [];
        const [fullMatch, issue, pages, plates, noteOrYear1, noteOrYear2] = match;

        // Debugging example (uncomment if needed)
        // if (blockDataText.includes("Fruhstorfer, 1910")) {
        //     console.log("---------");
        //     console.log("fullMatch: ", fullMatch);
        //     console.log("issue: ", issue);
        //     console.log("pages: ", pages);
        //     console.log("plates: ", plates);
        //     console.log("noteOrYear1: ", noteOrYear1);
        //     console.log("noteOrYear2: ", noteOrYear2);
        // }

        // Parse page range
        const [pageStart, pageEnd] = pages ? pages.split("-") : [undefined, undefined];

        // Parse plate range
        const [plateStart, plateEnd] = plates ? plates.split("-") : [undefined, undefined];

        // Track year(s)
        let pageRangeYear: number | undefined;
        let pageRangeYearBracketed: boolean | undefined;

        // Collect notes from the parentheses text
        if (noteOrYear1) notes.push(noteOrYear1);
        if (noteOrYear2) notes.push(noteOrYear2);

        // Identify any 4-digit years in those notes
        const years = [noteOrYear1, noteOrYear2]
            .filter((note) => note?.match(/\d{4}/))
            .map((note) => parseInt(note!.match(/\d{4}/)![0]));

        if (years.length > 0) {
            pageRangeYear = Math.max(...years);
            const yearNote = notes.find((note) =>
                note.includes(pageRangeYear!.toString())
            );
            if (yearNote?.includes("[")) {
                pageRangeYearBracketed = true;
            }
        }

        results.push({
            pageRangeIssue: issue,
            pageRangeVolume: undefined,
            pageRangeStart: pageStart,
            pageRangeEnd: pageEnd || pageStart,
            plateRangeStart: plateStart,
            plateRangeEnd: plateEnd || plateStart,
            pageRangeYear,
            pageRangeYearBracketed,
            alternativeJournalTitle,
            pageNotes: notes,
        });
    }

    // Remove the matched text now that we've parsed it
    // Build a new string that omits the matched segments
    let newText = "";
    let lastIndex = 0;

    // Sort by match index so we remove in the correct order
    const sortedMatches = allMatches.sort((a, b) => (a.index ?? 0) - (b.index ?? 0));

    for (const m of sortedMatches) {
        // m.index is where the match started
        if (m.index !== undefined) {
            newText += cleanedBlockDataText.slice(lastIndex, m.index);
            lastIndex = m.index + m[0].length; // skip the matched segment
        }
    }
    // Append remainder after the last match
    newText += cleanedBlockDataText.slice(lastIndex);

    // We now have a version of cleanedBlockDataText with matched text removed
    cleanedBlockDataText = newText;

    // if (blockDataText.includes('Fruhstorfer, 1910')) {
    //     console.log("\n--------------------")
    //     console.log("cleanedBlockDataText:\n", cleanedBlockDataText)
    // }

    // Updated primary page pattern
    const pagePattern = /^\s*(?:(?:(\d+)\s*)?(?:\(Suppl\.\))?\s*)?(?:\(([0-9A-Za-z,-]+)\))?\s*:\s*(?:([0-9ivxlcdmIVXLCDM]+)(?:-([0-9ivxlcdmIVXLCDM]+))?)?(?:(?:,\s*)?pl\.\s*([0-9ivxlcdmIVXLCDM]+)(?:-([0-9ivxlcdmIVXLCDM]+))?)?(?:\s*(?:,\s*f\.\s*[0-9-]+)?)?(?:\s*(\((?:\[?[0-9]{4}\]?|[0-9]{4})\)))?,?\s*$/i;

    // Secondary pattern
    const secondaryPagePattern = /:\s*([0-9ivxlcdmIVXLCDM]+(?:-[0-9ivxlcdmIVXLCDM]+)?(?:,[0-9ivxlcdmIVXLCDM]+(?:-[0-9ivxlcdmIVXLCDM]+)?)*)?(?:,\s*pl\.\s*([0-9ivxlcdmIVXLCDM]+)(?:-([0-9ivxlcdmIVXLCDM]+))?)?(?:\s*(?:,\s*f\.\s*[0-9-]+)?)?$/i;

    // Tertiary pattern for plate-only matches
    const tertiaryPagePattern = /:\s*pl\.\s*([0-9ivxlcdmIVXLCDM]+)(?:-([0-9ivxlcdmIVXLCDM]+))?(?:\s*(?:,\s*f\.\s*[0-9-]+)?)/i;

    const lines = cleanedBlockDataText.split('\n');
    // reformat cleanedBlockDataText for special cases
    lines.forEach((line, index) => {
        let modifiedLine = line;
        modifiedLine = modifiedLine.replace(/(\d+)pls,?\s*/g, (match, p1) => `pl. 1-${p1}, `).trim();
        modifiedLine = modifiedLine.replace(/:\s*\[(\d+(?:-\d+)?)\]/g, (match, p2) => `: ${p2}`);

        if ((modifiedLine.match(/-/g) || []).length === 2) {
            modifiedLine = modifiedLine.replace(/(\d+)-(\d+)\s*,\s*(\d+)-(\d+)/g, '$1-$4');
        }

        lines[index] = modifiedLine;

    });

    // ✅ handle cases where different journals are applied to the same bibliography entry
    // check for alternative title
    const alternativeJournalTitlePattern = /^([A-Z].*?)(?:\s+\d+\s*(?:\([^)]+\))?)?\s*:/;

    const remainingResults = lines
        .map(line => {
            let alternativeJournalTitle: string | undefined;
            const journalTitleMatch = line.match(alternativeJournalTitlePattern);
            if (journalTitleMatch) {
                alternativeJournalTitle = journalTitleMatch[1].trim();
                line = line.slice(alternativeJournalTitle.length).trim();
            }
            // Attempt to match using the primary pattern
            let modifiedLine = line;

            // If we detect comma-separated page numbers, simplify to a range
            if (modifiedLine.includes(',') && /\d/.test(modifiedLine)) {
                // Extract the part after the colon and before any plate references
                const pageSection = modifiedLine.split(':')[1]?.split('pl.')[0];
                if (pageSection) {
                    // Extract all numbers from the page section
                    const numbers = pageSection.match(/\d+/g);
                    if (numbers) {
                        const numericValues = numbers.map(n => parseInt(n)).filter(n => !isNaN(n));
                        if (numericValues.length > 0) {
                            const minPage = Math.min(...numericValues);
                            const maxPage = Math.max(...numericValues);
                            // Replace the complex page range with a simple range
                            modifiedLine = modifiedLine.replace(pageSection, ` ${minPage}-${maxPage}, `);
                        }
                    }
                }
            }

            // Now try to match the simplified line with our existing patterns
            let match = modifiedLine.match(pagePattern);

            if (match) {
                let [, volumeOrFirstPart, issueOrSecondPart, pageRangeStart, pageRangeEnd, plateStart, plateEnd, yearWithBrackets] = match;

                let pageRangeVolume: string | undefined;
                let pageRangeIssue: string | undefined;

                if (issueOrSecondPart) {
                    pageRangeVolume = volumeOrFirstPart;
                    pageRangeIssue = issueOrSecondPart;
                } else if (volumeOrFirstPart && volumeOrFirstPart.includes('-')) {
                    pageRangeIssue = volumeOrFirstPart;
                } else {
                    pageRangeVolume = volumeOrFirstPart;
                }

                // If pageRangeEnd is undefined, set it to pageRangeStart
                if (pageRangeStart && !pageRangeEnd) {
                    pageRangeEnd = pageRangeStart;
                }

                if (plateStart && !plateEnd) {
                    // If plateStart is defined but plateEnd is not, set plateEnd to the same value as plateStart
                    plateEnd = plateStart
                }

                let year: number | undefined;
                let yearBracketed: boolean | undefined;

                if (yearWithBrackets) {
                    const yearString = yearWithBrackets.replace(/[\[\]()]/g, '');
                    const parsedYear = parseInt(yearString, 10);
                    if (!isNaN(parsedYear)) {
                        // year can be parsed as number
                        year = parsedYear;
                        yearBracketed = yearWithBrackets.includes('[');
                    }
                    // leave year and yearBracketed as undefined if year is not parsable
                }

                return {
                    alternativeJournalTitle,
                    pageRangeIssue: pageRangeIssue,
                    pageRangeVolume: pageRangeVolume,
                    pageRangeStart: pageRangeStart,
                    pageRangeEnd: pageRangeEnd,
                    plateRangeStart: plateStart,
                    plateRangeEnd: plateEnd,
                    pageRangeYear: year,
                    pageRangeYearBracketed: yearBracketed,
                    pageNotes: undefined
                };
            } else {
                // Attempt to match using the secondary pattern
                match = line.match(secondaryPagePattern);

                if (match) {
                    let [, start, end, plateStart, plateEnd] = match;

                    // Combine multiple ranges into one if present
                    if (start) {
                        const ranges = start.split(',').map(r => r.trim()).filter(r => r.length > 0);
                        let startPage: string | undefined;
                        let endPage: string | undefined;

                        ranges.forEach(range => {
                            const [s, e] = range.split('-').map(part => part.trim());
                            if (startPage === undefined || parsePage(s) < parsePage(startPage)) {
                                startPage = s;
                            }
                            if (endPage === undefined || parsePage(e || s) > parsePage(endPage)) {
                                endPage = e || s;
                            }
                        });

                        start = startPage ?? '';
                        end = endPage ?? '';
                    }

                    if (start && !end) {
                        // If start is defined but end is not, set end to the same value as start
                        end = start;
                    }

                    if (plateStart && !plateEnd) {
                        // If plateStart is defined but plateEnd is not, set plateEnd to the same value as plateStart
                        plateEnd = plateStart;
                    }

                    return {
                        alternativeJournalTitle,
                        pageRangeIssue: undefined,
                        pageRangeVolume: undefined,
                        pageRangeStart: start,
                        pageRangeEnd: end,
                        plateRangeStart: plateStart,
                        plateRangeEnd: plateEnd,
                        pageRangeYear: undefined,
                        pageRangeYearBracketed: undefined,
                        pageNotes: undefined
                    };
                } else {
                    // Attempt to match using the tertiary pattern
                    match = line.match(tertiaryPagePattern);
                    if (match) {
                        let [, plateStart, plateEnd] = match;

                        if (plateStart && !plateEnd) {
                            plateEnd = plateStart;
                        }

                        return {
                            alternativeJournalTitle,
                            pageRangeIssue: undefined,
                            pageRangeVolume: undefined,
                            pageRangeStart: undefined,
                            pageRangeEnd: undefined,
                            plateRangeStart: plateStart,
                            plateRangeEnd: plateEnd,
                            pageRangeYear: undefined,
                            pageRangeYearBracketed: undefined,
                            pageNotes: undefined
                        };
                    }
                }
            }
        })
        .filter((item): item is NonNullable<typeof item> => item !== undefined);

    results.push(...remainingResults);

    // if (blockDataText.includes('Godart, 1819')) {
    //     console.log("\n--------------------")
    //     console.log("results 2:", remainingResults)
    // }

    return results;
}

// Helper function to parse page strings into comparable numbers
function parsePage(page: string): number {
    // Check if the page is a Roman numeral
    const romanNumerals: { [key: string]: number } = {
        i: 1, ii: 2, iii: 3, iv: 4, v: 5,
        vi: 6, vii: 7, viii: 8, ix: 9, x: 10,
        xi: 11, xii: 12, xiii: 13, xiv: 14, xv: 15,
        xvi: 16, xvii: 17, xviii: 18, xix: 19, xx: 20
    };

    const lowerPage = page.toLowerCase();
    if (romanNumerals[lowerPage]) {
        return romanNumerals[lowerPage];
    } else {
        return parseInt(page, 10) || 0;
    }
}

// test examples
// (1): 1-7,  pl. 1-40 (1872),
// : 1
// : 1-10
// : pl. 1
// : pl. 1-4
// : 1-10, pl. 1
// : 1, pl. 1-4
// (1) : 1
// (1): 1
// (1): pl. 1
// (1): pl. 1-4
// (1): 1-10, pl. 1
// (1): 1, pl. 1-4
// (1) : 1 (1950)
// (1): 1-10 (1950)
// (1): pl. 1 (1950)
// (1): pl. 1-4 (1950)
// (1): 1-10, pl. 1 (1950)
// (1): 1, pl. 1-4 (1950)
// : 1 (1950)
// : 1-10 (1950)
// : pl. 1 (1950)
// : pl. 1-4 (1950)
// : 1-10, pl. 1 (1950)
// : 1, pl. 1-4 (1950)
// (1): 1-7,  pl. 1-40 (1872),
// : i
// : i-xix
// : i-xix, pl. 1
// : i, pl. 1-4
// (1) : i
// (1): i-xix
// (1): i-xix, pl. 1
// (1): i, pl. 1-4
// (1) : i (1950)
// (1): i-xix (1950)
// (1): i-xix, pl. 1 (1950)
// (1): i, pl. 1-4 (1950)
// : i (1950)
// : i-xix (1950)
// : i-xix, pl. 1 (1950)
// : i, pl. 1-4 (1950)
// NEW CASES (Volume only)
// 2: 1-7,  pl. 1-40 (1872),
// 2 : 1
// 2: 1-10
// 2: pl. 1
// 2: pl. 1-4
// 2: 1-10, pl. 1
// 2: 1, pl. 1-4
// 2 : 1 (1950)
// 2: 1-10 (1950)
// 2: pl. 1 (1950)
// 2: pl. 1-4 (1950)
// 2: 1-10, pl. 1 (1950)
// 2: 1, pl. 1-4 (1950)
// 2 : i
// 2: i-xix
// 2: i-xix, pl. 1
// 2: i, pl. 1-4
// 2 : i (1950)
// 2: i-xix (1950)
// 2: i-xix, pl. 1 (1950)
// 2: i, pl. 1-4 (1950)

function getPlateIncrement(link: string): number | null {
    // Normalize the URL pattern by replacing either stream or details
    const normalizedLink = link.replace(/^https:\/\/archive\.org\/(stream|details)\//, 'https://archive.org/details/');

    // URLs that should not be incremented
    const noIncrementPattern = /^https:\/\/archive\.org\/details\/(mothbookpopularg|illustrationsofe11)/;

    // URLs that increment by 2
    const increment2Pattern = /^https:\/\/archive\.org\/details\/(journalofnewyork|proceedingsofent|proceedingsofdav|illustrationsofe|diegrossschmette|transactionsofam1186768amera)/;

    // URLs that increment by 4
    const increment4Pattern = /^https:\/\/archive\.org\/details\/(illustrationsofz|transactionsofam|contributionston|catalogueoflepid)/;

    if (noIncrementPattern.test(normalizedLink)) {
        return null;
    }
    if (increment2Pattern.test(normalizedLink)) {
        return 2;
    }
    if (increment4Pattern.test(normalizedLink)) {
        return 4;
    }
    return 1;
}

export function generatePages(
    pageRangeStart: string | undefined,
    pageRangeEnd: string | undefined,
    plateRangeStart: string | undefined,
    plateRangeEnd: string | undefined,
    element: Element,
    articleInfo: {
        title?: string,
        citedAs?: string[],
        issue?: Issue
    }
): Page[] {
    const pages: Page[] = [];
    const romanNumerals = ['i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix', 'x', 'xi', 'xii', 'xiii', 'xiv', 'xv', 'xvi', 'xvii', 'xviii', 'xix', 'xx'];


    function isRoman(str: string): boolean {
        return /^[ivxlcdm]+$/i.test(str);
    }

    function romanToInt(s: string): number {
        return romanNumerals.indexOf(s.toLowerCase()) + 1;
    }

    function intToRoman(num: number): string {
        return romanNumerals[num - 1];
    }

    function generateRange(start: string, end: string, type: 'page' | 'plate', element: Element): Page[] {
        const rangePages: Page[] = [];

        if (isRoman(start)) {
            const startNum = romanToInt(start);
            const endNum = end ? romanToInt(end) : startNum;
            const isUpperCase = start === start.toUpperCase();

            let startingLink: string | undefined
            if (type === 'page') {
                startingLink = findExternalLink(startNum.toString(), element, true)[0] // assume first match is page
            } else {
                startingLink = findExternalLink(startNum.toString(), element, true)[1] // assume second match is plate
            }

            for (let i = startNum; i <= endNum; i++) {
                let currentLink = startingLink;
                if (startingLink && i > startNum) {
                    const increment = type === 'plate' ? getPlateIncrement(startingLink) : 1;
                    if (increment === null) {
                        currentLink = ""; // Clear entire link for non-sequential plates
                    } else {
                        // Only perform replacement if we have a valid increment
                        currentLink = startingLink.replace(/page\/n?(\d+)\/mode/, (match, pageNum) => {
                            const newPageNum = parseInt(pageNum) + ((i - startNum) * increment);
                            return match.replace(pageNum, newPageNum.toString());
                        });
                    }
                }
                const label = isUpperCase ? intToRoman(i).toUpperCase() : intToRoman(i);
                const page = {
                    id: uuidv7(),
                    label,
                    type,
                    link: currentLink || undefined
                };
                // const cacheKey = generatePageCacheKey(page, articleInfo);
                // let cachedPage = getPageFromCache(cacheKey);

                // if (!cachedPage) {
                //     cachedPage = page;
                //     addPageToCache(cacheKey, page);
                // }

                // rangePages.push(cachedPage);
                rangePages.push(page);
            }
        } else {
            const startNum = parseInt(start);
            const endNum = end ? parseInt(end) : startNum;

            let startingLink: string | undefined
            if (type === 'page') {
                startingLink = findExternalLink(startNum.toString(), element, true)[0] // assume first matched "1" is page
            } else {
                if (startNum === 1) {
                    startingLink = findExternalLink(startNum.toString(), element, true)[1] // assume second matched "1" link is plate
                } else {
                    startingLink = findExternalLink(startNum.toString(), element, true)[0]
                }
            }

            for (let i = startNum; i <= endNum; i++) {
                let currentLink = startingLink;
                if (startingLink && i > startNum) {
                    const increment = type === 'plate' ? getPlateIncrement(startingLink) : 1;
                    if (increment === null) {
                        currentLink = ""; // Clear entire link for non-sequential plates
                    } else {
                        // Only perform replacement if we have a valid increment
                        currentLink = startingLink.replace(/page\/n?(\d+)\/mode/, (match, pageNum) => {
                            const newPageNum = parseInt(pageNum) + ((i - startNum) * increment);
                            return match.replace(pageNum, newPageNum.toString());
                        });
                    }
                }
                const label = i.toString();
                const page = {
                    id: uuidv7(),
                    label,
                    type,
                    link: currentLink || undefined
                };
                // const cacheKey = generatePageCacheKey(page, articleInfo);
                // let cachedPage = getPageFromCache(cacheKey);

                // if (!cachedPage) {
                //     cachedPage = page;
                //     addPageToCache(cacheKey, page);
                // }

                // rangePages.push(cachedPage);
                rangePages.push(page);
            }
        }

        return rangePages;
    }

    if (pageRangeStart) {
        pages.push(...generateRange(pageRangeStart, pageRangeEnd || pageRangeStart, 'page', element));
    }

    if (plateRangeStart) {
        pages.push(...generateRange(plateRangeStart, plateRangeEnd || plateRangeStart, 'plate', element));
    }

    // if (articleInfo.citedAs) {
    // if (articleInfo.citedAs[0] === "Arima, 1996") {
    //     console.log("PAGES:", pages)
    // }
    // }

    return pages;
}

