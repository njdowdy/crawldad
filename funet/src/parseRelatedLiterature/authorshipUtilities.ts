import { uuidv7 } from "uuidv7";
import { Authorship, Person } from "../types";
import { getPersonFromCache, upsertPersonToCache } from "./personCacheManager";

function generatePerson(author: string): Person {
    // "Hampson" OR "H. Klements" OR "de Freina"
    // ✅ Hyphenated names: "Herrich-Schäffer"
    const nameParts = author.trim().split(' ');
    let givenName: string | undefined;
    let surname: string;
    let middleNames: { position: number, middleName: string }[] | undefined;
    let salutation: string | undefined;

    const surnamePrefixes = ['St.', 'St', 'da', 'de', 'del', 'della', 'den', 'der', 'des', 'di', 'du', 'la', 'le', 'los', 'ten', 'ter', 'van', 'von', 'zu'];

    if (nameParts.length === 1) {
        surname = nameParts[0];
    } else if (nameParts.length === 2 && nameParts[0].match(/^[A-Z]\.$/)) {
        givenName = nameParts[0].replace('.', '');
        surname = nameParts[1];
    } else {
        const lastPartIndex = nameParts.length - 1;
        const secondLastPartIndex = lastPartIndex - 1;

        if (surnamePrefixes.includes(nameParts[secondLastPartIndex].toLowerCase())) {
            surname = `${nameParts[secondLastPartIndex]} ${nameParts[lastPartIndex]}`;
            givenName = nameParts.slice(0, secondLastPartIndex).join(' ');
        } else {
            surname = nameParts[lastPartIndex];
            givenName = nameParts.slice(0, lastPartIndex).join(' ');
        }
    }
    // avoid cases where givenName is parsed as e.g., "Staudinger in" for "Staudinger"
    if (givenName === `${surname} in`) {
        givenName = undefined
    }

    // special rules
    // ! the following are taxa with extremely long names that will need to be manually vetted most likely

    if (givenName === "f alcanor f. ensifer" && (surname === "Rousseau-Decelle")) {
        givenName = ''
        surname = 'Rousseau-Decelle'
    }

    if ((givenName === "napaeae meridionalis f. regressiva" || givenName === "napaeae meridionalis f. rapaeula" || givenName === "napaeae meridionalis f. pseudocanidia") && (surname === "Stauder")) {
        givenName = ''
        surname = 'Stauder'
    }


    if ((givenName === "gilolensis" || givenName === "celebensis" || givenName === "papuensis") && (surname === "Wallace")) {
        givenName = ''
        surname = 'Wallace'
    }



    if (givenName === "Maj.") {
        givenName = ''
        salutation = 'Major'
    }

    if (givenName?.endsWith('v.')) {
        givenName = givenName.replace(/\s+v\.\s*/, '')
        surname = 'von ' + surname
    }

    if (givenName === "Fischer" && (surname === "von Roeslerstamm" || surname === "von Röslerstamm" || surname === "von Rösslerstamm" || surname === "von Roesslerstamm")) {
        givenName = 'J'
        middleNames = [{ position: 1, middleName: 'E' }]
        surname = 'Fischer von Röslerstamm'
    }

    if (givenName === "Schuster" && surname === "von Forstner") {
        givenName = 'W'
        surname = 'Schuster von Forstner'
    }

    if (givenName === "van" && (surname === "der Hoeven")) {
        givenName = ''
        surname = 'van der Hoeven'
    }

    if ((givenName === "van" || givenName === "Van") && ((surname === "den Berg") || (surname === "den Bergh"))) {
        givenName = ''
        surname = 'Van den Berg'
    }

    if (givenName === "Rego" && (surname === "Barros")) {
        givenName = 'A'
        surname = 'Rego Barros'
    }

    if (givenName === "Grote" && (surname === "MS")) {
        givenName = 'A'
        middleNames = [{ position: 1, middleName: 'R' }]
        surname = 'Grote'
    }

    if (givenName === "de Selys" && (surname === "Longchamps")) {
        givenName = 'E'
        surname = 'de Selys Longchamps'
    }

    if (surname === "Grote") {
        givenName = 'A'
        middleNames = [{ position: 1, middleName: 'R' }]
        surname = 'Grote'
    }

    if (surname === "Capr?") {
        givenName = ''
        surname = 'Capronnier'
    }

    if (givenName === "Chiarelli" && (surname === "de Gahn")) {
        givenName = ''
        surname = 'Chiarelli de Gahn'
    }

    if (givenName === "González" && (surname === "Prada")) {
        givenName = 'R'
        surname = 'González Prada'
    }

    if (givenName === "San" && (surname === "Blas")) {
        givenName = ''
        surname = 'San Blas'
    }

    if (givenName === "Raymundo" && (surname === "da Silva")) {
        givenName = ''
        surname = 'Raymundo da Silva'
    }

    if (givenName === "Fernández" && (surname === "Rubio")) {
        givenName = ''
        surname = 'Fernández Rubio'
    }

    if (givenName === "Popescu" && (surname === "Gorj")) {
        givenName = 'A'
        surname = 'Popescu-Gorj'
    }

    if (givenName === "Regteren" && (surname === "Altena")) {
        givenName = 'C'
        middleNames = [{ position: 1, middleName: 'O' }]
        surname = 'van Regteren Altena'
    }

    if (givenName === "Passerin" && (surname === "d'Entrèves")) {
        givenName = 'P'
        surname = "Passerin d'Entrèves"
    }

    if (givenName === "Monzon" && (surname === "Sierra")) {
        givenName = 'J'
        surname = "Monzón Sierra"
    }

    if (givenName === "Souza" && (surname === "Moraes")) {
        givenName = 'S'
        surname = "de Souza Moraes"
    }

    if (givenName === "Schmidt" && (surname === "Nielsen")) {
        givenName = 'E'
        surname = "Schmidt Nielsen"
    }

    if (givenName === "van" && (surname === "der Wolf")) {
        givenName = 'H'
        middleNames = [{ position: 1, middleName: 'W' }]
        surname = "van der Wolf"
    }

    if (givenName === "van" && (surname === "den Brink")) {
        givenName = ''
        surname = "van den Brink"
    }

    if (givenName === "Expósito" && (surname === "Hermosa")) {
        givenName = 'A'
        surname = "Expósito Hermosa"
    }

    if (givenName === "Martínez" && (surname === "Borrego")) {
        givenName = ''
        surname = "Martínez Borrego"
    }

    if (givenName === "W. S." && (surname === "Wright")) {
        givenName = 'W'
        middleNames = [{ position: 1, middleName: 'S' }]
        surname = "Wright"
    }

    if (givenName === "R.E." && (surname === "Turner")) {
        givenName = 'R'
        middleNames = [{ position: 1, middleName: 'E' }]
        surname = "Turner"
    }

    if (givenName === "L. E." && (surname === "Dunn")) {
        givenName = 'L'
        middleNames = [{ position: 1, middleName: 'E' }]
        surname = "Dunn"
    }

    if (givenName === "K" && (surname === "L.")) {
        givenName = 'K'
        middleNames = [{ position: 1, middleName: 'L' }]
        surname = "Dunn"
    }

    if ((givenName === "L.B." || givenName === "L. B.") && (surname === "Prout")) {
        givenName = 'L'
        middleNames = [{ position: 1, middleName: 'B' }]
        surname = "Prout"
    }

    if (givenName === "Jenner" && (surname === "Weir")) {
        givenName = 'J'
        surname = "Jenner Weir"
    }

    if (givenName === "Silva" && (surname === "del Pozo")) {
        givenName = ''
        surname = "Silva del Pozo"
    }

    if (givenName === "Vives" && (surname === "Moreno")) {
        givenName = 'A'
        surname = "Vives Moreno"
    }

    if (givenName === "Núñez" && (surname === "Bustos")) {
        givenName = 'E'
        surname = "Núñez Bustos"
    }

    if (givenName === "Núñez" && (surname === "Bustos")) {
        givenName = 'E'
        surname = "Núñez Bustos"
    }

    if (givenName === "Oritz" && (surname === "Garcia")) {
        givenName = ''
        surname = "Ortiz-García"
    }

    if (givenName === "dos" && (surname === "Passos")) {
        givenName = ''
        surname = 'dos Passos'
    }

    if (givenName === "St" && (surname === "Leger")) {
        givenName = ''
        surname = 'St. Leger'
    }

    if (givenName === "van" && (surname === "den Pol")) {
        givenName = ''
        surname = 'van den Pol'
    }

    if (givenName === "van" && (surname === "de Weghe")) {
        givenName = ''
        surname = 'van de Weghe'
    }

    if (givenName === "van" && (surname === "de Poll")) {
        givenName = 'J'
        middleNames = [{ position: 1, middleName: 'R' }, { position: 2, middleName: 'H' }]
        surname = 'Neervoort van de Poll'
    }

    if (givenName === "Dalla" && (surname === "Torre")) {
        givenName = 'K'
        middleNames = [{ position: 1, middleName: 'W' }]
        surname = 'Dalla Torre'
    }

    if (givenName === "W. G." && (surname === "Wright")) {
        givenName = 'W'
        middleNames = [{ position: 1, middleName: 'G' }]
        surname = 'Wright'
    }

    if (givenName === "J. E." && (surname === "Smith")) {
        givenName = 'J'
        middleNames = [{ position: 1, middleName: 'E' }]
        surname = 'Smith'
    }

    if (givenName === "J. B." && (surname === "Smith")) {
        givenName = 'J'
        middleNames = [{ position: 1, middleName: 'B' }]
        surname = 'Smith'
    }

    if (givenName === "Hy." && (surname === "Edwards")) {
        givenName = 'H'
        surname = 'Edwards'
    }

    if (givenName === "R. G." && (surname === "Maza")) {
        givenName = 'R'
        middleNames = [{ position: 1, middleName: 'G' }]
        surname = 'de la Maza'
    }

    if (givenName === "de" && (surname === "la Maza")) {
        givenName = 'R'
        middleNames = [{ position: 1, middleName: 'G' }]
        surname = 'de la Maza'
    }

    if (givenName === "González" && (surname === "Cota")) {
        givenName = ''
        surname = 'González Cota'
    }

    if (givenName === "Salazar" && (surname === "Escobar")) {
        givenName = 'J'
        middleNames = [{ position: 1, middleName: 'A' }]
        surname = 'Salazar-Escobar'
    }

    if (givenName === "Llorente" && (surname === "Bousquets")) {
        givenName = 'J'
        middleNames = [{ position: 1, middleName: 'E' }]
        surname = 'Llorente Bousquets'
    }

    if (givenName === "Luis" && (surname === "Martinez")) {
        givenName = 'A'
        middleNames = [{ position: 1, middleName: 'M' }]
        surname = 'Luis Martinez'
    }

    if (givenName === "Vargas" && (surname === "Fernandez")) {
        givenName = 'I'
        surname = 'Vargas Fernández'
    }

    if (givenName === "Fischer" && (surname === "von Waldheim" || surname === "de Waldheim" || surname === "von Waldehiem")) {
        givenName = 'G'
        surname = 'Fischer von Waldheim'
    }

    if ((givenName === "Skowron" && (surname === "Volponi")) || (surname === "Volponi") || (surname === "Skowron")) {
        givenName = 'M'
        surname = 'Skowron Volponi'
    }

    if (givenName === "Lass de" && (surname === "la Vega")) {
        givenName = ''
        surname = 'Lasso de la Vega'
    }


    if (givenName === "van" && (surname === "der Poorten")) {
        givenName = ''
        surname = 'van der Poorten'
    }

    if (givenName === "Rhé" && (surname === "Philipe")) {
        givenName = 'G'
        middleNames = [{ position: 1, middleName: 'W' }, { position: 2, middleName: 'V' }]
        surname = 'de Rhé Philipe'
    }

    if (givenName === "Fernandez" && (surname === "Rubio")) {
        givenName = 'F'
        surname = 'Fernández-Rubio'
    }

    if (givenName === "Gómez" && (surname === "Bustillo")) {
        givenName = 'M'
        middleNames = [{ position: 1, middleName: 'R' }]
        surname = 'Gómez-Bustillo'
    }

    if (givenName === "Muñoz" && (surname === "Sariot")) {
        givenName = ''
        surname = 'Muñoz Sariot'
    }

    if (givenName === "Febvay" && (surname === "du Couedic")) {
        givenName = ''
        surname = 'Febvay du Couedic'
    }

    if (givenName === "Sánchez" && (surname === "Mesa")) {
        givenName = ''
        surname = 'Sánchez Mesa'
    }

    if (givenName === "Torre" && (surname === "Callejas")) {
        givenName = 'S'
        middleNames = [{ position: 1, middleName: 'L' }]
        surname = 'de la Torre y Callejas'
    }

    if (givenName === "Snellen" && (surname === "van Vollenhoven")) {
        givenName = 'S'
        middleNames = [{ position: 1, middleName: 'C' }]
        surname = 'Snellen van Vollenhoven'
    }

    if (givenName === "Brunner" && (surname === "von Wattenwyl")) {
        givenName = 'C'
        surname = 'Brunner von Wattenwyl'
    }

    if (givenName === "J." && (surname === "de Joannis")) {
        givenName = 'J'
        surname = 'de Joannis'
    }

    if (givenName === "M. J." && (surname === "Smith")) {
        givenName = 'M'
        middleNames = [{ position: 1, middleName: 'J' }]
        surname = 'Smith'
    }

    if (givenName === "W. H." && (surname === "Edwards")) {
        givenName = 'W'
        middleNames = [{ position: 1, middleName: 'H' }]
        surname = 'Edwards'
    }

    if (givenName === "G. P." && (surname === "Arruda")) {
        givenName = 'G'
        middleNames = [{ position: 1, middleName: 'P' }]
        surname = 'Arruda'
    }

    if (surname === "(C.") {
        givenName = 'C'
        surname = 'Felder'
    }

    if (surname === "?Koiwaya") {
        givenName = ''
        surname = 'Koiwaya'
    }

    if (givenName === "Martins" && (surname === "Neto")) {
        givenName = 'W'
        middleNames = [{ position: 1, middleName: 'H' }]
        surname = 'Edwards'
    }


    if (surname === "Mattoon") {
        givenName = 'S'
        middleNames = [{ position: 1, middleName: 'O' }]
        surname = 'Mattoon'
    }

    if (givenName === "Gómez" && (surname === "de Aizpurua")) {
        givenName = ''
        surname = 'Gómez de Aizpurua'
    }

    if (givenName === "Fernández" && (surname === "Yépez")) {
        givenName = 'F'
        middleNames = [{ position: 1, middleName: 'J' }]
        surname = 'Fernández Yépez'
    }

    if (givenName === "Fernández" && (surname === "Vidal")) {
        givenName = ''
        surname = 'Fernández Vidal'
    }

    if (givenName === "de" && (surname === "la Harpe")) {
        givenName = ''
        surname = 'de la Harpe'
    }

    if (givenName === "da Silva" && (surname === "Cruz")) {
        givenName = ''
        surname = 'da Silva Cruz'
    }

    if (givenName === "Vázquez" && (surname === "Figueroa")) {
        givenName = ''
        surname = 'Vázquez Figueroa'
    }

    if (givenName === "Faure-Bignet" && (surname === "de Simonest")) {
        givenName = ''
        surname = 'Faure-Bignet de Simonest'
    }

    if (surname === "?Laasonen") {
        givenName = ''
        surname = 'Laasonen'
    }


    if (surname === "?Salvin") {
        givenName = ''
        surname = 'Salvin'
    }

    if (surname === "?Fruhstorfer") {
        givenName = ''
        surname = 'Fruhstorfer'
    }

    if (givenName === "Vega" && (surname === "Escandon")) {
        givenName = 'F'
        surname = 'Vega Escandón'
    }

    if (givenName === "Palanca" && (surname === "Soler")) {
        givenName = ''
        surname = 'Palanca Soler'
    }

    if (givenName === "Castan" && (surname === "Lanaspa")) {
        givenName = ''
        surname = 'Castan Lanaspa'
    }

    if (givenName === "Calle" && (surname === "Pascual")) {
        givenName = ''
        surname = 'Calle Pascual'
    }

    if (givenName === "de Toledo" && (surname === "Piza")) {
        givenName = 'S'
        surname = 'de Toledo Piza'
    }

    if (givenName === "Rosas" && (surname === "Costa")) {
        givenName = 'J'
        surname = 'Rosas Costa'
    }

    if ((givenName === "Gómez" || givenName === "Gomez") && (surname === "Bustilo" || surname === "Bustillo")) {
        givenName = 'M'
        surname = 'Gómez Bustilo'
    }

    if (givenName === "F. M." && (surname === "Brown")) {
        givenName = 'F'
        middleNames = [{ position: 1, middleName: 'M' }]
        surname = 'Brown'
    }

    if (givenName === "Abós" && (surname === "Castel")) {
        givenName = 'F'
        middleNames = [{ position: 1, middleName: 'P' }]
        surname = 'Abós Castel'
    }

    if (givenName === "Vitalis" && (surname === "de Salvaza")) {
        givenName = 'M'
        middleNames = [{ position: 1, middleName: 'R' }]
        surname = 'Vitalis de Salvaza'
    }

    if (givenName === "Palisot" && (surname === "de Beauvois") || (surname === "de Beauvais")) {
        givenName = 'A'
        middleNames = [{ position: 1, middleName: 'M' }, { position: 2, middleName: 'F' }, { position: 3, middleName: 'J' }]
        surname = 'Palisot de Beauvois'
    }

    if (givenName === "Bivar" && (surname === "de Sousa")) {
        givenName = 'A'
        surname = 'Bivar de Sousa'
    }

    if (givenName === "Mast" && (surname === "de Maeght")) {
        givenName = 'J'
        surname = 'Mast de Maeght'
    }

    if (givenName === "Crosson" && (surname === "du Cormier")) {
        givenName = 'A'
        surname = 'Crosson du Cormier'
    }

    if (givenName === "Gonzalo" && (surname === "Fidel")) {
        givenName = ''
        surname = 'Gonzalo Fidel'
    }

    if (givenName === "Nargis" && (surname === "Viqar")) {
        givenName = 'S'
        surname = 'Nargis Viqar'
    }

    if (surname === "?Geoffroy") {
        givenName = ''
        surname = 'Geoffroy'
    }

    if (givenName === "Lencina" && (surname === "Gutiérrez")) {
        givenName = 'F'
        surname = 'Lencina Gutiérrez'
    }

    if (givenName === "Desmier" && (surname === "de Chenon")) {
        givenName = 'R'
        surname = 'Desmier de Chenon'
    }

    if (givenName === "St" && (surname === "Lauren")) {
        givenName = 'Â'
        middleNames = [{ position: 1, middleName: 'M' }]
        surname = 'da Costa Lima'
    }

    if (givenName === "Costa" && (surname === "Lima")) {
        givenName = 'R'
        surname = 'St. Laurent'
    }

    if (givenName === "Lycklama á" && (surname === "Nyeholf" || surname === "Nyeholt")) {
        givenName = ''
        surname = 'Lycklama á Nyeholt'
    }

    if (givenName === "Bang" && (surname === "Haas")) {
        // ! Note: will need later disambiguation between father (A. Bang-Haas) and son (O. Bang-Haas)
        givenName = ''
        surname = 'Bang-Haas'
    }

    if ((givenName === "Donckier" || givenName === "Donkier") && (surname === "de Donceel" || surname === "de Donchel") || surname === "de Douceel") {
        givenName = 'C'
        surname = 'Donckier de Donceel'
    }

    if (givenName && surname === "etc.") {
        surname = givenName
        givenName = ''
    }

    if ((givenName === 'Crombrugghe' || givenName === 'Crombrugge') && (surname === 'de Picquendaele' || surname === 'de Piquencdaele') || surname === 'de Piequendaele' || surname === 'de Pickendaele') {
        givenName = ''
        surname = 'Crombrugghe de Picquendaele'
    }

    if (surname) {
        surname = surname.replace('[', '').replace(']', '').trim()
    }

    const cacheKey = `${givenName || ''}|${surname}`;
    const cachedPerson = getPersonFromCache(cacheKey);

    if (cachedPerson) {
        return cachedPerson;
    }

    const person: Person = {
        id: uuidv7(),
        easternNameOrder: false,
        givenName,
        surname,
        middleNames,
        salutation
    };

    upsertPersonToCache(person);
    return person;
}

function parseAuthorship(authorList: string[]): Authorship[] {
    // "Hampson & H. Klements" OR "H. Klements" OR "Hampson"
    const parsedAuthorship: Authorship[] = []
    authorList.map((author, index) => {
        const authorship: Authorship = {
            author: generatePerson(author),
            position: index,
        }
        parsedAuthorship.push(authorship)
    })
    return parsedAuthorship // ➡️ [{ id: uuidv7(), surname: "Hampson", easternOrder: false }, position: 0}]
}

function extractYear(yearInfo: string): { year: number | undefined, bracketedYear: boolean } {
    // "([1865])" OR "1865" OR "(1865)" OR "?" OR "177?"
    if (!yearInfo || yearInfo.includes('?') || yearInfo.includes('MS')) {
        return { year: undefined, bracketedYear: false };
    }
    const yearMatch = yearInfo.match(/\d+/);
    const year = yearMatch && !isNaN(parseInt(yearMatch[0])) ? parseInt(yearMatch[0]) : undefined;
    const bracketedYear = yearInfo.includes('[') && yearInfo.includes(']');
    return { year, bracketedYear }
}

export function extractAuthorshipString(blockDataText: string): { authorshipString: string, citedAs: string[] } {
    // Remove duplicate years in square brackets (e.g., "1881 [1882]" becomes "1881")
    let cleanedBlockDataText = blockDataText.replace(/(\d+)\s+\[\d+\]/g, '$1')

    // if we find text where only the year has parentheses, we will remove the parentheses
    // example: Graeser, (1890) -> Graeser, 1890
    // example: Johnson & Graeser, ([1890]) -> Johnson & Graeser, [1890]
    cleanedBlockDataText = cleanedBlockDataText.replace(/\(\[?(\d+)\]?\)/g, (match, year) => {
        // If there are square brackets inside the parentheses, keep them.
        // Otherwise, remove the parentheses.
        if (match.includes('[')) {
            return `[${year}]`
        } else {
            return year
        }
    });

    const authorship = cleanedBlockDataText.split('\n')[0]
    const authorshipParts = authorship.split(';')
    const citedAs: string[] = [];
    // console.log("Authorship Parts:", authorshipParts)

    authorshipParts.forEach(part => {
        if (part.trim().match(/^.+,\s+(\[?[0-9-]+\]?)(?:[xXzZ]|AA|aa)?$/)) {
            citedAs.push(part.replace(/\n\n=.*$/g, '').trim());
        }
    });

    // console.log("Authorship:", citedAs[0])
    // console.log("citedAs:", citedAs)
    return { authorshipString: citedAs[0], citedAs }; // ➡️ {authorshipString: "Hampson, 1901", citedAs: ["Hampson, 1901"]} OR {authorshipString: "Stretch, 1872", citedAs: ["Stretch, 1872", "Stretch, 1874"]}
}

export function extractAbbreviation(blockDataText: string): string | undefined {
    const authorship = blockDataText.split('\n')[0];
    const abbreviation = authorship.split(';')[0].trim();

    // Updated so it stops at the first ']' instead of enforcing the end of the string
    const match = abbreviation.match(/^\[([A-Za-z0-9]+)\]/);

    if (match) {
        // Return whatever was captured inside the brackets up to the first ']'
        return match[1];
    } else {
        return undefined;
    }
}

export function parseAuthorshipString(authorshipString: string): { year: number | undefined, bracketedYear: boolean, authorship: Authorship[] } {
    // "Hampson, 1901" OR "Hampson, [1901]" OR "Hampson & H. Klements, 1901"
    // ✅ Any 3+ author cases like "Hampson, Williams, and Klements"? - Olepa schleini has "Witt, Müller, Kravchenko, Miller, Hausmann & Speidel, 2005"
    // ✅ "Draudt in Seitz, 1931": "Draudt" not "Draudt in"
    // ✅ "C. &  R. Felder, 1862" resolves to 2 people with shared last name

    const authorList: string[] = [];

    // OPTION 1: PUSH ALL AUTHORS TO AUTHOR LIST -> "Ronkay & Kobayashi in Hreblay & Ronkay, 1997" -> [ 'Ronkay', 'Kobayashi', 'Hreblay', 'Ronkay' ]
    // const authorList1 = authorshipString.split(/[,&]/).slice(0, -1).map(x => {
    //     // This existing replace ensures that trailing "in Author" is removed if we haven't handled it above.
    //     return x.replace(/\s+in\s+[A-Za-z]+$/, '').trim();
    // });
    // authorList1.forEach(x => {
    //     x.split(' in ').forEach(y => authorList.push(y)) // deal with "name in X" components
    // })

    // OPTION 2: ONLY PUSH AUTHORS AFTER "IN" -> "Ronkay & Kobayashi in Hreblay & Ronkay, 1997" -> [ 'Hreblay', 'Ronkay' ]
    authorshipString = authorshipString.replace(/\s*,?\s*et\.?\s+al\.?/, '').replace(/\s*,?\s*c\.\s+s\./, '')
    const matchIn = authorshipString.match(/\s+in\s+/);
    if (matchIn) {
        const start = matchIn.index as number;
        const len = matchIn[0].length; // the length of " in "
        authorshipString = authorshipString.substring(start + len).trim();
    }
    const authorList1 = authorshipString.split(/[,&]/).slice(0, -1).map(x => {
        // This existing replace ensures that trailing "in Author" is removed if we haven't handled it above.
        return x.replace(/\s+in\s+[A-Za-z]+$/, '').trim();
    });
    authorList1.forEach(x => {
        if (x.trim()) {
            authorList.push(x.trim());
        }
    });

    // Handle cases where authors share a surname, e.g. "C. & R. Felder, 1862"
    const processedAuthorList = [];
    let sharedSurname = '';

    for (let i = 0; i < authorList.length; i++) {
        const author = authorList[i];
        if (author.match(/^[A-Z]\.?$/)) {
            // If the current author is just an initial
            if (i < authorList.length - 1) {
                // Get the surname from the next author
                const nextAuthor = authorList[i + 1];
                sharedSurname = nextAuthor.split(' ').pop() || '';
                processedAuthorList.push(`${author} ${sharedSurname}`);
            } else {
                // If it's the last author, just add it as is
                processedAuthorList.push(`${author} ${sharedSurname}`);
            }
        } else {
            processedAuthorList.push(author);
            // Update sharedSurname in case the next author is an initial
            sharedSurname = author.split(' ').pop() || '';
        }
    }

    // if (authorshipString.includes("Ronkay & Kobayashi in Hreblay & Ronkay, 1997")) {
    //     console.log("authorList1:", authorList1)
    //     console.log("authorList:", authorList)
    //     console.log("processedAuthorList:", processedAuthorList)
    // }

    const authorship = parseAuthorship(processedAuthorList);

    // ✅ Authorship without year info: "Kuznetsov"
    // ✅ Year is incomplete or is denoted with '?': "Smith, 177?" or "Kuznetsov, ?"
    // ✅ Handle "Grehan, Mielke, Turner & Nielsen, 2023"
    const lastCommaIndex = authorshipString.lastIndexOf(',');
    const yearPart = authorshipString.substring(lastCommaIndex + 1);
    // console.log("yearPart:", yearPart)
    const { year, bracketedYear } = yearPart
        ? extractYear(yearPart.trim())
        : { year: undefined, bracketedYear: false };
    return { year, bracketedYear, authorship }; // ➡️ { 1901, false, [{author: { id: uuidv7(), surname: "Hampson", easternOrder: false }, position: 0}]}
}
