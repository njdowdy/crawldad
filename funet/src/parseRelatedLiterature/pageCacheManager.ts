import { Issue, Page } from "../types";
import { generateArticleCacheKey } from "./articleCacheManager";

// Main Map from cacheKey -> Page
const pageCache: Map<string, Page> = new Map();

// Additional index from page.id -> cacheKey
const pageIdIndex: Map<string, string> = new Map();

/**
 * Generates a stable cache key for a Page in combination
 * with the article info, e.g. "introduction|section|articlekey".
 */
export function generatePageCacheKey(
    page: Page,
    articleInfo: { title?: string; citedAs?: string[]; issue?: Issue },
): string {
    const articleKey = generateArticleCacheKey(articleInfo);
    return `${page.label || ""}|${page.type || ""}|${articleKey}`.toLowerCase().trim();
}

/**
 * Retrieves an existing Page by cacheKey.
 */
export function getPageFromCache(cacheKey: string): Page | undefined {
    return pageCache.get(cacheKey);
}

/**
 * Adds or merges a Page into the cache. If any Page in
 * the cache already has the same ID, it skips adding.
 */
export function addPageToCache(cacheKey: string, page: Page): void {
    if (!page.id) return;

    // Check if we already stored a Page with this ID anywhere in the cache
    if (pageIdIndex.has(page.id)) {
        return; // Skip: already in the cache
    }

    const existingPage = pageCache.get(cacheKey);
    if (existingPage) {
        // Merge data if there's an existing page under the same cacheKey
        Object.assign(existingPage, page);
        // Record the ID -> cacheKey mapping in the index
        pageIdIndex.set(page.id, cacheKey);
        return;
    }

    // Otherwise, add the new page
    pageCache.set(cacheKey, page);
    // Track that we have the ID in the cache
    pageIdIndex.set(page.id, cacheKey);
}

/**
 * Clears all entries from both Maps, keeping them consistent.
 */
export function clearPageCache(): void {
    pageCache.clear();
    pageIdIndex.clear();
}

/**
 * Returns the total number of pages in the cache.
 */
export function getPageCacheSize(): number {
    return pageCache.size;
}

/**
 * Returns a list of all Pages in the cache.
 */
export function getAllPagesFromCache(): Page[] {
    return Array.from(pageCache.values());
}

/**
 * Returns all cacheKeys as an array.
 */
export function getAllPageCacheKeys(): string[] {
    return Array.from(pageCache.keys());
}
