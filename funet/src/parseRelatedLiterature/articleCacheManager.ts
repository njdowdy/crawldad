import { uuidv7 } from "uuidv7";
import { Article, Authorship, Issue, Page, PageSubelement } from "../types";
import { addPageToCache, generatePageCacheKey, getPageFromCache } from "./pageCacheManager";

const articleCache: Map<string, Article> = new Map();

export function getArticleFromCache(cacheKey: string): Article | undefined {
    const article = articleCache.get(cacheKey);
    // if (article?.title === 'Rhopaloceren aus Holländisch-Neu-Guinea') {
    //     console.log("Searched by cacheKey: ", article.id);
    // }
    return article
}

export function generateArticleCacheKey(
    articleOrValues:
        | Article
        | { title?: string, citedAs?: string[], issue?: Issue }
        | { title?: string, citedAs?: string[], volumeValue?: string, issueLabel?: string, journalTitleOrAbbreviation?: string }
): string {
    let title = '';
    let citedAs: string | string[] = '';
    let issueLabel = '';
    let volumeValue = '';
    let journalTitleOrAbbreviation = '';

    if ('volumeValue' in articleOrValues) {
        ({
            title = '',
            citedAs =[],
            volumeValue = '',
            issueLabel = '',
            journalTitleOrAbbreviation = ''
        } = articleOrValues);
        citedAs = citedAs[0] || '';
    } else if ('issue' in articleOrValues) {
        const { issue } = articleOrValues;
        title = articleOrValues.title || '';
        citedAs = articleOrValues.citedAs?.[0] || '';
        issueLabel = issue?.label || '';
        volumeValue = issue?.volume?.label || '';
        journalTitleOrAbbreviation = issue?.volume?.journal?.title || issue?.volume?.journal?.abbreviation || '';
    }

    return `${citedAs}|${title}|${issueLabel}|${volumeValue}|${journalTitleOrAbbreviation}`.toLowerCase().trim();
}

export function upsertArticle(article: Article, proposedCacheKey: string): Article {
    // 1) Deduplicate pages first (as existing code does).
    const upsertedArticle = article;
    // Process and de-duplicate pages
    // if (upsertedArticle.citedAs[0] === "Joicey & Talbot, 1923") {
    //     console.log("CL6")
    //     console.log(JSON.stringify(upsertedArticle.pages))
    // }
    if (upsertedArticle.pages) {
        upsertedArticle.pages = upsertedArticle.pages.map(page => {
            const pageCacheKey = generatePageCacheKey(page, {
                title: upsertedArticle.title,
                citedAs: upsertedArticle.citedAs,
                issue: upsertedArticle.issue
            });
            // if (upsertedArticle.citedAs[0] === "Joicey & Talbot, 1923") {
            //     console.log("CL7")
            //     console.log("pageCacheKey: ", pageCacheKey)
            // }
            const existingPage = getPageFromCache(pageCacheKey);
            // if (upsertedArticle.citedAs[0] === "Joicey & Talbot, 1923") {
            //     console.log("CL8")
            //     console.log("existingPage: ", existingPage)
            // }
            if (existingPage) {
                const mergedPage = mergePages(existingPage, page);
                addPageToCache(pageCacheKey, mergedPage);
                return mergedPage;
            } else {
                // page is not in the cache, so we store it there
                if (!page.id) {
                    page.id = uuidv7();
                }
                addPageToCache(pageCacheKey, page);
                return page;
            }
        });
    }

    // 2) Check whether there is an existing article in the cache by the proposed cacheKey.
    const existingArticleByCacheKey = getArticleFromCache(proposedCacheKey);

    // 3) Also check whether we already have an article with the same article.id in the cache.
    let existingArticleById: Article | undefined = undefined;
    if (upsertedArticle.id) {
        existingArticleById = getArticleById(upsertedArticle.id);
    }

    // 4) Decide which cache key to use for the merge. We favor whichever article is already in the cache.
    let finalCacheKey = proposedCacheKey;
    let finalExistingArticle = existingArticleByCacheKey;

    // If there's already an article with the same ID (but under a different cacheKey), then we should use that one:
    if (existingArticleById) {
        // We must figure out which key belongs to the existing articleById.
        // Because articleCache is a Map<cacheKey, Article>, we find the existing key whose value has the same .id.
        for (const [k, v] of articleCache) {
            if (v.id === existingArticleById.id) {
                finalCacheKey = k;
                finalExistingArticle = v;
                break;
            }
        }
    }

    // 5) If we did not find any existing matches by cacheKey or by id, then we have a truly new article.
    if (!finalExistingArticle) {
        // There's no existing article by this key or this id, so just upsert it under the proposed cacheKey.
        if (!upsertedArticle.id) {
            upsertedArticle.id = uuidv7();
        }
        articleCache.set(finalCacheKey, upsertedArticle);
        return upsertedArticle;
    }

    // 6) Merge our current (upserted) article with the existing one, and keep the existing entry’s cacheKey.
    const mergedArticle = {
        ...finalExistingArticle,
        ...upsertedArticle,
        pages: mergeArticlePages(finalExistingArticle, upsertedArticle)
    };

    articleCache.set(finalCacheKey, mergedArticle);
    return mergedArticle;
}

/**
 * Merges the .pages arrays from an existing article and a new one, 
 * making sure we do not duplicate pages.
 */
function mergeArticlePages(baseArticle: Article, newArticle: Article): Page[] {
    if (!newArticle.pages) {
        return baseArticle.pages || [];
    }
    // Create a Map for O(1) lookups keyed by pageCacheKey in the new article
    const newPagesMap = new Map(
        newArticle.pages.map(page => [generatePageCacheKey(page, newArticle), page])
    );

    // Filter out old pages that are being replaced, then combine with new pages in one pass
    const combined = [
        ...(baseArticle.pages || []).filter(
            p => !newPagesMap.has(generatePageCacheKey(p, baseArticle))
        ),
        ...newArticle.pages
    ];

    return combined;
}

function mergePages(existingPage: Page, newPage: Page): Page {
    const mergedPage: Page = {
        id: existingPage.id || newPage.id,
        type: existingPage.type || newPage.type,
        label: existingPage.label || newPage.label,
        link: existingPage.link || newPage.link,
        content: existingPage.content || newPage.content,
    };

    // Merge subelements if they exist
    if (existingPage.subelements && newPage.subelements) {
        const mergedSubelements = existingPage.subelements.map((existingSubElement, index) => {
            const newSubElement = newPage.subelements![index];
            if (newSubElement && existingSubElement) {
                return {
                    id: existingSubElement.id || newSubElement.id,
                    type: existingSubElement.type || newSubElement.type,
                    label: existingSubElement.label || newSubElement.label,
                    labelText: existingSubElement.labelText || newSubElement.labelText,
                    link: existingSubElement.link || newSubElement.link
                };
            }
            return existingSubElement;
        });

        if (mergedSubelements.length > 0) {
            mergedPage.subelements = mergedSubelements;
        }
    } else {
        const subelements = existingPage.subelements || newPage.subelements || [];
        if (subelements.length > 0) {
            mergedPage.subelements = subelements;
        }
    }

    return mergedPage;
}

export function getArticleById(articleId: string): Article | undefined {
    const article = Array.from(articleCache.values()).find(article => article.id === articleId);
    // if (article?.title === 'Rhopaloceren aus Holländisch-Neu-Guinea') {
    //     console.log("Searched by id: ", article.id);
    // }
    return article
}

export function getArticleByCitedAs(citedAs: string): Article | undefined {
    // First, try to find an exact match for the citedAs value
    let article = Array.from(articleCache.values()).find(article =>
        article.citedAs.some(cited => cited.replace('(', '').replace(')', '').trim() === citedAs.replace('(', '').replace(')', '').trim())
    );

    // If no exact match is found, handle the case where an article's citedAs array might contain
    // something like "Hreblay & Kobayashi in Hreblay & Ronkay, 1997", while the searched value
    // is "Hreblay & Kobayashi, 1997"
    if (!article) {
        const citedParts = citedAs.split(',');
        // We only proceed if we can reasonably parse out the "authors part" and the "year part"
        // e.g. "Hreblay & Kobayashi, 1997" -> ["Hreblay & Kobayashi", " 1997"]
        if (citedParts.length >= 2) {
            const authorsPart = citedParts[0]?.replace('(', '').replace(')', '').trim();
            const yearPart = citedParts[citedParts.length - 1]?.replace('(', '').replace(')', '').trim(); // last chunk, in case there's more than one comma
            const escapedAuthorsPart = escapeRegex(authorsPart);
            const escapedYearPart = escapeRegex(yearPart);
            // Build a pattern that checks for:
            //   "<authorsPart> in <anything>, <yearPart>"
            // Example: "Hreblay & Kobayashi in Hreblay & Ronkay, 1997"
            // when searching for "Hreblay & Kobayashi, 1997"
            const pattern = new RegExp(`^${escapedAuthorsPart} in .*,\\s*${escapedYearPart}$`, 'i');

            // Find if there's an article with any citedAs entry that matches the pattern
            article = Array.from(articleCache.values()).find(possibleArticle =>
                possibleArticle.citedAs.some(cited => pattern.test(cited.replace('(', '').replace(')', '').trim()))
            );
        } else {
            const citedParts2 = citedAs.split(' ');
            // something like "Hreblay in Hreblay & Ronkay, 1997", while the searched value
            // is "Hreblay 1997"
            // e.g. "Hreblay 1997" -> ["Hreblay", " 1997"]
            if (citedParts2.length >= 2) {
                const authorsPart = citedParts[0]?.replace('(', '').replace(')', '').trim();
                const yearPart = citedParts[citedParts.length - 1]?.replace('(', '').replace(')', '').trim(); // last chunk, in case there's more than one comma

                // Build a pattern that checks for:
                //   "<authorsPart> in <anything>, <yearPart>"
                // Example: "Hreblay & Kobayashi in Hreblay & Ronkay, 1997"
                // when searching for "Hreblay & Kobayashi, 1997"
                const pattern = new RegExp(`^${authorsPart} in .*,\\s*${yearPart}$`, 'i');

                // Find if there's an article with any citedAs entry that matches the pattern
                article = Array.from(articleCache.values()).find(possibleArticle =>
                    possibleArticle.citedAs.some(cited => pattern.test(cited.replace('(', '').replace(')', '').trim()))
                );
            }
        }
    }

    return article;
}

/**
 * Escape special regex characters in a string so it can be used safely
 * inside a RegExp constructor without causing syntax errors.
 */
function escapeRegex(str: string): string {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

export function getArticleByAbbreviation(abbreviation: string): Article | undefined {
    const article = Array.from(articleCache.values()).find(article =>
        article.abbreviation === abbreviation
    );
    return article
}

export function getArticleByFunetRefLink(funetRefLink: string, issueNumber?: string): Article | undefined {
    // we need issue number because funet frustratingly combines multiple issues into a single funetRefLink / bibliography entry
    // I think they never combine multiple volumes, so we don't need that included at the moment
    const article = Array.from(articleCache.values()).find(article =>
        article.funetRefLink === funetRefLink &&
        (!issueNumber || article?.issue?.label === issueNumber)
    );
    // if (article?.title === 'Rhopaloceren aus Holländisch-Neu-Guinea') {
    //     console.log("Searched by funet ref: ", article.id);
    // }
    return article
}

type PartialArticleInfo = {
    authors?: Authorship[];
    year?: string;
    journalTitle?: string;
    journalAbbreviation?: string;
    volume?: string;
    issue?: string;
}

export function findBestArticleMatch(info: PartialArticleInfo): Article | undefined {
    const candidates = Array.from(articleCache.values()).filter(article => {
        let matches = 0;
        let totalChecks = 0;

        // Check authors
        if (info.authors && info.authors.length > 0) {
            totalChecks++;
            const allAuthorsMatch = info.authors.every(searchAuthor => {
                return article.authors?.some(articleAuthor => {
                    // Match all provided fields for each author
                    let authorMatches = true;

                    // Match position if provided
                    if (searchAuthor.position !== undefined) {
                        authorMatches = authorMatches &&
                            articleAuthor.position === searchAuthor.position;
                    }

                    if (searchAuthor.author?.surname) {
                        authorMatches = authorMatches &&
                            articleAuthor.author?.surname?.toLowerCase() ===
                            searchAuthor.author.surname.toLowerCase();
                    }

                    if (searchAuthor.author?.givenName) {
                        authorMatches = authorMatches &&
                            articleAuthor.author?.givenName?.toLowerCase() ===
                            searchAuthor.author.givenName.toLowerCase();
                    }

                    return authorMatches;
                });
            });

            if (allAuthorsMatch) {
                matches++;
            }
        }

        // Check year
        if (info.year) {
            totalChecks++;
            if (article.issue?.volume?.year?.toString() === info.year) {
                matches++;
            }
        }

        // Check journal title
        if (info.journalTitle) {
            totalChecks++;
            if (article.issue?.volume?.journal?.title?.toLowerCase() ===
                info.journalTitle.toLowerCase()) {
                matches++;
            }
        }

        // Check journal abbreviation
        if (info.journalAbbreviation) {
            totalChecks++;
            if (article.issue?.volume?.journal?.abbreviation?.toLowerCase() ===
                info.journalAbbreviation.toLowerCase()) {
                matches++;
            }
        }

        // Check volume
        if (info.volume) {
            totalChecks++;
            if (article.issue?.volume?.label === info.volume) {
                matches++;
            }
        }

        // Check issue
        if (info.issue) {
            totalChecks++;
            if (article.issue?.label === info.issue) {
                matches++;
            }
        }

        // Return true if all provided fields match
        return totalChecks > 0 && matches === totalChecks;
    });

    // If we have exactly one match, return it
    if (candidates.length === 1) {
        return candidates[0];
    }

    // Return undefined if no match or ambiguous matches
    return undefined;
}

export function getAllArticlesFromCache(): Article[] {
    return Array.from(articleCache.values());
}

export function getAllArticlesCacheKeys(): string[] {
    return Array.from(articleCache.keys());
}

export function clearArticleCache(): void {
    articleCache.clear();
}

export function getAllArticleCacheKeys(): string[] {
    return Array.from(articleCache.keys());
}

export function getArticleCacheSize(): number {
    return articleCache.size;
}