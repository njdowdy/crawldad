import { Article, Page } from "../types"
import { extractAbbreviation, extractAuthorshipString, parseAuthorshipString } from "./authorshipUtilities"
import { extractPageParameters, generatePages } from "./pageUtilities"
import { composeArticleObject, composeIssueObject, findExternalLink, parseDOI, parseIssueNumber, parseJournalName, parseTitleString, parseVolumeNumber } from "./publicationUtilities"

export function parseRelatedLiterature(element: Element) {
    const articles: Article[] = []
    let blockDataText = element.textContent ?? ''
    if (blockDataText === '') {
        return
    }

    // ! FUNET SPECIAL CASE
    // there is a malformed reference on the page for genus Morrisonia
    // that leads to a pageEnd value of 6312009, which exceeds memory limits and breaks script
    // we will manually remove this erroneous text
    const textToReplace = "//archive.org/stream/journaloflepid6312009lepi#page/'.($nbr).'/mode/1up', "
    blockDataText = blockDataText.replace(textToReplace, '')

    // Handle "No." cases
    const numberMatch = blockDataText.match(/No\.\s*(\d+)-(\d+)/)
    if (numberMatch) {
        const issueStart = parseInt(numberMatch[1])
        const issueEnd = parseInt(numberMatch[2])
        blockDataText = blockDataText.replace(/No\.\s*\d+-\d+/, '')

        const colonCount = (blockDataText.match(/:/g) || []).length
        if (colonCount === issueEnd - issueStart + 1) {
            const issueNumbers = Array.from({ length: issueEnd - issueStart + 1 }, (_, i) => i + issueStart)
            let issueIndex = 0
            blockDataText = blockDataText.replace(/\n\s*:/g, (match) => {
                return ` \n(${issueNumbers[issueIndex++]}):`
            })
        }
    }

    // extract components
    // console.log("blockDataText:", blockDataText)
    const { authorshipString, citedAs } = extractAuthorshipString(blockDataText)
    const abbreviation = extractAbbreviation(blockDataText)
    const pageParams = extractPageParameters(blockDataText)

    // if (blockDataText.includes("Fruhstorfer, 1910")) {
    //     console.log("---------------------")
    //     console.log("pageParams: ", pageParams)
    //     console.log("---------------------")
    // }

    // parse extracted components
    const funetRefLink = element.id
    // if (blockDataText.includes('New Noctuidae (Lepidoptera) species from Taiwan and the adjacent areas')) {
    //     console.log("citedAs:", citedAs)
    //     console.log("blockDataText:", blockDataText)
    //     console.log("authorshipString:", authorshipString)
    // }

    // if (authorshipString === undefined) {
    //     console.log("TEST1:", authorshipString)
    // }
    const { year, bracketedYear, authorship } = parseAuthorshipString(authorshipString)
    const title = parseTitleString(blockDataText)
    let { journalAbbreviation, journalTitle } = parseJournalName(blockDataText, element)
    if (journalAbbreviation === '' && journalTitle === '') {
        journalTitle = title // assume it's a book
    }
    const volumeNumber = parseVolumeNumber(blockDataText, element)
    const cleanedVolumeNumber = volumeNumber?.replace('.-', '').replace('Internet archive page/plate links are for [1830] 2nd ed.', '')
    const articleExternalLink = findExternalLink(journalTitle || journalAbbreviation || '', element, false)[0]

    // if (blockDataText.includes('New Noctuidae (Lepidoptera) species from Taiwan and the adjacent areas')) {
    //     console.log("blockDataText:", blockDataText)
    //     console.log("authorship:", authorship)
    //     console.log("title:", title)
    //     console.log("journalAbbreviation:", journalAbbreviation)
    //     console.log("journalTitle:", journalTitle)
    //     // console.log("element:", element.textContent)
    // }
    const issueNumber = parseIssueNumber(blockDataText, journalTitle || journalAbbreviation || '', element)
    const issueExternalLink = findExternalLink(`(${issueNumber})`, element, true)[0]
    let doi: string | undefined
    if (issueExternalLink) {
        doi = parseDOI(issueExternalLink)
    }

    // Handle cases where pageParams is empty or has entries
    const processArticle = (page?: typeof pageParams[0]): Article | undefined => {
        let tempIssueNumber = issueNumber
        let tempVolumeNumber = cleanedVolumeNumber
        let tempIssueExternalLink = issueExternalLink
        let tempBracketedYear = bracketedYear
        let tempYear = year
        let tempArticleTitle = title
        let tempJournalTitle = journalTitle
        let tempJournalAbbreviation = journalAbbreviation
        let pages: Page[] | undefined = undefined
        let notes: string[] | undefined = undefined
        let useCache = true;

        if (page && page.alternativeJournalTitle) {

            if (page.alternativeJournalTitle.includes('.')) {
                tempJournalAbbreviation = page.alternativeJournalTitle;
                tempJournalTitle = ''; // Clear the journal title if we have an abbreviation
            } else {
                tempJournalTitle = page.alternativeJournalTitle;
                tempJournalAbbreviation = ''; // Clear the journal abbreviation if we have a title
            }

            // Update the article title with the alternative title
            useCache = false; // Disable cache when alternative journal title is present
        }

        if (page) {
            if (page.pageNotes?.length !== 0) {
                notes = page.pageNotes
            }
            if (page.pageRangeVolume !== undefined) {
                tempVolumeNumber = page.pageRangeVolume
            }
            if (page.pageRangeIssue !== undefined) {
                tempIssueNumber = page.pageRangeIssue
                tempIssueExternalLink = findExternalLink(`(${tempIssueNumber})`, element, true)[0]
            }
            if (page.pageRangeYear !== undefined) {
                tempYear = page.pageRangeYear
            }
            if (page.pageRangeYearBracketed !== undefined) {
                tempBracketedYear = page.pageRangeYearBracketed
            }
        }

        // compose Issue
        const issueToWrite = composeIssueObject(
            tempIssueNumber ?? '',
            tempVolumeNumber ?? '',
            tempJournalTitle,
            tempJournalAbbreviation,
            tempBracketedYear ?? undefined,
            tempYear ?? undefined,
            tempIssueExternalLink ?? undefined,
            useCache // New parameter
        )

        // fix article title in the case of "No. x-y"
        //? manually fix titles for, e.g.: "Synopsis of the Bombycidae of the United States. (1 & 2)"
        if (numberMatch) {
            tempArticleTitle = `${tempArticleTitle.trim()} No. ${tempIssueNumber}`
        }

        // adjust Issue/Volume details as needed if page is provided
        if (page) {
            pages = generatePages(
                page.pageRangeStart,
                page.pageRangeEnd,
                page.plateRangeStart,
                page.plateRangeEnd,
                element, {
                title: tempArticleTitle,
                citedAs: citedAs,
                issue: issueToWrite
            })

            // if (citedAs[0] === "Butler, 1871") {
            //     console.log("PAGES: ", pages)
            // }

        }

        // compose Article
        const articleToWrite = {
            citedAs,
            title: tempArticleTitle,
            issue: issueToWrite,
            tags: undefined,
            pages,
            authors: authorship,
            abbreviation,
            funetRefLink,
            externalLink: articleExternalLink,
            doi,
            notes
        };

        const cachedArticle = composeArticleObject(
            articleToWrite.citedAs,
            articleToWrite.title,
            articleToWrite.issue,
            articleToWrite.tags,
            articleToWrite.pages,
            articleToWrite.authors,
            articleToWrite.abbreviation,
            articleToWrite.funetRefLink,
            articleToWrite.externalLink,
            articleToWrite.doi,
            articleToWrite.notes
        );

        // if (citedAs[0] === "Butler, 1871") {
        //     console.log("CACHED ARTICLE:\n", cachedArticle)
        // }

        return cachedArticle;
    }



    // Process articles based on pageParams
    if (pageParams.length === 0) {
        const article = processArticle();
        if (article) {
            articles.push(article);
        }
    } else {
        pageParams.forEach(page => {
            const article = processArticle(page);
            if (article) {
                articles.push(article);
            }
        });
    }

    // if (blockDataText.includes('NACL93d')) {
    //     // console.log("result:", JSON.stringify(articles, null, 2))
    // }
    // if (citedAs[0] === "Butler, 1871") {
    //     console.log(`PROCESSED ${pageParams.length} ISSUES IN BIBLIOGRAPHIC ENTRY`)
    // }

    return articles
}


// CHECKLIST

// type Article = {
//     id?: string,
//     ✅ title?: string,
//     ✅ issue?: Issue,
//     ✅ citedAs: string[],
//     ✅ tags?: Tag[],
//     pages?: Page[],
//     ✅ authors?: Authorship[],
//     ✅ abbreviation?: string,
//     ✅ funetRefLink?: string, // ! NOTE THESE ARE NOT UNIQUE TO ARTICLES, ONLY BIBLIOGRAPHY LINE ITEMS (multiple articles)
//     externalLink?: string,
//     ✅ doi?: string,
//     notes?: string[],
// }

// type Authorship = {
//     ✅ position: number,
//     ✅ author: Person,
//     ✅ creditCategories?: { role: CreditCategories, degree?: CreditDegree }[],
// }

// type Person = {
//     ✅ id?: string,
//     ✅ givenName?: string,
//     ✅ surname?: string,
//     ✅ easternNameOrder: boolean,
// }


// type Journal = {
//     ✅ id?: string,
//     ✅ title: string,
//     ✅ abbreviation: string,
// }

// type Volume = {
//     ✅ id?: string,
//     ✅ label: string, // e.g., volume number
//     year?: number,
//     bracketedYear?: boolean,
//     ✅ journal: Journal,
// }

// type Issue = {
//     ✅ id?: string,
//     ✅ label: string, // e.g., issue number
//     ✅ volume: Volume,
//     ✅ externalLink?: string,
// }
