import { uuidv7 } from "uuidv7";
import { Person } from "../types";

const personCache: Map<string, Person> = new Map();

export function getPersonFromCache(cacheKey: string): Person | undefined {
    const normalizedKey = cacheKey.toLowerCase().trim();
    return personCache.get(normalizedKey);
}

export function addPersonToCache(cacheKey: string, person: Person): void {
    if (!person.id) return;

    const normalizedKey = cacheKey.toLowerCase().trim();

    // 1. Check if there's already a Person in the cache with the same UUID.
    for (const [existingKey, existingPerson] of personCache.entries()) {
        if (existingPerson.id === person.id) {
            // If the name-based key hasn't changed, just merge.
            if (existingKey === normalizedKey) {
                Object.assign(existingPerson, person);
            } else {
                // Otherwise, remove it from the old key, merge, then insert under the new key.
                Object.assign(existingPerson, person);
                personCache.delete(existingKey);
                personCache.set(normalizedKey, existingPerson);
            }
            return;
        }
    }

    // 2. If no matching ID, check if someone else in the cache has the same name-based key.
    const existingNameBasedPerson = personCache.get(normalizedKey);
    if (existingNameBasedPerson) {
        Object.assign(existingNameBasedPerson, person);
        return;
    }

    // 3. If no conflict, simply insert.
    personCache.set(normalizedKey, person);
}

export function clearPersonCache(): void {
    personCache.clear();
}

export function getPersonCacheSize(): number {
    return personCache.size;
}

export function upsertPersonToCache(person: Person): Person {
    const cacheKey = [
        person.givenName || '',
        (person.middleNames || []).join(' '),
        person.surname || '',
        (person.suffix || []).join(', ')
    ].join('|').toLowerCase().trim();

    const existingPerson = getPersonFromCache(cacheKey);

    if (existingPerson) {
        Object.assign(existingPerson, person);
        return existingPerson;
    }

    const personToCache: Person = {
        ...person,
        id: person.id || uuidv7()
    };

    addPersonToCache(cacheKey, personToCache);
    return personToCache;
}

export function getPersonByName(name: string): Person | undefined {
    const normalizedKey = name.toLowerCase().trim();
    return getPersonFromCache(normalizedKey);
}

export function getAllPersonsFromCache(): Person[] {
    return Array.from(personCache.values());
}

export function getAllPersonCacheKeys(): string[] {
    return Array.from(personCache.keys());
}