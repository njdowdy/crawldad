import { uuidv7 } from "uuidv7";
import { HigherLevelTaxon } from "../types";
import { addHigherTaxonToCache, getHigherTaxonFromCache } from "./higherTaxonCacheManager";

export function parseHigherLevelTaxa(url: string): { higherLevelTaxa: HigherLevelTaxon[], finalParentId: string } {
    const higherLevelTaxa: HigherLevelTaxon[] = []
    const urlParts = url.split('_')
    const lepidopteraIndex = urlParts.indexOf('lepidoptera');
    const cleanedUrlParts = urlParts.filter(part => part.trim() !== '');
    const taxa = cleanedUrlParts.slice(lepidopteraIndex, -1)
    let parentId: string | null = null

    taxa.forEach(taxon => {
        // Check if taxon already exists in cache
        const existingTaxon = getHigherTaxonFromCache(taxon);

        if (existingTaxon) {
            // Use existing taxon's ID as parent for next iteration
            parentId = existingTaxon.id;
        } else {
            // Create new taxon
            const uuid = uuidv7()
            const parsedTaxon = {
                id: uuid,
                rank: determineRank(taxon),
                name: taxon,
                parentId: parentId
            }
            higherLevelTaxa.push(parsedTaxon)
            addHigherTaxonToCache(parsedTaxon)
            parentId = uuid
        }
    })
    return { higherLevelTaxa, finalParentId: parentId ?? '' };
}

function determineRank(taxon: string): string {
    const lowerTaxon = taxon.toLowerCase();

    // Check specific cases first
    if (lowerTaxon.endsWith('lepidoptera')) {
        return 'order';
    }
    if (lowerTaxon.endsWith('sia')) {
        return 'suborder';
    }

    // Check suffixes
    if (lowerTaxon.endsWith('oidea')) {
        return 'superfamily';
    }
    if (lowerTaxon.endsWith('idae')) {
        return 'family';
    }
    if (lowerTaxon.endsWith('inae')) {
        return 'subfamily';
    }
    if (lowerTaxon.endsWith('ini')) {
        return 'tribe';
    }
    if (lowerTaxon.endsWith('ina')) {
        return 'subtribe';
    }

    // Default case
    return 'clade';
}