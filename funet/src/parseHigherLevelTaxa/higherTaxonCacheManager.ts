import { HigherLevelTaxon } from "../types";

const taxonCache: Map<string, HigherLevelTaxon> = new Map();

export function getHigherTaxonFromCache(name: string): HigherLevelTaxon | undefined {
    return taxonCache.get(name.toLowerCase());
}

export function addHigherTaxonToCache(taxon: HigherLevelTaxon): void {
    if (!taxon.id) return;
    taxonCache.set(taxon.name.toLowerCase(), taxon);
}

export function clearHigherTaxonCache(): void {
    taxonCache.clear();
}

export function getAllHigherTaxaFromCache(): HigherLevelTaxon[] {
    return Array.from(taxonCache.values());
}

export function getHigherTaxaCacheSize(): number {
    return taxonCache.size;
}
