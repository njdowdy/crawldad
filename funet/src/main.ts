import { getDocument, getMainNavigationElement, parseBlocks, getLocalFileHTML } from './parseBlocks';
// import path from 'path';
import { clearPersonCache, getAllPersonsFromCache, getPersonCacheSize, upsertPersonToCache } from "./parseRelatedLiterature/personCacheManager";
import { clearJournalCache, getAllJournalsFromCache, getJournalCacheSize } from "./parseRelatedLiterature/journalCacheManager";
import { clearVolumeCache, getAllVolumesFromCache, getVolumeCacheSize } from "./parseRelatedLiterature/volumeCacheManager";
import { clearIssueCache, getAllIssuesFromCache, getIssueCacheSize } from "./parseRelatedLiterature/issueCacheManager";
import { clearPageCache, getAllPagesFromCache, getPageCacheSize } from "./parseRelatedLiterature/pageCacheManager";
import { clearArticleCache, getAllArticlesFromCache, getArticleByCitedAs, getArticleCacheSize, upsertArticle } from "./parseRelatedLiterature/articleCacheManager";
import { BrowserManager } from './browserManager';
import { injectGenusParent, parseTaxa } from './parseNames/parse';
import { resolveSynonymReferences, resolveTypeTaxonIds } from './parseNameBlock';
import { parseMentions } from './parseMentions/parse';
import { clearWebResourceCache, getAllWebResourcesFromCache, getTotalWebSubelementCount, getWebResourceCacheSize } from './parseWebResource/webResourceCacheManager';
import { injectResolveUnresolvedNames, resolveUnresolvedNames } from './parseUnresolvedNames/parse';
import { parseInteractions } from './parseHosts/parse';
import { parseMisapplications } from './parseMisapplications/parse';
import { parseHigherLevelTaxa } from './parseHigherLevelTaxa/parse';
import { addHigherTaxonToCache, clearHigherTaxonCache, getAllHigherTaxaFromCache, getHigherTaxaCacheSize } from './parseHigherLevelTaxa/higherTaxonCacheManager';
import { addTaxonToCache, clearTaxonCache, getAllTaxonFromCache, getTaxonCacheSize } from './parseNames/taxonCacheManager';
import {
    clearHostCache,
    getAllHostsFromCache,
    getHostCacheSize
} from "./parseHosts/hostCacheManager";
import { createWriteStream } from 'fs';
import { clearDistributionCache, getAllDistributionsFromCache, getDistributionCacheSize } from './parseDistributions/assertedDistributionCacheManager';
import { clearLocalityCache, getAllLocalitiesFromCache, getLocalityCacheSize } from './parseDistributions/typeLocalityCacheManager';
import { loadCacheData } from './fileUtilities';
import { readFile, writeFile } from 'fs/promises';
import { Article, TaxonRecord } from './types';
import { parseAuthorshipString } from './parseRelatedLiterature/authorshipUtilities';
import { uuidv7 } from 'uuidv7';

async function writeResultsToFile(data: any[], filename: string, append: boolean = false): Promise<void> {
    if (data.length === 0) return;

    // If not appending, just write directly using streams
    if (!append) {
        const stream = createWriteStream(filename);
        stream.write('[\n');

        for (let i = 0; i < data.length; i++) {
            const line = JSON.stringify(data[i], null, 2);
            stream.write(line);
            if (i < data.length - 1) {
                stream.write(',\n');
            }
        }

        stream.write('\n]');
        stream.end();
        return;
    }

    // For append mode, we need to handle existing data
    try {
        const stream = createWriteStream(filename);
        stream.write('[\n');

        // Read existing file if it exists
        let existingData: any[] = [];
        try {
            const fileContent = await readFile(filename, 'utf8');
            existingData = JSON.parse(fileContent);
        } catch (error) {
            // File doesn't exist or is invalid, continue with empty existing data
        }

        // Merge arrays while handling potential duplicates
        const mergedData = [...existingData];
        for (const newItem of data) {
            const existingIndex = mergedData.findIndex(item =>
                item.id === newItem.id ||
                item.url === newItem.url ||
                (item.name === newItem.name && item.type === newItem.type)
            );

            if (existingIndex === -1) {
                mergedData.push(newItem);
            } else {
                mergedData[existingIndex] = {
                    ...mergedData[existingIndex],
                    ...newItem
                };
            }
        }

        // Write merged data using streams
        for (let i = 0; i < mergedData.length; i++) {
            const line = JSON.stringify(mergedData[i], null, 2);
            stream.write(line);
            if (i < mergedData.length - 1) {
                stream.write(',\n');
            }
        }

        stream.write('\n]');
        stream.end();
    } catch (error) {
        console.error(`Error writing to ${filename}:`, error);
        throw error;
    }
}

/**
 * Removes any <a> elements whose text is comprised solely of one or more Unicode superscript characters
 * (e.g. <a href="#foo">²</a>).
 */
export async function removeSuperscriptLinks(document: Document): Promise<void> {
    // This pattern covers ², ³, ¹, and the general Superscripts and Subscripts block (U+2070–U+209F)
    const superscriptPattern = /^[\u00B2\u00B3\u00B9\u2070-\u209F]+$/;

    document.querySelectorAll('a').forEach(a => {
        const linkText = (a.textContent || '').trim();
        if (superscriptPattern.test(linkText)) {
            a.remove();
        }
    });
}

async function removeEmptyElements(document: Document): Promise<void> {
    // Target li elements in the same locations as our other functions
    const allLiElements = document.querySelectorAll(`
        ul.SP > li > div.TN > div.NAMES > ul.SN > li,
        ul.SSP > li > div.TN > div.NAMES > ul.SN > li
    `);

    allLiElements.forEach((li) => {
        // Get all text content, including nested elements
        const text = (li.textContent || "").trim();

        // If there's no text content, remove the li element
        if (!text) {
            li.remove();
        }
    });
}

async function fixSubspeciesHTML(document: Document, genusName: string): Promise<void> {
    // Add highlighting to injected elements
    const styleSheet = document.createElement("style");
    styleSheet.textContent = `
        .injected-element {
            background-color: #ffeb3b !important; /* Light yellow */
            padding: 2px 4px !important;
            border-radius: 2px !important;
        }
        .injected-element-0 {
            background-color: #ff4242 !important; /* Light red */
            padding: 2px 4px !important;
            border-radius: 2px !important;
        }
    `;
    document.head.appendChild(styleSheet);

    // ----------------------------------
    // Step 0) If the parent "ul.SP > li > div.TN" has no "div.NAMES", inject one.
    //         This ensures the parent species gets a NAMES/SN block too.
    // ----------------------------------
    document.querySelectorAll('ul.SP > li > div.TN').forEach((tnDiv) => {
        // If already has a div.NAMES, skip
        if (tnDiv.querySelector('div.NAMES')) return;

        const parentBlockId = tnDiv.id;
        // If no ID or the ID includes '_', we skip,
        // because '_' generally denotes a subspecies block.
        if (!parentBlockId || parentBlockId.includes('_')) return;

        // Inject a local name block just like we do for subspecies
        injectLocalNameIfNoNAMES(tnDiv, genusName, parentBlockId, document);
    });

    // Helper to detect if an element has a particular "needed class or ID"
    const hasNeededMarker = (el: Element | null, neededClassOrId: string): boolean => {
        if (!el) return false;
        return el.id === neededClassOrId || el.classList.contains(neededClassOrId);
    };

    // 1) If the first <li> in each ul.SSP does not have a "species_species" block,
    //    inject that block so "themis_themis" (for example) is always present at the top of the ul.
    document.querySelectorAll('ul.SP > li > ul.SSP').forEach((sspUl) => {
        const firstLi = sspUl.querySelector('li');
        if (!firstLi) return;

        const firstLiDivTN = firstLi.querySelector('div.TN');
        if (!firstLiDivTN) return;

        let firstLiId = firstLiDivTN.id || '';
        if (!firstLiId.includes('_')) return;

        const speciesName = firstLiId.split('_')[0];
        if (!speciesName) return;

        const neededClassOrId = `${speciesName}_${speciesName}`;

        // Inject a missing nominative block at the top if needed
        if (!hasNeededMarker(firstLiDivTN, neededClassOrId)) {
            const newLi = document.createElement('li');

            const newDivTN = document.createElement('div');
            newDivTN.className = `TN ${neededClassOrId} injected-element-0`;
            newDivTN.id = neededClassOrId;

            const newSpanTN = document.createElement('span');
            newSpanTN.className = 'TN';
            const newI = document.createElement('i');
            newI.textContent = `${genusName} ${speciesName} ${speciesName}`;

            newSpanTN.appendChild(newI);
            newSpanTN.appendChild(document.createTextNode('\n'));
            newDivTN.appendChild(newSpanTN);
            newLi.appendChild(newDivTN);

            // Insert at the top of the ul
            sspUl.insertBefore(newLi, sspUl.firstChild);
        }
    });

    // 2) For each ul.SSP > li:
    //    (A) If it's the nominative subspecies (e.g. X_X), clone parent's first SN <li> if available,
    //        then convert its binomial form into a trinomial by duplicating the species epithet.
    //    (B) If it's non-nominative, only inject if there's no existing div.NAMES in that li.
    const processedBlockIds = new Set<string>();
    const sspLis = document.querySelectorAll('ul.SP > li > ul.SSP > li');

    sspLis.forEach((sspLi) => {
        // Grab (or create) the top-level div.TN
        let finalTN = sspLi.querySelector('div.TN');
        if (!finalTN) {
            finalTN = document.createElement('div');
            finalTN.className = 'TN';
            sspLi.appendChild(finalTN);
        }

        const blockId = finalTN.id || '';
        if (!blockId.includes('_')) return; // skip if no "X_Y" ID pattern
        if (processedBlockIds.has(blockId)) {
            // Already processed
            return;
        }
        processedBlockIds.add(blockId);

        const [speciesPart, subspeciesPart] = blockId.split('_');
        const isNominative = speciesPart === subspeciesPart;

        // (A) Nominative logic: attempt to clone parent's first SN item if it exists
        if (isNominative) {
            const parentSpeciesLi = sspLi.closest('ul.SP > li');
            let clonedParent = false;
            if (parentSpeciesLi) {
                const parentFirstSNLi = parentSpeciesLi.querySelector<HTMLElement>(
                    'div.TN > div.NAMES > ul.SN > li:first-child'
                );
                if (parentFirstSNLi) {
                    // Set up .NAMES and .SN
                    let divNAMES = finalTN.querySelector('div.NAMES');
                    if (!divNAMES) {
                        divNAMES = document.createElement('div');
                        divNAMES.className = 'NAMES';
                        // If there's a div.MENTIONS inside finalTN, insert the new div.NAMES above it:
                        const mentionsDiv = finalTN.querySelector('div.MENTIONS');
                        if (mentionsDiv) {
                            finalTN.insertBefore(divNAMES, mentionsDiv);
                        } else {
                            finalTN.appendChild(divNAMES);
                        }
                    }
                    let ulSN = divNAMES.querySelector('ul.SN');
                    if (!ulSN) {
                        ulSN = document.createElement('ul');
                        ulSN.className = 'SN';
                        divNAMES.appendChild(ulSN);
                    }

                    const clonedItem = parentFirstSNLi.cloneNode(true) as HTMLElement;
                    clonedItem.classList.add('injected-element');
                    ulSN.insertBefore(clonedItem, ulSN.firstChild);
                    clonedParent = true;

                    // Convert the text in the cloned <i> from binomial to trinomial
                    // by repeating the species epithet if it's present.
                    const clonedI = clonedItem.querySelector('i');
                    if (clonedI) {
                        const text = (clonedI.textContent || '').trim();
                        const tokens = text.split(/\s+/);
                        if (tokens.length >= 2) {
                            // Typically [Genus, species, ...rest]
                            // Insert the species part again (2nd token) to make it a nominative subspecies
                            let species = tokens[1];
                            // Double-check it matches speciesPart to avoid duplicating the wrong epithet
                            if (species.toLowerCase() === speciesPart.toLowerCase()) {
                                tokens.splice(2, 0, species);
                                clonedI.textContent = tokens.join(' ');
                            }
                        }
                    }
                }
            }
            // If we didn't clone a parent's item, fallback to local <span.TN> injection
            if (!clonedParent) {
                injectLocalNameIfNoNAMES(finalTN, genusName, blockId, document);
            }
        } else {
            // (B) Non-nominative logic: only inject if there's no existing div.NAMES
            const existingDivNames = finalTN.querySelector('div.NAMES');
            if (!existingDivNames) {
                injectLocalNameIfNoNAMES(finalTN, genusName, blockId, document);
            }
        }
    });

    // 3) Last-ditch fallback: if there is a div.TN but still no div.NAMES or <li> in <ul.SN>,
    //    copy local <span.TN><i>...</i></span> inside that sub-species block and insert at the top.
    document.querySelectorAll('ul.SP > li > ul.SSP > li > div.TN').forEach((tnDiv) => {
        let divNAMES = tnDiv.querySelector('div.NAMES');
        if (!divNAMES) {
            divNAMES = document.createElement('div');
            divNAMES.className = 'NAMES';
            // If there's a div.MENTIONS inside finalTN, insert the new div.NAMES above it:
            const mentionsDiv = tnDiv.querySelector('div.MENTIONS');
            if (mentionsDiv) {
                tnDiv.insertBefore(divNAMES, mentionsDiv);
            } else {
                tnDiv.appendChild(divNAMES);
            }
        }
        let ulSN = divNAMES.querySelector('ul.SN');
        if (!ulSN) {
            ulSN = document.createElement('ul');
            ulSN.className = 'SN';
            divNAMES.appendChild(ulSN);
        }

        // If <li> is still missing, do a final injection at the top
        if (!ulSN.querySelector('li')) {
            const spanTN = tnDiv.querySelector('span.TN');
            if (spanTN) {
                const iTag = spanTN.querySelector('i');
                if (iTag) {
                    const fallbackLi = document.createElement('li');
                    fallbackLi.classList.add('injected-element');

                    const newI = document.createElement('i');
                    newI.textContent = iTag.textContent || '';
                    fallbackLi.appendChild(newI);

                    // Copy trailing text if any
                    const nextSibling = iTag.nextSibling;
                    if (nextSibling && nextSibling.nodeType === 3) {
                        fallbackLi.appendChild(document.createTextNode(nextSibling.textContent || ''));
                    }

                    // Insert at the top of the SN list
                    ulSN.insertBefore(fallbackLi, ulSN.firstChild);
                }
            }
        }
    });
}

/**
 * Injects a new SN <li> for local <span class="TN"> if there's no forced content yet.
 * This is used when:
 * (1) Nominative subspecies fails to find parent's SN, or
 * (2) Non-nominative has no existing div.NAMES to skip.
 */
function injectLocalNameIfNoNAMES(finalTN: Element, genusName: string, blockId: string, doc: Document) {
    // Ensure a NAMES and SN structure
    let divNAMES = finalTN.querySelector('div.NAMES');
    if (!divNAMES) {
        divNAMES = doc.createElement('div');
        divNAMES.className = 'NAMES';

        // If there's a div.MENTIONS inside finalTN, insert the new div.NAMES above it:
        const mentionsDiv = finalTN.querySelector('div.MENTIONS');
        if (mentionsDiv) {
            finalTN.insertBefore(divNAMES, mentionsDiv);
        } else {
            finalTN.appendChild(divNAMES);
        }
    }

    let ulSN = divNAMES.querySelector('ul.SN');
    if (!ulSN) {
        ulSN = doc.createElement('ul');
        ulSN.className = 'SN';
        divNAMES.appendChild(ulSN);
    }

    // Grab local <span.TN>… <i>…</i>
    const localSpanTN = finalTN.querySelector('span.TN');
    if (!localSpanTN) return;

    const localI = localSpanTN.querySelector('i');
    if (!localI) return;

    // This is the entire text inside the <i> tag (e.g. "Delias g. filarorum Nihira & Kawamura, 1987")
    const localIText = (localI.textContent || '').trim();

    // Also capture any trailing text after </i> if present
    let trailingText = '';
    const nextSib = localI.nextSibling;
    if (nextSib && nextSib.nodeType === 3) {
        trailingText = (nextSib.textContent || '').trim();
    }

    // If our blockId has an underscore, the first part (splitted[0]) is the species epithet
    // e.g. blockId = "ganymedes_filarorum" => splitted[0] = "ganymedes"
    let newSpeciesEpithet = '';
    if (blockId && blockId.includes('_')) {
        const splitted = blockId.split('_');
        newSpeciesEpithet = splitted[0]; // "ganymedes"
    }

    // We want to transform something like:
    //   localIText = "Delias g. filarorum Nihira & Kawamura, 1987"
    // into:
    //   "Delias ganymedes filarorum Nihira & Kawamura, 1987"
    // by removing the second token ("g.") from localIText
    // and inserting the newSpeciesEpithet.

    let finalNameWithAuth = localIText; // fallback
    if (newSpeciesEpithet) {
        const tokens = localIText.trim().split(/\s+/);
        // Typically tokens[0] = "Delias", tokens[1] = "g.", tokens[2..] = remainder
        // We'll rebuild it as: genusName + " " + newSpeciesEpithet + " " + (tokens.slice(2).join(" "))
        if (tokens.length >= 2) {
            const tail = tokens.slice(2).join(' ');
            // e.g. "filarorum Nihira & Kawamura, 1987"
            finalNameWithAuth = `${genusName} ${newSpeciesEpithet}${tail ? ' ' + tail : ''
                }`;
        }
    }

    // Create a new <li> element and inject it at the top of the SN list
    const clonedItem = doc.createElement('li');
    clonedItem.classList.add('injected-element');

    const newI = doc.createElement('i');
    newI.textContent = finalNameWithAuth;
    clonedItem.appendChild(newI);

    if (trailingText) {
        clonedItem.appendChild(doc.createTextNode(' ' + trailingText));
    }

    // Insert at the top of the SN list
    ulSN.insertBefore(clonedItem, ulSN.firstChild);
}

async function processFile(url: string, nextFile: string | null, timestamp: string): Promise<void> {
    const startTime = Date.now();
    const timings = {
        htmlRetrieval: 0,
        htmlProcessing: 0,
        total: 0
    };

    // track added data counts
    const initialPersonCacheSize = getPersonCacheSize();
    const initialJournalCacheSize = getJournalCacheSize();
    const initialVolumeCacheSize = getVolumeCacheSize();
    const initialIssueCacheSize = getIssueCacheSize();
    const initialPageCacheSize = getPageCacheSize();
    const initialArticleCacheSize = getArticleCacheSize();
    const initialWebResourceCacheSize = getWebResourceCacheSize();
    const initialTotalWebSubelementCount = getTotalWebSubelementCount();
    const initialHigherLevelTaxaCacheSize = getHigherTaxaCacheSize();
    const initialHostCacheSize = getHostCacheSize();

    // Measure HTML retrieval time
    const htmlRetrievalStart = Date.now();
    const htmlString = await getLocalFileHTML(url);
    timings.htmlRetrieval = Date.now() - htmlRetrievalStart;

    if (!htmlString) {
        throw new Error('Failed to fetch HTML from file');
    }

    // Measure HTML processing time
    const htmlProcessingStart = Date.now();
    const document = await getDocument(htmlString);

    // Parse Higher Level taxonomy
    const { higherLevelTaxa, finalParentId: genusParentId } = parseHigherLevelTaxa(url);
    higherLevelTaxa.map(x => addHigherTaxonToCache(x));

    // Get the genus name from div.PH > div
    const genusName = document.querySelector('div.PH > div.TN > span.TN > i')?.textContent;

    // Write initial HTML before injection
    const debugFileName = url.split('/').pop()?.replace('.html', '') || 'unknown';
    // await writeFile(
    //     `./data/debugging/${debugFileName}_preInjection.html`,
    //     document.documentElement.outerHTML
    // );

    await removeFiCheck(document)

    await fixIncompleteMaps(document)

    await fixMalformedHTMLContent(document)

    await fixMalformedTL(document)

    await removeAbbrElements(document)

    await removeDoubleQuestionMarks(document)

    await removeMentionItalics(document)

    await fixRomanNumerals(document)

    await removeEmptyElements(document)

    await removeSuperscriptLinks(document)

    genusName && await fixSubspeciesHTML(document, genusName)

    genusName && await expandNameAbbreviations(document, genusName)

    genusName && await fixMissingGenusNames(document, genusName)

    await injectMissingSNs(document)

    // Write final HTML after injection
    // await writeFile(
    //     `./data/debugging/${debugFileName}_postInjection.html`,
    //     document.documentElement.outerHTML
    // );

    const [parsedBlocks, mainNavigation] = await Promise.all([
        Promise.resolve(parseBlocks(document)),
        Promise.resolve(getMainNavigationElement(document))
    ]);
    timings.htmlProcessing = Date.now() - htmlProcessingStart;
    timings.total = Date.now() - startTime;

    // Prepare results object
    const results = {
        parsedBlocks: parsedBlocks.map(block => ({
            blockName: block.blockName,
            blockType: block.blockType,
            parentBlock: block.parentBlock,
            blockData: block.blockData,
            blockDataText: block.blockData.textContent,
            parsedBlockData: block.parsedBlockData || null
        })),
        mainNavigation: mainNavigation ? {
            tagName: mainNavigation.tagName,
            className: mainNavigation.className,
            id: mainNavigation.id,
        } : null,
    };

    // Taxonomy logic
    const taxa = parseTaxa(results.parsedBlocks);
    const taxaWithGenusParent = injectGenusParent(taxa, genusParentId);
    const taxaWithInjectedMentions = parseMentions(results.parsedBlocks, taxaWithGenusParent);
    const taxaWithInjectedMisapplications = parseMisapplications(results.parsedBlocks, taxaWithInjectedMentions);
    const taxWithInjectedHosts = parseInteractions(results.parsedBlocks, taxaWithInjectedMisapplications);
    const taxaWithResolvedSynonyms = resolveSynonymReferences(taxWithInjectedHosts, genusName || '');
    const unresolvedNamesWithResolvedSources = resolveUnresolvedNames(results.parsedBlocks);
    const taxaWithInjectedUnresolvedNames = injectResolveUnresolvedNames(unresolvedNamesWithResolvedSources, taxaWithResolvedSynonyms);
    const taxaWithTSResolved = resolveTypeTaxonIds(taxaWithInjectedUnresolvedNames);

    //TODO after the last iteration, we should try to do a broader reconciliation for fields that might reference other fields, e.g., originalGenus so we fill in the id value rather than the string value

    // Now we resolve authorship references as well
    // resolveScientificNameAuthorships(taxaWithTSResolved);

    // const taxaWithANUResolved = resolveAcceptedNameUsageIds(taxaWithTSResolved)

    // Write to file (this turned out to be very slow to do on every iteration)
    // await writeResultsToFile(results.parsedBlocks, './data/results/blocks.json');
    // await writeResultsToFile(taxaWithTSResolved, './data/results/taxa.json');
    // await appendToJsonArray('./data/results/taxa.json', taxaWithTSResolved);

    // NEW CODE: Store each resolved Taxon in an in-memory cache
    for (const taxon of taxaWithTSResolved) {
        // if (taxon.acceptedNameUsage === 'Xestia rhaetica (Staudinger, 1871)' && taxon.taxonRank === 'species') {
        //     console.log("ADDING Xestia rhaetica (Staudinger, 1871) TO CACHE!")
        // }
        addTaxonToCache(taxon);
        // if (taxon.acceptedNameUsage === 'Xestia rhaetica (Staudinger, 1871)' && taxon.taxonRank === 'species') {
        //     const taxonCachedata = getAllTaxonFromCache();
        //     taxonCachedata.forEach(t => {
        //         if (t.acceptedNameUsage === 'Xestia rhaetica (Staudinger, 1871)' && t.taxonRank === 'species') {
        //             console.log("Xestia rhaetica (Staudinger, 1871) PRESENT IN CACHE!")
        //         }
        //     })
        // }
    }

    // const duration = Date.now() - startTime;
    // console.log(`Parsed ${taxaWithTSResolved.length} names (Total: ${getTaxonCacheSize()})`);
    // console.log(`Added ${getHigherTaxaCacheSize() - initialHigherLevelTaxaCacheSize} higher level taxa (Total: ${getHigherTaxaCacheSize()})`);
    // console.log(`Added ${getWebResourceCacheSize() - initialWebResourceCacheSize} web resources (Total: ${getWebResourceCacheSize()})`);
    // console.log(`Added ${getTotalWebSubelementCount() - initialTotalWebSubelementCount} web resource subelements (Total: ${getTotalWebSubelementCount()})`);
    // console.log(`Added ${getPersonCacheSize() - initialPersonCacheSize} people (Total: ${getPersonCacheSize()})`);
    // console.log(`Added ${getJournalCacheSize() - initialJournalCacheSize} journals (Total: ${getJournalCacheSize()})`);
    // console.log(`Added ${getVolumeCacheSize() - initialVolumeCacheSize} volumes (Total: ${getVolumeCacheSize()})`);
    // console.log(`Added ${getIssueCacheSize() - initialIssueCacheSize} issues (Total: ${getIssueCacheSize()})`);
    // console.log(`Added ${getArticleCacheSize() - initialArticleCacheSize} articles (Total: ${getArticleCacheSize()})`);
    // console.log(`Added ${getPageCacheSize() - initialPageCacheSize} pages (Total: ${getPageCacheSize()})`);
    // console.log(`Added ${getHostCacheSize() - initialHostCacheSize} hosts (Total: ${getHostCacheSize()})`);
    // console.log(`✅ Completed processing in ${duration}ms`);
}

type HtmlIndex = {
    index: number,
    url: string,
    file: string,
    nextUrl: string,
    lastUpdated: string,
    scrapeDate: string
}

async function main(startingIndex?: number, maxFilesToProcess?: number): Promise<void> {
    console.time('Total processing time');

    // Add skip list
    const skipList = [
        'lepidoptera_ditrysia_geometroidea_geometridae_larentiinae_archirhoe.html',  // missing genus TN entry; type species unknown, reference missing
        // Add more problematic files here
    ];

    // Load existing cache data before we begin
    await loadCacheData();

    // Load the index of files from ./data/html/index.json
    const indexFileContent = await readFile('./data/html/index.json', 'utf8');
    const indexData: HtmlIndex[] = JSON.parse(indexFileContent);
    if (!startingIndex) {
        startingIndex = Math.min(...indexData.map(entry => entry.index));
    }
    const maxIndex = Math.max(...indexData.map(entry => entry.index));
    const minIndex = Math.min(...indexData.map(entry => entry.index));
    const maxPossibleFilesToProcess = maxIndex - startingIndex + 1;
    if (!maxFilesToProcess || maxFilesToProcess > maxPossibleFilesToProcess) {
        maxFilesToProcess = maxPossibleFilesToProcess;
    }

    // User-defined starting index
    let currentIndex = startingIndex;
    let filesProcessed = 0;

    const timestamp = Date.now().toString();

    try {
        while (true) {
            // Check if we've hit the maximum files to process
            if (filesProcessed >= maxFilesToProcess) {
                console.log(`\nReached maximum files to process (${maxFilesToProcess}), stopping...`);
                break;
            }

            // Find the entry with the current index
            const dataEntry = indexData.find((entry: HtmlIndex) => entry.index === currentIndex);
            if (!dataEntry) {
                console.log(`No data entry found for index ${currentIndex}, stopping...`);
                break;
            }

            // Extract just the filename part after the last slash and numbers
            const fileName = dataEntry.file.split('/').pop()?.replace(/^\d+_/, '');
            if (fileName && skipList.includes(fileName)) {
                console.log(`\n⏭️  Skipping ${fileName} (in skip list)`);
                currentIndex++;
                continue;
            }

            console.log(`\n🚀 Index: ${currentIndex} (${filesProcessed + 1}${maxFilesToProcess ? `/${maxFilesToProcess} -> ${((filesProcessed + 1) / maxFilesToProcess * 100).toFixed(2)}%` : ''})\nFile: ${dataEntry.file}`);

            // Compute the next file path (if it exists)
            const nextIndex = currentIndex + 1;
            const nextDataEntry = indexData.find((entry: any) => entry.index === nextIndex);
            const nextFile = nextDataEntry ? nextDataEntry.file : null;


            // Process the file
            const result = await processFile(dataEntry.file, nextFile, timestamp);

            // If processFile returns null, we stop
            if (result === null) {
                console.log('Stopping because processFile returned null');
                break;
            }

            filesProcessed++;
            currentIndex++;
        }
    } finally {

        // Log final cache sizes
        console.log('\n-------------------');
        console.log('Final cache sizes:');
        console.log(`Taxa: ${getTaxonCacheSize()}`);
        console.log(`Higher Taxa: ${getHigherTaxaCacheSize()}`);
        console.log(`Web Resources: ${getWebResourceCacheSize()}`);
        console.log(`Web Resource Subelements: ${getTotalWebSubelementCount()}`);
        console.log(`People: ${getPersonCacheSize()}`);
        console.log(`Journals: ${getJournalCacheSize()}`);
        console.log(`Volumes: ${getVolumeCacheSize()}`);
        console.log(`Issues: ${getIssueCacheSize()}`);
        console.log(`Articles: ${getArticleCacheSize()}`);
        console.log(`Pages: ${getPageCacheSize()}`);
        console.log(`Hosts: ${getHostCacheSize()}`);
        console.log(`Asserted Distributions Locations: ${getDistributionCacheSize()}`);
        console.log(`Type Localities: ${getLocalityCacheSize()}`);


        // Write all cache data to files before clearing
        const taxonCachedata = getAllTaxonFromCache();
        const articleCacheData = getAllArticlesFromCache();
        const webCacheData = getAllWebResourcesFromCache();
        const higherTaxaData = getAllHigherTaxaFromCache();
        const personCacheData = getAllPersonsFromCache();
        const journalCacheData = getAllJournalsFromCache();
        const volumeCacheData = getAllVolumesFromCache();
        const issueCacheData = getAllIssuesFromCache();
        const pageCacheData = getAllPagesFromCache();
        const hostCacheData = getAllHostsFromCache();
        const assertedDistributionData = getAllDistributionsFromCache();
        const localityData = getAllLocalitiesFromCache();

        // final reconciliation of taxon cache

        // Write final caches and keys
        await Promise.all([
            writeResultsToFile(taxonCachedata, `./data/caches/taxonCache.json`),
            writeResultsToFile(articleCacheData, `./data/caches/articleCache.json`),
            writeResultsToFile(webCacheData, `./data/caches/webCache.json`),
            writeResultsToFile(higherTaxaData, `./data/caches/higherTaxaData.json`),
            writeResultsToFile(personCacheData, `./data/caches/personCache.json`),
            writeResultsToFile(journalCacheData, `./data/caches/journalCache.json`),
            writeResultsToFile(volumeCacheData, `./data/caches/volumeCache.json`),
            writeResultsToFile(issueCacheData, `./data/caches/issueCache.json`),
            writeResultsToFile(pageCacheData, `./data/caches/pageCache.json`),
            writeResultsToFile(hostCacheData, `./data/caches/hostCache.json`),
            writeResultsToFile(assertedDistributionData, `./data/caches/assertedDistributionCache.json`),
            writeResultsToFile(localityData, `./data/caches/localityCache.json`),
            // writeResultsToFile(getAllArticleCacheKeys(), `./data/caches/articleCacheKeys.json`),
            // writeResultsToFile(getAllPageCacheKeys(), `./data/caches/pageCacheKeys.json`),
            // writeResultsToFile(getAllIssueCacheKeys(), `./data/caches/issueCacheKeys.json`),
            // writeResultsToFile(getAllVolumeCacheKeys(), `./data/caches/volumeCacheKeys.json`),
            // writeResultsToFile(getAllJournalCacheKeys(), `./data/caches/journalCacheKeys.json`),
            // writeResultsToFile(getAllPersonCacheKeys(), `./data/caches/personCacheKeys.json`)
        ]);

        // Clear all caches...
        clearTaxonCache();
        clearArticleCache();
        clearWebResourceCache();
        clearHigherTaxonCache();
        clearPersonCache();
        clearJournalCache();
        clearVolumeCache();
        clearIssueCache();
        clearPageCache();
        clearHostCache();
        clearDistributionCache();
        clearLocalityCache();

        console.log('-------------------\n\n');
        console.timeEnd('Total processing time');
        BrowserManager.closeBrowser();
    }
}


/**
 * Checks all the <i> elements under:
 *   • ul.SP > li > div.TN > span.TN and ul.SSP > li > div.TN > span.TN,
 *   • ul.SP > li > div.TN > div.NAMES > ul.SN > li > i and ul.SSP > li > div.TN > div.NAMES > ul.SN > li > i.
 * If the text does not start with a capital letter (A–Z) or "?", prepends "?{genusName} ".
 * Adds a "changed-lcase" class to both the parent <span.TN> and the <i> tag, highlighting them in blue.
 */
export async function fixMissingGenusNames(document: Document, genusName: string): Promise<void> {
    // Append CSS to the document's head for highlighting
    const styleSheet = document.createElement("style");
    styleSheet.textContent += `
    .changed-lcase {
      background-color: #2196f3 !important; /* Light blue */
      padding: 2px 4px !important;
      border-radius: 2px !important;
    }
    `;
    document.head.appendChild(styleSheet);

    // Abbreviations we watch for (f., var., ssp., etc.)
    const anyAbbreviationPattern = /\b(f|var|ssp|subsp|ab|cv|sp|spp|cf)\.\b/i;

    // Grab <i> elements in SP/SSP blocks, PLUS <i> elements that are descendants of div.TN with
    // an id matching "u[0-9]+" (e.g., "u1234") under ul.SN or ul.LR
    const allITags = document.querySelectorAll(`
        ul.SP > li > div.TN > span.TN > i,
        ul.SP > li > div.TN > div.NAMES > ul.SN > li > i,
        ul.SSP > li > div.TN > span.TN > i,
        ul.SSP > li > div.TN > div.NAMES > ul.SN > li > i,
        div.TN[id^="u"] ul.SN li i,
        div.TN[id^="u"] ul.LR li i
    `);

    allITags.forEach((iTag) => {
        let textContent = (iTag.textContent || "").trim();
        // Check if this is the first <i> tag in its parent context
        const isFirstITag = iTag.parentElement?.querySelectorAll('i').length === 1;

        // --------------------------------------------------
        // (1) If text doesn't start with uppercase or '?',
        //     prepend "?{genusName}" (e.g. "?Argyresthia ")
        // --------------------------------------------------
        let injectedGenus = false;
        if (!/^[A-Z?]/.test(textContent)) {
            textContent = `?${genusName} ${textContent}`;
            iTag.textContent = textContent;

            iTag.classList.add('changed-lcase');
            const parentSpan = iTag.closest('span.TN');
            if (parentSpan) {
                parentSpan.classList.add('changed-lcase');
            }
            injectedGenus = true;
        }

        // --------------------------------------------------
        // (2) Get parent div.TN ID (often "species_subspecies" or "u####")
        // --------------------------------------------------
        const parentDiv = iTag.closest('div.TN');
        if (!parentDiv || !parentDiv.id) return;

        const splitted = parentDiv.id.split('_');
        if (splitted.length === 0) return;

        // E.g. "oreasella_andereggiella" => baseEpithet = "oreasella"
        const baseEpithet = splitted[0];

        // --------------------------------------------------
        // (3) Possibly inject the missing epithet if:
        //     • iTag text doesn't already contain it
        //     • AND (we just injected genus OR there's an abbreviation)
        // --------------------------------------------------
        const alreadyHasEpithet = iTag.textContent
            ?.toLowerCase()
            .includes(baseEpithet.toLowerCase());

        const hasAbbreviation = anyAbbreviationPattern.test(iTag.textContent || "");
        if (!alreadyHasEpithet && (injectedGenus || hasAbbreviation)) {
            const tokens = iTag.textContent!.split(/\s+/);

            // (a) If iTag has exactly one word and no abbreviation, 
            //     we assume that single word is the true species epithet,
            //     so we do NOT inject the parentDiv's first token.
            //
            //     Example:
            //       <i>andereggiella</i> => "?Argyresthia andereggiella"
            //     NOT => "?Argyresthia ?oreasella andereggiella"
            if (tokens.length === 1 && !hasAbbreviation) {
                // In this scenario, we do nothing more; 
                // we've already prepended "?{genusName}" in step (1).
                return;
            }

            // (b) If there's an underscore in the ID, it might indicate
            //     "species_subspecies" or similar. We'll inject the base epithet
            //     with a "?". But only do so if the user has multiple tokens
            //     or recognized abbreviations.
            //
            // For example, if the ID is "halimede_passercula" and the text 
            // is something like "?Argyresthia var. passercula",
            // we inject "?halimede" after the genus token:
            // => "?Argyresthia ?halimede var. passercula"
            if (splitted.length > 1) {
                const epithetToInsert = `?${baseEpithet}`;
                tokens.splice(1, 0, epithetToInsert);
                iTag.textContent = tokens.join(' ');
                iTag.classList.add('changed-lcase');
                const parentSpan = iTag.closest('span.TN');
                if (parentSpan) parentSpan.classList.add('changed-lcase');
            }
        }
    });
}

export async function removeAbbrElements(document: Document): Promise<void> {
    // This function looks for all <abbr> elements inside <i> tags within the
    // specified subsections. If the abbr text matches certain patterns, we replace
    // or remove them as needed. This helps avoid disruptions to name parsing.
    // Specifically:
    //  1. If we detect "[= ... ]", remove the <abbr> entirely.
    //  2. If we detect taxonomic abbreviations (e.g. "var.", "f.", "ssp.") with optional gender symbols,
    //     replace the <abbr> with a text node containing the matched pattern.
    //  3. Otherwise we remove the abbr altogether.

    const abbrElements = document.querySelectorAll(
        `
        li > i > abbr,
        div.NAMES > ul.SN > li > a > i > abbr
        `
    );

    abbrElements.forEach((abbrEl) => {
        const textContent = abbrEl.textContent || "";

        // 1) Check if <abbr> is bookended by two <i> tags:
        const prevElem = abbrEl.previousElementSibling;
        const nextElem = abbrEl.nextElementSibling;

        // If the pattern is <i>—<abbr>—<i>, merge them into one <i>.
        if (
            prevElem?.tagName === 'I' &&
            nextElem?.tagName === 'I' &&
            prevElem.parentNode === abbrEl.parentNode &&
            nextElem.parentNode === abbrEl.parentNode
        ) {
            const abbreviation = ' ' + textContent.trim() + ' ';
            // Merge all text into the prev <i>:
            prevElem.textContent = (prevElem.textContent || '') + abbreviation + (nextElem.textContent || '');
            // Remove the next <i> and the <abbr>:
            nextElem.remove();
            abbrEl.remove();
        }
        else {
            // Otherwise proceed with your existing logic, e.g.:

            // 1) If we detect "[= ... ]", remove the <abbr> entirely:
            const bracketedEqualsMatch = textContent.match(/\[=\s.*?\]/);
            if (bracketedEqualsMatch) {
                abbrEl.remove();
                return;
            }
            const bracketedEqualsMatch2 = textContent.match(/\[\s*\??\s*\]/);
            if (bracketedEqualsMatch2) {
                abbrEl.remove();
                return;
            }

            // 2) Check known taxonomic abbreviations:
            const match = textContent.match(
                /^\s*((?:Subgenus|var\.|f\.|aff\.|ab\.|cv\.|subvar\.|sp\.|spp\.|ssp\.)\??\s*[♀♂]?\s*)/
            );

            if (match) {
                let capturedAbbr = match[1];
                // Remove question marks if you want to:
                capturedAbbr = capturedAbbr.replace(/\?/g, '');

                // If it's exactly "Subgenus" or "aff.", remove it entirely:
                if (capturedAbbr.trim() === 'Subgenus' || capturedAbbr.trim() === 'aff.') {
                    abbrEl.remove();
                } else {
                    // Otherwise see if there's a single <i> before or after to merge with, etc.
                    const prevI = abbrEl.previousElementSibling;
                    if (prevI?.tagName === 'I') {
                        // Merge the abbreviation into the trailing space of prevI
                        prevI.textContent += ' ' + capturedAbbr.trim();
                        abbrEl.remove();
                    } else {
                        // Fallback: just replace <abbr> with a text node
                        const newTextNode = document.createTextNode(' ' + capturedAbbr);
                        abbrEl.replaceWith(newTextNode);
                    }
                }
            } else {
                // Otherwise remove the abbr
                abbrEl.remove();
            }
        }
    });
}

export async function expandNameAbbreviations(document: Document, genusName: string): Promise<void> {
    // Insert a style element for optional highlighting of expanded names
    const styleSheet = document.createElement("style");
    styleSheet.textContent = `
        .expanded-names {
            background-color: #4caf50 !important; /* Light green */
            padding: 2px 4px !important;
            border-radius: 2px !important;
        }
    `;
    document.head.appendChild(styleSheet);

    // Look for <i> in both SP and SSP blocks, similar to how fixMissingGenusNames does
    const allITags = document.querySelectorAll(`
        ul.SP > li > div.TN > span.TN > i,
        ul.SP > li > div.TN > div.NAMES > ul.SN > li > i,
        ul.SSP > li > div.TN > span.TN > i,
        ul.SSP > li > div.TN > div.NAMES > ul.SN > li > i
    `);

    allITags.forEach((iTag) => {
        const originalText = (iTag.textContent || "").trim();
        const pattern = new RegExp(`^${genusName[0]}\\.\\s+`); // e.g. /^D\.\s+/ for "Delias"

        // If the text begins with something like "D. "
        if (!pattern.test(originalText)) {
            // Skip if not matching
            return;
        }

        // Find the nearest div.TN, extract the blockId (e.g. "kuehni_prinsi"), then get the first item as the nominative epithet
        const parentDiv = iTag.closest('div.TN');
        if (!parentDiv || !parentDiv.id) {
            return;
        }
        const blockId = parentDiv.id; // e.g. "kuehni_prinsi"
        const splitId = blockId.split('_');
        if (splitId.length === 0) {
            return;
        }
        const nominativeEpithet = splitId[0]; // e.g. "kuehni"

        // Step 1: Replace the abbreviated genus, e.g. "D. " => "Delias "
        // So "D. k. prinsi" => "Delias k. prinsi"
        let newText = originalText.replace(pattern, `${genusName} `);

        // Step 2: Insert the nominative epithet right after the genus name
        // Then remove any single-letter abbreviation token (e.g. "k.") immediately after it.
        // So "Delias k. prinsi" => split -> ["Delias", "k.", "prinsi"]
        // -> splice(1, 0, "kuehni") => ["Delias", "kuehni", "k.", "prinsi"] 
        // -> remove "k." if it's a single-letter-with-dot => ["Delias", "kuehni", "prinsi"]
        // => "Delias kuehni prinsi"
        const tokens = newText.split(/\s+/);

        // If there's at least one token, tokens[0] should be "Delias".
        // Insert the nominative epithet at index 1, pushing the rest to the right.
        tokens.splice(1, 0, nominativeEpithet);

        // If the next token (now at index 2) is a single-letter abbreviation (e.g. "k."), remove it
        if (tokens[2] && /^\??[A-Za-z]\.$/.test(tokens[2])) {
            tokens.splice(2, 1);
        }

        newText = tokens.join(' ');

        // Update the <i> text
        iTag.textContent = newText.replace(/(\s+ssp\.)([0-9]+)/g, '$1 $2');

        // Optionally highlight the changed <i> and its parent <span.TN>
        iTag.classList.add("expanded-names");
        const parentSpan = iTag.closest('span.TN');
        if (parentSpan) {
            parentSpan.classList.add("expanded-names");
        }
    });
}

/**
 * Scans through all the taxa, checks if the "scientificNameAuthorship" has a corresponding
 * entry (article) in the articleCache (matching the first string in citedAs array),
 * and upserts one if not found. This allows us to keep track of the authorship as an article record.
 */
export function resolveScientificNameAuthorships(taxa: TaxonRecord[]): void {
    for (const taxon of taxa) {
        // If no authorship information, skip
        if (!taxon.scientificNameAuthorship) {
            continue;
        }

        // Prepare the citedAs array (in this simple version, it's one element)
        const citedAsArray = [
            taxon.scientificNameAuthorship.replace('(', '').replace(')', '').trim()
        ];

        // If the first element (index 0) is empty or falsy, skip
        if (!citedAsArray[0]) {
            continue;
        }

        // Reuse the standard getArticleByCitedAs / upsertArticle logic
        let article: Article = { citedAs: [] };
        if (citedAsArray[0].includes('[B')) {
            console.log("BAD ENTRY:", taxon)
        }
        const existingArticle = getArticleByCitedAs(citedAsArray[0]);
        // if (citedAsArray[0].includes('Lower, 1905')) {
        //     console.log("citedAs:", citedAsArray[0])
        //     console.log("existingArticle:", existingArticle)
        // }
        if (existingArticle) {
            // We've already got an article with this citation
            article = existingArticle;
        } else {
            // Parse authors from the string (e.g. "Johnson, Daniels & Walsingham, 1889", etc.)
            // if (citedAsArray[0] === undefined) {
            //     console.log("TEST3:", citedAsArray[0])
            // }
            const authorshipParsed = parseAuthorshipString(citedAsArray[0]);

            // Create a new Article object
            article.citedAs = citedAsArray;
            article.title = ''
            article.authors = authorshipParsed.authorship.map((entry, index) => {
                const person = upsertPersonToCache(entry.author);
                return { position: index, author: person };
            });

            // Optionally store the numeric year and bracket info, if you like:
            if (authorshipParsed.year) {
                const journal = { id: uuidv7(), title: '', abbreviation: '' }
                const volume = { id: uuidv7(), year: authorshipParsed.year, label: '', bracketedYear: authorshipParsed.bracketedYear, journal }
                article.issue = { id: uuidv7(), volume, label: '' };
            }

            // This upsert ensures the article is cached,
            // and each Person is added to the PersonCache by parseAuthorshipString
            upsertArticle(article, citedAsArray[0]);
        }
        // Optionally link the Article record to this Taxon:
        // taxon.authorshipArticleId = article.id;
    }
}

/**
 * Converts all capitalized Roman numerals (I to X) followed by a dash and some text 
 * (e.g. IV-ella, IX-otherstuff) into a lowercase form (e.g. iv-ella, ix-otherstuff). 
 * Also highlights converted elements in purple by adding a "converted-roman-numeral" CSS class.
 */
export async function fixRomanNumerals(document: Document): Promise<void> {
    // Insert optional style for highlighting.
    const styleSheet = document.createElement("style");
    styleSheet.textContent = `
        .converted-roman-numeral {
            background-color: #bb86fc !important; /* Light purple */
            padding: 2px 4px !important;
            border-radius: 2px !important;
        }
    `;
    document.head.appendChild(styleSheet);

    // Select <i> elements in both SP and SSP blocks (same pattern as the original).
    const allITags = document.querySelectorAll(`
        ul.SP > li > div.TN > span.TN > i,
        ul.SP > li > div.TN > div.NAMES > ul.SN > li > i,
        ul.SSP > li > div.TN > span.TN > i,
        ul.SSP > li > div.TN > div.NAMES > ul.SN > li > i
    `);

    allITags.forEach((iTag) => {
        const originalText = iTag.textContent || "";
        // Regex matches a word boundary, then the group of Roman numerals I–X, 
        // then a dash, then any non-whitespace characters.
        const pattern = /\b(I|II|III|IV|V|VI|VII|VIII|IX|X)-(\S+)/g;

        // Replace each match, lowercasing the Roman numeral portion, and keep the rest intact.
        const newText = originalText.replace(pattern, (match, roman, rest) => {
            return roman.toLowerCase() + '-' + rest;
        });

        // If a transformation took place, update the <i> text and highlight.
        if (newText !== originalText) {
            iTag.textContent = newText.trim();
            iTag.classList.add("converted-roman-numeral");

            // Highlight the parent <span.TN> as well.
            const parentSpan = iTag.closest('span.TN');
            if (parentSpan) {
                parentSpan.classList.add("converted-roman-numeral");
            }
        }
    });
}

/**
 * Removes any <i> tags beyond the first one found within each
 * div.MENTIONS > ul.LR > li element, preserving only the text
 * of those removed <i> tags (while the first <i> remains intact).
 */
export async function removeMentionItalics(document: Document): Promise<void> {
    const mentionListItems = document.querySelectorAll('div.MENTIONS > ul.LR > li');

    mentionListItems.forEach((li) => {
        const italicElements = li.querySelectorAll('i');
        italicElements.forEach((italicElement, index) => {
            // Keep only the first <i> intact
            if (index === 0) return;

            // Replace subsequent <i> tags with their text content
            const textContent = italicElement.textContent || '';
            italicElement.replaceWith(textContent);
        });
    });
}

export async function fixMalformedHTMLContent(document: Document): Promise<void> {
    // 1) Collect your <li> elements from each section (mentions, misID, and literature).
    const mentionListItems = document.querySelectorAll('ul.LR > li');
    const tnItems = document.querySelectorAll('span.TN');
    const mapItems = document.querySelectorAll('span.MAP > li');
    const snListItems = document.querySelectorAll('ul.SN > li');
    const misIDListItems = document.querySelectorAll('ul.MISID > li');
    const literatureListItems = document.querySelectorAll('ul.RL > li');
    // 2) Convert each NodeList into an array of objects, tagging each <li> with a "type" property.
    const combinedItems = [
        ...Array.from(mentionListItems).map(li => ({ type: 'mention', li })),
        ...Array.from(misIDListItems).map(li => ({ type: 'misID', li })),
        ...Array.from(literatureListItems).map(li => ({ type: 'literature', li })),
        ...Array.from(snListItems).map(li => ({ type: 'synonym', li })),
        ...Array.from(tnItems).map(li => ({ type: 'tn', li })),
        ...Array.from(mapItems).map(li => ({ type: 'map', li }))
    ];

    // 3) Process each item in a single loop, using a top-level switch or if-block 
    //    to apply only the relevant replacements for that item type.
    combinedItems.forEach(({ type, li }) => {
        let html = li.innerHTML;
        if (type === 'map') {
            if (html.includes('<span class="MAP"> Africa, Madagascar, EU - C.Asia, S.Russia, Asia Minor, Transcaucasus, Syria, Iran, Iraq, Kashmir, AU, </span>')) {
                html.replace(
                    '<span class="MAP"> Africa, Madagascar, EU - C.Asia, S.Russia, Asia Minor, Transcaucasus, Syria, Iran, Iraq, Kashmir, AU, </span>',
                    '<span class="MAP"> Africa, Madagascar, EU - C.Asia, S.Russia, Asia Minor, Transcaucasus, Syria, Iran, Iraq, Kashmir, AU</span>'
                )
            }
            li.innerHTML = html;

        } else if (type === 'tn') {
            if (html.includes('[&')) {
                html = html.replace(/\s+\[\&.*?\]/g, '');
            }
            if (html.match(/^\?ssp\./) || html.match(/<i><abbr> \?ssp\. <\/abbr><\/i>/)) {
                li.remove();
                return;
            }
            if (html.includes('<i>Polelassothys callista')) {
                html = html.replace(' (Tams) Kiriakoff, 1962', ' Tams, 1962');
            }
            if (html.includes('Xanthorhoe ramaria') || html.includes('Xanthorhoe dodata')) {
                html = html.replace('?Swett', 'Swett')
            }
            if (html.includes('Oriens gola') || html.includes('Xanthorhoe dodata')) {
                html = html.replace('?Lee', 'Lee')
            }
            if (html.includes('O. g. yunnana')) {
                html = html.replace('?Lee', 'Lee')
            }
            if (html.includes('Nepytia swetti')) {
                html = html.replace('?Barnes &amp; ?Benjamin', 'Barnes &amp; Benjamin')
            }
            if (html.includes('Hesperia nevada')) {
                html = html.replace(' and ', ' &amp; ')
            }
            if (html.includes('Nepytia swetti')) {
                html = html.replace('?Barnes &amp; ?Benjamin', 'Barnes &amp; Benjamin')
            }
            if (html.includes('Eupithecia') || html.includes('E.')) {
                html = html.replace('?Swett', 'Swett')
            }
            if (html.includes('<i>Hemileuca <abbr>sp.</abbr> </i>')) {
                document.querySelector('ul.SP > li > div#u17')?.remove()
            }
            html = html.replace(' (4): (Suppl.) ', ' (4) (Suppl.): ')
            html = html.replace(' (8): (Suppl.) ', ' (8) (Suppl.): ')

            li.innerHTML = html;
        } else if (type === 'mention') {
            // // Apply replacements only for "mention" items:
            // Apply replacements only for "mention" items:
            if (html.includes('Moriuti, 1982, in Inoue')) {
                html = html.replace(
                    /Moriuti,\s*1982,\s*in\s*Inoue,?\s*(?:<i>)?\s*c?\.?\s*s?\.?\s*,?\s*(?:<\/i>)?\s*,?\s*\<b\>\s*1\s*\<\/b\>\s*\:\s*(\d+)\s*,\s*2\s*\:\s*(\d+)/,
                    (_, num1, num2) =>
                        `Moriuti, 1982, in Inoue c. s., <b>1</b>: ${num1}; Moriuti, 1982, in Inoue c. s., <b>2</b>: ${num2}`
                );
            }
            if (html.includes('<i>Pyrausta panopealis')) {
                html = html.replace('<a href="#27966"><i> J. Lep. Soc. </i><b>44</b> (3)</a>', '<a href="#27966"><i> J. Lep. Soc. </i><b>44</b> (3)</a>: ');
            }
            if (html.includes('<i>Uresiphita reversalis')) {
                html = html.replace('<a href="#2186"><i> J. Lep. Soc. </i><b>43</b> (4)</a>', '<a href="#2186"><i> J. Lep. Soc. </i><b>43</b> (4)</a>: ');
            }
            if (html.includes('<i>Drepana robusta')) {
                html = html.replace('fig.', 'f.');
            }
            if (html.includes('<i>Albara discispilaria')) {
                html = html.replace(' ::REF#c:', '');
            }
            if (html.includes('<i>Thisanotia chrysonuchella')) {
                html = html.replace(' u. 55 (biology)', '');
            }
            if (html.includes('<i>Parapoynx seminealis')) {
                html = html.replace('<a href="#3664"><i> J. Lep. Soc. </i><b>55</b> (3)</a>', '<a href="#3664"><i> J. Lep. Soc. </i><b>55</b> (3)</a>: ');
            }
            if (html.includes('<i>Cledeobia netricalis')) {
                html = html.replace('(10): <a href="https://archive.org/stream/wienerentomologi07wien#page/336/mode/1up">336</a>,  (12):<a href="https://archive.org/stream/wienerentomologi07wien#page/456/mode/1up">456</a>', '(10): <a href="https://archive.org/stream/wienerentomologi07wien#page/336/mode/1up">336</a>; Lederer, 1863, <a href="#18052"><i>Wien. ent. Monats. </i><b>7</b></a>  (12): <a href="https://archive.org/stream/wienerentomologi07wien#page/456/mode/1up">456</a>');
            }
            if (html.includes('<i>Ectomyelois muriscus')) {
                html = html.replace('<a href="#34363"><i> J. Lep. Soc. </i><b>40</b> (1)</a>', '<a href="#34363"><i> J. Lep. Soc. </i><b>40</b> (1)</a>: ');
            }
            if (html.includes('<i>Ampittia luanchuanensis')) {
                html = html.replace(' (note, = <a href="./#Thoressa"><i>Pedesta</i></a> or <a href="./#Thoressa"><i>Thoressa</i></a>)', '');
            }
            if (html.includes('<i>Terias harina')) {
                html = html.replace('(note, <i>formosa</i> group)', '');
            }
            if (html.includes('<i>Poanes viator viator')) {
                html = html.replace('<a href="#21249"><i> J. Lep. Soc. </i><b>59</b> (3)</a>', '<a href="#21249"><i> J. Lep. Soc. </i><b>59</b> (3)</a>: ');
            }
            if (html.includes('<i>Pyrgus malvae')) {
                html = html.replace('; ::SPRK', '');
            }
            if (html.includes('<i>Euripus funebris')) {
                html = html.replace('Leech, [1892], <a href="#18107"><i>Butts China Japan Corea</i></a> (1): <a href="https://archive.org/stream/butterfliesfromc01leecrich#page/150/mode/1up">150</a>,  (2):<a href="https://archive.org/stream/butterfliesfromc02leecrich#page/655/mode/1up">655</a>,  (1): pl. 16,  f. 1;', 'Leech, [1892], <a href="#18107"><i>Butts China Japan Corea</i></a> (1): <a href="https://archive.org/stream/butterfliesfromc01leecrich#page/150/mode/1up">150</a>, pl. 16,  f. 1; Leech, [1892], <a href="#18107"><i>Butts China Japan Corea</i></a> (2): <a href="https://archive.org/stream/butterfliesfromc02leecrich#page/655/mode/1up">655</a>;');
            }
            if (html.includes('<i>Thecla syncellus')) {
                html = html.replace(
                    'Hewitson, 1867, <a href="#13721"><i>Ill. diurn. Lep. Lycaenidae</i></a> (3): <a href="https://archive.org/stream/illustrationsofd01hewi#page/n156/mode/1up">109</a>,  (6):<a href="https://archive.org/stream/illustrationsofd01hewi#page/n232/mode/1up">183</a>,  (3): pl. <a href="https://archive.org/stream/illustrationsofd02hewi#page/n122/mode/1up">46</a>,  f. 207-208,  (6): pl. <a href="https://archive.org/stream/illustrationsofd02hewi#page/n174/mode/1up">72</a>,  f. 553-554;',
                    'Hewitson, 1867, <a href="#13721"><i>Ill. diurn. Lep. Lycaenidae</i></a> (3): <a href="https://archive.org/stream/illustrationsofd01hewi#page/n156/mode/1up">109</a>, pl. <a href="https://archive.org/stream/illustrationsofd02hewi#page/n122/mode/1up">46</a>,  f. 207-208; Hewitson, 1867, <a href="#13721"><i>Ill. diurn. Lep. Lycaenidae</i></a> (6): <a href="https://archive.org/stream/illustrationsofd01hewi#page/n232/mode/1up">183</a>, pl. <a href="https://archive.org/stream/illustrationsofd02hewi#page/n174/mode/1up">72</a>,  f. 553-554;');
            }
            if (html.includes('<i>decorata</i>')) {
                html = html.replace('145#', '145');
            }

            if (html.includes('<i>Calycopis cyanus') || html.includes('<i>Calycopis vesulus') || html.includes('<i>Camissecla ledaea')) {
                html = html.replace(' (note, <span class="ALERT">??)</span>', '');
            }

            if (html.includes('<i>Polelassothys callista')) {
                html = html.replace('Tams; Kiriakoff, 1962', 'Tams, 1962');
            }
            if (html.includes('Glyphipterix simpliciella')) {
                html = html.replace('c 47 (<i>aechimiella</i>), 52 (<i>nattani</i>), 53 (<i>cognatella</i> = <i>variella</i>, <i>desiderella</i>, <i>fischeriella</i>, <i>roeslerstammella</i>, 48 = <i>conjunctella = </i>colluripenella_)', '47, 48, 52, 53');
                html = html.replace('Lempke, 19, 19', 'Lempke, 1976, 19')
            }
            if (html.includes('<i>Iocharis<abbr>[sic, <i>Iochares</i>]</abbr></i>')) {
                html = html.replace(',  (3):', '; (3): ');
            }
            if (html.includes('<i>Tasta micaceata')) {
                html = html.replace('63, f. 91, 103, pl. 4', '63, pl. 4, f. 91, 103').replace(': 84 (part),  f. 668; ', ': 84,  f. 668; ');
            }
            if (html.includes('<i>Eupithecia catalinata')) {
                html = html.replace('<a href="#29109"><i> J. Lep. Soc. </i><b>37</b> (2)</a>', '<a href="#29109"><i> J. Lep. Soc. </i><b>37</b> (2)</a>: ');
            }
            if (html.includes('<i>Automerula auletes')) {
                html = html.replace('<a href="#10604"><i>Lambillionea </i><b>102</b> (1, II)</a>', '<a href="#10604"><i>Lambillionea </i><b>102</b> (1, II)</a>: ');
            }
            if (html.includes('<i>Hemileuca hera hera')) {
                html = html.replace('<a href="#12497"><i> J. lep. Soc. </i><b>56</b> (3)</a>', '<a href="#12497"><i> J. lep. Soc. </i><b>56</b> (3)</a>: ');
            }
            if (html.includes('<i>Hylesia acuta')) {
                html = html.replace('<a href="#33618"><i> J. Lep. Soc. </i><b>42</b> (2)</a>', '<a href="#33618"><i> J. Lep. Soc. </i><b>42</b> (2)</a>: ');
            }
            if (html.includes('<i>Prochoerodes truxaliata')) {
                html = html.replace('<a href="#22693"><i> J. Lep. Soc. </i><b>41</b> (4)</a>', '<a href="#22693"><i> J. Lep. Soc. </i><b>41</b> (4)</a>: ');
            }
            if (html.includes('<i>Enypia venata') || html.includes('<i>Enypia griseata')) {
                html = html.replace('<a href="#29109"><i> J. Lep. Soc. </i><b>37</b> (2)</a>', '<a href="#29109"><i> J. Lep. Soc. </i><b>37</b> (2)</a>: ');
            }
            if (html.includes('<i>Braclunia<abbr>[sic, <i>Brachmia</i>]</abbr></i>')) {
                html = html.replace(',  (3):', '; (3): ');
            }
            if (html.includes('<i>Elachista <abbr>(Elachista) sp.</abbr></i>')) {
                html = html.replace(' (Russia, S.Ural, Cheliabinsk, 55°01´N 60°06´E, 350, Miass, Ilmen State Res.)', '');
            }
            if (html.includes('<i>Elachista <abbr>(Elachista) sp.nr. </abbr>kilmunella</i>')) {
                html = html.replace(' (Russia, S.Ural, Cheliabinsk, 52°39´N 59°34´E, 350m, Arkaim res. nr. Amurskii vill.)', '');
            }
            if (html.includes('<i>monotyla</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (list, unplaced in <a href="../../../cosmopterigidae/chrysopeleiinae#Chrysopeleiinae"><i>Chrysopeleiidae</i></a>)', '(list)');
            }
            if (html.includes('<i>bicolorella</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (list, unplaced in <a href="../../../../tineoidea/tineidae#Tineidae"><i>Tineidae</i></a>)', '(list)');
            }
            if (html.includes('<i>Condica</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' ::MNA26.1, 20 (note);', '');
            }
            if (html.includes('<i>Antiphalera montana')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(', = _Antiphalera bilineata (Hampson, 1896)?', '');
            }
            if (html.includes('<i>Stauroplitis dentata')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (note, ?<a href="./#annulata"><i>Stauroplitis annulata</i></a> Gaede, 1930)', '');
            }
            if (html.includes('<i>Schinia')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('<a href="#4312"><i> J. Lep. Soc. </i><b>43</b> (3)</a>', '<a href="#4312"><i> J. Lep. Soc. </i><b>43</b> (3)</a>: ');
                html = html.replace('<a href="#23041"><i> J. Lep. Soc. </i><b>42</b> (2)</a>', '<a href="#23041"><i> J. Lep. Soc. </i><b>42</b> (2)</a>: ');
                html = html.replace('<a href="#12681"><i> J. Lep. Soc. </i><b>52</b> (4)</a>', '<a href="#12681"><i> J. Lep. Soc. </i><b>52</b> (4)</a>: ');
            }
            if (html.includes('<i>Acontia decisa')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (unknown, type lost))', '');
            }
            if (html.includes('<i>Acontia quadrata')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (unknown, type lost)', '');
            }
            if (html.includes('<i>Copaxa semioculata') || html.includes('<i>Copaxa orientalis')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (larva)', '').replace(' (gen)', '');
            }
            if (html.includes('<i>Eupackardia calleta')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('<a href="#22964"><i> J. Lep. Soc. </i><b>53</b> (4)</a>', '<a href="#22964"><i> J. Lep. Soc. </i><b>53</b> (4)</a>: ');
            }
            if (html.includes('<i>Promylea lunigerella')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('<a href="#20575"><i> J. Lep. Soc. </i><b>57</b> (1)</a>', '<a href="#20575"><i> J. Lep. Soc. </i><b>57</b> (1)</a>: ');
            }
            if (html.includes('<i>Zela cowani')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(',  (m holotype) f.', '');
            }

            if (html.includes('<i>Spragueia jaguaralis')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' list)', ' (list)');
            }

            if (html.includes('<i>thrypsiphila</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (list, unplaced in <a href="../../../cosmopterigidae/chrysopeleiinae#Chrysopeleiinae"><i>Chrysopeleiidae</i></a>)', '(list)');
            }
            if (html.includes('<i>trilychna</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (list, unplaced in <a href="../../../cosmopterigidae/chrysopeleiinae#Chrysopeleiinae"><i>Chrysopeleiidae</i></a>)', '(list)');
            }
            if (html.includes('<i>Scythris')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (Russia, S.Buryatia, 51°11-13´N 106°10-12´E, 700m, Hamar Daban Mts, Murtoy R., Guzinoe ozero village 6km NW, forest steppe)', '');
            }
            if (html.includes('<i>Polychrosis ephippias')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (Natal, Umkomaas)', '');
            }
            if (html.includes('<i>Spatalistis bifasciana')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (missidentification, = <i>egesta</i>)', '').replace(' (missidentification, = <i>egesta</i>)', '');
            }
            if (html.includes('<i>Lantanophaga pusillidactylus')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('; Don Herbison-Evans', '');
            }
            if (html.includes('Glyphipteryx thrasonella')) {
                html = html.replace(' (<i>struvei</i>)', '')
                html = html.replace(' (reprint 1952-53)', '')
            }
            if (html.includes('Glyphipteryx bergstraesserella')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(', e 47 (<i>altiorella</i>, <i>bergstraesserella</i>, <i>pietruskii</i>)', ', 47')
            }
            if (html.includes('Glyphipteryx linneana')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (syn. of bergstraesserella)', '')
            }
            if (html.includes('Argyroploce lamyra')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (Kegalle, Ceylon)', '')
            }
            if (html.includes('<i>Antithesia montana')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (neotype Chapelco-Techos, 1400m, Neuquén, Argentina)', '')
            }
            if (html.includes('<i>Epiblema infelix')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (part, ♀ gen.)', '')
            }
            if (html.includes('?<i>Anateinoma')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' [implied from]', '')
            }
            if (html.includes('<i>Eucosma larana')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' [lectotype]', '').replace(' [lectotype]', '')
            }
            if (html.includes('<i>Mictocommosis</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('150,', '150;')
            }
            if (html.includes('<i>Charitographa</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('151,', '151;')
            }
            if (html.includes('<i>Thaumatographa</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('151,', '151;')
            }
            if (html.includes('<i>Thaumatographa decoris</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('151,', '151;')
            }
            if (html.includes('<i>Thaumatographa eremnotorna</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('227,', '227;')
            }
            if (html.includes('Esaki, 1952, 456,')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('151,', '151;')
            }
            if (html.includes('<i>Cossula arpi') || html.includes('<i>Cossula oletta') || html.includes('<i>Cossula omaia') || html.includes('<i>Cossula elegans') || html.includes('<i>Cossula tacita')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' h;', ' f. h;')
            }
            if (html.includes('<i>Chionaema fasciola</i>')) {
                html = html.replace(
                    '  (1-2): pl. <a href="https://archive.org/stream/bonnerzoolo3419521953zool#page/n92/mode/1up">2</a>,  f. 42-43;',
                    ' Daniel, 1953, <a href="#6136">Bonn. zool. Beitr. <b>3</b></a> (1-2): pl. <a href="https://archive.org/stream/bonnerzoolo3419521953zool#page/n92/mode/1up">2</a>,  f. 42-43;'
                );
            }
            if (html.includes('<i>Chionaema sanguinea</i>')) {
                html = html.replace(
                    '  (1-2): pl. <a href="https://archive.org/stream/bonnerzoolo3419521953zool#page/n92/mode/1up">2</a>,  f. 48;',
                    ' Daniel, 1953, <a href="#6136">Bonn. zool. Beitr. <b>3</b></a> (1-2): pl. <a href="https://archive.org/stream/bonnerzoolo3419521953zool#page/n92/mode/1up">2</a>,  f. 48;'
                );
            }
            if (html.includes('<i>Chionaema tienmushanensis</i>')) {
                html = html.replace(
                    '  (1-2): pl. <a href="https://archive.org/stream/bonnerzoolo3419521953zool#page/n92/mode/1up">2</a>,  f. 46-47;',
                    ' Daniel, 1953, <a href="#6136">Bonn. zool. Beitr. <b>3</b></a> (1-2): pl. <a href="https://archive.org/stream/bonnerzoolo3419521953zool#page/n92/mode/1up">2</a>,  f. 46-47;'
                );
            }
            if (html.includes('<i>Morrisonia latex</i>')) {
                html = html.replace(
                    '<i>Morrisonia latex</i>; <a href="#23685">[POOLE]</a>; Wood &amp; Butler, 1989, <a href="#33644"> J. Lep. Soc. <b>43</b> (4)</a>;',
                    '<i>Morrisonia latex</i>; <a href="#23685">[POOLE]</a>;  <a href="#33644">Wood &amp; Butler, 1989</a>;'
                );
            }
            if (html.includes('<i>Limenitis bocki</i>') || html.includes('<i>Limenitis hollandi</i>')) {
                html = html.replace('(Limenitis) ', '');
            }
            if (
                html.includes('<i>Calephelis virginiensis</i>') ||
                html.includes('<i>Calephelis laverna</i>') ||
                html.includes('<i>Calephelis candiope</i>') ||
                html.includes('<i>Calephelis iris</i>') ||
                html.includes('<i>Calephelis velutina</i>')
            ) {
                html = html.replace('2&3', '2-3');
            }
            if (html.includes('<i>Penestoglossa marocanella</i>') ||
                html.includes('<i>Penestoglossa dardoinella</i>') ||
                html.includes('<i>Penestoglossa tauricella</i>')) {
                html = html.replace(/ <a href="https:\/\/www\.zobodat\.at\/pdf\/ZOEV_25_0073-0076\.pdf">\(Schluß\)<\/a>\:?/, '');
            }
            if (html.includes('<i>Endoclyta')) {
                html = html.replace('(erklärung) ', '');
            }
            if (html.includes('<i>Conistra rubiginea') || html.includes('<i>Conistra ligula') || html.includes('<i>Conistra veronicae')) {
                html = html.replace('<a href="#1623"><i>Abhandlunge zur Larvalsystemtik der Insekten, No. </i><b>4</b></a>', '<a href="#1623"><i>Abhandlunge zur Larvalsystemtik der Insekten, No. </i><b>4</b></a>: ').replace(': :', ':');
            }
            if (html.includes('<i>Lithophane thujae')) {
                html = html.replace('<a href="#33058"><i>J. Lep. Soc. </i><b>53</b> (3)</a>', '<a href="#33058"><i>J. Lep. Soc. </i><b>53</b> (3)</a>: ');
            }
            if (html.includes('<i>Endoclita kosemponis')) {
                html = html.replace(' ::REF#:', '');
            }
            if (html.includes('<i>Endoclita kosemponis')) {
                html = html.replace(' ::REF#:', '');
            }
            if (html.includes('<i>Depressaria <abbr>sp. </abbr>A</i>')) {
                html = html.replace(' (Park Co., WY, Lemhi Co., CA, Alpine Co., CA, Modoc Co., CA)', '');
            }
            if (html.includes('<i>Depressaria <abbr>sp. </abbr>B</i>')) {
                html = html.replace(' (Modoc Co., CA)', '');
            }
            if (html.includes('<i>Stenoma reticens</i>')) {
                html = html.replace(' (Zululand, Nkwaleni)', '');
            }
            if (html.includes('<i>Anacampsis speciosella</i>')) {
                html = html.replace(' (Guatemala,Panama,Brazil (Amazonas))', '');
            }
            if (html.includes('<i>Acleris cristana</i>')) {
                html = html.replace(' Manley Jacobs', ' Manley & Jacobs');
            }
            if (html.includes('<i>Psilogramma increta')) {
                html = html.replace(', Abb. 1', '');
            }
            if (html.includes('<i>Callionima pan neivae')) {
                html = html.replace('tab. 7 (missp.)', '');
            }
            if (html.includes('<i>Proserpinus clarkiae')) {
                html = html.replace('<a href="#22492"><i> J. Lep. Soc. </i><b>53</b> (4)</a>', '<a href="#22492"><i> J. Lep. Soc. </i><b>53</b> (4)</a>: ');
            }
            if (html.includes('<i>Arctonotus lucidus')) {
                html = html.replace('<a href="#22492"><i> J. Lep. Soc. </i><b>53</b> (4)</a>', '<a href="#22492"><i> J. Lep. Soc. </i><b>53</b> (4)</a>: ');
                html = html.replace('<a href="#25878"><i> J. Lep. Soc. </i><b>55</b> (2)</a>', '<a href="#25878"><i> J. Lep. Soc. </i><b>55</b> (2)</a>: ');
            }
            if (html.includes('<i>Eurema tilaha nicevillei')) {
                html = html.replace('pl. back', 'pl. 0,');
            }


            if (html.includes('<i>Acleris literana</i>')) {
                html = html.replace('at al.', 'et al.');
            }
            if (html.includes('<i>Pseudomaniola helche')) {
                html = html.replace(' (note, =<a href="./#mirabilis"><i>Pseudomaniola mirabilis</i></a>?)', '');
            }
            if (html.includes('<i>Leptotes plinius')) {
                html = html.replace(' (text: not in Africa)', '');
            }
            if (html.includes('<i>Tongeia hainani')) {
                html = html.replace(': p. 69', ': 69');
            }
            if (html.includes('<i>Dercas verhuelli doubledayi')) {
                html = html.replace(',  pl. title,  ', ', pl. 0, ');
            }

            if (html.includes('<i>Acleris compsoptila</i>')) {
                html = html.replace(/ \(part\)/g, '');
            }
            if (html.includes('<i>Ratarda furvivestita</i>')) {
                html = html.replace(/fig\./g, 'f.');
            }
            if (html.includes('<i>Sphinx exulans</i>')) {
                html = html.replace(',  (5-6): pl. ', '; (5-6): pl. ');
            }
            if (html.includes('<i>Xylomyges</i>')) {
                html = html.replace(' [wrong TS <i>Noctua eridania</i> Cramer]', '');
            }
            if (html.includes('<i>Egira simplex')) {
                html = html.replace('<a href="#29109"><i> J. Lep. Soc. </i><b>37</b> (2)</a>', '<a href="#29109"><i> J. Lep. Soc. </i><b>37</b> (2)</a>: ');
            }
            if (html.includes('<i>Morrisonia latex')) {
                html = html.replace('<a href="#33644"><i> J. Lep. Soc. </i><b>43</b> (4)</a>', '<a href="#33644"><i> J. Lep. Soc. </i><b>43</b> (4)</a>: ');
            }
            if (html.includes('<i>Paectes oculatrix')) {
                html = html.replace('<a href="#25320"><i> Cutworm moths Ontario and Quebec</i></a>', '<a href="#25320"><i> Cutworm moths Ontario and Quebec</i></a>: ');
            }
            if (html.includes('<i>Sumatratarda diehlii</i>')) {
                html = html.replace(/fig\./g, 'f.');
            }
            if (html.includes('<i>Prochoreutis myllerana</i>')) {
                html = html.replace(/fig\./g, 'f.');
            }
            if (html.includes('<i>Phycodes radiata</i>')) {
                html = html.replace(' footnote', '');
            }
            if (html.includes('; Moore, 1882, 152; Warren, 1888, 337')) {
                html = html.replace('(f. 284)', 'f. 284').replace('(f. 663)', 'f. 663');
            }
            if (html.includes('Itame varadaria')) {
                html = html.replace('<a href="#22691"><i> J. Lep. Soc. </i><b>43</b> (4)</a>', '<a href="#22691"><i> J. Lep. Soc. </i><b>43</b> (4)</a>: ');
            }
            if (html.includes('<i>Oreta extensa')) {
                html = html.replace(' [part.]', '');
            }

            if (html.includes('<i>Simaethis nemorana')) {
                // console.log(html)
                html = html.replace(/\(&amp; Rogenhofer\)/g, '&amp; Rogenhofer').replace(/, ne\. 44/g, '');
            }
            if (html.includes('<i>Tortrix pariana')) {
                html = html.replace(', no. 8', '').replace(', note 116', '');
            }
            if (html.includes('<i></i>; Heppner, 1981, 51 (<i>Prochoreutis phalaraspis</i>)')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (<i>Prochoreutis phalaraspis</i>)', '').replace(' (<i>Prochoreutis philonyma</i>, <i>P. sehestediana</i>)', '')
            }
            if (html.includes('<i>Tebenna bjerkandrella')) {
                html = html.replace(', and <i>Tebenna kawabei</i>', '');
            }
            if (html.includes('<i>Choreutis dolosalis')) {
                html = html.replace(' (1-7)', '');
            }
            if (html.includes('<i>Choreutis sehestediana')) {
                html = html.replace(', c. s. 65', ', 65');
            }
            if (html.includes('<i>Cochylis vibrana')) {
                html = html.replace(': 12', '');
            }
            if (html.includes('; Meess, 1910, in Spuler, 297, pl. 87,')) {
                html = html.replace('pl. 10: 33', 'pl. 10');
            }
            if (html.includes('<i>Niaca curvimargo')) {
                html = html.replace(' (incertae sedis, <a href="../../../bombycoidea/lasiocampidae/macromphaliinae/euglyphis#Euglyphis"><i>Euglyphis</i></a> or <a href="../../../noctuoidea/nolidae#Nolidae"><i>Nolidae</i></a>)', '');
            }
            if (html.includes('<i>Niphadolepis improba')) {
                html = html.replace('106 (Uganda, Bwamba)', '106');
            }
            if (html.includes('<i>Zygaena afghana panjaoica')) {
                html = html.replace(' (note, <i>panjoica</i> [sic]))', '');
            }
            if (html.includes('<i>Zygaena turkmenica isfahanica')) {
                html = html.replace(' ::REF#a:', '').replace(' (note)', '');
            }
            if (html.includes('<i>Cheromettia apicata')) {
                html = html.replace(' ♂', '♂').replace(' ♀', '♀');
            }
            if (html.includes('<i>Parides anchises marthilia')) {
                html = html.replace(' ::REF#e:', '');
            }
            if (html.includes('<i>Parnassius nomion')) {
                html = html.replace(' [for Alaska missid.?]', '');
            }
            if (html.includes('<i>Lirimiris lignitecta')) {
                html = html.replace('<b>1</b>', '<b>1</b>: ');
            }
            if (html.includes('<i>Litodonta hydromeli')) {
                html = html.replace('<a href="#7630"><i>Proc. Ent. Soc. Wash. </i><b>6</b> (1)</a>', '<a href="#7630"><i>Proc. Ent. Soc. Wash. </i><b>6</b> (1)</a>: ');
                html = html.replace('<a href="#33189"><i> J. Lep. Soc. </i><b>41</b> (4)</a>', '<a href="#33189"><i> J. Lep. Soc. </i><b>41</b> (4)</a>: ');
            }
            if (html.includes('<i>Anthene rubrimaculata')) {
                html = html.replace(' (note: not in Kenya)', '');
            }

            if (html.includes('<i>Leucophobetron punctata')) {
                html = html.replace(' (incertae sedis, not limacodid?)', '');
            }
            if (html.includes('<i>Leptalis caesta')) {
                html = html.replace(' (as syn., missp.?)', '');
            }
            if (html.includes('<i>Sorema</i>')) {
                html = html.replace('(missp., not <a href="../../../bombycoidea/lasiocampidae/lasiocampinae/porela#Porela"><i>Sorema</i></a> Walker, 1855); ', '');
            }
            if (html.includes('<i>Parnassius charltonius')) {
                html = html.replace(' (descr. of male  Allotype Kotandar Pass, Paghman Mts, Kabul, 10-12500ft)', '');
            }
            if (html.includes('<i>Pyromorpha (Gingla) semifulva')) {
                html = html.replace(' [<span class="ALERT">??</span> incorrect ref.]', '');
            }
            if (html.includes('<i>Papilio bicolor')) {
                html = html.replace(',  [3]:(Additions-Corrections) <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n433/mode/1up">vii</a>', '');
            }
            if (html.includes('<i>Heraclides tasso')) {
                html = html.replace(' (species, but maybe hybrid)', '');
            }
            if (html.includes('<i>Paenasmia')) {
                html = html.replace(' (missp. of <i>Panosmia</i> Wood-Mason &amp; de Nicéville, 1886)', '');
            }
            if (html.includes('<i>Pereute callinira')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('title', '1')
            }
            if (html.includes('<i>Catasticta sororrensis')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(': (pl. expl, missp.)', '')
            }
            if (html.includes('<i>Catasticta')) {
                html = html.replace(/\(#[0-9]\) /g, '');
            }
            if (html.includes('<i>Delias')) {
                html = html.replace(/ \+ figs/g, '');
                html = html.replace(/, figs/g, '');
            }
            if (html.includes('<i>Azeta versicolor')) {
                html = html.replace(' (1)', ' (1): ');
            }
            if (html.includes('<i>Mursa phtisialis')) {
                html = html.replace(' (2)', ' (2): ');
            }
            if (html.includes('<i>Zale phaeocapna')) {
                html = html.replace(' (4)', ' (4): ');
            }
            if (html.includes('<i>Eilema uniola') || html.includes('<i>Eilema caniola')) {
                html = html.replace('<a href="#10783"><i>Boletín Asoc. esp. Entom., </i><b>9</b></a>', '<a href="#10783"><i>Boletín Asoc. esp. Entom., </i><b>9</b></a>: ');
            }
            if (html.includes('<i>Paidia rica')) {
                html = html.replace('<a href="#10784"><i>Graellsia </i><b>42</b></a>', '<a href="#10784"><i>Graellsia </i><b>42</b></a>: ');
            }
            if (html.includes('<i>Clytie</i> (Cato')) {
                html = html.replace(' (Appendix):', '');
            }
            if (html.includes('<i>Phlogophora isoscelata')) {
                html = html.replace(' f 164', ' f. 164');
            }
            if (html.includes('<i>Ceratomia sonorensis')) {
                html = html.replace(' ::MABL;', ' <a href="#R2">MABL</a>;');
            }
            if (html.includes('<i>Paonias myops')) {
                html = html.replace('101-102 &amp; 118 (gen.)', '101-102, 118');
            }
            if (html.includes('<i>Paonias macrops')) {
                html = html.replace('116-117 &amp; 118 (gen.)', '116-118');
            }

            if (html.includes('<i>Agrisius guttivittata')) {
                html = html.replace(',  (1-2):', '; Daniel, 1953, <a href="#6136"><i>Bonn. zool. Beitr. </i><b>3</b></a> (1-2):');
            }
            if (html.includes('<i>Agylla stötzneri')) {
                html = html.replace(',  (1-2):', '; Daniel, 1953, <a href="#6136"><i>Bonn. zool. Beitr. </i><b>3</b></a> (1-2):');
            }
            if (html.includes('<i>Gnophaela latipennis')) {
                html = html.replace('<a href="#11075"><i> J. Lep. Soc. </i><b>40</b> (3)</a>', '<a href="#11075"><i> J. Lep. Soc. </i><b>40</b> (3)</a>: ');
            }
            if (html.includes('<i>Pelochyta albipars')) {
                html = html.replace(' (<a href="../../lithosiinae/talara#albipars"><i>Talara albipars</i></a> Hampson, 1914)', '');
            }
            if (html.includes('<i>Agylla subinfuscata')) {
                html = html.replace(',  (1-2):', '; Daniel, 1953, <a href="#6136"><i>Bonn. zool. Beitr. </i><b>3</b></a> (1-2):');
            }
            if (html.includes('?<i>Stictane fuscus')) {
                html = html.replace(' (Peninsular Malaysia,Cambodia)', '');
            }
            if (html.includes('<i>Cathocida')) {
                html = html.replace(' (missp., as syn. of <i>Eubabpe</i>)', '');
            }
            if (html.includes('<i>Coscinia romeii')) {
                html = html.replace('<a href="#10785"><i>Eos </i><b>68</b> (2)</a>', '<a href="#10785"><i>Eos </i><b>68</b> (2)</a>: ');
            }
            if (html.includes('<i>Haploa confusa')) {
                html = html.replace('<a href="#18485"><i> J. Lep. Soc. </i><b>41</b> (3)</a>', '<a href="#18485"><i> J. Lep. Soc. </i><b>41</b> (3)</a>: ');
            }
            if (html.includes('<i>Hypoprepia fucosa')) {
                html = html.replace('<a href="#20802"><i> J. Lep. Soc. </i><b>56</b> (4)</a>', '<a href="#20802"><i> J. Lep. Soc. </i><b>56</b> (4)</a>: ');
            }
            if (html.includes('<i>Orgyia difficilis')) {
                html = html.replace(' ♀', '');
            }
            if (html.includes('<i>Syngrapha interrogationis')) {
                html = html.replace(', pl. ), f. 6', '').replace(', gen. 265, 330', '');
            }
            if (html.includes('<i>Syngrapha angulidens')) {
                html = html.replace(' (2)', ' (2): ');
            }
            if (html.includes('<i>Melinoides')) {
                html = html.replace('(55): <a href="https://archive.org/stream/CUbiodiversity1126759-9842#page/n171/mode/1up">64</a>,  (68):<a href="https://archive.org/stream/CUbiodiversity1126759-9842#page/110/mode/1up">110</a>, <a href="https://archive.org/stream/CUbiodiversity1126759-9842#page/124/mode/1up">124</a>', '(55): <a href="https://archive.org/stream/CUbiodiversity1126759-9842#page/n171/mode/1up">64</a>; Herrich-Schäffer, [1855], <a href="#13578"><i>Syst. Bearb. Schmett. Europ. </i><b>6</b></a> (68): <a href="https://archive.org/stream/CUbiodiversity1126759-9842#page/110/mode/1up">110</a>, <a href="https://archive.org/stream/CUbiodiversity1126759-9842#page/124/mode/1up">124</a>');
            }
            if (html.includes('<i>Phegoptera')) {
                html = html.replace('<b>1</b>: <a href="https://archive.org/stream/sammlungneuerode01herr#page/84/mode/1up">84</a>, ', '');
            }
            if (html.includes('<i>Automolis divisa')) {
                html = html.replace(' ♂', '');
            }
            if (html.includes('<i>Spilosoma screabile')) {
                html = html.replace('<a href="#11193"><i>The Afrotropical Tiger-Moths</i></a>', '<a href="#11193"><i>The Afrotropical Tiger-Moths</i></a>: ');
            }
            if (html.includes('<i>Phalaena (Bombyx) pudica')) {
                html = html.replace('; Esper, 1789, <a href="#8724"><i>Die. Schmett., Th. III </i>(Suppl.)</a> (1-2): <a href="https://archive.org/stream/dieschmetterling00espe#page/n211/mode/1up">26</a>,  (3-4): pl. <a href="https://archive.org/stream/dieschmetterling43espe#page/n176/mode/1up">84</a>,  f. 1', '; Esper, 1789, <a href="#8724"><i>Die. Schmett., Th. III </i>(Suppl.)</a> (1-2): <a href="https://archive.org/stream/dieschmetterling00espe#page/n211/mode/1up">26</a>; Esper, 1789, <a href="#8724"><i>Die. Schmett., Th. III </i>(Suppl.)</a>  (3-4): pl. <a href="https://archive.org/stream/dieschmetterling43espe#page/n176/mode/1up">84</a>,  f. 1');
            }
            if (html.includes('<i>Nicaea')) {
                html = html.replace(' (emend. <i>Nikaea</i> Moore, 1879))', '');
            }
            if (html.includes('<i>derubrata')) {
                html = html.replace(' (not <a href="./#pyrrhina"><i>Mesothen pyrrhina</i></a> Jones, 1914)', '');
            }
            if (html.includes('<i>Xanthothrix ranunculi')) {
                html = html.replace('<a href="#5687"><i>Bull. Sth. Calif. Acad. Sci. </i><b>9</b></a>', '<a href="#5687"><i>Bull. Sth. Calif. Acad. Sci. </i><b>9</b></a>: ');
                html = html.replace('<a href="#22978"><i>J. Lep. Soc. </i><b>56</b> (3)</a>', '<a href="#22978"><i>J. Lep. Soc. </i><b>56</b> (3)</a>: ');
            }

            if (html.includes('<i>Nygmia sp.2613')) {
                html = html.replace(' (Brunei, 300m Ulu Temburong / 1465m, Bukit Retak)', '');
            }
            if (html.includes('<i>Phalaena (Bombyx) coenobita')) {
                html = html.replace('; Esper, 1789, <a href="#8724"><i>Die. Schmett., Th. III </i>(Suppl.)</a> (1-2): <a href="https://archive.org/stream/dieschmetterling00espe#page/n199/mode/1up">14</a>,  (3-4): pl. <a href="https://archive.org/stream/dieschmetterling43espe#page/n172/mode/1up">82</a>,  f. 2', '; Esper, 1789, <a href="#8724"><i>Die. Schmett., Th. III </i>(Suppl.)</a> (1-2): <a href="https://archive.org/stream/dieschmetterling00espe#page/n199/mode/1up">14</a>; Esper, 1789, <a href="#8724"><i>Die. Schmett., Th. III </i>(Suppl.)</a>  (3-4): pl. <a href="https://archive.org/stream/dieschmetterling43espe#page/n172/mode/1up">82</a>,  f. 2');
            }
            if (html.includes('<i>Sideridis rosea')) {
                html = html.replace('<a href="#21410"><i> J. Lep. Soc. </i><b>37</b> (3)</a>', '<a href="#21410"><i> J. Lep. Soc. </i><b>37</b> (3)</a>: ');
            }
            if (html.includes('<i>Hadena ectypa')) {
                html = html.replace('<a href="#11074"><i>U.S. Dep. of Agriculture Tech. Bull. no. </i><b>1450</b></a>', '<a href="#11074"><i>U.S. Dep. of Agriculture Tech. Bull. no. </i><b>1450</b></a>: ');
            }
            if (html.includes('<i>Papaipema')) {
                html = html.replace('<a href="#11121"><i> J. Lep. Soc. </i><b>57</b> (2)</a>', '<a href="#11121"><i> J. Lep. Soc. </i><b>57</b> (2)</a>: ');
            }
            if (html.includes('<i>Disclisioprocta stellata')) {
                html = html.replace('<a href="#1936"><i> J. Lep. Soc. </i><b>52</b> (2)</a>', '<a href="#1936"><i> J. Lep. Soc. </i><b>52</b> (2)</a>: ');
            }
            if (html.includes('<i>Phyciodes chinchipensis')) {
                html = html.replace(' (insertae cedis, <a href="./#Phystis"><i>Phystis</i></a>?)', '');
            }
            if (html.includes('<i>Phyciodes rima')) {
                html = html.replace(' (insertae cedis, <a href="../telenassa#Telenassa"><i>Telenassa</i></a>?)', '');
            }
            if (html.includes('<i>Kumothales inexpectata') || html.includes('<i>Itylos mashenka')) {
                html = html.replace(' ♂', '');
            }
            if (html.includes('<i>Charaxes etheocles cedreatis')) {
                html = html.replace(' ♀', '');
            }
            if (html.includes('<i>Itylos fumosus')) {
                html = html.replace(', male V', '');
            }
            if (html.includes('<i>Euploea goudoti')) {
                html = html.replace(' (maybe labelled as <i>A. comorana</i>?)', '');
            }
            if (html.includes('<i>Euploea desjardinsii')) {
                html = html.replace(' (labelled as <i>A. crawshayi</i>)', '');
            }
            if (html.includes('<i>Ithomia aethra</i>')) {
                html = html.replace('; Hewitson, 1871, <a href="#13705"><i>Ill. exot. Butts </i>[2]</a> (Ithomia XXVIII-XXIX): [<a href="https://archive.org/stream/illustrationsofn02hewi#page/n79/mode/1up">32</a>],  f. [16],  f. 190', '; Hewitson, 1871, <a href="#13705"><i>Ill. exot. Butts </i>[2]</a>: [<a href="https://archive.org/stream/illustrationsofn02hewi#page/n79/mode/1up">32</a>], f. 16, 190');
            }
            if (html.includes('<i>Esakiozephyrus bieti dohertyi')) {
                html = html.replace(' ::REF#a:', '');
            }


            if (html.includes('<i>Arna sp.2565')) {
                html = html.replace(' (170m, nr Danum Valley Field Centre, Sabah)', '');
            }
            if (html.includes('<i>Arna sp.2778')) {
                html = html.replace(' (Kretam, Sabah)', '');
            }
            if (html.includes('<i>Perina sp.2483')) {
                html = html.replace(' (note, Sri Lanka)', '');
            }
            if (html.includes('<i>"Euproctis" sp.1135')) {
                html = html.replace(': 95, f. 255, pl. 5 (1930m, G. Kinabalu)', ': 95, pl. 5, f. 255')
            }
            if (html.includes('<i>"Euproctis" sp.2443')) {
                html = html.replace(' (Paka Cave, G. Kinabalu)', '');
            }
            if (html.includes('<i>hortuellus')) {
                html = html.replace('48 &amp; 54', '48, 54');
            }
            if (html.includes('<i>Crambus mandschuricus')) {
                html = html.replace('fig. 9; Caradja, 1926, b 162', 'f. 9; Caradja, 1926, 162');
            }
            if (html.includes('<i>Diarsia latimacula')) {
                html = html.replace(' (group, incertae sedis)', '');
            }
            if (html.includes('<i>Lymantria brotea brotea')) {
                html = html.replace(' (neotyped as <i>Lymantria galinara</i> Swinhoe, 1903)', '');
            }
            if (html.includes('<i>Baniana inaequalis')) {
                html = html.replace(' Lafontaine &amp; Walsh, 2010, ::REF#: 6 [key]', ' Lafontaine &amp; Walsh, 2010, 6');
            }
            if (html.includes('<i>Hypena vetustalis')) {
                html = html.replace('<a href="#1936"><i> J. Lep. Soc. </i><b>52</b> (2)</a>', '<a href="#1936"><i> J. Lep. Soc. </i><b>52</b> (2)</a>: ');
            }
            if (html.includes('Eva petulans')) {
                html = html.replace(' (invalid, chimaera)', '');
            }
            if (html.includes('<i>Dyscia')) {
                html = html.replace(/;?\s*RUGL.*?(?=$|;)/g, '');
            }
            if (html.includes('<i>Dyscia rjabovi')) {
                html = html.replace('RUGL*, #1218', '');
            }
            if (html.includes('Clossiana alberta')) {
                html = html.replace(' ::NAB', ' <a href="#24213">[NAB]</a>');
            }
            if (html.includes('Melitaea minerva sultanensis') || html.includes('Melitaea minerva balbina')) {
                html = html.replace('<a href="#13797"><i>Entomologist </i><b>73</b> (922)</a>', '<a href="#13797"><i>Entomologist </i><b>73</b> (922)</a>: ');
            }
            if (html.includes('Melitaea didyma caucasi')) {
                html = html.replace('Verity, 1929, <a href="#31775"><i>Ent. Rec. J. Var. </i><b>41</b></a> (3): <a href="https://archive.org/stream/entomologistsrec4041192829tutt#page/n386/mode/1up">43</a>,  (7-8):<a href="https://archive.org/stream/entomologistsrec4041192829tutt#page/n473/mode/1up">112</a>', 'Verity, 1929, <a href="#31775"><i>Ent. Rec. J. Var. </i><b>41</b></a> (3): <a href="https://archive.org/stream/entomologistsrec4041192829tutt#page/n386/mode/1up">43</a>; Verity, 1929, <a href="#31775"><i>Ent. Rec. J. Var. </i><b>41</b></a>  (7-8): <a href="https://archive.org/stream/entomologistsrec4041192829tutt#page/n473/mode/1up">112</a>');
            }
            if (html.includes('Passova passova gortyna')) {
                html = html.replace('::JSL#: ', '');
            }
            if (html.includes('Melanargia halimede coreana')) {
                html = html.replace(' (<span class="ALERT">??</span> Okamoto, 1926)', '');
            }
            if (html.includes('Karanasa voigti')) {
                html = html.replace(' (descr. male - Kotandar Pass, Paghman Mts., Kabul, 12000ft)', '');
            }
            if (html.includes('Karanasa bolorica hodja')) {
                html = html.replace(' (descr. female - Bala Quran, Anjuman Valley, NE Hindukush Mts., Badakhshan, 11-12500fr)', '');
            }
            if (html.includes('Jamides allectus sarmice')) {
                html = html.replace('138 &amp; 140', '138, 140');
            }
            if (html.includes('Lampides allectus')) {
                html = html.replace(
                    '; Grose-Smith &amp; Kirby, 1897, <a href="#11589"><i>Rhop. Exot.</i></a> [3] <b>2</b>: (Lampides) <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n334/mode/1up">6</a>,  [3] <b>2</b>:<a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n339/mode/1up">9</a>,  [3] <b>2</b>: pl. [<a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n328/mode/1up">1</a>],  f. 15-16,  [3] <b>2</b>: pl. <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n337/mode/1up">2</a>,  f. 3;',
                    '; Grose-Smith &amp; Kirby, 1897, <a href="#11589"><i>Rhop. Exot.</i></a> <b>2</b>: (Lampides) <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n334/mode/1up">6</a>, <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n339/mode/1up">9</a>, pl. <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n328/mode/1up">1</a>, f. 15-16, pl. <a href="https://archive.org/stream/rhopaloceraexoti03smit#page/n337/mode/1up">2</a>, f. 3;');
            }

            if (html.includes('Papilio julia')) {
                html = html.replace(' [as type of <a href="./#Dryas"><i>Dryas</i></a>]', '');
            }
            if (html.includes('Acraea cuva')) {
                html = html.replace('<i>Acraea cuva</i>; Grose-Smith &amp; Kirby, 1889, <a href="#11589"><i>Rhop. Exot.</i></a> [2] <b>1</b>: (Acraea) <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n161/mode/1up">2</a>,  [2] <b>3</b>:<a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n197/mode/1up">24</a>,  [2] <b>1</b>: pl. <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n159/mode/1up">1</a>,  f. 5,  [2] <b>3</b>: pl. <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n195/mode/1up">7</a>,  f. 4', '<i>Acraea cuva</i>; Grose-Smith &amp; Kirby, 1889, <a href="#11589"><i>Rhop. Exot.</i></a> <b>1</b>: (Acraea) <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n161/mode/1up">2</a>, pl. <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n159/mode/1up">1</a>, f. 5;  Grose-Smith &amp; Kirby, 1889, <a href="#11589"><i>Rhop. Exot.</i></a> <b>3</b>: <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n197/mode/1up">24</a>, pl. <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n195/mode/1up">7</a>, f. 4');
            }
            if (html.includes('Acraea welwitschii welwitschii')) {
                html = html.replace('pl. .', 'pl. ');
            }
            if (html.includes('Nacaduba ancyra subfestivus')) {
                html = html.replace(' (Celebes, part)', '');
            }

            if (html.includes('Mycalesis terminus pallens')) {
                html = html.replace(' Note)', '');
            }
            if (html.includes('Euploea transfixa')) {
                html = html.replace(' (incertae cedis: ? nymphaline, ?<a href="../../limenitidinae/neptis#Neptis"><i>Neptis</i></a>?)', '');
            }
            if (html.includes('Thecla longula')) {
                html = html.replace(' (neotype loc.)', '');
            }
            if (html.includes('<i>Solanorum goleta')) {
                html = html.replace(',  (Appendix 3, C) f. ', '');
            }
            if (html.includes('<i>Thecloxurina atymna')) {
                html = html.replace(', 7 - 18', ', 7-18');
            }
            if (html.includes('<i>Thecla (Theritas) tuneta')) {
                html = html.replace('row d, f. "tuneta","tuneta U"', 'f. d');
            }
            if (html.includes('<i>Thecla (Theritas) tuneta splendor')) {
                html = html.replace(', row d, f. "splendor"', 'f. d');
            }
            if (html.includes('<i>Thecla loxurina')) {
                html = html.replace(', row g', 'f. g');
            }
            if (html.includes('<i>Thecla loxurina fassli')) {
                html = html.replace(', row e, f. "socorrensis"', 'f. e');
            }


            if (html.includes('<i>Arcas tuneta')) {
                html = html.replace(', f. "A. tuneta"', '');
            }
            if (html.includes('<i>Thecla<abbr> ?</abbr> <abbr>sp.</abbr></i>')) {
                html = html.replace('<i>Thecla<abbr> ?</abbr> <abbr>sp.</abbr></i>', '<i>?Thecla sp.</i>').replace('f. "T. ? sp. ♂ R, "T. ? sp. ♂ V"', 'f. R, V');
            }
            if (html.includes('<i>Ahlbergia chalybeia')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: II:4, I:77, III:82;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4;Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 77; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 82;');
            }
            if (html.includes('<i>Ahlbergia ferrea')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:33, II:4, III:11;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 33; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 11;');
            }
            if (html.includes('<i>Ahlbergia pluto')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:277, II:4, III:33;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 277; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 33;');
            }
            if (html.includes('<i>Ahlbergia frivaldszkyi')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:138, II:4, III:82;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 138; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 82;');
            }
            if (html.includes('<i>Ahlbergia circe')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:81, II:4, III:83;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 81; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 83;');
            }
            if (html.includes('<i>Callophrys haradai')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:152, II:19, IV:51',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 152; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 19; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> IV: 51');
            }
            if (html.includes('<i>Ahlbergia leechi')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:193, II:4, III:23',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 193; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 23');
            }
            if (html.includes('<i>Ahlbergia nicevillei')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:242, II:4, III:83;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 242; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 4; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 83;');
            }
            if (html.includes('<i>Callophrys albilinea')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: I:11, II:19, III:100',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> I: 11; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 19; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> III: 100');
            }
            if (html.includes('<i>Cissatsuma albilinea')) {
                html = html.replace(
                    ': I:11',
                    ' I: 11');
            }
            if (html.includes('<i>Thecla gigantea')) {
                html = html.replace(
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a>: II. 107;',
                    '; Bridges, 1988, <a href="#3233"><i>Cat. Lycaenidae &amp; Riodinidae</i></a> II: 107;');
                html = html.replace(
                    '; Bridges, 1994, <a href="#3236"><i>Cat. Riodinidae &amp; Lycaenidae</i></a>: IX. 127;',
                    '; Bridges, 1994, <a href="#3236"><i>Cat. Riodinidae &amp; Lycaenidae</i></a> IX: 127;');
            }
            if (html.includes('<i>Thecla (Theritas) imperialis')) {
                html = html.replace(
                    ', row c., f.',
                    ', f. c, f');
            }
            if (html.includes('<i>Thecla danaus')) {
                html = html.replace(
                    ', row b, f. "danaus";',
                    ', f. b;');
            }
            if (html.includes('<i>Papilio alpheus')) {
                html = html.replace(
                    ': (type spec.) 94 (<a href="./#Capys"><i>Capys</i></a>), 407 (<a href="./#Capys"><i>Scoptes</i></a>)',
                    ';');
            }
            if (html.includes('<i>Thecla atymna')) {
                html = html.replace(
                    ', row g',
                    ', f. g');
            }
            if (html.includes('<i>Argyrogrammana trochilia')) {
                html = html.replace(
                    ': 72, f. 2c-d ♀, 2e ♂',
                    ': 72, pl. 0, f. 2c-2d, pl. 0, f. 2e ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana rameli')) {
                html = html.replace(
                    ': 72, f. 3b-c, 17 ♂, 3d-c ♀',
                    ': 72, pl. 0, f. 3b-3d, pl. 0, f. 17 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana johannismarci')) {
                html = html.replace(
                    ': 72, f. 4a-b ♂, 18 ♂',
                    ': 72, pl. 0, f. 4a-4b, pl. 0, f. 18 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana leptographia')) {
                html = html.replace(
                    ': 76, f. 6a-b (m.type), 20 ♂, 6c ♀',
                    ': 76, pl. 0, f. 6a-6c, pl. 0, f. 20 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana glaucopis glaucopis')) {
                html = html.replace(
                    ': 76, f. 7b-c (m.type), 21 ♂, 7d-c ♀',
                    ': 76, pl. 0, f. 7b-7d, pl. 0, f. 21 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana stilbe holosticta')) {
                html = html.replace(
                    ': 77, f. 10c-d (m.type), 23 ♂, 10c-f ♀',
                    ': 77, pl. 0, f. 10c-10f, pl. 0, f. 23 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana sublimis')) {
                html = html.replace(
                    ': 77, f. 11a-b ♂, 24 ♂, 11c ♀',
                    ': 77, pl. 0, f. 11a-11c, pl. 0, f. 24 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana placibilis')) {
                html = html.replace(
                    ': 78, f. 9a-b (m.type), 9e-f ♂, 22 ♂, 9c-d ♀',
                    ': 78, pl. 0, f. 9a-9f, pl. 0, f. 22 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana occidentalis')) {
                html = html.replace(
                    ': 78, f. 12a-b (m.type), 25 ♂, 12c-d ♀',
                    ': 78, pl. 0, f. 12a-12d, pl. 0, f. 25 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Argyrogrammana crocea')) {
                html = html.replace(
                    ': 79, f. 13a-b (m.type), 26 ♂, 13c-d ♀',
                    ': 79, pl. 0, f. 13a-13d, pl. 0, f. 26 ♂');
                // console.log(html)
            }
            if (html.includes('<i>Chalodeta chitinosa')) {
                html = html.replace(
                    ': f. 1&amp;2 B',
                    ': pl. 0, f. 1, 2');
                // console.log(html)
            }
            if (html.includes('<i>Detritivora gynaea')) {
                html = html.replace(
                    ': 149, f. 1&amp;2 E',
                    ': 149, pl. 0, f. 1, 2');
                // console.log(html)
            }
            if (html.includes('<i>Dachetola azora')) {
                html = html.replace(
                    ': f. 1&amp;2 A',
                    ': pl. 0, f. 1, 2');
                // console.log(html)
            }

            if (html.includes('<i>Amblypodia nobilis')) {
                html = html.replace(
                    '; Hewitson, 1863, <a href="#13721"><i>Ill. diurn. Lep. Lycaenidae</i></a> (1): <a href="https://archive.org/stream/illustrationsofd01hewi#page/n45/mode/1up">8</a>,  (4):<a href="https://archive.org/stream/illustrationsofd01hewi#page/n53/mode/1up">14b</a>',
                    '; Hewitson, 1863, <a href="#13721"><i>Ill. diurn. Lep. Lycaenidae</i></a> (1): <a href="https://archive.org/stream/illustrationsofd01hewi#page/n45/mode/1up">8</a>; Hewitson, 1863, <a href="#13721"><i>Ill. diurn. Lep. Lycaenidae</i></a> (4): <a href="https://archive.org/stream/illustrationsofd01hewi#page/n53/mode/1up">14b</a>');
            }
            if (html.includes('<i>Miletus polycletus')) {
                html = html.replace(
                    '; Fruhstorfer, 1908, <a href="#10261"><i>Int. ent. Zs. </i><b>2</b></a> (15): <a href="https://archive.org/stream/internationaleen02inte#page/91/mode/1up">91</a>,  (16):<a href="https://archive.org/stream/internationaleen02inte#page/99/mode/1up">99</a> (list);',
                    '; Fruhstorfer, 1908, <a href="#10261"><i>Int. ent. Zs. </i><b>2</b></a> (15): <a href="https://archive.org/stream/internationaleen02inte#page/91/mode/1up">91</a>; Fruhstorfer, 1908, <a href="#10261"><i>Int. ent. Zs. </i><b>2</b></a> (16): <a href="https://archive.org/stream/internationaleen02inte#page/99/mode/1up">99</a> (list);');
            }
            if (html.includes('<i>Diophthalma<abbr>')) {
                html = html.replace(
                    ' (unj. emend. of <i>Diophtalma</i> Boisduval, [1836])',
                    '');
            }

            if (html.includes('<i>Arhopala singla')) {
                html = html.replace(
                    'v. 20, 20a',
                    'f. 20');
            }
            if (html.includes('Tenaris rothschildi')) {
                html = html.replace('Grose-Smith &amp; Kirby, 1894, <a href="#11589"><i>Rhop. Exot.</i></a> [2] <b>2</b>: (Tenaris) <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n132/mode/1up">5</a>,  [2] <b>2</b>:<a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n138/mode/1up">9</a>,  [2] <b>2</b>: pl. <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n131/mode/1up">2</a>,  f. 1-3',
                    'Grose-Smith &amp; Kirby, 1894, <a href="#11589"><i>Rhop. Exot.</i></a> <b>2</b>: (Tenaris) <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n132/mode/1up">5</a>, <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n138/mode/1up">9</a>, pl. <a href="https://archive.org/stream/rhopaloceraexoti02smit#page/n131/mode/1up">2</a>, f. 1-3');
            }
            if (html.includes('Papilio sylvestris')) {
                html = html.replace(' (ts of <a href="../../../../../../../aves/apodiformes/trochilidae/doricha#Doricha"><i>Doricha</i></a>, missp.)', '');
            }
            if (html.includes('Argyrogrammana')) {
                html = html.replace(', 2nd col. 3rd row', '');
            }
            if (html.includes('Dianesia carteri carteri')) {
                html = html.replace(' (note, the type species)', '');
            }

            if (html.includes('<i>Gross-Schmett. Erde </i>')) {
                html = html.replace(/<a([^>]+)>(\d+)<\/a>\s+([a-z])/g, (match, linkAttrs, numStr, letter) => {
                    const num = parseInt(numStr, 10);
                    if (!Number.isNaN(num)) {
                        // If it's under 100, treat it as a plate reference
                        if (num < 100) {
                            return `pl. <a${linkAttrs}>${numStr}</a>, f. ${letter}`;
                        } else {
                            // Otherwise, treat it as a normal page reference
                            return `<a${linkAttrs}>${numStr}</a>, f. ${letter}`;
                        }
                    }
                    // Fallback if something odd is matched
                    return match;
                });
                html = html.replace(/pl\. pl\./g, 'pl.')
            }
            if (html.includes('<i> Macrolep. of world') || html.includes('<i> Lepid. Cat. </i>')) {
                html = html.replace(/,\s*row\s*([a-z])/g, ', f. $1')
            }
            if (html.includes('Häuser &amp; Boppré, 1997')) {
                const platePattern = /pl\.\s*\d+(?:\s+[A-Z](?:-[A-Z])?)*(?:,\s*\d+(?:\s+[A-Z](?:-[A-Z])?)*)*/g;
                html = html.replace(platePattern, (match) => {
                    // Remove the leading "pl."
                    let str = match.trim().replace(/^pl\.\s*/, '');

                    // Split by comma to get each plate reference chunk:
                    // e.g. "3 W X, 7 A-B" => ["3 W X", "7 A-B"]
                    const chunks = str.split(/\s*,\s*/);

                    const transformed = chunks.map((chunk) => {
                        // E.g. "3 W X" => ["3", "W", "X"]
                        const tokens = chunk.trim().split(/\s+/);
                        if (tokens.length === 0) return ''; // Safety fallback

                        // The first token is the plate number, e.g. "3"
                        const plateNum = tokens[0];
                        // The rest (if any) are letters or ranges, e.g. ["W", "X"] or ["A-B"]
                        const letters = tokens.slice(1);

                        if (letters.length === 0) {
                            // "pl. 3"
                            return 'pl. ' + plateNum;
                        } else {
                            // "pl. 3, f. W, X" or "pl. 3, f. A-B"
                            return 'pl. ' + plateNum + ', f. ' + letters.join(', ');
                        }
                    });
                    // Join all transformed references with comma+space
                    // e.g. ["pl. 3, f. W, X", "pl. 7, f. A-B"] => "pl. 3, f. W, X, pl. 7, f. A-B"
                    return transformed.join(', ');
                });
            }

            html = html.replace('<a href="#3236"><i>Cat. Riodinidae &amp; Lycaenidae</i></a>: (VIII) ', '<a href="#3236"><i>Cat. Riodinidae &amp; Lycaenidae</i></a> (VIII): ').replace(' (Luchuan, China?)', '');
            html = html.replace(/,\s*[Nn]o\.\s*\d+/g, '');
            html = html.replace(/\s*\(\??emend\..*?\)s*/g, '');
            html = html.replace(' (m.gen)', '').replace(' (f.gen)', '').replace(' (gen)', '');
            html = html.replace(/ \(partim, female\)/g, '').replace(/ \(name\)/g, '').replace(/ \(name, missp\.\)/g, '').replace(/ \(text\)/g, '').replace(/ \(key\)/g, '').replace(/ 8key\)/g, '');
            html = html.replace(/ \(Papilio\)/g, '');
            html = html.replace(/ \[spell\.\?\]/g, '').replace(/ \[in part\]/g, '');
            html = html.replace(/ \(Dismorphia\)/g, '');
            html = html.replace(/ \(Ornithoptera\)/g, '');
            html = html.replace(/ \(Pierinae\)/g, '');
            html = html.replace(/ front page/g, '');
            html = html.replace(/ \(missp\. of <i>Panosmia<\/i> Wood-Mason & de Nicéville\)/g, '');
            html = html.replace(/ \(missp\..*?\)/g, '');
            html = html.replace(/ \(Pieridae\)/g, '');
            html = html.replace(/ \(Ixias\)/g, '');
            html = html.replace(/ \(Belenois\)/g, '');
            html = html.replace(/ \(Pinacopteryx\)/g, '');
            html = html.replace(/ \(Delias\)/g, '');
            html = html.replace(/ \(Mylothris\)/g, '');
            html = html.replace(/ \(Huphina\)/g, '');
            html = html.replace(/ \(Appias\)/g, '');
            html = html.replace(/ \(Callosune\)/g, '');
            html = html.replace(/ \(Dichorragia\)/g, '');
            html = html.replace(/ \(Kallima\)/g, '');
            html = html.replace(/ \(Doleschallia\)/g, '');
            html = html.replace(/ \(Apatura\)/g, '');
            html = html.replace(/ \(Thaleropis\)/g, '');
            html = html.replace(/ \(Hypolimnas\)/g, '');
            html = html.replace(/ \(Hestina\)/g, '');
            html = html.replace(/ \(Mynes\)/g, '');
            html = html.replace(/ \(Algiers, \d+\)/g, '');
            html = html.replace(/ \(Elodina\)/g, '');
            html = html.replace(/ \(Hypolycaena-etc\)/g, '');
            html = html.replace(/ \(Cirrochroa\)/g, '');
            html = html.replace(/ \(Cupha\)/g, '');
            html = html.replace(/ \(Euthalia-Tanaecia\)/g, '');
            html = html.replace(/ \(Euthalia\)/g, '');
            html = html.replace(/ \(Terinos\)/g, '');
            html = html.replace(/ \(Euphaedra\)/g, '');
            html = html.replace(/ \(Cymothoe\)/g, '');
            html = html.replace(/ \(Neptis\)/g, '');
            html = html.replace(/ \(Limenitis\)/g, '');
            html = html.replace(/ \(Euryphene\)/g, '');
            html = html.replace(/ \(Cethosia\)/g, '');
            html = html.replace(/ \(Acraea\)/g, '');
            html = html.replace(/ \(Planema\)/g, '');
            html = html.replace(/ \(Pedaliodes\)/g, '');
            html = html.replace(/ \(Oxeoschistus\)/g, '');
            html = html.replace(/ \(Charaxes\)/g, '');
            html = html.replace(/ \(Euxanthe\)/g, '');
            html = html.replace(/ \(Morpho\)/g, '');
            html = html.replace(/ \(Tenaris\)/g, '');
            html = html.replace(/ \(Lycaenidae\)/g, '');
            html = html.replace(/ \(Ilerda\)/g, '');
            html = html.replace(/ \(Thysonotis\)/g, '');
            html = html.replace(/ \(Holochila\)/g, '');
            html = html.replace(/ \(Asthipa\)/g, '');
            html = html.replace(/ \(Ravadeba\)/g, '');
            html = html.replace(/ \(Athipa\)/g, '');
            html = html.replace(/ \(Erycinidae\)/g, '');
            html = html.replace(/ \(Libythea\)/g, '');
            html = html.replace(/ \(Hypochrysops\)/g, '');
            html = html.replace(/ \(Waigeum\)/g, '');
            html = html.replace(/ \(Arhopala\)/g, '');
            html = html.replace(/ \(Pseudonotis\)/g, '');
            html = html.replace(/ \(m\.type\)/g, '');
            html = html.replace(/ \(f\.type\)/g, '');
            html = html.replace(/ \(Horaga\)/g, '');
            html = html.replace(/ \(Dicallaneura-etc\)/g, '');
            html = html.replace(/ \(incertae cedis:nymphaline\)/g, '');
            html = html.replace(/ \(Morphotenaris-Hyantis\)/g, '');
            html = html.replace(/ \(Ragadia-Argyronympha-Hypocysta\)/g, '');
            html = html.replace(/ \(Elymnias\)/g, '');
            html = html.replace(/ \(Bruasa\)/g, '');
            html = html.replace(/ \(Mycalesis\)/g, '');
            html = html.replace(/ \(Prothoe\)/g, '');
            html = html.replace(/ \(Jamides\)/g, '');
            html = html.replace(/ \(Lampides\)/g, '');
            html = html.replace(/ \(Epimastidia\)/g, '');
            html = html.replace(/ \[<i>elegans<\/i> implied\]/g, '');
            html = html.replace(/ \(unrecog\.,missp\.\)/g, '');
            html = html.replace(/\(erklärung\) /g, '');
            html = html.replace(/ \:\:REF#\:/g, '');
            html = html.replace(/,\s*fn\.\s*[0-9]+/g, '');
            html = html.replace(/ \.?f\:? /g, ' f. ');
            html = html.replace(/\(\? Kashmir \(Aru\)\)/g, '');
            html = html.replace('<a href="#1623"><i>Abhandlunge zur Larvalsystemtik der Insekten, No. </i><b>4</b></a>', '<a href="#1623"><i>Abhandlunge zur Larvalsystemtik der Insekten, No. </i><b>4</b></a>: ').replace(': :', ':');
            html = html.replace('<a href="#1936"><i> J. Lep. Soc. </i><b>52</b> (2)</a>', '<a href="#1936"><i> J. Lep. Soc. </i><b>52</b> (2)</a>: ').replace(': :', ':');
            html = html.replace('<a href="#20928"><i>Boletín de la Asociación española de Entomología, </i><b>21</b></a>', '<a href="#20928"><i>Boletín de la Asociación española de Entomología, </i><b>21</b></a>: ');
            html = html.replace(/\s*\[<i>melaina<\/i>\]\s*/g, '');
            html = html.replace(' f: ', ' f. ');
            html = html.replace(' d. ', ' f. ');
            html = html.replace(' Back,', '');
            html = html.replace(' back,', '');
            html = html.replace('<a href="#32405"><i> J. Res. Lepid. </i><b>24</b> (4)</a>;', '<a href="#32405"><i> J. Res. Lepid. </i><b>24</b> (4)</a>: ;')
            html = html.replace('<a href="#7145"><i>Ann. Missouri Bot. Gard. </i><b>74</b></a>;', '<a href="#7145"><i>Ann. Missouri Bot. Gard. </i><b>74</b></a>: ;')
            html = html.replace('<a href="#28622"><i> J. Lep. Soc. </i><b>54</b> (4)</a>;', '<a href="#28622"><i> J. Lep. Soc. </i><b>54</b> (4)</a>: ;')
            html = html.replace('<i>The Butterflies of Zambia</i>;', '<i>The Butterflies of Zambia</i>: ;');
            html = html.replace('<a href="#10606"><i>Rev. Bras. Zool. </i><b>21</b> (3)</a>;', '<a href="#10606"><i>Rev. Bras. Zool. </i><b>21</b> (3)</a>: ;')
            html = html.replace('<a href="#10944"><i>New Zealand Butterflies</i></a>;', '<a href="#10944"><i>New Zealand Butterflies</i></a>: ;');
            // if (html.includes('<i>Epitola goodi') || html.includes('<i>Epitola catuna') || html.includes('<i>Epitola henleyi') || html.includes('<i>Epitola doleta') || html.includes('<i>Epitola barombiensis')) {
            // console.log("\nHTML:\n", html)
            html = html.replace(/\[<a href="https:\/\/archive.org\/stream\/rhopaloceraexoti03smit#page\/n[0-9]+\/mode\/1up">[0-9]+<\/a>\] /g, '');
            // }
            html = html.replace(/\(note, <a href=".\/#.*?"><i>.*?<\/i><\/a> group\)/g, '');
            html = html.replace(/(note, <i>.*?<\/i> group)/g, '')

            if (html.includes('<a href="#13353"><i>Metamorphosis </i><b>4</b></a>')) {
                html = html.replace(',  (2):', '; Henning, 1993, <a href="#13353"><i>Metamorphosis </i><b>4</b></a> (2): ')
            }
            html = html.replace(' (4): (Suppl.) ', ' (4) (Suppl.): ')
            html = html.replace(' (8): (Suppl.) ', ' (8) (Suppl.): ')

            html = html.replace(/,\s*n\.\s*.*?(;|\n|$)/g, '$1').replace('\n', '');
            // Assign modified HTML back to the <li>
            li.innerHTML = html;

        } else if (type === 'misID') {
            // Apply replacements relevant only for "misID" items:
            // Assign modified HTML back to the <li>
            if (html.includes('<i>Acleris compsoptila</i>')) {
                html = html.replace(' (part)', '');
            }
            if (html.includes('<i>Thecla danaus</i>')) {
                html = html.replace('f. "T. danaus ♂ R", "T. danaus ♀ R"', 'f. R');
            }
            if (html.includes('<i>Lymantria galinara')) {
                html = html.replace(' ♀', '');
            }
            if (html.includes('costaricana')) {
                // console.log(html)
                html = html.replace('*Lithacodia"', 'Lithacodia');
            }
            if (html.includes('<i>Tasta micaceata')) {
                html = html.replace(': 84 (part),  f. 666; ', ': 84,  f. 666; ').replace(': 84 (part),  f. 669; ', ': 84,  f. 669; ');
            }
            if (html.includes('<i>Thecla danaus')) {
                html = html.replace(
                    '"T. danaus ♂ R", "T. danaus ♀ R"',
                    'R');
            }
            li.innerHTML = html;
        } else if (type === 'literature') {
            // Apply replacements relevant only for "literature" items:
            if (html.includes('Rebel, 1940')) {
                html = html.replace(
                    'Rebel, 1940<br>\nZur Kenntnis einiger Subfamilien der <i>Psychide</i>\n<a href="https://www.zobodat.at/pdf/ZOEV_25_0059-0065-Tafel%20fehlt.pdf"><i>Zs. Wiener EntVer. </i><b>25</b></a>\n: 59-65,  pl. 11, \n <a href="https://www.zobodat.at/pdf/ZOEV_25_0073-0076.pdf">(Schluß)</a>: 73-76',
                    'Rebel, 1940<br>\nZur Kenntnis einiger Subfamilien der <i>Psychide</i>\n<a href="https://www.zobodat.at/pdf/ZOEV_25_0059-0065-Tafel%20fehlt.pdf"><i>Zs. Wiener EntVer. </i><b>25</b></a>\n: 59-65,  pl. 11, : 73-76',
                );
            }
            if (html.includes('Schawerda, 1936')) {
                html = html.replace(
                    'Schawerda, 1936<br>\nBeitrag zur Mikrolepidopterenfauna Sardiniens\n<a href="https://www.zobodat.at/pdf/ZOEV_21_0060-0064.pdf"><i>Zs. Öst. EntVer. </i><b>21</b></a>\n: 60-64, \n <a href="https://www.zobodat.at/pdf/ZOEV_21_0065-0067.pdf">(Fortsetzung)</a>: 65-67, \n <a href="https://www.zobodat.at/pdf/ZOEV_21_0079-0083.pdf">(Schluß)</a>: 79-83',
                    'Schawerda, 1936<br>\nBeitrag zur Mikrolepidopterenfauna Sardiniens\n<a href="https://www.zobodat.at/pdf/ZOEV_21_0060-0064.pdf"><i>Zs. Öst. EntVer. </i><b>21</b></a>\n: 60-64, : 65-67, : 79-83',
                );
            }
            if (html.includes('Korb, Fric &amp; Bartonova, 2016')) {
                html = html.replace(
                    'https://kmkjournals.com/journals/REJ/REJ_Index_Volumes/REJ_25/REJ_25_1_075_078_Korb_et_al ',
                    'https://kmkjournals.com/journals/REJ/REJ_Index_Volumes/REJ_25/REJ_25_1_075_078_Korb_et_al');
            }
            html = html.replace(' (4): (Suppl.) ', ' (4) (Suppl.): ')
            html = html.replace(' (8): (Suppl.) ', ' (8) (Suppl.): ')
            html = html.replace('de Selys de Longchamps', 'de Selys Longchamps')

            // Assign modified HTML back to the <li>
            li.innerHTML = html;
        } else if (type === 'synonym') {
            if (html.includes('<i>Penestoglossa mauretanica</i>')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' <a href="https://www.zobodat.at/pdf/ZOEV_25_0073-0076.pdf">(Schluß)</a>', '');
            }
            if (html.includes('<i>nigrella</i>')) {
                html = html.replace(' <a href="https://www.zobodat.at/pdf/ZOEV_21_0079-0083.pdf">(Schluß)</a>', '');
            }
            if (html.includes('Euxoa montigenarum')) {
                html = html.replace('Rougeot and Laporte', 'Rougeot &amp; Laporte')
            }
            if (html.includes('<i>Epichnopterix hellenidensis')) {
                html = html.replace(' :REF#: 145', ' Europa meridionalis, Greece West, Epirus, Voria Pindos, Zagoria, Umg. Kipi 1 km SW, 750–800 m NN');
            }
            if (html.includes('Agrotis lata')) {
                html = html.replace(':REF#e: 45, pl. 5 i  (Deckert i.l.);Algeria', 'Algeria')
            }

            if (html.includes('[&')) {
                html = html.replace(/\s+\[\&.*?\]/g, '');
            }
            if (html.includes('<i>Anarsia<abbr> ?</abbr> albapulvella</i>')) {
                html = html.replace('Anarsia<abbr> ?</abbr>', '?Anarsia');
            }
            if (html.includes('<i>Chimabacche?<abbr>[sic]</abbr>')) {
                html = html.replace('<i>Chimabacche?<abbr>[sic]</abbr>', '<i>?Chimabacche');
            }
            if (html.includes('<i>Chamina?<abbr>[sic]</abbr>')) {
                html = html.replace('<i>Chamina?<abbr>[sic]</abbr>', '<i>?Chamina');
            }
            if (html.includes('<i>Gelechia segetella')) {
                html = html.replace('; <b>TL</b>: :REF#a: 512, pl. 89, f. 7', '');
            }
            if (html.includes('<i>Scrobipalpomima pseudogrisescens')) {
                html = html.replace('; <b>TL</b>: :REF#: 141', '');
            }
            if (html.includes('<i>Homoeoprepes felisae')) {
                html = html.replace(':REF#b: 375;', '');
            }
            if (html.includes('<i>Antithesia montana')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' [lost?]', '')
            }
            if (html.includes('<i>Cossula coerulescens')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(':REF#b: 633;', '')
            }
            if (html.includes('<i>Parasa stekolnikovi')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('REF#: 122;', '')
            }
            if (html.includes('<i>Chlosyne strope')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(' (acastus) ', '')
            }
            if (html.includes('<i>Sphinx trimaculata')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('<a href="#8722"><i>Die. Schmett., Th. II </i>(Suppl.) Abs. <b>2</b></a> (5-6): <a href="https://archive.org/stream/dieschmetterling00espe#page/n149/mode/1up">16</a>; </a> (3-4): pl. <a href="https://archive.org/stream/dieschmetterling00esper#page/n86/mode/1up">40</a>,  f. 7-8', '')
            }
            if (html.includes('<i>Semioptila latifulva')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(':REF#b: 269;Germann', 'German')
            }
            if (html.includes('<i>Semioptila lydia')) {
                // console.log("\nHTML:\n", html)
                html = html.replace(':REF#b: 729;', '')
            }
            if (html.includes('<i>Bintha clathrata')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('; <b>TL</b>: :REF#a: cxvii', '')
            }
            if (html.includes('<i>Paonias oplerorum')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('106-107 &amp; 118 (gen.)', '106-107, 118')
            }
            if (html.includes('<i>Paonias hyatti')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('103-105 &amp; 118 (gen.)', '103-105, 118')
            }
            if (html.includes('<i>Paonias emmeli')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('108-109 &amp; 118 (gen.)', '108-109, 118')
            }
            if (html.includes('O. g. yunnana')) {
                html = html.replace('?Lee', 'Lee')
            }
            if (html.includes('Astictopterus stellata')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('?Trimen', 'Trimen')
            }
            if (html.includes('<i>Hemeroplanes parce')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('Guiarti', 'guiarti')
            }
            if (html.includes('<i>Calliomma neivai')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('Oiticica', 'oiticica')
            }
            if (html.includes('<i>Eumorpha Jussieuae')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('Jussieuae', 'jussieuae')
            }
            if (html.includes('<i>Euchloe Crameri')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('Euchloe Crameri', 'Euchloe crameri')
            }
            if (html.includes('<i>Papilio memnon-agenor')) {
                // console.log("\nHTML:\n", html)
                html = html.replace('Papilio memnon-agenor f. ensifer Rousseau-Decelle, [1933]', '')
            }
            if (html.includes('<i>Forsterinaria coipa')) {
                html = html.replace(':REF#: 4, 40, f. 14E, 13E;', '')
            }
            if (html.includes('<i>Aclonophlebia baliocosma')) {
                html = html.replace(':REF#b: 575;', '')
            }
            if (html.includes('Pseudeuchalcia</i>')) {
                html = html.replace('?<i><abbr>Subgenus</abbr>', '<i><abbr>Subgenus</abbr>')
            } // I think because this one is not confidently subgenus, the original rank is determine to be genus
            if (html.includes(' Pseudrhyacia</i>')) {
                html = html.replace('?<i><abbr>Subgenus</abbr>', '<i><abbr>Subgenus</abbr>')
            } // I think because this one is not confidently subgenus, the original rank is determine to be genus
            if (html.includes(' Cavifrons</i>')) {
                html = html.replace('?<i><abbr>Subgenus</abbr>', '<i><abbr>Subgenus</abbr>')
            } // I think because this one is not confidently subgenus, the original rank is determine to be genus
            if (html.includes('Eudryas Stae.&nbsp;')) {
                html = html.replace('<i>Eudryas Stae.&nbsp;Johannis <abbr>[= <i>sanctaejohannis</i>]</abbr></i>', '<i>Eudryas sanctaejohannis</i>')
            }
            if (html.includes('Hylophila milleri')) {
                html = html.replace('Capr[?],', 'Capronnier,')
            }
            if (html.includes('Hypsopygia flavamaculata')) {
                html = html.replace('?Shaffer', 'Shaffer')
            }
            if (html.includes('Miostauropus')) {
                html = html.replace('?Okagaki &amp; Nakamura, 1959', 'Okagaki &amp; Nakamura, 1959; ')
            }
            if (html.includes('galaica</i>')) {
                html = html.replace('Palanca Soler & Castan Lanaspa & Calle Pascual', 'Palanca Soler, Castan Lanaspa, & Calle Pascual')
            }
            if (html.includes('Thereta Rothschildi')) {
                html = html.replace('Thereta Rothschildi', 'Thereta rothschildi')
            }
            if (html.includes('?Edwards')) {
                html = html.replace('?Edwards', 'Edwards')
            }
            if (html.includes('<i>Dasychira melissograpta')) {
                html = html.replace(':REF#e: 163;', '')
            }
            if (html.includes('<i>Ideopsis gaura<abbr> var. </abbr>pseudocostalis</i>')) {
                html = html.replace(':REF#a: 49;', '')
            }
            if (html.includes('<i>Hesperia Caïd')) {
                html = html.replace('<i>Hesperia Caïd', '<i>Hesperia caid')
            }
            if (html.includes("<i>Catagramma d'Orbignyi")) {
                html = html.replace("<i>Catagramma d'Orbignyi", '<i>Catagramma dorbignyi')
            }
            if (html.includes("<i>Erebia Boisduvalii")) {
                html = html.replace("<i>Erebia Boisduvalii", '<i>Erebia boisduvalii')
            }
            if (html.includes("<i>Pseudonympha D'Urbani")) {
                html = html.replace("<i>Pseudonympha D'Urbani <abbr>[= <i>durbani</i>]</abbr></i>", "<i>Pseudonympha durbani</i>")
            }
            if (html.includes("<i>Charaxes saturnus<abbr> ab. </abbr>Pagenstecheri</i>")) {
                html = html.replace("<i>Charaxes saturnus<abbr> ab. </abbr>Pagenstecheri</i>", "<i>Charaxes saturnus ab. pagenstecheri</i>")
            }
            if (html.includes('<i>Lycaena dorcas<abbr> var. </abbr>dosPassosi</i>')) {
                html = html.replace('<i>Lycaena dorcas<abbr> var. </abbr>dosPassosi</i>', '<i>Lycaena dorcas var. dospassosi</i>')
            }
            if (html.includes('<i>Catopyrops ancyra (subfestivus) altijavana</i>')) {
                html = html.replace('<i>Catopyrops ancyra (subfestivus) altijavana</i>', '<i>Catopyrops ancyra altijavana</i>')
            }

            if (html.includes('<i>Hypena costinotalis')) {
                html = html.replace(':REF#b: ', '')
            }
            if (html.includes('<i>Tarvia?')) {
                html = html.replace('<i>Tarvia?<abbr>[sic]</abbr> martina</i>', '<i>?Tarvia martina</i>')
            }
            if (html.includes('<i>Chaeticneme?')) {
                html = html.replace('<i>Chaeticneme?<abbr>[sic]</abbr> lidderdali</i>', '<i>?Chaeticneme lidderdali</i>')
            }
            if (html.includes('<i>Matigramma aderces')) {
                html = html.replace('REF#a: 147;', '')
            }
            if (html.includes('<i>Catopsilia florella (f) wandriana')) {
                html = html.replace('(f)', '')
            }
            if (html.includes('<i>Erebia pandrose')) {
                html = html.replace(' (sthennyo) ', '')
            }
            if (html.includes('<i>Sideridis rivularis pacifica')) {
                html = html.replace(':REF#a: 613;', '')
            }
            if (html.includes('<i>Lithosia Arthus&nbsp;Bertrand <abbr>[= <i>arthusbertrand</i>]</abbr></i>')) {
                html = html.replace('<i>Lithosia Arthus&nbsp;Bertrand <abbr>[= <i>arthusbertrand</i>]</abbr></i>', '<i>Lithosia arthusbertrand</i>')
            }
            if (html.includes('Eupithecia')) {
                html = html.replace('?Swett', 'Swett')
            }
            if (html.includes('alienellus</i>')) {
                html = html.replace('?Zincken', 'Zincken')
            }
            if (html.includes('Argyrogrammana caesarion</i>')) {
                html = html.replace('?Rebillard', 'Rebillard')
            }
            if (html.includes('de Selys de Longchamps')) {
                html = html.replace('de Selys de Longchamps', 'de Selys Longchamps')
            }
            if (html.includes('Melanargia galatea syracusia')) {
                html = html.replace('Zeller[?]', 'Zeller')
            }
            if (html.includes('Redonda leukasmena')) {
                html = html.replace(':REF#: ', '')
            }
            if (html.includes('Colias arida cakana')) {
                html = html.replace(':REF#: 102;', '')
            }


            if (html.includes('Charaxes mafuga')) {
                html = html.replace(' ♀', '')
            }
            if (html.includes('<i>Lycaena escheri (m) subtus-impunctata</i>')) {
                html = html.replace('<i>Lycaena escheri (m) subtus-impunctata</i>', '<i>Lycaena escheri subtusimpunctata</i>')
            }
            if (html.includes('<i>Plebejus (Lysandra) coridon (albicans) burgalesa</i>')) {
                html = html.replace('<i>Plebejus (Lysandra) coridon (albicans) burgalesa</i>', '<i>Plebejus (Lysandra) coridon burgalesa</i>')
            }
            if (html.includes('<i>Plebejus (Lysandra) coridon (hispana) semperi</i>')) {
                html = html.replace('<i>Plebejus (Lysandra) coridon (hispana) semperi</i>', '<i>Plebejus (Lysandra) coridon semperi</i>')
            }
            if (html.includes('<i>Polyommatus pheretiades (pheretiades) pseudomicrus</i>')) {
                html = html.replace('<i>Polyommatus pheretiades (pheretiades) pseudomicrus</i>', '<i>Polyommatus pheretiades pseudomicrus</i>')
            }

            if (html.includes('<i>Plebejus (Lysandra) coridon (albicans) anamariae</i>')) {
                html = html.replace('<i>Plebejus (Lysandra) coridon (albicans) anamariae</i>', '<i>Plebejus (Lysandra) coridon anamariae</i>')
            }
            if (html.includes('<i>Plebejus (Lysandra) coridon (albicans) esteparina</i>')) {
                html = html.replace('<i>Plebejus (Lysandra) coridon (albicans) esteparina</i>', '<i>Plebejus (Lysandra) coridon esteparina</i>')
            }
            html = html.replace('?Rebel', 'Rebel')
            html = html.replace('?Elwes', 'Elwes')
            html = html.replace('?Aigner', 'Aigner')
            html = html.replace(' (4): (Suppl.) ', ' (4) (Suppl.): ')
            html = html.replace(' (8): (Suppl.) ', ' (8) (Suppl.): ')

            if (html.includes('<i>Gross-Schmett. Erde')) {
                // console.log(html)
                html = html.replace(/<a([^>]+)>(\d+)<\/a>\s+([a-z])/g, (match, linkAttrs, numStr, letter) => {
                    const num = parseInt(numStr, 10);
                    if (!Number.isNaN(num)) {
                        // If it's under 100, treat it as a plate reference
                        if (num < 100) {
                            return `pl. <a${linkAttrs}>${numStr}</a>, f. ${letter}`;
                        } else {
                            // Otherwise, treat it as a normal page reference
                            return `<a${linkAttrs}>${numStr}</a>, f. ${letter}`;
                        }
                    }
                    // Fallback if something odd is matched
                    return match;
                });
                html = html.replace(/pl\. pl\./g, 'pl.')
            }
            if (html.includes('Häuser &amp; Boppré, 1997')) {
                const platePattern = /pl\.\s*\d+(?:\s+[A-Z](?:-[A-Z])?)*(?:,\s*\d+(?:\s+[A-Z](?:-[A-Z])?)*)*/g;
                html = html.replace(platePattern, (match) => {
                    // Remove the leading "pl."
                    let str = match.trim().replace(/^pl\.\s*/, '');

                    // Split by comma to get each plate reference chunk:
                    // e.g. "3 W X, 7 A-B" => ["3 W X", "7 A-B"]
                    const chunks = str.split(/\s*,\s*/);

                    const transformed = chunks.map((chunk) => {
                        // E.g. "3 W X" => ["3", "W", "X"]
                        const tokens = chunk.trim().split(/\s+/);
                        if (tokens.length === 0) return ''; // Safety fallback

                        // The first token is the plate number, e.g. "3"
                        const plateNum = tokens[0];
                        // The rest (if any) are letters or ranges, e.g. ["W", "X"] or ["A-B"]
                        const letters = tokens.slice(1);

                        if (letters.length === 0) {
                            // "pl. 3"
                            return 'pl. ' + plateNum;
                        } else {
                            // "pl. 3, f. W, X" or "pl. 3, f. A-B"
                            return 'pl. ' + plateNum + ', f. ' + letters.join(', ');
                        }
                    });
                    // Join all transformed references with comma+space
                    // e.g. ["pl. 3, f. W, X", "pl. 7, f. A-B"] => "pl. 3, f. W, X, pl. 7, f. A-B"
                    return transformed.join(', ');
                });
            }
            html = html.replace('loc. f.', 'f.loc')
            if (html.match(/^\?ssp\./)) {
                li.remove();
                return;
            }
            // Assign modified HTML back to the <li>
            li.innerHTML = html;
        }
    });


    document.querySelector('div#arienis_u10')?.remove()
    document.querySelectorAll('div#Sancterila_Armentulus > div.NAMES > ul.SN > li')?.forEach(x => x.remove())
}

export async function fixMalformedTL(document: Document): Promise<void> {
    const nameListItems = document.querySelectorAll(`
        div.NAMES > ul.SN > li
    `);
    nameListItems.forEach((li) => {
        let html = li.innerHTML;
        const htmlText = li.textContent
        // console.log(html)
        if (html.includes('; ; <b>TL</b>: REF#x: 109;Panama')) {
            html = html.replace('; ; <b>TL</b>: REF#x: 109;Panama', '; ; <b>TL</b>: Panama');
        }
        // if (htmlText?.includes('REF#x')) {
        //     console.log(li.textContent)
        //     console.log(html)
        // }
        // Reassign the (possibly modified) HTML back to the <li>, preserving all other tags
        li.innerHTML = html;
    });
}


/**
 * Checks each "ul.SP > li > div.TN" block to ensure the publication year from
 * its <span.TN> matches the first <li> in the "div.NAMES > ul.SN" (if either exists).
 * If they don't match (or if the "SN" entry is missing the year), it injects
 * a new <li> into the first position of that "SN" list, copying the entire
 * innerHTML from <span.TN>.
 *
 * This helps maintain consistency between the displayed species name + author/year
 * in the main <span.TN> heading and the first entry in the names list (SN).
 *
 * Now enhanced to handle square brackets around the year. For example, if
 * <span class="TN"><i>Brachycyttarus subteralbatus</i> Hampson, [1893]</span>
 * has a better-matching <li> that also has "[1893]" (versus a plain "1893"),
 * that item is considered a perfect match.
 */
export async function injectMissingSNs(document: Document): Promise<void> {
    // 1) Insert a style element that makes the injected <li> items orange for easy visibility
    const styleSheet = document.createElement('style');
    styleSheet.textContent = `
        .injected-snyear {
            background-color: orange !important;
            padding: 2px 4px !important;
            border-radius: 2px !important;
        }
    `;
    document.head.appendChild(styleSheet);

    /**
     *  ------------------------------------------------------------------------
     *  We capture optional square brackets around the year. For example:
     *    "Hampson, [1893]" => author = "Hampson", year = "[1893]"
     *    "Walker, 1863"   => author = "Walker",  year = "1863"
     *    "Walker, (1863)" => author = "Walker",  year = "1863"
     *  We remove parentheses that may be wrapped around the year, but keep
     *  the square brackets if present, so "[1893]" is retained exactly.
     *  ------------------------------------------------------------------------
     */
    const authorYearRegex = /(\S+),?\s*\(?(\[?\d{4}\]?)\)?/;

    /**
     * Helper function to parse "author + year" from the text, returning a combined string
     * like "Hampson [1893]" or "Walker 1863" or null if no match. We remove outer parentheses
     * from the year but intentionally keep square brackets for exact matching.
     */
    function parseAuthorYearFromString(text: string): { author: string; year: string; combined: string } | null {
        const match = text.match(authorYearRegex);
        if (!match) {
            return null;
        }
        // Group 1 = preceding token (e.g. "Hampson"), group 2 = possibly "[1893]" or "1893"
        const rawAuthor = match[1].replace(/[)\(]/g, '').trim();

        let rawYear = match[2];
        // Remove parentheses if wrapping the year, but keep any square brackets
        if (rawYear.startsWith('(') && rawYear.endsWith(')')) {
            rawYear = rawYear.slice(1, -1);
        }

        const combined = `${rawAuthor} ${rawYear}`;
        return { author: rawAuthor, year: rawYear, combined };
    }

    // Grab all top-level TN blocks inside ul.SP (and similarly for ul.SSP)
    const divTNElements = document.querySelectorAll('ul.SP > li > div.TN, ul.SSP > li > div.TN');

    divTNElements.forEach(divTN => {
        // (1) Find the <span class="TN"> inside this div.TN
        const spanTN = divTN.querySelector<HTMLElement>('span.TN');
        if (!spanTN) return;

        // (2) Extract "author + year" (if it exists) from the <span.TN> text
        const spanText = spanTN.textContent || '';
        const spanAY = parseAuthorYearFromString(spanText);

        // (3) If there's no "names" section, do nothing
        const namesDiv = divTN.querySelector('div.NAMES');
        if (!namesDiv) return;

        const ulSN = namesDiv.querySelector('ul.SN');
        if (!ulSN) return;

        /**
         *  ------------------------------------------------------------------------
         *  Sometimes the matching li will be among subsequent list items (not just the 0th).
         *  If none match, inject. If a match is found, hoist that matching li to the top.
         *  ------------------------------------------------------------------------
         */

        const allLi = Array.from(ulSN.querySelectorAll(':scope > li'));

        // If there's no li at all and we do have a year in the span, we inject
        if (allLi.length === 0) {
            if (!spanAY) {
                return; // We have no year reference in the span anyway, so do nothing
            }
            const newLi = document.createElement('li');

            // Check for parentheses-labeled year. If so, check for special-case text
            if (spanAY.year && spanText.includes(`(${spanAY.year})`)) {
                // Known edge-case "INSERT SOMETHING"
                if (spanTN.innerHTML.trim() === 'INSERT SOMETHING') {
                    newLi.innerHTML = 'INSERT SOMETHING';
                } else {
                    // Raise an error if this doesn't match a known forced scenario
                    throw new Error(
                        `[Manual Correction Error 1] We found an entry that ends with a parenthesized year, but the content doesn't match the known edge case. Found content: ${spanTN.innerHTML}`
                    );
                }
                newLi.classList.add('injected-snyear');
                ulSN.insertBefore(newLi, ulSN.firstChild);
            } else {
                // Otherwise just copy the entire innerHTML from <span.TN> for injection
                newLi.innerHTML = spanTN.innerHTML;
                newLi.classList.add('injected-snyear');
                ulSN.insertBefore(newLi, ulSN.firstChild);
            }
            return;
        }

        // If we don't have a year in the span, we skip all injection logic.
        if (!spanAY) {
            return;
        }

        /**
         * Try to find an li whose "combined" author+year exactly matches the "spanAY.combined".
         * If found, that is our best match. If multiple exist, the first found is used.
         */
        let bestMatchIndex: number | null = null;

        allLi.forEach((li, idx) => {
            const liText = li.textContent || '';
            const liAY = parseAuthorYearFromString(liText);
            if (liAY && liAY.combined === spanAY.combined) {
                bestMatchIndex = idx;
            }
        });

        if (bestMatchIndex !== null) {
            // We have found a perfect match. If it's not already the first item, hoist it.
            if (bestMatchIndex !== 0) {
                const liToHoist = allLi[bestMatchIndex];
                ulSN.insertBefore(liToHoist, ulSN.firstChild);
            }
            // Do not inject anything, because we found a perfect match.
            return;
        }

        // If we get here, no exact match was found. We revert to original "inject if mismatch" logic.
        const firstSNLi = allLi[0];
        const liText = firstSNLi.textContent || '';
        const liAY = parseAuthorYearFromString(liText);

        // Check whether the first li is missing a year or has a different authorYear
        const mismatch = !liAY || liAY.combined !== spanAY.combined;
        if (mismatch) {
            const newLi = document.createElement('li');
            // Same special-case checks for parentheses-labeled year
            if (spanText.includes(`(${spanAY.year})`)) {
                // Known forced transformations
                if (spanTN.innerHTML.trim() === '<span class="ficheck"></span><i>Argyresthia bergiella</i> (Ratzeburg, 1840)') {
                    newLi.innerHTML = '<span class="ficheck"></span><i>Blastotere bergiella</i> Ratzeburg, 1840';
                } else if (spanTN.innerHTML.trim() === '<i>Coryphista meadii</i> (Packard, 1874)') {
                    newLi.innerHTML = '<i>Scotosia meadii</i> Packard, 1874';
                } else if (spanTN.innerHTML.trim() === '<i>Pterocypha floridata</i> (Walker, 1863)') {
                    newLi.innerHTML = '<i>Phibalapteryx floridata</i> Walker, 1863';
                } else {
                    // Not a recognized forced scenario
                    throw new Error(
                        `[Manual Correction Error 2] We found an entry that ends with a parenthesized year, but the content doesn't match the known edge case. Found content: ${spanTN.innerHTML.trim()}`
                    );
                }
                newLi.classList.add('injected-snyear');
                ulSN.insertBefore(newLi, ulSN.firstChild);
            } else {
                newLi.innerHTML = spanTN.innerHTML;
                newLi.classList.add('injected-snyear');
                ulSN.insertBefore(newLi, ulSN.firstChild);
            }
        }
    });
}

/**
 * Scans each <li> under div.NAMES > ul.SN and replaces any sequence of two or more "?" 
 * characters (like "??" or "???…") with a single "?".
 */
export async function removeDoubleQuestionMarks(document: Document): Promise<void> {
    const nameListItems = document.querySelectorAll('div.NAMES > ul.SN > li');

    nameListItems.forEach(li => {
        // Using innerHTML so that any textual occurrences of "???" get reduced to a single "?" 
        // (while preserving the existing markup within the <li>).
        li.innerHTML = li.innerHTML.replace(/\?{2,}/g, '?');
    });
}


/**
 * Removes all <span class="ficheck"></span> elements from the document.
 */
export async function removeFiCheck(document: Document): Promise<void> {
    document.querySelectorAll("span.ficheck").forEach(span => {
        span.remove();
    });
    document.querySelectorAll("span.MAP").forEach(span => {
        span.innerHTML = span.innerHTML.replace(/,\s*/g, '')
    })
}

/**
 * Remove ', ' from incomplete span.MAPS elements in the document.
 */
export async function fixIncompleteMaps(document: Document): Promise<void> {
    document.querySelectorAll("span.MAP").forEach(span => {
        span.innerHTML = span.innerHTML.replace(/,\s*/g, '')
    })
}

// Start the process
main(1).catch(console.error); // clear
// main(2651, 50).catch(console.error); // clear
// main(3051, 50).catch(console.error); // clear
// main(3201, 50).catch(console.error); // clear
// main(3351, 50).catch(console.error); // clear
// main(3401, 50).catch(console.error); // clear
// main(3451, 50).catch(console.error); // clear
// main(3501, 50).catch(console.error); // clear
// main(3551, 50).catch(console.error); // clear
// main(3601, 50).catch(console.error); // clear
// main(3751, 50).catch(console.error); // clear
// main(3851, 50).catch(console.error); // clear
// main(3901, 50).catch(console.error); // clear
// main(3951, 50).catch(console.error); // clear
// main(4001, 50).catch(console.error); // clear
// main(4051, 50).catch(console.error); // clear
// main(4101, 50).catch(console.error); // clear
// main(4151, 50).catch(console.error); // clear
// main(4201, 50).catch(console.error); // clear
// main(4251, 50).catch(console.error); // clear
// main(4301, 50).catch(console.error); // clear
// main(4351, 50).catch(console.error); // clear
// main(4401, 50).catch(console.error); // clear
// main(4451, 50).catch(console.error); // clear
// main(4501, 50).catch(console.error); // clear
// main(4551, 50).catch(console.error); // clear
// main(4601, 50).catch(console.error); // clear
// main(4651, 50).catch(console.error); // clear
// main(4701, 50).catch(console.error); // clear
// main(4751, 50).catch(console.error); // clear
// main(4801, 50).catch(console.error); // clear
// main(4851, 50).catch(console.error); // clear
// main(4901, 50).catch(console.error); // clear
// main(4951, 50).catch(console.error); // clear
// main(5001, 50).catch(console.error); // clear
// main(5051, 50).catch(console.error); // clear
// main(5101, 50).catch(console.error); // clear
// main(5151, 50).catch(console.error); // clear
// main(5201, 50).catch(console.error); // clear
// main(5251, 50).catch(console.error); // clear
// main(5301, 50).catch(console.error); // clear
// main(5351, 50).catch(console.error); // clear
// main(5401, 50).catch(console.error); // clear
// main(5451, 50).catch(console.error); // clear
// main(5501, 50).catch(console.error); // clear
// main(5551, 50).catch(console.error); // clear
// main(5601, 50).catch(console.error); // clear
// main(5651, 50).catch(console.error); // clear
// main(5701, 50).catch(console.error); // clear
// main(5751, 50).catch(console.error); // clear
// main(5801, 50).catch(console.error); // clear
// main(5851, 50).catch(console.error); // clear
// main(5901, 50).catch(console.error); // clear
// main(5951, 50).catch(console.error); // clear
// main(6001, 50).catch(console.error); // clear
// main(6051, 50).catch(console.error); // clear
// main(6101, 50).catch(console.error); // clear
// main(6151, 50).catch(console.error); // clear
// main(6201, 50).catch(console.error); // clear
// main(6251, 50).catch(console.error); // clear
// main(6301, 50).catch(console.error); // clear
// main(6351, 50).catch(console.error); // clear
// main(6401, 50).catch(console.error); // clear
// main(6451, 50).catch(console.error); // clear
// main(6501, 50).catch(console.error); // clear
// main(6551, 50).catch(console.error); // clear
// main(6601, 50).catch(console.error); // clear
// main(6651, 50).catch(console.error); // clear
// main(6701, 50).catch(console.error); // clear
// main(6751, 50).catch(console.error); // clear
// main(6801, 50).catch(console.error); // clear
// main(6851, 50).catch(console.error); // clear
// main(6901, 50).catch(console.error); // clear
// main(6951, 50).catch(console.error); // clear
// main(7001, 50).catch(console.error); // clear
// main(7051, 50).catch(console.error); // clear
// main(7101, 50).catch(console.error); // clear
// main(7151, 50).catch(console.error); // clear
// main(7201, 50).catch(console.error); // clear
// main(7251, 50).catch(console.error); // clear
// main(7301, 50).catch(console.error); // clear
// main(7351, 50).catch(console.error); // clear
// main(7401, 50).catch(console.error); // clear
// main(7451, 50).catch(console.error); // clear
// main(7501, 50).catch(console.error); // clear
// main(7551, 50).catch(console.error); // clear
// main(7601, 50).catch(console.error); // clear
// main(7651, 50).catch(console.error); // clear
// main(7701, 50).catch(console.error); // clear
// main(7751, 50).catch(console.error); // clear
// main(7801, 50).catch(console.error); // clear
// main(7851, 50).catch(console.error); // clear
// main(7901, 50).catch(console.error); // clear
// main(7951, 50).catch(console.error); // clear
// main(8001, 50).catch(console.error); // clear
// main(8051, 50).catch(console.error); // clear
// main(8101, 50).catch(console.error); // clear
// main(8151, 50).catch(console.error); // clear
// main(8201, 50).catch(console.error); // clear
// main(8251, 50).catch(console.error); // clear
// main(8301, 50).catch(console.error); // clear
// main(8351, 50).catch(console.error); // clear
// main(8401, 50).catch(console.error); // clear
// main(8451, 50).catch(console.error); // clear
// main(8501, 50).catch(console.error); // clear
// main(8551, 50).catch(console.error); // clear
// main(8601, 50).catch(console.error); // clear
// main(8651, 50).catch(console.error); // clear
// main(8701, 50).catch(console.error); // clear
// main(8751, 50).catch(console.error); // clear
// main(8801, 50).catch(console.error); // clear
// main(8851, 50).catch(console.error); // clear
// main(8901, 50).catch(console.error); // clear
// main(8951, 50).catch(console.error); // clear
// main(9001, 50).catch(console.error); // clear
// main(9051, 50).catch(console.error); // clear
// main(9101, 50).catch(console.error); // clear
// main(9151, 50).catch(console.error); // clear
// main(9201, 50).catch(console.error); // clear
// main(9251, 50).catch(console.error); // clear
// main(9301, 50).catch(console.error); // clear
// main(9351, 50).catch(console.error); // clear
// main(9401, 50).catch(console.error); // clear
// main(9451, 50).catch(console.error); // clear
// main(9501, 50).catch(console.error); // clear
// main(9551, 50).catch(console.error); // clear
// main(9601, 50).catch(console.error); // clear
// main(9651, 50).catch(console.error); // clear
// main(9701, 50).catch(console.error); // clear
// main(9751, 50).catch(console.error); // clear
// main(9801, 50).catch(console.error); // clear
// main(9851, 50).catch(console.error); // clear
// main(9901, 50).catch(console.error); // clear
// main(9951, 50).catch(console.error); // clear
// main(10001, 50).catch(console.error); // clear
// main(10051, 50).catch(console.error); // clear
// main(10101, 50).catch(console.error); // clear
// main(10151, 50).catch(console.error); // clear
// main(10201, 50).catch(console.error); // clear
// main(10251, 50).catch(console.error); // clear
// main(10301, 50).catch(console.error); // clear
// main(10351, 50).catch(console.error); // clear
// main(10401, 50).catch(console.error); // clear
// main(10451, 50).catch(console.error); // clear
// main(10501, 50).catch(console.error); // clear
// main(10551, 50).catch(console.error); // clear
// main(10601, 50).catch(console.error); // clear
// main(10651, 50).catch(console.error); // clear
// main(10701, 50).catch(console.error); // clear
// main(10751, 50).catch(console.error); // clear
// main(10801, 50).catch(console.error); // clear
// main(10851, 50).catch(console.error); // clear
// main(10901, 50).catch(console.error); // clear
// main(10951, 50).catch(console.error); // clear
// main(11001, 50).catch(console.error); // clear
// main(11051, 50).catch(console.error); // clear
// main(11101, 50).catch(console.error); // clear
// main(11151, 50).catch(console.error); // clear
// main(11201, 50).catch(console.error); // clear
// main(11251, 50).catch(console.error); // clear
// main(11301, 50).catch(console.error); // clear
// main(11351, 50).catch(console.error); // clear
// main(11401, 50).catch(console.error); // clear
// main(11451, 50).catch(console.error); // clear
// main(11501, 50).catch(console.error); // clear
// main(11551, 50).catch(console.error); // clear
// main(11601, 50).catch(console.error); // clear
// main(11651, 50).catch(console.error); // clear
// main(11701, 50).catch(console.error); // clear
// main(11751, 50).catch(console.error); // clear
// main(11801, 50).catch(console.error); // clear
// main(11851, 50).catch(console.error); // clear
// main(11901, 50).catch(console.error); // clear
// main(11951, 50).catch(console.error); // clear
// main(12001, 50).catch(console.error); // clear
// main(12051, 50).catch(console.error); // clear
// main(12101, 50).catch(console.error); // clear
// main(12151, 50).catch(console.error); // clear
// main(12201, 50).catch(console.error); // clear
// main(12251, 50).catch(console.error); // clear
// main(12301, 50).catch(console.error); // clear
// main(12351, 50).catch(console.error); // clear
// main(12401, 50).catch(console.error); // clear
// main(12451, 50).catch(console.error); // clear
// main(12501, 50).catch(console.error); // clear
// main(12551, 50).catch(console.error); // clear
// main(12601, 50).catch(console.error); // clear
// main(12651, 50).catch(console.error); // clear
// main(12701, 50).catch(console.error); // clear
// main(12751, 50).catch(console.error); // clear
// main(12801, 50).catch(console.error); // clear
// main(12851, 50).catch(console.error); // clear
// main(12901, 50).catch(console.error); // clear
// main(12951, 50).catch(console.error); // clear
// main(13001, 50).catch(console.error); // clear
// main(13051, 50).catch(console.error); // clear
// main(13101, 50).catch(console.error); // clear
// main(13151, 50).catch(console.error); // clear
// main(13201, 50).catch(console.error); // clear
// main(13251, 50).catch(console.error); // clear
// main(13301, 50).catch(console.error); // clear
// main(13351, 50).catch(console.error); // clear
// main(13401, 50).catch(console.error); // clear
// main(13451, 50).catch(console.error); // clear
// main(13501, 50).catch(console.error); // clear
// main(13551, 50).catch(console.error); // clear
// main(13601, 50).catch(console.error); // clear
// main(13651, 50).catch(console.error); // clear
// main(13701, 50).catch(console.error); // clear
// main(13751, 50).catch(console.error); // clear
// main(13801, 50).catch(console.error); // clear
// main(13851, 50).catch(console.error); // clear
// main(13901, 50).catch(console.error); // clear
// main(13951, 50).catch(console.error); // clear
// main(14001, 50).catch(console.error); // clear
// main(14051, 50).catch(console.error); // clear
// main(14101, 50).catch(console.error); // clear
// main(14151, 50).catch(console.error); // clear
// main(14201, 50).catch(console.error); // clear
// main(14251, 7).catch(console.error); // clear

// main(1, 1).catch(console.error); // agrionympha
// main(7, 1).catch(console.error); // micropterix
// main(639, 1).catch(console.error); // argyresthia
// main(672, 1).catch(console.error); // glyphipterix
// main(1000, 1).catch(console.error); // pantelamprus
// main(1998, 1).catch(console.error); // symbatica
// main(2000, 1).catch(console.error); // synactias
// main(3000, 1).catch(console.error); // simpliciavalva
// main(4000, 1).catch(console.error); // eurybia
// main(4032, 1).catch(console.error); // calephelis
// main(5000, 1).catch(console.error); // parasarpa
// main(6000, 1).catch(console.error); // difundella
// main(7000, 1).catch(console.error); // loxomorpha
// main(8000, 1).catch(console.error); // bustilloxia
// main(8319, 1).catch(console.error); // coryphista 
// main(8320, 1).catch(console.error); // archirhoe // ! NEEDS MANUAL ENTRY; missing genus TN entry; type species unknown, reference missing... I say skip this one because it is so poorly formatted
// main(8321, 1).catch(console.error); // pterocypha // ! NEEDS MANUAL REVIEW; data inaccurate according to forum herbulot
// main(9000, 1).catch(console.error); // picrophylla
// main(10000, 1).catch(console.error); // pyripnoa
// main(10591, 1).catch(console.error); // agrocholorta
// main(10837, 1).catch(console.error); // morrisonia
// main(11000, 1).catch(console.error); // selambina
// main(12000, 1).catch(console.error); // paracroma
// main(12683, 1).catch(console.error); // catocala
// main(13000, 1).catch(console.error); // synalamis
// main(13334, 1).catch(console.error); // eilema
// main(13439, 1).catch(console.error); // cyana
// main(13627, 1).catch(console.error); // amata
// main(13923, 1).catch(console.error); // grammia
// main(14257, 1).catch(console.error); // vulsinia


// ! NOTE: Geometridae is pretty bad in funet; we should rely on Forum Herbulot
// ! There is a problem handling figures that are not preceeded by "pl. X, "
// Detritivora gynaea e.g. ": 641, f. 2C-D, 4D, 6C, 9;" does not parse figures correctly
