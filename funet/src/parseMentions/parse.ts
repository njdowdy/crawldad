import { ParsedBlock } from "../parseBlocks";
import { extractPageSubelements } from "../parseNameBlock";
import { generateArticleCacheKey, getArticleByCitedAs, getArticleByFunetRefLink, upsertArticle } from "../parseRelatedLiterature/articleCacheManager";
import { extractAuthorshipString, parseAuthorshipString } from "../parseRelatedLiterature/authorshipUtilities";
import { addPageToCache, generatePageCacheKey, getPageFromCache } from "../parseRelatedLiterature/pageCacheManager";
import { upsertPersonToCache } from "../parseRelatedLiterature/personCacheManager";
import { addOrUpdateWebResource, getWebResourceByFunetRefLink, upsertWebResourceSubelement } from "../parseWebResource/webResourceCacheManager";
import { Article, DataConnection, LinkedResource, Page, PageSubelement, TaxonRecord } from "../types";
import { uuidv7 } from "uuidv7";

/**
 * Main entry point: Takes a DOM element, extracts the text content, splits it on semicolons,
 * and then delegates each part to either parseAsWebMention or parseAsLiteratureMention.
 */
export function parseMentionBlock(element: Element): DataConnection | undefined {
    const rawText = element.textContent?.trim();
    if (!rawText) {
        return undefined;
    }

    // Split on semicolons
    const allParts = rawText.split(";").map(part => part.trim()).filter(Boolean);

    // If there's fewer than 2 parts, there's not really anything to parse
    if (allParts.length < 2) {
        return undefined;
    }

    let mentionedAs = ''
    let mentionItems: string[]
    // Test if first entry contains a name or not
    if (!rawText.match(/^\s*;/g)) {
        // If the rawText does not start with "; ...", the first item is the 'mentionedAs' (e.g., "Micropterix aglaella")
        mentionedAs = allParts[0];
        mentionItems = allParts.slice(1); // and all other items are citations
    } else {
        // If the rawText starts with "; ...", there is no mentioned name given so we set it to: '' above
        mentionItems = allParts.slice(0); // and all items are citations
    }
    // Everything after that is a separate mention item

    const linkedResources: LinkedResource[] = [];

    for (const item of mentionItems) {
        // Now we pass the DOM element to "isWebMention" to check if there's an anchor with a numeric reference.
        if (isWebMention(item, element)) {
            const webResource = parseAsWebMention(item, element);
            linkedResources.push(webResource);
        } else {
            const literatureResources = parseAsLiteratureMention(item, element);
            linkedResources.push(...literatureResources);
        }
    }

    return {
        id: uuidv7(),
        dataConnectionType: "applied_name",
        mentionedAs,
        linkedResources
    };
}

export function isAbbreviatedLiteratureMention(text: string, containerElement: Element): boolean {
    const possibleAnchor = Array.from(containerElement.querySelectorAll('a'))
        .find(a => (a.textContent || '').includes(text.trim()));
    // if (text.includes('RFEL')) {
    //     console.log("possibleAnchor:", possibleAnchor?.outerHTML)
    // }
    if (possibleAnchor) {
        const href = possibleAnchor.getAttribute('href') || '';
        if (/^#[0-9]+/.test(href)) {
            return true;  // This is an abbreviated literature mention
        }
    }
    return false;
}

/**
 * Simple helper to decide if an item is a "web mention."
 * We consider it a web mention if its entire content is within square brackets.
 */
export function isWebMention(item: string, containerElement: Element): boolean {
    const trimmed = item.trim();

    // 1) Check if there's an <a> tag containing this text, with an href that starts with "#[0-9]+"
    //    If so, we consider it an abbreviated literature mention, so return false.
    const isLiterature = isAbbreviatedLiteratureMention(trimmed, containerElement)
    // if (item.includes('RFEL')) {
    //     console.log("RFEL text:", item)
    //     console.log("RFEL literature:", isLiterature)
    //     console.log("containerElement:", containerElement.outerHTML)
    // }
    // 2) Otherwise, fall back to the original check for bracketed text.
    if (isLiterature) {
        return false;
    } else {
        return trimmed.startsWith("[") && trimmed.endsWith("]");
    }
}

/**
 * Stub function that will eventually parse a single web mention.
 */
export function parseAsWebMention(mentionText: string, containerElement: Element): LinkedResource {
    // 1) Extract the abbreviation (the text inside the square brackets).
    const abbreviation = mentionText.slice(1, -1).trim();

    // 2) Find the resourceLink by locating the <span class="ext"> (which should contain our bracketed text)
    //    and then reading the href of any <a> child, if present.
    let resourceLink: string | undefined;
    const extSpans = Array.from(containerElement.querySelectorAll('span.ext'));
    for (const span of extSpans) {
        if ((span.textContent || '').includes(mentionText)) {
            const anchors = span.querySelectorAll('a');
            for (const anchor of anchors) {
                const href = anchor.getAttribute('href');
                if (href?.startsWith('http')) {
                    resourceLink = href;
                    break;
                }
            }
            break;
        }
    }

    // 3) Check for an existing web resource by the abbreviation, or create a new one if none is found.
    let webResource = getWebResourceByFunetRefLink(abbreviation);
    if (!webResource) {
        webResource = {
            id: uuidv7(),
            abbreviation,
            resourceName: abbreviation,
            webSubelement: []
        };
        addOrUpdateWebResource(webResource);
    }
    let sourceId = webResource.id;

    // 4) If a resourceLink was found, upsert it as a subelement.
    if (resourceLink) {
        const subId = upsertWebResourceSubelement(abbreviation, resourceLink, "web_page");
        if (subId) {
            sourceId = subId;
        }
    }

    // 5) Construct and return the LinkedResource object pointing to the (sub)resource.
    return {
        id: uuidv7(),
        source: sourceId,
        resource_type: "web"
    };
}

/**
 * Parses a literature-style mention of the form:
 * "Zeller-Lukashort, Kurz & Kurz, 2007, Nota Lepid. 30 (2): 265, 249 (list)"
 * and returns a list of LinkedResource objects. Callers can then "push" these
 * into their main mentions list afterward.
 */
export function parseAsLiteratureMention(mentionText: string, element: Element): LinkedResource[] {
    // ----------------------------------------------------------------
    // First normalize the mentionText before proceeding.
    // ----------------------------------------------------------------
    if (!mentionText.includes(':')) {
        mentionText = normalizeMentionText(mentionText);
    }

    // Remove a lone '?' when it's right before a 4-digit year:
    mentionText = mentionText.replace(/\?(\d{4})/g, '$1');

    // This list will store the final LinkedResource entries.
    const mentionDetailList: LinkedResource[] = [];
    // console.log("mentionText:", mentionText)

    /**
     * 1) We first attempt to extract the "refDetails" substring by looking for any valid year pattern
     *    (e.g., 1892, 1892?, 1892], 1892?], 1892?]?), followed by a comma, then capturing everything
     *    up to the first colon.
     *
     *    Examples:
     *      "Kirby, 1892, Syn. Cat. Lepid. Het. 1:"    -> refDetails = "Syn. Cat. Lepid. Het. 1"
     *      "Kirby, 1892?], Syn. Cat. Lepid. Het. 1:"  -> refDetails = "Syn. Cat. Lepid. Het. 1"
     *      "Kirby, 1892?]?, Syn. Cat. Lepid. Het. 1:" -> refDetails = "Syn. Cat. Lepid. Het. 1"
     */
    let refDetails: string | undefined;
    const initialRegex = /^(?:.*?)\d{4}[?\]]*,\s*([^:]+):/;
    let detailMatch = mentionText.match(initialRegex);

    const citation = mentionText.match(/^([A-Z][\s\S]*?\d{4}\]?\)?)/);

    while (!detailMatch) {
        // First iteration, we attempt the "else block" logic (altDetailRegex).
        const altDetailRegex = /^\s*(\[[A-Z0-9]+\]).*$/;
        const altDetailMatch = mentionText.match(altDetailRegex);
        if (altDetailMatch) {
            // We've found something matching altDetailRegex; set refDetails and break out.
            refDetails = altDetailMatch[1].trim();
            break;
        }

        // If we're still here and no detailMatch yet, try a second detailRegex.
        const secondRegex = /^(?:.*?)\d{4}[?\]]*,\s*([^:]+)$/;
        const secondMatch = mentionText.match(secondRegex);
        if (secondMatch) {
            // Assign this to detailMatch, break, and handle it outside the loop.
            detailMatch = secondMatch;
            break;
        }

        // If we still haven't found a match, bail out entirely.
        break;
    }

    // Once out of the loop, if detailMatch is found, set refDetails from detailMatch[1].
    if (detailMatch) {
        refDetails = detailMatch[1].trim();
    }

    // 2) If we didn't match or can't determine refDetails, we fall back to the existing parentheses logic.
    let textBeforeColon: string;
    if (!refDetails) {
        // Original fallback: simply grab everything before the first colon (or the entire string if no colon).
        textBeforeColon = mentionText.split(":")[0] || mentionText;
        textBeforeColon = textBeforeColon.trim();
        refDetails = textBeforeColon;
    } else {
        // We got a valid refDetails from the loop logic, so that's our "textBeforeColon".
        textBeforeColon = refDetails;
    }

    // 2.1) Grab all parentheses in refDetails to see if there's a single or multiple sets:
    const allParenMatches = refDetails.match(/\([^)]*\)/g) || [];

    /**
     * 3) If we found parentheses, refine "refDetails" by including everything up to and including the last parentheses group,
     *    or only parentheses group, if there's exactly one. Otherwise we keep it as is.
     */
    if (allParenMatches.length > 0) {
        // If multiple parentheses sets, pick the last one; if only one, pick that single one.
        const captureParen = allParenMatches.length === 1
            ? allParenMatches[0]
            : allParenMatches[allParenMatches.length - 1];

        // Include everything from the start through the end of that parentheses group.
        const lastIndex = refDetails.lastIndexOf(captureParen);
        refDetails = refDetails.slice(0, lastIndex + captureParen.length).trim();
    }
    // 4) Determine issueNumber by using:
    //    • the last parentheses content if multiple
    //    • the single parentheses content if exactly one
    let issueNumber: string | undefined = "";
    if (allParenMatches.length > 1) {
        const lastParenContent = allParenMatches[allParenMatches.length - 1]
            .replace(/^\(/, "")
            .replace(/\)$/, "")
            .trim();
        issueNumber = lastParenContent;
    } else if (allParenMatches.length === 1) {
        issueNumber = allParenMatches[0]
            .replace(/^\(/, "")
            .replace(/\)$/, "")
            .trim();
    }

    // 5) Attempt to locate a <a href="#..."> in the DOM that might correspond to our refDetails.
    //    But first, define a "safe" version of refDetails so TS knows it's a string.
    const safeRefDetails = refDetails ?? "";

    let funetRefLink: string | undefined;
    const linkElements = element.querySelectorAll('a');

    // We'll make up to three attempts to match anchorText:
    // 1) refDetails as is
    // 2) refDetails truncated up to the last '('
    // 3) refDetails truncated up to the first ')'
    // 4) textBeforeColon
    // 5) capture text up to the first digit
    // 6) capture text up to the first '('
    // 7) capture text between square brackets
    linkElements.forEach(anchor => {
        if (funetRefLink) return; // If we've already found a match, skip further processing.

        const anchorText = (anchor.textContent || '').replace(/\s+/g, ' ').trim();

        // Create our attempts array:
        const attempts: string[] = [];

        // Attempt #1: the entire refDetails
        attempts.push(safeRefDetails);

        // Attempt #2: capture text up to the last '('
        const lastParenIndex = safeRefDetails.lastIndexOf('(');
        if (lastParenIndex !== -1) {
            attempts.push(safeRefDetails.substring(0, lastParenIndex).trim());
        }

        // Attempt #3: capture text up to the first ')'
        const firstParenIndex = safeRefDetails.indexOf(')');
        if (firstParenIndex !== -1 && firstParenIndex < safeRefDetails.length) {
            attempts.push(safeRefDetails.substring(0, firstParenIndex + 1).trim());
        }

        // Attempt #4: textBeforeColon
        attempts.push(textBeforeColon);

        // Attempt #5: capture text up to the first digit
        {
            const match = safeRefDetails.match(/^[^\d]+/);
            if (match) {
                attempts.push(match[0].trim());
            }
        }

        // Attempt #6: capture text up to the first '('
        {
            const match = safeRefDetails.match(/^[^(]+/);
            if (match) {
                attempts.push(match[0].trim());
            }
        }

        // Attempt #7: capture text between square brackets
        {
            const match = safeRefDetails.match(/(\[.*?\])/);
            if (match) {
                attempts.push(match[1].trim());
            }
        }

        // Now check each attempt
        // if (mentionText.includes('Birchall')) {
        //     console.log("anchorText:", anchorText)
        // }
        for (const candidate of attempts) {
            // if (mentionText.includes('Sattler, 1973')) {
            //     console.log("anchorText:", anchorText)
            //     console.log("candidate:", candidate)
            //     console.log("IS_EQUAL:", anchorText === candidate)
            // }

            if (anchorText.replace('  ', ' ').trim() === candidate.replace('  ', ' ').trim()) {
                const maybeHref = anchor.getAttribute('href') || '';
                if (maybeHref.startsWith('#')) {
                    funetRefLink = maybeHref.replace('#', '');
                }
                break; // Stop checking attempts once we match
            }
        }
    });

    // if (mentionText.includes('Birchall')) {
    //     console.log("mentionText:", mentionText)
    //     console.log("safeRefDetails:", safeRefDetails)
    //     console.log("funetRefLink:", funetRefLink)
    // }

    // 6) Check for an existing article or create a new one
    let article: Article = {
        id: uuidv7(),
        funetRefLink,
        title: '',
        citedAs: [],
        pages: [],
    };
    if (funetRefLink) {
        if (issueNumber === "Suppl.") {
            issueNumber = "supplement";
        }
        // if (mentionText.includes('Issiki')) {
        //     console.log("issueNumber:", issueNumber)
        // }
        const existingArticle = getArticleByFunetRefLink(funetRefLink, issueNumber ?? '');
        if (existingArticle) {
            article = existingArticle;
        }
    } else {
        let citedAsArray: string[] | undefined;

        if (citation) {
            // console.log("citation:", citation[0])
            const { authorshipString, citedAs } = extractAuthorshipString(citation[0]);
            // console.log("citedAs:", citedAs)
            citedAsArray = citedAs;
        }
        if (citedAsArray === undefined) {
            // console.log("mentionText:", mentionText)
            // console.log("refDetails:", refDetails)
            // console.log("citation:", citation)
            // console.log("safeRefDetails:", safeRefDetails)
            // handle malformed references e.g., "Eurybia (Eurybiina); Stichel" by returning nothing
            if (safeRefDetails.match(/^[\p{L}\.\s\-']+$/u)) { return [] }
        } else {
            // Parse authors from the string (e.g. "Johnson, Daniels & Walsingham, 1889", etc.)
            // console.log("authorshipString4:", citedAsArray)
            // if (citedAsArray[0] === undefined) {
            //     console.log("TEST2:", citedAsArray[0])
            //     console.log("TEST2:", mentionText)
            // }
            const existingArticle = getArticleByCitedAs(citedAsArray[0]);
            // if (citedAsArray[0].includes('Lower, 1905')) {
            //     console.log("citedAs:", citedAsArray[0])
            //     console.log("existingArticle:", existingArticle)
            // }
            if (existingArticle) {
                // We've already got an article with this citation
                article = existingArticle;
            } else {
                const authorshipParsed = parseAuthorshipString(citedAsArray[0]);

                // Create a new Article object
                article.citedAs = citedAsArray;
                article.authors = authorshipParsed.authorship.map((entry, index) => {
                    const person = upsertPersonToCache(entry.author);
                    return { position: index, author: person };
                });

                // Optionally store the numeric year and bracket info, if you like:
                if (authorshipParsed.year) {
                    const journal = { id: uuidv7(), title: '', abbreviation: '' }
                    const volume = { id: uuidv7(), year: authorshipParsed.year, label: '', bracketedYear: authorshipParsed.bracketedYear, journal }
                    article.issue = { id: uuidv7(), volume, label: '' };
                }

                // This upsert ensures the article is cached,
                // and each Person is added to the PersonCache by parseAuthorshipString
                upsertArticle(article, citedAsArray[0]);
            }
        }
    }

    // 7) Split out the page/figure info (everything after the first colon)
    const colonIndex = mentionText.indexOf(':');
    const pageSection = colonIndex !== -1
        ? mentionText.substring(colonIndex + 1).trim()
        : '';

    // if (mentionText.includes('Birchall')) {
    //     console.log("article:", article)
    // }

    // If no page/figure info, just link to the article and return.
    if (!pageSection) {
        finalizeArticleUpsert(article);
        mentionDetailList.push({
            id: uuidv7(),
            source: article.id,
            resource_type: "literature"
        });
        return mentionDetailList;
    }

    // 8) Handle page/figure parsing
    const pageRefs = pageSection.split(/,(?!\s*f\.)/).map(s => s.trim());
    // Merge pageRef items if one contains "f." and the following do not start with "pl." or "f."
    const mergedPageRefs = consolidateFigureReferences(pageRefs);

    // if (pageSection.includes("♀")) {
    //     console.log("pageSection:", pageSection)
    //     console.log("pageRefs:", pageRefs)
    //     console.log("mergedPageRefs:", mergedPageRefs)
    // }

    let anyPageOrSubelementLinked = false;

    mergedPageRefs.forEach(pageRefString => {
        // Preliminary cleanup
        if (!pageRefString) return;
        if (pageRefString.includes("nr. ")) {
            // this is a "number", usually in a list, but I really do not care to break other stuff for this, so we ignore
            return
        }
        if (pageRefString.startsWith("#")) {
            // this is a "number", usually in a list or an ID like Hodges, but I really do not care to break other stuff for this, so we ignore
            return
        }
        if (pageRefString.includes("r. ")) {
            // no idea what this is, so ignore
            return
        }
        if (pageRefString.includes("t. ")) {
            // no idea what this is, so ignore
            return
        }
        if (pageRefString.includes("m. ")) {
            // no idea what this is, so ignore
            return
        }
        if (pageRefString.includes("?")) {
            // no idea what this is, so ignore
            pageRefString = pageRefString.replace(/\s*\?\s*/, '').trim()
        }
        // no idea what square brackets mean, so ignore
        pageRefString = pageRefString.replace('[', '').replace(']', '').trim()

        // If this reference has subelements, parse them out.
        const subelements = extractPageSubelements(pageRefString);
        if (subelements && subelements.length) {
            anyPageOrSubelementLinked = true;
            subelements.forEach(({ reference }) => {
                const upsertedPage = upsertPageDirectly(article, reference);
                upsertedPage.label = upsertedPage.label.replace(/ #[0-9A-Za-z]+/, "").trim()
                if (reference.subelements && reference.subelements.length) {
                    reference.subelements.forEach(sub => {
                        const subId = upsertSubelementDirectly(upsertedPage, sub);
                        if (subId) {
                            mentionDetailList.push({
                                id: uuidv7(),
                                source: subId,
                                resource_type: "literature"
                            });
                        }
                    });
                } else if (upsertedPage.id) {
                    mentionDetailList.push({
                        id: uuidv7(),
                        source: upsertedPage.id,
                        resource_type: "literature"
                    });
                }
            });
        } else {
            // Handle non-subelement pages
            const cleaned = pageRefString.toLowerCase();
            const isPlate = cleaned.startsWith('pl.');
            const newPage: Page = {
                id: undefined,
                label: isPlate ? pageRefString.replace(/^pl\.\s*/, '') : pageRefString,
                type: isPlate ? 'plate' : 'page',
                subelements: []
            };
            const upsertedPage = upsertPageDirectly(article, newPage);

            // Check if the reference also has figure(s)
            const figureMatch = pageRefString.match(/f\.\s*(.*)/);
            if (figureMatch && figureMatch[1]) {
                const allFigs = parseFigureRange(figureMatch[1]);
                allFigs.forEach(figureLabel => {
                    const sub: PageSubelement = {
                        id: undefined,
                        label: figureLabel,
                        type: 'illustration'
                    };
                    const subId = upsertSubelementDirectly(upsertedPage, sub);
                    if (subId) {
                        mentionDetailList.push({
                            id: uuidv7(),
                            source: subId,
                            resource_type: "literature"
                        });
                    }
                });
                anyPageOrSubelementLinked = true;
            } else if (upsertedPage.id) {
                mentionDetailList.push({
                    id: uuidv7(),
                    source: upsertedPage.id,
                    resource_type: "literature"
                });
                anyPageOrSubelementLinked = true;
            }
        }
    });

    // 9) If no pages or subelements linked, add a link to the article itself
    if (!anyPageOrSubelementLinked && article.id) {
        mentionDetailList.push({
            id: uuidv7(),
            source: article.id,
            resource_type: "literature"
        });
    }

    // 10) Finalize changes to the article
    finalizeArticleUpsert(article);

    return mentionDetailList;
}

/**
 * Normalizes strings
 * Examples:
 *   1) "Meyrick, 1914, 28 (syn.)" => "Meyrick, 1914: 28 (syn.)"
 *   2) "Heppner, 1982, b 222, 257" => "Heppner, 1982: 222, 257"
 *   3) "Inoue, 1954, 50, nr. 219" => "Inoue, 1954: 50"
 *   4) "Issiki, 1957, in Esaki et al., 34, pl. 5, f. 137" => "Issiki in Esaki et al., 1957: 34, pl. 5, f. 137"
 */
function normalizeMentionText(original: string): string {
    let text = original;

    // --------------------------------------------------------
    // 1) If there's a pattern like ", [year], <stuff>" => 
    //    replace the comma following the year with a colon.
    //    We'll do this carefully so it won't conflict with the
    //    "Issiki, 1957, in Esaki et al." pattern below.
    // --------------------------------------------------------
    // We'll handle the "in <author(s)>" special case separately.

    // handle "Issiki, 1957, in Esaki et al., 34, pl. 5, f. 137"
    // we want to move "1957" after "in Esaki et al.," => "Issiki in Esaki et al., 1957: 34, pl. 5, f. 137"
    // strategy: check if there's "in [some text], [year]," => rearrange

    // FIRST: detect the pattern "([A-Za-z]+), (19XX|20XX), in ..."
    // and reorder if found.
    // This captures "Issiki, 1957, in Esaki et al.," etc.
    const inPattern = /^([^,]+),\s*(\d{4}),\s*in\s+([^,]+),\s*(.*)$/;
    const inMatch = text.match(inPattern);
    if (inMatch) {
        const authorPart = inMatch[1];   // e.g. "Issiki"
        const yearPart = inMatch[2];     // e.g. "1957"
        const inAuthors = inMatch[3];    // e.g. "Esaki et al."
        const rest = inMatch[4];        // e.g. "34, pl. 5, f. 137"

        // reconstruct => "Issiki in Esaki et al., 1957, 34, pl. 5, f. 137"
        text = `${authorPart} in ${inAuthors}, ${yearPart}, ${rest}`;
    }

    // Now replace the comma after the year with a colon.
    // We'll look for "<year>, " => "<year>:" if a year is followed by another numeric or page info.
    const yearCommaPattern = /(\b\d{4}\b)\s*,/;
    text = text.replace(yearCommaPattern, `$1:`);

    // --------------------------------------------------------
    // 2) Remove single letters surrounded by spaces (like "b 222")
    //    except if there might be a year or something else. 
    //    We'll do a simple approach: replace " [a-zA-Z] " -> " ".
    // --------------------------------------------------------
    // This will handle "b 222" => " 222"
    // Then we'll do a trim at the end to clean up extra spaces.
    text = text.replace(/\s+[a-zA-Z]\s+/g, ' ');

    // --------------------------------------------------------
    // 3) If there's ", nr." => remove everything from "nr." to the end
    //    e.g. "Inoue, 1954: 50, nr. 219" => "Inoue, 1954: 50"
    // --------------------------------------------------------
    // We'll detect "(\s+nr\.\s*\d+.*)" after a comma or a colon:
    const nrPattern = /(,\s*nr\.\s*\d.*$)/;
    text = text.replace(nrPattern, '');
    // We'll detect "(\s+no\.\s*\d+.*)" after a comma or a colon:
    const noPattern = /(,\s*no\.\s*\d.*$)/;
    text = text.replace(noPattern, '');

    // Check if there's a second pattern for "nr." after the first run:
    const nrPattern2 = /(:\s*\d+,\s*nr\.\s*\d.*$)/;
    text = text.replace(nrPattern2, match => match.replace(/,\s*nr\.\s*\d+.*/, ''));

    // --------------------------------------------------------
    // 4) If there's a bracket abbreviation "[ABCD]" followed by a comma and digits,
    //    e.g. "[MP7], 253" => "[MP7]". Strip away the trailing numeric portion.
    // --------------------------------------------------------
    text = text.replace(/(\[[^\]]+\])\s*,?\s*\d+(\s.*)?/g, '$1');
    text = text.replace(/(\[[^\]]+\])\s*,\s*\[.*-season\]/g, '$1'); //[PBSA], [dry-season] -> [PBSA]

    // --------------------------------------------------------
    // 5) Trim trailing commas/spaces, ensure final clean-up
    // --------------------------------------------------------
    text = text.replace(/\s*,\s*$/, '').trim();

    return text;
}

function upsertPageDirectly(article: Article, candidatePage: Page): Page {
    if (!article.pages) {
        article.pages = [];
    }
    if (!candidatePage.id) {
        candidatePage.id = uuidv7();
    }

    const pageCacheKey = generatePageCacheKey(candidatePage, {
        title: article.title,
        citedAs: article.citedAs,
        issue: article.issue
    });

    const existing = getPageFromCache(pageCacheKey);
    let finalPage: Page;

    if (existing) {
        finalPage = mergePages(existing, candidatePage);
        finalPage.label = finalPage.label.replace(' ?', '').replace('incertae sedis', '').replace(' (list', '').replace(/ key/, '').replace(/ \(note/, '').replace(/^insert /, '').replace(/^gen\. /, '').replace(/ implied$/, '').replace(/^\.\s*/, '').replace(/^\(/, '').replace(/\)$/, '')
        addPageToCache(pageCacheKey, finalPage);
    } else {
        finalPage = candidatePage;
        finalPage.label = finalPage.label.replace(' ?', '').replace('incertae sedis', '').replace(' (list', '').replace(/ key/, '').replace(/ \(note/, '').replace(/^insert /, '').replace(/^gen\. /, '').replace(/ implied$/, '').replace(/^\.\s*/, '').replace(/^\(/, '').replace(/\)$/, '')
        addPageToCache(pageCacheKey, finalPage);
    }

    const existingInArticle = article.pages.find((p: Page) => p.id === finalPage.id);
    if (!existingInArticle) {
        article.pages.push(finalPage);
    } else {
        Object.assign(existingInArticle, mergePages(existingInArticle, finalPage));
    }

    return finalPage;
}

function upsertSubelementDirectly(page: Page, sub: PageSubelement): string | undefined {
    if (!page.subelements) {
        page.subelements = [];
    }
    if (!sub.id) {
        sub.id = uuidv7();
    }

    const existing = page.subelements.find(
        (s) => (s.type === sub.type && s.label === sub.label)
    );
    if (existing) {
        existing.labelText = existing.labelText || sub.labelText;
        existing.link = existing.link || sub.link;
        if (!existing.id) {
            existing.id = sub.id;
        }
        return existing.id;
    } else {
        page.subelements.push(sub);
        return sub.id;
    }
}

function mergePages(existing: Page, incoming: Page): Page {
    const merged: Page = {
        id: existing.id || incoming.id,
        label: existing.label || incoming.label,
        type: existing.type || incoming.type,
        content: existing.content || incoming.content,
        link: existing.link || incoming.link,
        subelements: []
    };

    if (!existing.subelements) existing.subelements = [];
    if (!incoming.subelements) incoming.subelements = [];

    const subelementsMap = new Map<string, PageSubelement>();
    const makeKey = (s: PageSubelement) => `sub__${s.type || ''}__${s.label || ''}`.toLowerCase();

    existing.subelements.forEach(s => {
        if (!s.id) s.id = uuidv7();
        subelementsMap.set(makeKey(s), s);
    });

    incoming.subelements.forEach(s => {
        const key = makeKey(s);
        if (!s.id) s.id = uuidv7();

        if (!subelementsMap.has(key)) {
            subelementsMap.set(key, s);
        } else {
            const mergedSub = subelementsMap.get(key)!;
            mergedSub.labelText = mergedSub.labelText || s.labelText;
            mergedSub.link = mergedSub.link || s.link;
        }
    });

    merged.subelements = [...subelementsMap.values()];
    return merged;
}

function finalizeArticleUpsert(article: any) {
    if (!article.id) {
        article.id = uuidv7();
    }
    const articleCacheKey = generateArticleCacheKey(article);
    upsertArticle(article, articleCacheKey);
}

function parseFigureRange(figureText: string): string[] {
    const result: string[] = [];
    const parts = figureText.split(',').map(p => p.trim());
    parts.forEach(part => {
        if (part.includes('-')) {
            const [start, end] = part.split('-').map(n => n.trim());
            const startNum = parseInt(start, 10);
            const endNum = parseInt(end, 10);
            if (!isNaN(startNum) && !isNaN(endNum) && startNum <= endNum) {
                for (let i = startNum; i <= endNum; i++) {
                    result.push(i.toString());
                }
            }
        } else {
            result.push(part);
        }
    });
    return result;
}

function consolidateFigureReferences(pageRefs: string[]): string[] {
    const result: string[] = [];
    let current: string | null = null;

    for (const pageRef of pageRefs) {
        if (current === null) {
            // First item in the loop
            current = pageRef;
        } else {
            // If the current entry contains "f." AND the new pageRef does NOT start with "pl." or "f."
            // then merge them, otherwise finalize the current entry and move on.
            if (/\bf\.\s*/.test(current) && !/^(pl\.|f\.)\s*/i.test(pageRef)) {
                current = `${current}, ${pageRef}`;
            } else {
                result.push(current);
                current = pageRef;
            }
        }
    }

    // Push the final item if it exists
    if (current !== null) {
        result.push(current);
    }

    return result;
}

export function parseMentions(parsedBlocks: ParsedBlock[], taxa: TaxonRecord[]): TaxonRecord[] {
    const taxaWithInjectedMentions = structuredClone(taxa);

    const blocksByName = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        if (block.blockName) {
            blocksByName.set(block.blockName, block);
        }
    });

    const mentionsBlocks = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        if (block.blockType === "MentionsBlock") {
            mentionsBlocks.set(block.blockName, block);
        }
    });

    mentionsBlocks.forEach(mentionsBlock => {
        let parentData: TaxonRecord | null
        const validParentBlock = blocksByName.get(`${mentionsBlock.parentBlock}_NameBlock_1`);
        parentData = validParentBlock?.parsedBlockData

        if (parentData?.id) {
            const taxonToUpdate = taxaWithInjectedMentions.find(taxon => taxon.id === parentData?.id);

            if (taxonToUpdate && mentionsBlock.parsedBlockData) {
                if (!taxonToUpdate.dataConnections) {
                    taxonToUpdate.dataConnections = [];
                }
                taxonToUpdate.dataConnections.push(mentionsBlock.parsedBlockData);
            }
        }
    });

    return taxaWithInjectedMentions;
}