import { uuidv7 } from "uuidv7";
import { getOrCreateLocality } from "../parseDistributions/typeLocalityCacheManager";

export interface ParentBlockData {
    id: string,
    acceptedNameUsage: string | undefined;
    assertedDistribution: string[] | undefined;
    vernacularNames: { language: string, vernacularNames: string[] }[] | undefined;
    typeLocality?: string;
}

// ! I don't really know WTF funet is doing with nominate subspecies TBH
// ! e.g., Grammia quenseli has synonyms, but Grammia quenseli quenseli has different ones
export function parseParentBlock(element: Element, blockType: string): ParentBlockData {
    const text = element.textContent || '';
    const lines = text.split('\n').map(line => line.trim());

    // Parse accepted name usage from first line
    let acceptedNameUsage = lines[0] || undefined;
    if (blockType === 'SubgenusBlock') {
        acceptedNameUsage = acceptedNameUsage?.replace('Subgenus', '').trim()
    }

    // // Remove asserted distribution information after the year
    // if (acceptedNameUsage) {
    //     // First capture everything up to (and including) the year + optional parentheses/brackets
    //     const regex = /(?:[0-9]+|MS)\]?\)?(.*)$/
    //     const match = acceptedNameUsage.match(regex);
    //     if (match) {
    //         acceptedNameUsage = acceptedNameUsage.replace(match[1], '').trim();
    //     }
    //     if (acceptedNameUsage.includes('collocalia')) {
    //         console.log("acceptedNameUsage:", acceptedNameUsage)
    //     }
    // }

    // Parse distribution from second line or from the removed part of first line
    let assertedDistribution: string[] | undefined;

    // Parse distribution from MAP span
    const mapSpan = element.querySelector('span.MAP');
    if (mapSpan?.textContent) {
        // Remove asserted distribution information from acceptedNameUsage
        if (acceptedNameUsage) { acceptedNameUsage = acceptedNameUsage?.replace(mapSpan?.textContent, '').trim() }
        assertedDistribution = mapSpan.textContent
            .trim()
            .replace(/\[see maps\]$/i, '') // Remove [see maps] from the end
            .replace(/See \[maps\]$/i, '') // Remove See [maps] from the end
            // Split by commas or semicolons, but not within parentheses
            .split(/[,;]\s*(?![^(]*\))/)
            .map(location => location
                // Handle compound directions first
                .replace(/\bS.Africa\b/g, 'South Africa')
                .replace(/\bNW\./g, 'Northwestern ')
                .replace(/\bNE\./g, 'Northeastern ')
                .replace(/\bSW\./g, 'Southwestern ')
                .replace(/\bSE\./g, 'Southeastern ')
                // Then handle single directions
                .replace(/\bN\./g, 'Northern ')
                .replace(/\bS\./g, 'Southern ')
                .replace(/\bE\./g, 'Eastern ')
                .replace(/\bW\./g, 'Western ')
                .replace(/\bC\./g, 'Central ')
                // Handle North America abbreviations
                .replace(/\bNA\./g, '(North America) ') // e.g., Georgia (North America) vs Georgia (Europe)
                .replace(/\bWNA\b/g, 'Western North America')
                .replace(/\bAU\b/g, 'Australia')
                .replace(/\bAP\b/g, 'Palearctic Asia')
                .replace(/\bAS\b/g, 'Tropical Asia')
                .replace(/\bAC\b/g, 'Central Asia')
                .replace(/\bAN\b/g, 'Northern Asia')
                .replace(/\bAM\b/g, 'Asia Minor')
                .replace(/\bAF\b/g, 'Africa')
                .replace(/\bEU\b/g, 'Europe')
                .replace(/\bSEU\b/g, 'Southern Europe')
                .replace(/\bCEU\b/g, 'Central Europe')
                .replace(/\bNAF\b/g, 'Northern Africa')
                .replace(/\bSAF\b/g, 'Southern Africa')
                .replace(/\bEAF\b/g, 'Eastern Africa')
                .replace(/\bTAF\b/g, 'Tropical Africa')
                .replace(/\bSSAF\b/g, 'Subsaharan Africa')
                .replace(/\bC\.A\.R\.\b/g, 'Central African Republic')
                .replace(/\bENA\b/g, 'Eastern North America')
                .replace(/\bNA\b/g, 'North America')
                .replace(/\bSNA\b/g, 'Southern North America')
                .replace(/\bUSA\b/g, 'United States of America')
                // Handle common misspellings
                .replace(/\bCosa Rica\b/g, 'Costa Rica')
                .replace(/\bBrazial\b/g, 'Brazil')
                .replace(/\bGuaemala\b/g, 'Guatemala')
                .replace(/\bQueendsland\b/g, 'Guatemala')
                .replace(/\Massaschusetts\b/g, 'Massachusetts')
                .replace(/\Transsylvania\b/g, 'Transylvania')
                .replace(/\Arizon\b/g, 'Arizona')
                .replace(/\Alamaba\b/g, 'Alabama')
                .replace(/\UAE\b/g, 'United Arab Emirates')
                .replace(/\.\s*$/, '') // Remove trailing period and spaces
                .replace(/\s*\.+\s*$/, '')
                // Deal with abbreviations
                .replace(/\bMts\b/g, 'Mts.')
                .replace(/\sIs\./g, ' Island ')
                .replace(/\sI\./g, ' Island ')
                .replace(/\sI$/g, 'Island')
                .replace(/\sI\.$/g, 'Island')
                .replace(/\sIs\.$/g, 'Island')
                .replace(/\sIs$/g, 'Island')
                .replace('=', '')
                .trim()
            )
            .filter(location => {
                // Remove entries that are only punctuation
                const nonPunctuation = location.replace(/[\s\p{P}]/gu, '');
                return nonPunctuation.length > 0;
            });
    }
    // if (acceptedNameUsage && acceptedNameUsage.includes('collocalia')) {
    //     console.log("acceptedNameUsage:", acceptedNameUsage)
    // }
    // if (assertedDistribution === undefined) {
    //     const distributionLine = lines[1] || '';
    //     if (distributionLine && !distributionLine.startsWith('\n')) {
    //         // Remove "See [maps]" and clean up the distribution text
    //         const distributionText = distributionLine
    //             .replace(/\. See \[maps\]/g, '')
    //             .replace(/^[.;]\s*/, ''); // Remove leading period/semicolon and spaces

    //         // Split on both commas and dashes (surrounded by spaces), then clean and join with commas
    //         assertedDistribution = distributionText
    //             .split(/\s*[,\-]\s*/)  // Split on comma or dash with optional spaces
    //             .map(part => part.trim())
    //             .filter(part => part.length > 0)
    //     }
    // }

    // Parse vernacular names by looking specifically for spans with class CN
    const vernacularData: { language: string, vernacularName: string }[] = [];
    const vernacularSpans = Array.from(element.querySelectorAll('span.CN > span'));

    vernacularSpans.forEach(span => {
        const name = span.textContent?.trim();
        const lang = span.getAttribute('lang');
        if (name && lang) {
            vernacularData.push({
                language: lang,
                vernacularName: name
            });
        }
    });

    // Return vernacularData if entries exist, otherwise undefined
    const parsedData = vernacularData.length > 0 ? vernacularData : undefined;

    let vernacularNamesParsed
    if (parsedData) {
        // Group vernacular names by language
        const groupedByLanguage = parsedData.reduce((acc, entry) => {
            const languageName = getLanguageName(entry.language);
            if (!acc[languageName]) {
                acc[languageName] = [];
            }
            acc[languageName].push(entry.vernacularName);
            return acc;
        }, {} as Record<string, string[]>);

        // Convert to final format
        vernacularNamesParsed = Object.entries(groupedByLanguage).map(([language, names]) => ({
            language,
            vernacularNames: names
        }));
    }

    // // Add these logs to see what's happening
    // if (acceptedNameUsage === 'Grammia doris (Boisduval, 1869)') {
    //     console.log('Lines:', lines);
    //     console.log('Vernacular names:', vernacularData);
    //     console.log('Parsed data:', parsedData);
    // }

    // Parse type locality from the element
    let typeLocality: string | undefined;
    const typeLocalityMatch = element.textContent?.match(/TL:\s*([^\n;]+)/);
    if (typeLocalityMatch) {
        typeLocality = typeLocalityMatch[1].trim();
        // convert typeLocality to id
        if (typeLocality && !typeLocality.includes("REF#")) {
            let locItem = getOrCreateLocality(typeLocality.trim());
            typeLocality = locItem.id
        }
    }

    return {
        id: uuidv7(),
        acceptedNameUsage,
        assertedDistribution,
        vernacularNames: vernacularNamesParsed,
        typeLocality
    };
}

/**
 * Maps ISO language codes to full language names
 * Includes both ISO 639-1 codes and BCP 47 extended language tags
 */
export function getLanguageName(code: string): string {
    // Normalize code to lowercase for comparison
    const normalizedCode = code.toLowerCase();

    // Handle extended language tags (e.g., en-US)
    const [baseCode, region] = normalizedCode.split('-');

    // Base language codes
    const baseLanguages: { [key: string]: string } = {
        'ar': 'Arabic',
        'bg': 'Bulgarian',
        'cs': 'Czech',
        'da': 'Danish',
        'de': 'German',
        'el': 'Greek',
        'en': 'English',
        'es': 'Spanish',
        'et': 'Estonian',
        'fi': 'Finnish',
        'fr': 'French',
        'he': 'Hebrew',
        'hi': 'Hindi',
        'hu': 'Hungarian',
        'id': 'Indonesian',
        'it': 'Italian',
        'ja': 'Japanese',
        'ko': 'Korean',
        'lt': 'Lithuanian',
        'lv': 'Latvian',
        'nl': 'Dutch',
        'no': 'Norwegian',
        'pl': 'Polish',
        'pt': 'Portuguese',
        'ro': 'Romanian',
        'ru': 'Russian',
        'sk': 'Slovak',
        'sl': 'Slovenian',
        'sv': 'Swedish',
        'th': 'Thai',
        'tr': 'Turkish',
        'uk': 'Ukrainian',
        'vi': 'Vietnamese',
        'zh': 'Chinese'
    };

    // Region codes for extended language tags
    const regions: { [key: string]: string } = {
        'us': 'American',
        'gb': 'British',
        'ca': 'Canadian',
        'au': 'Australian',
        'nz': 'New Zealand',
        'br': 'Brazilian',
        'pt': 'European Portuguese',
        'mx': 'Mexican',
        'es': 'European Spanish',
        'cn': 'Simplified Chinese',
        'tw': 'Traditional Chinese',
        'hk': 'Hong Kong Chinese'
    };

    const baseName = baseLanguages[baseCode] || code;

    // If there's a region code, combine it with the base language
    if (region && regions[region]) {
        return `${regions[region]} ${baseName}`;
    }

    return baseName;
}