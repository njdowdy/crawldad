import { getDocument, getPageHTML, ParsedBlock } from "./parseBlocks";
import { parseRelatedLiterature } from "./parseRelatedLiterature/parse";
import * as fs from 'fs';
import * as path from 'path';

/**
 * Properly merges a relative URL with a base URL, handling "../" notation correctly
 * @param {string} relativeUrl - The relative URL (e.g., "../family" or "../../family/genus")
 * @param {string} currentUrl - The current base URL
 * @returns {string} The properly merged absolute URL
 */
function mergeUrls(relativeUrl: string, currentUrl: string): string {
    // Split both URLs into parts, removing empty parts
    const currentParts = currentUrl.split('/').filter(part => part);
    const relativeParts = relativeUrl.split('/').filter(part => part);

    // Remove 'https:' or 'http:' from the beginning if present
    const startIndex = currentParts[0] === 'https:' || currentParts[0] === 'http:' ? 1 : 0;
    const baseParts = currentParts.slice(startIndex);

    // Count how many levels up we need to go
    const upLevels = relativeParts.filter(part => part === '..').length;

    // Remove the appropriate number of segments from current URL
    const finalBaseParts = baseParts.slice(0, -upLevels);

    // Add the new path parts (excluding the '..' parts)
    const newParts = [...finalBaseParts, ...relativeParts.filter(part => part !== '..')];

    // Join all parts with slashes and add protocol at the start
    return `https://${newParts.join('/')}/`;
}

/**
 * Gets all related literature elements from the document
 * @param {Document} document - The JSDOM Document object to parse.
 * @returns {Element[]} An array of related literature elements
 */
export function getRelatedLiteratureElements(document: Document): Element[] {
    return Array.from(document.querySelectorAll('ul.RL'));
}

/**
 * Parses RelatedLiteratureBlocks from an array of elements.
 * @param {Element[]} relatedLiteratureElements - Array of related literature elements to parse.
 * @returns {ParsedBlock[]} An array of parsed RelatedLiteratureBlocks.
 */
export function parseRelatedLiteratureBlocks(relatedLiteratureElements: Element[]): ParsedBlock[] {
    const parsedBlocks: ParsedBlock[] = [];
    let blockCount = 0;

    relatedLiteratureElements.forEach((element) => {
        const prevSibling = element.previousElementSibling;
        if (prevSibling && prevSibling.textContent?.trim() === 'Some related literature:') {
            blockCount++;
            const blockName = `RelatedLiteratureBlock_${blockCount}`;

            // ? omitted because I think it is not needed
            // Add the main RelatedLiteratureBlock
            // parsedBlocks.push({
            //     blockName,
            //     blockData: element,
            //     parentBlock: null,
            //     blockType: 'RelatedLiteratureBlock'
            // });

            // Parse individual literature items
            const literatureItems = element.querySelectorAll('li');
            literatureItems.forEach((item, index) => {
                if (item.textContent?.trim()) {
                    parsedBlocks.push({
                        blockName: `${blockName}_Item_${index + 1}`,
                        blockData: item,
                        parentBlock: blockName,
                        blockType: 'RelatedLiteratureItem',
                        parsedBlockData: parseRelatedLiterature(item)
                    });
                }
            });
        }
    });

    return parsedBlocks;
}

async function fetchWithRetry(url: string, maxRetries: number = 5): Promise<Response> {
    for (let attempt = 1; attempt <= maxRetries; attempt++) {
        try {
            const response = await fetch(url);
            if (response.ok) return response;
            throw new Error(`HTTP error! status: ${response.status}`);
        } catch (error) {
            if (attempt === maxRetries) throw error;
            console.log(`Attempt ${attempt} failed, retrying in 10 seconds...`);
            await new Promise(resolve => setTimeout(resolve, 10000)); // 10 second wait
        }
    }
    throw new Error(`Failed after ${maxRetries} attempts`);
}

/**
 * Downloads pages recursively starting from a URL until a target URL is reached
 * @param {string} startUrl - The URL to start downloading from
 * @param {string} targetUrl - The URL at which to stop downloading
 * @param {string} nextLinkSelector - CSS selector for finding the next page link
 * @returns {Promise<string[]>} Array of downloaded page URLs
 */
export async function downloadPagesUntilTarget(
    startUrl: string = "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/zeugloptera/micropterigoidea/micropterigidae/agrionympha/",
    targetUrl: string = "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/vulsinia/",
): Promise<string[]> {
    const downloadedUrls: string[] = [];
    const lastUpdatedRegex = /<p class="?NOTE"?>(\d{1,2})\.(\d{1,2})\.(\d{4})/;
    let indexData: Array<{
        index: number;
        url: string;
        file: string;
        nextUrl?: string;
        lastUpdated?: string;
        scrapeDate: string;
    }> = [];
    let currentUrl = startUrl;
    let index = 1; // index will be loaded from index.json

    // Try to load existing index.json
    const dirPath = path.resolve(__dirname, '../../html');
    const indexPath = path.join(dirPath, 'index.json');

    if (fs.existsSync(indexPath)) {
        try {
            const existingData = JSON.parse(fs.readFileSync(indexPath, 'utf8'));
            if (Array.isArray(existingData) && existingData.length > 0) {
                indexData = existingData;
                const lastEntry = existingData[existingData.length - 1];
                if (lastEntry.nextUrl) {
                    currentUrl = lastEntry.nextUrl;
                    index = lastEntry.index + 1;
                    console.log(`Resuming from ${currentUrl}`);
                }
            }
        } catch (error) {
            console.error('Error reading index.json:', error);
            // Continue with default startUrl if there's an error reading the file
        }
    }

    while (true) {
        try {
            const response = await fetchWithRetry(currentUrl);
            const html = await response.text();
            downloadedUrls.push(currentUrl);

            // Generate filename from URL
            const urlParts = currentUrl.split('/');
            const lepidopteraIndex = urlParts.findIndex(part => part === 'lepidoptera');
            const filename = `${index.toString().padStart(6, '0')}_${urlParts
                .slice(lepidopteraIndex)
                .filter(part => part)
                .join('_')}.html`;

            // Create directory if it doesn't exist
            if (!fs.existsSync(dirPath)) {
                fs.mkdirSync(dirPath, { recursive: true });
            }

            // Save the HTML file
            const filePath = path.join(dirPath, filename);
            fs.writeFileSync(filePath, html);
            console.log(`Saved ${filename}`);

            // Add to index data (before getting the next URL)
            const dateMatch = html.match(lastUpdatedRegex);
            let lastUpdated: string | undefined;

            if (dateMatch) {
                // Convert from DD.MM.YYYY to ISO date string (YYYY-MM-DD)
                const day = dateMatch[1].padStart(2, '0');
                const month = dateMatch[2].padStart(2, '0');
                const year = dateMatch[3];
                lastUpdated = `${year}-${month}-${day}`;
            }

            indexData.push({
                index,
                url: currentUrl,
                file: path.join(dirPath, filename),
                nextUrl: undefined,
                lastUpdated,
                scrapeDate: new Date().toISOString()
            });

            // Save index.json after updating nextUrl
            fs.writeFileSync(
                path.join(dirPath, 'index.json'),
                JSON.stringify(indexData, null, 2)
            );

            if (currentUrl === targetUrl) {
                console.log('Reached target URL, stopping download');
                break;
            }

            const nextLinkRegex = /<td class="?next"?><span><a href="([^"]+)">/;
            const match = html.match(nextLinkRegex);

            if (!match || !match[1]) {
                console.log('No next link found, stopping download');
                break;
            }

            const relativeUrl = match[1];
            let nextUrl = mergeUrls(relativeUrl, currentUrl);
            // Due to some malformed pages in funet, some nextUrl values need to be overidden
            if (currentUrl === "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/geometroidea/geometridae/ennominae/sangalopsis/") {
                nextUrl = "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/geometroidea/geometridae/ennominae/astyochia/"
            }
            console.log('Next URL: ', nextUrl);

            // Update the nextUrl in the current entry
            indexData[indexData.length - 1].nextUrl = nextUrl;

            // Save index.json after updating nextUrl
            fs.writeFileSync(
                path.join(dirPath, 'index.json'),
                JSON.stringify(indexData, null, 2)
            );

            currentUrl = nextUrl;
            index++;

            await new Promise(resolve => setTimeout(resolve, 1000));

        } catch (error) {
            console.error(`Error downloading ${currentUrl} after all retries:`, error);
            break;
        }
    }

    return downloadedUrls;
}

async function scrapeLinks(url: string): Promise<string[]> {
    const basePath = "https://www.nic.funet.fi/pub/sci/bio/life/insecta/";
    const baseUrl = url.split('/').slice(-1).join("/") + '/';
    const linkList: string[] = [];
    let currentUrl = url;

    while (true) {
        const html = await getPageHTML(currentUrl);
        if (!html) break;

        const document = await getDocument(html);

        // Get all links with name attributes
        const links = Array.from(document.querySelectorAll('a[name]'))
            .map(link => link.getAttribute('href'))
            .filter((href): href is string => href !== null)
            // Resolve relative paths to absolute URLs
            .map(href => {
                const cleanHref = href.replace(/^\.\.\/+/, '');
                return new URL(cleanHref, basePath).toString();
            });

        // Filter out links that are base paths of other links
        const filteredLinks = links.filter(link =>
            !links.some(otherLink =>
                otherLink !== link && otherLink.startsWith(link)
            )
        );

        linkList.push(...filteredLinks);

        // Get the next page link
        const nextPage = document.querySelector('a[href]:has(img[alt="[NEXT]"])')?.getAttribute('href');
        if (!nextPage) break;

        if (!nextPage.match(/lepidoptera-\d+-list\.html$/)) break;

        currentUrl = baseUrl + nextPage;
    }

    return linkList;
}