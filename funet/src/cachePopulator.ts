// cachePopulator.ts
import { getAllArticlesFromCache, upsertArticle } from './parseRelatedLiterature/articleCacheManager';
import { addOrUpdateWebResource } from './parseWebResource/webResourceCacheManager';
import { addHigherTaxonToCache } from './parseHigherLevelTaxa/higherTaxonCacheManager';
import { addPersonToCache } from './parseRelatedLiterature/personCacheManager';
import { addJournalToCache } from './parseRelatedLiterature/journalCacheManager';
import { addVolumeToCache } from './parseRelatedLiterature/volumeCacheManager';
import { addIssueToCache } from './parseRelatedLiterature/issueCacheManager';
import { addPageToCache, getAllPagesFromCache } from './parseRelatedLiterature/pageCacheManager';
import { Article, HigherLevelTaxon, Host, Issue, Journal, Person, TaxonRecord, Volume, WebResource } from './types';
import { generateArticleCacheKey } from './parseRelatedLiterature/articleCacheManager';
import { generatePageCacheKey } from './parseRelatedLiterature/pageCacheManager';
import fs from 'fs/promises';
import path from 'path';
import { addTaxonToCache } from './parseNames/taxonCacheManager';
import { addHostToCache } from './parseHosts/hostCacheManager';
import { addDistributionToCache, Distribution } from './parseDistributions/assertedDistributionCacheManager';
import { createLocalityByName, Locality } from './parseDistributions/typeLocalityCacheManager';

export async function populateArticleCache(data: Article[]): Promise<void> {
    for (const item of data) {
        upsertArticle(item, generateArticleCacheKey(item))
    }
}

export async function populateWebResourceCache(data: WebResource[]): Promise<void> {
    for (const item of data) {
        if (item.abbreviation) {
            await addOrUpdateWebResource(item);
        }
    }
}

export async function populateHigherTaxonCache(data: HigherLevelTaxon[]): Promise<void> {
    for (const item of data) {
        await addHigherTaxonToCache(item);
    }
}

export async function populateTaxonCache(data: TaxonRecord[]): Promise<void> {
    for (const item of data) {
        await addTaxonToCache(item);
    }
}

export async function populatePersonCache(data: Person[]): Promise<void> {
    for (const item of data) {
        // !: we may still see some duplication of people with similar names; there's no way to resolve this here and will need to be disambiguated later with more contextual information
        const cacheKey = `${item.givenName || ''}|${item.surname}`;
        addPersonToCache(cacheKey, item);
    }
}

export async function populateJournalCache(data: Journal[]): Promise<void> {
    for (const item of data) {
        const cacheKey = `${item.title}|${item.abbreviation}`.toLowerCase().trim();
        await addJournalToCache(cacheKey, item);
    }
}

export async function populateVolumeCache(data: Volume[]): Promise<void> {
    for (const item of data) {
        const journalCacheKey = `${item.journal?.title}|${item.journal?.abbreviation}`.toLowerCase().trim();
        const volumeNumber = item.label;
        const year = item.year;
        const cacheKey = `${volumeNumber}|${year}|${journalCacheKey}`;
        await addVolumeToCache(cacheKey, item);
    }
}

export async function populateIssueCache(data: Issue[]): Promise<void> {
    for (const item of data) {
        const volumeCacheKey = `${item.volume?.label}|${item.volume?.year}|${item.volume?.journal?.title}|${item.volume?.journal?.abbreviation}`.toLowerCase().trim();
        const issueNumber = item.label;
        const year = item.volume?.year;
        const cacheKey = `${issueNumber}|${year}|${volumeCacheKey}`;
        await addIssueToCache(cacheKey, item);
    }
}

export async function populatePageCacheFromJson(): Promise<void> {
    const filePath = path.resolve(__dirname, 'pageCache.json');
    const fileContent = await fs.readFile(filePath, 'utf-8');
    const pages = JSON.parse(fileContent);

    for (const page of pages) {
        const primaryPageKey = generatePageCacheKey(page, {
            title: page.articleTitle,
            citedAs: page.articleCitedAs,
            issue: page.articleIssue
        });
        await addPageToCache(primaryPageKey, page);
    }
}

export async function verifyPageCache(articleData: Article[]): Promise<void> {
    // Populate pageCache from the existing cache
    const pageCache = getAllPagesFromCache();
    const articleCache = getAllArticlesFromCache();

    // Create sets for page IDs and article page IDs
    const pageIds = new Set<string>();
    const articlePageIds = new Set<string>();

    // Load all page.id values into pageIds set
    for (const page of pageCache) {
        pageIds.add(page.id!);
    }

    // Load all ids from article.pages into articlePageIds set
    for (const article of articleCache) {
        for (const page of article.pages || []) {
            articlePageIds.add(page.id!);
        }
    }

    // Find missing IDs in pageIds that are in articlePageIds
    const missingInPageIds = [...articlePageIds].filter(id => !pageIds.has(id));
    // console.log('Missing in pageIds:', missingInPageIds);

    // Find missing IDs in articlePageIds that are in pageIds
    const missingInArticlePageIds = [...pageIds].filter(id => !articlePageIds.has(id));
    // console.log('Missing in articlePageIds:', missingInArticlePageIds);
}

export async function populateHostCache(hosts: Host[]) {
    for (const host of hosts) {
        // Optionally check if already in cache, then skip or update.
        // For now, just add it. The manager will handle duplicates by name.
        addHostToCache(host.name);
    }
}

export async function populateAssertedDistributionCache(dists: Distribution[]) {
    for (const dist of dists) {
        // Optionally check if already in cache, then skip or update.
        // For now, just add it. The manager will handle duplicates by name.
        addDistributionToCache(dist.name);
    }
}

export async function populateLocalityCache(locs: Locality[]) {
    for (const loc of locs) {
        // Optionally check if already in cache, then skip or update.
        // For now, just add it. The manager will handle duplicates by name.
        createLocalityByName(loc.name);
    }
}