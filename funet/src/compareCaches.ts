import fs from 'fs';
import { Article, Page } from './types';

// Function to load JSON data from a file
function loadJSON(filePath: string): any {
    const data = fs.readFileSync(filePath, 'utf-8');
    return JSON.parse(data);
}

// Load the cache files
const articleCache: Article[] = loadJSON('./data/articleCache.json');
const pageCache: Page[] = loadJSON('./data/pageCache.json');

// Extract IDs from articleCache
articleCache.flatMap((article: Article) => {
    if (article.pages) {
        return article.pages.map((page: Page) => page.id)
    }
    console.log("ARTICLE WITHOUT PAGES:", article)
    return []  // Return empty array for articles without pages
});

// Create Sets for both caches
const pageCacheIds = new Set(pageCache.map((entry: Page) => entry.id));
const articleCacheIds = new Set(articleCache.flatMap((article: Article) =>
    article.pages?.map((page: Page) => page.id) || []
));

// Find differences using Set operations
const pageNotInArticle = [...pageCacheIds].filter(id => !articleCacheIds.has(id));
const articleNotInPage = [...articleCacheIds].filter(id => !pageCacheIds.has(id));

// Log the results
console.log('IDs in pageCache but not in articleCache:', JSON.stringify(pageNotInArticle, null, 2));
console.log('IDs in articleCache but not in pageCache:', JSON.stringify(articleNotInPage, null, 2));

// Sort articles by page count and display top 10
const sortedArticles = [...articleCache]
    .sort((a, b) => (b.pages?.length || 0) - (a.pages?.length || 0))
    .slice(0, 10);

console.log('\nTop 10 articles by page count:');
sortedArticles.forEach(article => {
    console.log(`${article.citedAs[0]}: ${article.pages?.length || 0} pages`);
});

