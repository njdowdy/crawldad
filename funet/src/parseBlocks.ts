import { chromium, Browser, BrowserContext, Page } from 'playwright';
import { JSDOM, DOMWindow } from 'jsdom';
import path from 'path';
import fs from 'fs/promises';
import { parseNameAndSynonymBlock } from './parseNameBlock';
// import { parseMentionBlock } from './parseMentionBlock';
import { TaxonRecord, WebMentionClaim } from './types';
// import { parseRelatedLiterature } from './parseRelatedLiterature1';
import { parseRelatedLiterature } from './parseRelatedLiterature/parse';
import { BrowserManager } from './browserManager';
import { parseParentBlock } from './parseParentBlock/parse';
import { parseWebResources } from './parseWebResource/parse';
import { parseUnresolvedName } from './parseUnresolvedNames/parse';
import { parseInteraction } from './parseHosts/parse';
import { parseMisapplicationBlock } from './parseMisapplications/parse';
import { parseMentionBlock } from './parseMentions/parse';

export type ParsedBlock = {
    blockName: string;
    blockData: Element;
    parentBlock: string | null;
    blockType: string;
    parsedBlockData?: TaxonRecord | WebMentionClaim;
}

type CommonName = {
    language: string | null;
    name: string | null;
}

/**
 * Fetches the HTML content of a given URL using Playwright.
 * @param {string} url - The URL to fetch HTML from.
 * @returns {Promise<string | null>} The HTML content as a string, or null if the fetch fails.
 */
export async function getPageHTML(url: string): Promise<string | null> {
    try {
        const page = await BrowserManager.getPage();

        // Try to clear storage, but don't fail if we can't
        try {
            await page.evaluate(() => localStorage.clear());
        } catch (e) {
            // Ignore localStorage errors
        }

        await page.context().clearCookies();

        const response = await page.goto(url, { waitUntil: 'networkidle', timeout: 10000 });
        if (!response || !response.ok()) {
            console.error(`Failed to load ${url}. Status: ${response?.status()}`);
            return null;
        }

        const content = await page.content();
        return content;
    } catch (error) {
        console.error(`Error fetching ${url}:`, error);
        return null;
    }
}

/**
 * Loads HTML content from a local file.
 * @param {string} filePath - The path to the local HTML file.
 * @returns {Promise<string>} The HTML content as a string.
 */
export async function getLocalFileHTML(filePath: string): Promise<string> {
    try {
        const absolutePath = path.resolve(filePath);
        const content = await fs.readFile(absolutePath, 'utf-8');
        return content;
    } catch (error) {
        console.error(`Error reading file ${filePath}:`, error);
        throw error;
    }
}

/**
 * Converts an HTML string to a JSDOM Document object.
 * @param {string} htmlString - The HTML content as a string.
 * @returns {Promise<Document>} A Promise that resolves to a JSDOM Document object.
 */
export async function getDocument(htmlString: string): Promise<Document> {
    const { window }: { window: DOMWindow } = new JSDOM(htmlString);
    return window.document;
}


/**
 * Parses the document and extracts structured blocks.
 * @param {Document} document - The JSDOM Document object to parse.
 * @returns {ParsedBlock[]} An array of parsed blocks.
 */
export function parseBlocks(document: Document): ParsedBlock[] {
    const parsedBlocks: ParsedBlock[] = [];
    let currentGenus: string | null = null;
    let currentSubgenus: string | null = null;
    let currentSpecies: string | null = null;
    const blockCounts: { [key: string]: number } = {};

    function getNextBlockName(prefix: string): string {
        blockCounts[prefix] = (blockCounts[prefix] || 0) + 1;
        const newPrefix = prefix.charAt(0).toUpperCase() + prefix.slice(1, -5) + 'Block';
        return `${newPrefix}_${blockCounts[prefix]}`;
    }

    function addBlock(element: Element, blockType: string, parent: string | null = null): string {
        const blockName = getNextBlockName(blockType.toLowerCase());
        const block: ParsedBlock = {
            blockName,
            blockData: element,
            parentBlock: parent,
            parsedBlockData: parseParentBlock(element, blockType),
            blockType
        };
        parsedBlocks.push(block);
        return blockName;
    }

    // First, process all RelatedLiterature blocks
    const relatedLiteratureElements = document.querySelectorAll('ul.RL');
    relatedLiteratureElements.forEach((element) => {
        const prevSibling = element.previousElementSibling;
        if (prevSibling && prevSibling.textContent) {
            if (prevSibling.textContent.trim() === 'References:') {
                addBlock(element, 'WebResourcesBlock');
                parseWebResourcesBlock(element, 'WebResourcesBlock', parsedBlocks);
            } else if (prevSibling.textContent.trim() === 'Some related literature:') {
                addBlock(element, 'RelatedLiteratureBlock');
                parseRelatedLiteratureList(element, 'RelatedLiteratureBlock', parsedBlocks);
            }
        }
    });


    // Then process all other elements
    const otherElements = document.querySelectorAll(
        'div.NAVBAR, div.PH > div.TN, ul.SP > li > div.TN, div.GROUP > div.TN, p.NOTE, ul.SP > li > ul.SSP > li > div.TN'
    );

    otherElements.forEach((element) => {
        if (element.matches('div.NAVBAR')) {
            addBlock(element, 'NAVBAR');
        } else if (element.matches('div.PH > div.TN')) {
            currentGenus = addBlock(element, 'GenusBlock');
            parseCommonSubBlocks(element, currentGenus, 'genus', parsedBlocks);
        } else if (element.matches('ul.SP > li > div.TN')) {
            const speciesBlock = addBlock(element, 'SpeciesBlock', currentSubgenus || currentGenus);
            parseCommonSubBlocks(element, speciesBlock, 'species', parsedBlocks);
            currentSpecies = speciesBlock;

        } else if (element.matches('div.GROUP > div.TN')) {
            // ignore "species groups"
            if (!element.textContent?.includes("species-group")) {
                const hasDirectB = Array.from(element.children).some(child => child.tagName === 'B');
                if (!hasDirectB) {
                    const isUnknownBlock = /^u\d+$/.test(element.id)
                    if (isUnknownBlock) {
                        // parse as a list of species-level names attached to genus
                        const speciesBlock = addBlock(element, 'SpeciesBlock', currentGenus);
                        parseCommonSubBlocks(element, speciesBlock, 'species', parsedBlocks, true);
                        currentSpecies = speciesBlock;
                    } else {
                        currentSubgenus = addBlock(element, 'SubgenusBlock', currentGenus);
                        parseCommonSubBlocks(element, currentSubgenus, 'subgenus', parsedBlocks);
                    }
                }
            }

        } else if (element.matches('p.NOTE')) {
            addBlock(element, 'Last update date');
        } else if (element.matches('ul.SP > li > ul.SSP > li > div.TN')) {
            // if (element.textContent?.includes("f. struvei Amsel")) {
            // console.log(currentSpecies)
            // }
            const subspeciesBlock = addBlock(element, 'SubspeciesBlock', currentSpecies);
            parseCommonSubBlocks(element, subspeciesBlock, 'subspecies', parsedBlocks);
        }

    });

    // Then, process all Unresolved Name blocks
    const unresolvedNameElements = document.querySelectorAll('div.GROUP > ul.LR');

    unresolvedNameElements.forEach((element) => {
        const prevSibling = element.previousElementSibling;
        if (prevSibling && prevSibling.textContent) {
            if (prevSibling.textContent.trim() === 'Unmatched external taxa') {
                addBlock(element, 'UnresolvedNameBlock');
                parseUnresolvedNameList(element, 'UnresolvedName', parsedBlocks);
            }
        }
    });

    return parsedBlocks;
}

/**
 * Parses the "related literature" list items associated with a Literature References block.
 * 
 * This function searches for and processes all "related literature" list items (ul.RL > li)
 * within a given parent element. Each non-empty related literature item is added as a new
 * LiteratureBlock to the parsedBlocks array.
 * 
 * @param {Element} parentElement - The parent element (Literature References block) 
 *                                  to search within for related literature list items.
 * @param {string} parentBlockName - The block name of the parent (Literature References block).
 *                                   This is used to create the related literature block names.
 * @param {ParsedBlock[]} parsedBlocks - The array of parsed blocks to append the new LiteratureBlocks to.
 * 
 * @returns {void} This function doesn't return a value, it modifies the parsedBlocks array in place.
 */
function parseRelatedLiteratureList(parentElement: Element, parentBlockName: string, parsedBlocks: ParsedBlock[]): void {
    const relatedLiteratureList = parentElement.querySelectorAll('li');
    relatedLiteratureList.forEach((literatureElement, index) => {
        if (literatureElement.textContent?.trim()) {
            parsedBlocks.push({
                blockName: `${parentBlockName}_RelatedLiteratureBlock_${index + 1}`,
                blockData: literatureElement,
                parentBlock: parentBlockName,
                blockType: 'RelatedLiteratureBlock',
                parsedBlockData: parseRelatedLiterature(literatureElement)
            });
        }
    });

    // parsedBlocks.forEach((block) => {
    //     if (block.blockType === 'RelatedLiteratureBlock') {
    //         const data = block.parsedBlockData as Article[];
    //         if (Array.isArray(data)) {
    //             data.map((x: Article) => {
    //                 if (x.citedAs[0] === 'Arima, 1996') {
    //                     // console.log(JSON.stringify(x, null, 2))
    //                 }
    //             });
    //         }
    //     }
    // });
}

/**
 * Parses the "Unmatched external taxa" list items associated with a UnresolvedNames block.
 * 
 * This function searches for and processes all "Unmatched external taxa" list items (ul.RL > li)
 * within a given parent element. Each non-empty related literature item is added as a new
 * LiteratureBlock to the parsedBlocks array.
 * 
 * @param {Element} parentElement - The parent element (UnresolvedNames block) 
 *                                  to search within for related literature list items.
 * @param {string} parentBlockName - The block name of the parent (UnresolvedNames block).
 *                                   This is used to create the related literature block names.
 * @param {ParsedBlock[]} parsedBlocks - The array of parsed blocks to append the new UnresolvedNames to.
 * 
 * @returns {void} This function doesn't return a value, it modifies the parsedBlocks array in place.
 */
function parseUnresolvedNameList(parentElement: Element, parentBlockName: string, parsedBlocks: ParsedBlock[]): void {
    const unresolvedNameList = parentElement.querySelectorAll('li');
    unresolvedNameList.forEach((unresolvedNameElement, index) => {
        if (unresolvedNameElement.textContent?.trim()) {
            parsedBlocks.push({
                blockName: `${parentBlockName}_UnresolvedNameBlock_${index + 1}`,
                blockData: unresolvedNameElement,
                parentBlock: parentBlockName,
                blockType: 'UnresolvedNameBlock',
                parsedBlockData: parseUnresolvedName(unresolvedNameElement)
            });
        }
    });
}

/**
 * Parses the "sensu" list items associated with a NameBlock or SynonymBlock.
 * 
 * This function searches for and processes all "sensu" list items (ul.SENSU > li)
 * within a given parent element. Each non-empty sensu item is added as a new
 * SensuBlock to the parsedBlocks array.
 * 
 * @param {Element} parentElement - The parent element (NameBlock or SynonymBlock) 
 *                                  to search within for sensu list items.
 * @param {string} parentBlockName - The block name of the parent (NameBlock or SynonymBlock).
 *                                   This is used to create the sensu block names.
 * @param {ParsedBlock[]} parsedBlocks - The array of parsed blocks to append the new SensuBlocks to.
 * 
 * @returns {void} This function doesn't return a value, it modifies the parsedBlocks array in place.
 */
function parseSensuList(parentElement: Element, parentBlockName: string, parsedBlocks: ParsedBlock[]): void {
    const sensuList = parentElement.querySelectorAll('ul.SENSU > li');
    sensuList.forEach((sensuElement, index) => {
        if (sensuElement.textContent?.trim()) {
            parsedBlocks.push({
                blockName: `${parentBlockName}_SensuBlock_${index + 1}`,
                blockData: sensuElement,
                parentBlock: parentBlockName,
                blockType: 'SensuBlock'
            });
        }
    });
}

/**
 * Helper function to parse common sub-blocks (Names, Mentions)
 * @param parentElement The parent element to search within
 * @param parentBlockName The name of the parent block
 * @param blockPrefix A prefix for the block type (e.g., 'genus', 'species', 'subgenus', 'subspecies')
 * @param parsedBlocks The array of parsed blocks to append to
 */
function parseCommonSubBlocks(parentElement: Element, parentBlockName: string, blockPrefix: string, parsedBlocks: ParsedBlock[], isUnknownName?: boolean): void {
    // Parse Names Block and Synonym Blocks
    const namesList = parentElement.querySelectorAll('div.NAMES > ul.SN > li');
    if (!isUnknownName) {
        if (namesList.length > 0) {
            // First item is the namesBlock
            if (namesList[0].textContent?.trim()) {
                const isFirstSubspeciesName = blockPrefix === 'subspecies' &&
                    parentBlockName.match(/^SubspeciesBlock_\d+$/);
                const nameBlock: ParsedBlock = {
                    blockName: `${parentBlockName}_NameBlock_1`,
                    blockData: namesList[0],
                    parsedBlockData: parseNameAndSynonymBlock(namesList[0]),
                    parentBlock: parentBlockName,
                    blockType: 'NameBlock'
                };

                // Only for first subspecies name block, get parent data
                if (isFirstSubspeciesName) {
                    const parentSpeciesBlock = parentElement.closest('ul.SP > li');
                    if (parentSpeciesBlock) {
                        // Extract vernacular names from parent
                        const vernacularNames = Array.from(parentSpeciesBlock.querySelectorAll('span.CN > span'))
                            .map(span => ({
                                language: span.getAttribute('lang') || 'unknown',
                                vernacularName: span.textContent?.trim() || ''
                            }))
                            .filter(vn => vn.vernacularName); // Filter out empty names

                        // Extract distribution from parent
                        // const distributionText = parentSpeciesBlock.querySelector('div.DIST')?.textContent?.trim();
                        const distributionText = parentSpeciesBlock.querySelector('span.MAP')?.textContent?.trim();
                        const assertedDistribution = distributionText
                            ?.split(/\s*[,\-]\s*/)
                            .map(part => part.trim())
                            .filter(part => part.length > 0);

                        // Add parent data to parsed block data
                        nameBlock.parsedBlockData = {
                            ...nameBlock.parsedBlockData,
                            vernacularNames: vernacularNames.length > 0 ? vernacularNames : undefined,
                            assertedDistribution: assertedDistribution?.length ? assertedDistribution : undefined
                        };
                    }
                }
                parsedBlocks.push(nameBlock);
            }


            // Remaining items are synonymBlocks
            for (let i = 1; i < namesList.length; i++) {
                const synonymElement = namesList[i];
                // Check if the element has any non-empty text content
                if (synonymElement.textContent?.trim()) {
                    const synonymBlock: ParsedBlock = {
                        blockName: `${parentBlockName}_SynonymBlock_${i}`,
                        blockData: synonymElement,
                        parsedBlockData: parseNameAndSynonymBlock(synonymElement),
                        // parsedBlockData: '',
                        parentBlock: parentBlockName,
                        blockType: 'SynonymBlock'
                    };
                    parsedBlocks.push(synonymBlock);

                    // Parse sensu list for SynonymBlock
                    //? TEMP REMOVAL: parseSensuList(synonymElement, synonymBlock.blockName, parsedBlocks);
                }
            }
        }
    } else {
        if (namesList.length > 0) {
            // First item is the namesBlock
            if (namesList[0].textContent?.trim()) {
                const isFirstSubspeciesName = blockPrefix === 'subspecies' &&
                    parentBlockName.match(/^SubspeciesBlock_\d+$/);
                const nameBlock: ParsedBlock = {
                    blockName: `${parentBlockName}_NameBlock_1`,
                    blockData: namesList[0],
                    parsedBlockData: parseNameAndSynonymBlock(namesList[0]),
                    parentBlock: parentBlockName,
                    blockType: 'UnknownNameBlock'
                };

                // Only for first subspecies name block, get parent data
                if (isFirstSubspeciesName) {
                    const parentSpeciesBlock = parentElement.closest('ul.SP > li');
                    if (parentSpeciesBlock) {
                        // Extract vernacular names from parent
                        const vernacularNames = Array.from(parentSpeciesBlock.querySelectorAll('span.CN > span'))
                            .map(span => ({
                                language: span.getAttribute('lang') || 'unknown',
                                vernacularName: span.textContent?.trim() || ''
                            }))
                            .filter(vn => vn.vernacularName); // Filter out empty names

                        // Extract distribution from parent
                        // const distributionText = parentSpeciesBlock.querySelector('div.DIST')?.textContent?.trim();
                        const distributionText = parentSpeciesBlock.querySelector('span.MAP')?.textContent?.trim();
                        const assertedDistribution = distributionText
                            ?.split(/\s*[,\-]\s*/)
                            .map(part => part.trim())
                            .filter(part => part.length > 0);

                        // Add parent data to parsed block data
                        nameBlock.parsedBlockData = {
                            ...nameBlock.parsedBlockData,
                            vernacularNames: vernacularNames.length > 0 ? vernacularNames : undefined,
                            assertedDistribution: assertedDistribution?.length ? assertedDistribution : undefined
                        };
                    }
                }
                parsedBlocks.push(nameBlock);
            }


            // Remaining items are synonymBlocks
            for (let i = 1; i < namesList.length; i++) {
                const synonymElement = namesList[i];
                // Check if the element has any non-empty text content
                if (synonymElement.textContent?.trim()) {
                    const synonymBlock: ParsedBlock = {
                        blockName: `${parentBlockName}_SynonymBlock_${i}`,
                        blockData: synonymElement,
                        parsedBlockData: parseNameAndSynonymBlock(synonymElement),
                        // parsedBlockData: '',
                        parentBlock: parentBlockName,
                        blockType: 'UnknownSynonymBlock'
                    };
                    parsedBlocks.push(synonymBlock);

                    // Parse sensu list for SynonymBlock
                    //? TEMP REMOVAL: parseSensuList(synonymElement, synonymBlock.blockName, parsedBlocks);
                }
            }
        }
    }

    // Parse Mentions Blocks
    const mentionsList = parentElement.querySelectorAll('div.MENTIONS > ul.LR > li');
    mentionsList.forEach((mentionElement, index) => {
        if (mentionElement.textContent?.trim()) {
            parsedBlocks.push({
                blockName: `${parentBlockName}_MentionBlock_${index + 1}`,
                blockData: mentionElement,
                parsedBlockData: parseMentionBlock(mentionElement),
                parentBlock: parentBlockName,
                blockType: 'MentionsBlock'
            });
        }
    });

    // Parse Misapplication Block
    // const misapplicationsList = parentElement.querySelectorAll('div.MENTIONS > div > ul.MISID > li');
    // misapplicationsList.forEach((misapplicationElement, index) => {
    //     if (misapplicationElement.textContent?.trim()) {
    //         parsedBlocks.push({
    //             blockName: `${parentBlockName}_MentionBlock_${index + 1}`,
    //             blockData: misapplicationElement,
    //             parsedBlockData: parseMisapplicationBlock(misapplicationElement),
    //             parentBlock: parentBlockName,
    //             blockType: 'MisapplicationBlock'
    //         });
    //     }
    // });
    const misapplicationsList = parentElement.querySelectorAll('div.MENTIONS > div > ul.MISID > li');
    misapplicationsList.forEach((misapplicationElement, index) => {
        if (misapplicationElement.textContent?.trim()) {
            if (!misapplicationElement.textContent?.trim().includes("Bankesia douglasii;  auct.") &&
                !misapplicationElement.textContent?.trim().includes("?Tinea confusella; ") &&
                !misapplicationElement.textContent?.trim().includes("Glyphipteryx xyridota; ") &&
                !misapplicationElement.textContent?.trim().includes("?Tinea seppella; ") &&
                !misapplicationElement.textContent?.trim().includes("?Glyphipteryx seppella; ") &&
                !misapplicationElement.textContent?.trim().includes("?Pyralis bahiensis; ") &&
                !misapplicationElement.textContent?.trim().includes("Tinea albinella (= Elachista obliquells)") &&
                !misapplicationElement.textContent?.trim().includes("Elachista albinella (= Elachista gangabella)") &&
                !misapplicationElement.textContent?.trim().includes("Acleris atomella Wood, 1837 (= Acleris lorquiniana)") &&
                !misapplicationElement.textContent?.trim().includes("Phalonia definita (= Phalonidia melanothica)") &&
                !misapplicationElement.textContent?.trim().includes("Eupoecilia anthemidana (= Cochylidia implicata)") &&
                !misapplicationElement.textContent?.trim().includes("Eupoecilia anthemidana (= Cochylidia heydeniana)") &&
                !misapplicationElement.textContent?.trim().includes("Zygaena lichas Fabricius, 1781") &&
                !misapplicationElement.textContent?.trim().includes("Speiredonia feducia") &&
                !misapplicationElement.textContent?.trim().includes("Heliura leneus") &&
                !misapplicationElement.textContent?.trim().includes("Agrotis orophila") &&
                !misapplicationElement.textContent?.trim().includes("tuberculana (= Nola aerugula)") &&
                !misapplicationElement.textContent?.trim().includes("Abraxas (Calospilos) aphorista") &&
                !misapplicationElement.textContent?.trim().includes("Brahmaea certhia (= Brahmaea wallichi") &&
                !misapplicationElement.textContent?.trim().includes("Mesolia plurimella (= Mesolia elongata") &&
                !(misapplicationElement.textContent?.trim().includes("iva") && misapplicationElement.textContent?.trim().includes("Hayward, 1940")) &&
                !misapplicationElement.textContent?.trim().includes("Papilio terpsicore; Cramer, [1780]") &&
                !misapplicationElement.textContent?.trim().includes("Acraea terpsichore") &&
                !misapplicationElement.textContent?.trim().includes("?Acraea terpsichore") &&
                !misapplicationElement.textContent?.trim().includes("Lycaena elorea; Staudinger, 1888") &&
                !misapplicationElement.textContent?.trim().includes("Strymon beon; Hübner, [1819]") &&
                !misapplicationElement.textContent?.trim().includes("?Thecla beon (= Calycopis isobeon)") &&
                !misapplicationElement.textContent?.trim().includes("?Calycopis beon (= Calycopis isobeon)")

            ) {
                parsedBlocks.push({
                    blockName: `${parentBlockName}_MisapplicationBlock_${index + 1}`,
                    blockData: misapplicationElement,
                    parsedBlockData: parseNameAndSynonymBlock(misapplicationElement, 'misapplication'),
                    parentBlock: parentBlockName,
                    blockType: 'SynonymBlock'
                });
            }
        }
    });

    // Parse Common Name Block (for SpeciesBlock and SubspeciesBlock)
    if (['species', 'subspecies'].includes(blockPrefix)) {
        const commonNameBlock = parentElement.querySelector('span.CN');
        if (commonNameBlock && commonNameBlock.textContent?.trim()) {
            parsedBlocks.push({
                blockName: `${parentBlockName}_CommonNameBlock_1`,
                blockData: commonNameBlock,
                parentBlock: parentBlockName,
                blockType: 'CommonNameBlock'
            });
        }
    }

    // Parse Host Block (for SpeciesBlock and SubspeciesBlock)
    if (['species', 'subspecies'].includes(blockPrefix)) {
        const hostBlocks = parentElement.closest('li')?.querySelectorAll(':scope > p');
        // this could be either Larvae or Parasites block
        if (hostBlocks) {
            hostBlocks.forEach((hostBlock, index) => {
                const text = hostBlock.textContent?.trim() || '';
                if (text) {
                    const data = {
                        blockName: `${parentBlockName}_LarvaeBlock_${index + 1}`,
                        blockData: hostBlock,
                        parentBlock: parentBlockName,
                        blockType: 'LarvaeBlock',
                        parsedBlockData: parseInteraction(hostBlock)
                    };
                    // if (data.parsedBlockData) {
                    //     console.log("blockName:", data.blockName)
                    //     console.log("blockData:", data.blockData)
                    //     console.log("parentBlock:", data.parentBlock)
                    //     console.log("blockType:", data.blockType)
                    //     console.log("parsedBlockData:", data.parsedBlockData)
                    //     console.log("------------\n")
                    // }
                    if (data.parsedBlockData) {
                        parsedBlocks.push(data);
                    }
                }
            });
        }

    }

    // Add automatic NameBlock only for the first subspecies in an SSP block
    // if (blockPrefix === 'subspecies') {
    //     // Check if this is the first subspecies in its group
    //     const sspList = parentElement.closest('ul.SSP');
    //     const firstSSP = sspList?.querySelector('li > div.TN');

    //     // Only create NameBlock if this is the first subspecies
    //     if (firstSSP === parentElement) {
    //         parsedBlocks.push({
    //             blockName: `${parentBlockName}_NameBlock_1`,
    //             blockData: parentElement,
    //             parsedBlockData: parseNameAndSynonymBlock(parentElement),
    //             parentBlock: parentBlockName,
    //             blockType: 'NameBlock'
    //         });
    //     }
    // }

}

/**
 * Extracts the "Next" link from the navigation table and converts it to an absolute URL.
 * @param {Document} document - The JSDOM Document object to parse.
 * @param {string} baseUrl - The base URL of the current page.
 * @returns {string | null} The absolute href of the "Next" link, or null if not found.
 */
export function getNextLink(document: Document, baseUrl: string): string | null {
    const nextLink: Element | null = document.querySelector('body > form > table.NAVIGATION > tbody > tr > td.next > span > a');
    const relativeUrl = nextLink?.getAttribute('href');

    if (!relativeUrl) return null;

    try {
        // Split the base URL into parts
        const baseUrlParts = baseUrl.split('/');
        // Remove the empty string after the last slash
        if (baseUrlParts[baseUrlParts.length - 1] === '') {
            baseUrlParts.pop();
        }

        // Count how many "../" sequences are at the start of the relative URL
        const upLevelMatches = relativeUrl.match(/^(?:\.\.\/)+/);
        if (upLevelMatches) {
            const upLevelCount = upLevelMatches[0].match(/\.\.\//g)!.length;
            // Remove the corresponding number of directories from baseUrlParts
            baseUrlParts.splice(-upLevelCount);
            // Remove the "../" sequences from the relative URL
            const newPath = relativeUrl.replace(/^(?:\.\.\/)+/, '');
            return baseUrlParts.join('/') + '/' + newPath;
        }

        // For absolute or other types of URLs, use URL constructor
        return new URL(relativeUrl, baseUrl).href;
    } catch (error) {
        console.error('Error converting relative URL to absolute:', error);
        return null;
    }
}

/**
 * Retrieves the main navigation element from the document.
 * @param {Document} document - The JSDOM Document object to parse.
 * @returns {Element | null} The main navigation element, or null if not found.
 */
export function getMainNavigationElement(document: Document): Element | null {
    return document.querySelector('div.NAVBAR');
}

/**
 * Fetches HTML content from a given URL or local file and parses it into a structured array of blocks.
 * @param {string} source - The URL of the webpage or path to the local HTML file.
 * @param {boolean} isLocalFile - Flag to indicate if the source is a local file.
 * @returns {Promise<ParsedBlock[]>} A promise that resolves to an array of ParsedBlock objects.
 * @throws {Error} Throws an error if the fetch operation fails or if the HTML cannot be parsed.
 */
export async function parseBlocksFromSource(source: string, isLocalFile: boolean = false): Promise<ParsedBlock[]> {
    try {
        let htmlContent: string | null;
        if (isLocalFile) {
            htmlContent = await getLocalFileHTML(source);
        } else {
            htmlContent = await getPageHTML(source);
        }

        if (!htmlContent) {
            throw new Error(`Failed to fetch HTML from ${source}`);
        }
        const document = await getDocument(htmlContent);
        return parseBlocks(document);
    } catch (error) {
        console.error(`Error parsing blocks from source ${source}:`, error);
        throw error;
    }
}

/**
 * Fetches HTML content from a given URL or local file and parses it into a structured array of blocks.
 * @param {string} source - The URL of the webpage or path to the local HTML file.
 * @param {boolean} isLocalFile - Flag to indicate if the source is a local file.
 * @returns {Promise<ParsedBlock[]>} A promise that resolves to an array of ParsedBlock objects.
 * @throws {Error} Throws an error if the fetch operation fails or if the HTML cannot be parsed.
 */
export async function getHTMLData(source: string, isLocalFile: boolean = false): Promise<Document> {
    try {
        let htmlContent: string | null;
        if (isLocalFile) {
            htmlContent = await getLocalFileHTML(source);
        } else {
            htmlContent = await getPageHTML(source);
        }

        if (!htmlContent) {
            throw new Error(`Failed to fetch HTML from ${source}`);
        }
        return await getDocument(htmlContent);
    } catch (error) {
        console.error(`Error loading html from ${source}:`, error);
        throw error;
    }
}

// parse common names
function parseCommonName(el: Element): CommonName[] {
    const commonNames: CommonName[] = [];
    const commonNameElements = el.querySelectorAll('span.CN');
    commonNameElements.forEach((commonNameElement) => {
        const language = commonNameElement.querySelector('span.LN')?.textContent || null;
        const name = commonNameElement.querySelector('span.N')?.textContent || null;
        commonNames.push({ language, name });
    });
    return commonNames;
}

/**
 * Parses the web references list items associated with a Web References block.
 * 
 * This function searches for and processes all web reference list items (ul.RL > li)
 * within a given parent element. Each non-empty web reference item is added as a new
 * WebReferenceBlock to the parsedBlocks array.
 * 
 * @param {Element} parentElement - The parent element (Web References block) 
 *                                 to search within for web reference list items.
 * @param {string} parentBlockName - The block name of the parent (Web References block).
 *                                  This is used to create the web reference block names.
 * @param {ParsedBlock[]} parsedBlocks - The array of parsed blocks to append the new WebReferenceBlocks to.
 * 
 * @returns {void} This function doesn't return a value, it modifies the parsedBlocks array in place.
 */
function parseWebResourcesBlock(parentElement: Element, parentBlockName: string, parsedBlocks: ParsedBlock[]): void {
    const webResourcesList = parentElement.querySelectorAll('li');
    webResourcesList.forEach((referenceElement, index) => {
        const textContent = referenceElement.textContent?.trim();
        // Skip items that start with [maps] or [*] where * is only numbers/special characters
        if (textContent && !/^\[(maps|\d+|[²³⁴ⁿ]+)\]/i.test(textContent)) {
            parsedBlocks.push({
                blockName: `${parentBlockName}_WebReferenceBlock_${index + 1}`,
                blockData: referenceElement,
                parentBlock: parentBlockName,
                blockType: 'WebReferenceBlock',
                parsedBlockData: parseWebResources(referenceElement)
            });
        }
    });
}

