import { uuidv7 } from "uuidv7";
import { ParsedBlock } from "../parseBlocks";
import { InfraspeciesType, TaxonRecord } from "../types";
import { addDistributionToCache, getDistributionFromCache } from "../parseDistributions/assertedDistributionCacheManager";
import { getOrCreateLocality, getLocalityById } from "../parseDistributions/typeLocalityCacheManager";
import { parseNameAndAuthorship } from "../parseNameBlock";
import { parseAuthorshipString } from "../parseRelatedLiterature/authorshipUtilities";


export function parseTaxa(parsedBlocks: ParsedBlock[]) {
    const taxa: TaxonRecord[] = [];

    // Create a map of all blocks by their blockName for easy parent lookup
    const blocksByName = new Map<string, ParsedBlock>();

    parsedBlocks.forEach(block => {
        if (block.blockName) {
            blocksByName.set(block.blockName, block);
        }
    });

    // First pass: collect only the blocks we want to process
    const validNameBlocks = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        if (block.blockType === "NameBlock" || block.blockType === "SynonymBlock" || block.blockType === "UnknownNameBlock") {
            validNameBlocks.set(block.blockName, block);
        }
    });
    // Second pass: process only the collected valid blocks
    let blockNameModifier: string;
    validNameBlocks.forEach(block => {
        const taxon: TaxonRecord = {
            ...block.parsedBlockData,
            valid: block.blockType === "NameBlock",
            taxonomicStatus: block.blockType === "NameBlock" ? "accepted" : "junior_synonym",
        };

        // Find parent ID and set acceptedNameUsage for synonyms
        let parentBlockName = `${block.parentBlock}`;
        let parentBlock = blocksByName.get(parentBlockName ?? "");
        let parentData: TaxonRecord | null;

        if (block.blockType === "SynonymBlock" || block.blockType === "UnknownNameBlock") {
            // If synonym, note its valid parent block
            blockNameModifier = "NameBlock";
            let validParentBlock;
            if (block.blockType !== 'UnknownNameBlock') {
                validParentBlock = blocksByName.get(`${block.parentBlock}_${blockNameModifier}_1`);
                parentData = validParentBlock?.parsedBlockData;
                if (parentData?.id) {
                    taxon.acceptedNameId = parentData.id;
                    taxon.acceptedNameUsage = parentData.acceptedNameUsage;
                }
            } else {
                validParentBlock = blocksByName.get(`GenusBlock_1_NameBlock_1`);
                taxon.acceptedNameId = taxon.id!;
                taxon.acceptedNameUsage = taxon.scientificName;
            }
        } else {
            // If valid name, pull usage data from the immediate parent
            const validParentBlock = blocksByName.get(`${block.parentBlock}`);
            parentData = validParentBlock?.parsedBlockData;
            if (parentData?.id) {
                taxon.acceptedNameId = taxon.id!;
                taxon.acceptedNameUsage = parentData.acceptedNameUsage;
                taxon.assertedDistribution = parentData.assertedDistribution;
                taxon.vernacularNames = parentData.vernacularNames;
                taxon.typeLocality = parentData.typeLocality?.trim();

                // convert typeLocality to id
                if (taxon.typeLocality && !taxon.typeLocality.includes("REF#")) {
                    const trimmed = taxon.typeLocality.trim();

                    const foundLocById = getLocalityById(trimmed);
                    if (foundLocById) {
                        taxon.typeLocality = foundLocById.id;
                    } else {
                        const locItem = getOrCreateLocality(trimmed);
                        taxon.typeLocality = locItem.id;
                    }
                }

                // convert assertedDistribution to an array of cached IDs:
                if (Array.isArray(taxon.assertedDistribution)) {
                    taxon.assertedDistribution = taxon.assertedDistribution.map(distName => {
                        const name = distName
                            .replace(/\.\.+/g, '')
                            .replace(/, \)/g, ')')
                            .trim();
                        let distItem = getDistributionFromCache(name);
                        if (!distItem) {
                            distItem = addDistributionToCache(name);
                        }
                        return distItem.id;
                    });
                }
            }
        }

        // get id of parent taxon (all names)
        parentBlockName = `${parentBlock?.parentBlock}_${blockNameModifier}_1`;  // need parent block's valid taxon
        parentBlock = blocksByName.get(parentBlockName ?? "");
        parentData = parentBlock?.parsedBlockData;

        // Only set parentId for valid names (NameBlocks)
        if (parentData?.id && (block.blockType === "NameBlock" || block.blockType === "UnknownNameBlock")) {
            taxon.parentId = parentData.id;
        }

        // all taxa are judged to be certainly known, unless explicitly stated
        if (block.blockType === "UnknownNameBlock" || block.blockType === "UnknownSynonymBlock") {
            taxon.uncertainStatus = true;
        } else {
            taxon.uncertainStatus = false;
        }

        // Handle abbreviated acceptedNameUsage
        if (taxon.acceptedNameUsage) {
            const hasCommaOrYear = /,|\b\d{4}\b/.test(taxon.acceptedNameUsage);

            if (!hasCommaOrYear && taxon.acceptedNameUsage.match(/^(\?)?[A-Z]\. /)) {
                // Case 1: Expand abbreviated name using parent's acceptedNameUsage
                const parentTaxon = taxa.find(t => t.id === taxon.parentId);
                if (parentTaxon?.acceptedNameUsage && parentTaxon?.specificEpithet) {
                    const fullNameParts = parentTaxon.acceptedNameUsage.split(' ');
                    const specificEpithetIndex = fullNameParts.findIndex(
                        part => part === parentTaxon.specificEpithet
                    );
                    if (specificEpithetIndex !== -1) {
                        const expandedParts = [...fullNameParts];
                        expandedParts.splice(specificEpithetIndex + 1, 0, parentTaxon.specificEpithet);
                        taxon.acceptedNameUsage = expandedParts.join(' ');
                    }
                }
            } else if (hasCommaOrYear && taxon.acceptedNameUsage.match(/^(\?)?[A-Z]\. /)) {
                // Case 2: Replace first two words with parent's first two words
                const parentTaxon = taxa.find(t => t.id === taxon.parentId);
                if (parentTaxon?.acceptedNameUsage) {
                    const parentFirstTwoWords = parentTaxon.acceptedNameUsage
                        .split(' ')
                        .slice(0, 2)
                        .join(' ');
                    const currentNameParts = taxon.acceptedNameUsage.split(' ');
                    const restOfName = currentNameParts.slice(2).join(' ');
                    taxon.acceptedNameUsage = `${parentFirstTwoWords} ${restOfName}`;
                }
            }
        }

        if (taxon.scientificNameAuthorship) {
            // Check if authorship is a single lowercase word
            const authorship = taxon.scientificNameAuthorship.trim();
            if (/^[a-z]+$/.test(authorship)) {
                taxon.scientificNameAuthorship = authorship.charAt(0).toUpperCase() + authorship.slice(1);
            }
        } else {
            // check for and remove the rare case where the author name is in square brackets; it breaks parsing
            const squareAuthorRegex = /^(.*\s+)\[([\p{L}\s\-\.']+)\](\s*,\s*\d{4}.*)$/u;
            const match = taxon.scientificName.match(squareAuthorRegex);
            if (match) {
                taxon.scientificNameAuthorship = (match[2] + match[3]).trim();
                taxon.scientificName = (match[1] + match[2] + match[3]).trim();
                taxon.canonicalName = match[1].trim();
                const { name, authorship } = parseNameAndAuthorship(taxon.scientificName);
                const author = parseAuthorshipString(authorship);
                taxon.namePublishedInYear = author.year?.toString() ?? '';
            }
        }

        // check for square author brackets on type taxon
        if (taxon.typeTaxonId) {
            const squareAuthorRegex = /^(.*\s+)\[([\p{L}\s\-\.']+)\](\s*,\s*\d{4}.*)$/u;
            const squareAuthorRegexNoYear = /^(.*\s+)\[([\p{L}\s\-\.']+)\]\s*$/u;
            const match1 = taxon.typeTaxonId.match(squareAuthorRegex);
            const match2 = taxon.typeTaxonId.match(squareAuthorRegexNoYear);

            if (match1) {
                taxon.typeTaxonId = (match1[1] + match1[2] + match1[3]).trim();
            }
            if (match2) {
                taxon.typeTaxonId = (match2[1] + match2[2]).trim();
            }
        }

        if (taxon.acceptedNameUsage && taxon.scientificNameAuthorship) {
            const hasYear = /,|\b\d{4}\b/.test(taxon.acceptedNameUsage);
            if (!hasYear) {
                const scientificNameAuthorshipHasYear = /,|\b\d{4}\b/.test(taxon.acceptedNameUsage);
                if (scientificNameAuthorshipHasYear) {
                    taxon.acceptedNameUsage = taxon.acceptedNameUsage + ` ${taxon.scientificNameAuthorship}`;
                }
            }
        }

        // Determine rank and additional epithets
        const blockHierarchy = block.parentBlock?.split('_')[0];
        const parsedBlockData: TaxonRecord = block.parsedBlockData;

        switch (blockHierarchy) {
            case "SubgenusBlock":
                taxon.taxonRank = "subgenus";
                // Extract subgenus name from scientificName
                {
                    const subgenusMatch = parsedBlockData.canonicalName.match(/^(\w+)/);
                    if (subgenusMatch) {
                        taxon.infragenericEpithet = subgenusMatch[1];
                    }
                }
                break;

            case "SpeciesBlock":
            case "SubspeciesBlock":
                {
                    if (taxon.acceptedNameUsage) {
                        const { authorship: acceptedNameUsageAuthorship } = parseNameAndAuthorship(taxon.acceptedNameUsage);
                        const cleanedAcceptedNameUsage = taxon.acceptedNameUsage
                            .replace(acceptedNameUsageAuthorship || '', '')
                            .replace('()', '')
                            .replace('[]', '')
                            .replace('?', '')
                            .trim();
                        // if (taxon.scientificNameAuthorship === "Amsel, 1938") {
                        //     console.log("cleanedAcceptedNameUsage1:", cleanedAcceptedNameUsage)
                        // }
                        const { taxonRank, infraspeciesType, specificEpithet, infraspecificEpithet, infragenericEpithet } =
                            determineSpeciesRankAndType(cleanedAcceptedNameUsage);

                        taxon.taxonRank = taxonRank;
                        taxon.infraspeciesType = infraspeciesType;
                        taxon.specificEpithet = specificEpithet;
                        taxon.infraspecificEpithet = infraspecificEpithet;
                        taxon.infragenericEpithet = infragenericEpithet;
                    }
                }
                break;

            case "GenusBlock":
                {
                    const { authorship: acceptedNameUsageAuthorship } = parseNameAndAuthorship(taxon.acceptedNameUsage);
                    const cleanedAcceptedNameUsage = taxon.acceptedNameUsage
                        .replace(acceptedNameUsageAuthorship || '', '')
                        .replace('()', '')
                        .replace('[]', '')
                        .trim();
                    // if (taxon.scientificNameAuthorship === "Amsel, 1938") {
                    //     console.log("cleanedAcceptedNameUsage:", cleanedAcceptedNameUsage)
                    // }
                    const { taxonRank, infraspeciesType, specificEpithet, infraspecificEpithet, infragenericEpithet } =
                        determineSpeciesRankAndType(cleanedAcceptedNameUsage);

                    taxon.taxonRank = taxonRank;
                    taxon.infraspeciesType = infraspeciesType;
                    taxon.specificEpithet = undefined;
                    taxon.infraspecificEpithet = undefined;
                    taxon.infragenericEpithet = infragenericEpithet;
                }
                break;
        }


        // ───────────────────────────────────────────────────────
        // Add original values based on scientificName
        // ───────────────────────────────────────────────────────
        if (taxon.scientificName) {
            // ------------------------------------------------
            // NEW LOGIC for handling '?' on the genus (first word) and specificEpithet (second word).
            // ------------------------------------------------
            const sciParts = taxon.scientificName.split(/\s+/);
            // Store the full first-word (including any leading '?') in originalGenus:
            taxon.originalGenus = transliterateEpithet(sciParts[0]).epithet;

            // If the first word starts with '?', mark explicitHigherTaxon as false,
            // then remove the '?' from the final usage in sciParts[0].
            if (sciParts[0].startsWith('?')) {
                taxon.explicitHigherTaxon = false;
                sciParts[0] = sciParts[0].replace(/^\?+/, '');
            } else {
                taxon.explicitHigherTaxon = true;
            }

            // If the second word is present and starts with '?', treat it similarly for the specificEpithet.
            if (sciParts[1]?.startsWith('?')) {
                // record it in originalSpecificEpithet
                taxon.originalSpecificEpithet = sciParts[1];
                taxon.explicitSpecificEpithet = false;
                // remove the '?' from the final usage
                sciParts[1] = sciParts[1].replace(/^\?+/, '');
            }

            taxon.scientificName = sciParts.join(' ');

            // Also adjust the canonicalName accordingly if it exists
            if (taxon.canonicalName) {
                const canParts = taxon.canonicalName.split(/\s+/);

                if (canParts[0]?.startsWith('?')) {
                    canParts[0] = canParts[0].replace(/^\?+/, '');
                }
                if (canParts[1]?.startsWith('?')) {
                    canParts[1] = canParts[1].replace(/^\?+/, '');
                }
                taxon.canonicalName = canParts.join(' ');
            }

            // Now that the '?' has been removed where needed, proceed with the rest:
            const cleanedScientificName = taxon.scientificName
                .replace(taxon.scientificNameAuthorship || '', '')
                .replace('()', '')
                .replace('[]', '')
                .trim();

            // if (taxon.scientificNameAuthorship === "Amsel, 1938") {
            //     console.log("cleanedScientificName:", cleanedScientificName)
            // }
            const { taxonRank, infraspeciesType, specificEpithet, infraspecificEpithet, infragenericEpithet } =
                determineSpeciesRankAndType(cleanedScientificName);

            taxon.originalRank = taxonRank;
            taxon.originalInfraspeciesType = infraspeciesType;
            taxon.originalInfraspecificEpithet = infraspecificEpithet;
            taxon.originalSpecificEpithet = taxon.originalSpecificEpithet ?? specificEpithet;
            if (!taxon.explicitSpecificEpithet) { taxon.specificEpithet = '?' + taxon.specificEpithet }
            taxon.originalInfragenericEpithet = infragenericEpithet;
        }

        // ───────────────────────────────────────────────────────
        // Apply transliteration to the entire scientificName and canonicalName
        // ───────────────────────────────────────────────────────
        if (taxon.scientificName) {
            let removedAuthorship: string | undefined;
            if (
                taxon.scientificNameAuthorship &&
                taxon.scientificName.includes(taxon.scientificNameAuthorship)
            ) {
                removedAuthorship = taxon.scientificNameAuthorship;
                taxon.scientificName = taxon.scientificName
                    .replace(removedAuthorship, "")
                    .trim();
            }

            const transliteratedScientific = transliterateEpithet(taxon.scientificName);
            if (transliteratedScientific.changed) {
                taxon.scientificName = transliteratedScientific.epithet;
                taxon.transliterated = transliteratedScientific.transliterated;
            }

            if (removedAuthorship) {
                taxon.scientificName = `${taxon.scientificName} ${removedAuthorship}`.trim();
            }
        }

        if (taxon.canonicalName) {
            const transliteratedCanonical = transliterateEpithet(taxon.canonicalName);
            if (transliteratedCanonical.changed) {
                taxon.canonicalName = transliteratedCanonical.epithet;
                taxon.transliterated = transliteratedCanonical.transliterated;
            }
        }

        // ───────────────────────────────────────────────────────
        // Fix any weird "\n\n=" trailing text
        // ───────────────────────────────────────────────────────
        if (taxon.acceptedNameUsage) {
            taxon.acceptedNameUsage = taxon.acceptedNameUsage
                .replace('\n\n=', '')
                .replace(/^\?/, '');
        }

        if (taxon.scientificName) {
            taxon.scientificName = taxon.scientificName
                .replace('\n\n=', '')
                .replace(/^\?/, '');
        }

        if (taxon.scientificNameAuthorship) {
            taxon.scientificNameAuthorship = taxon.scientificNameAuthorship
                .replace('\n\n=', '')
                .replace(/^\?/, '');
        }

        if (taxon.canonicalName) {
            taxon.canonicalName = taxon.canonicalName.replace(/^\?/, '');
        }

        // make sure invalid names get an acceptedNameUsage
        if (!taxon.valid && taxon.acceptedNameUsage === undefined) {
            taxon.acceptedNameUsage = taxon.acceptedNameId;
        }

        // If scientificNameAuthorship is still empty, derive from acceptedNameUsage
        if (!taxon.scientificNameAuthorship || taxon.scientificNameAuthorship === '') {
            const { name, authorship } = parseNameAndAuthorship(taxon.acceptedNameUsage);
            const author = parseAuthorshipString(authorship);
            taxon.namePublishedInYear = author.year?.toString() ?? '';

            const sameGenus = name.split(' ')[0] === taxon.scientificName.split(' ')[0];
            if (sameGenus) {
                taxon.scientificNameAuthorship = authorship;
                if (!taxon.scientificName.includes(`${authorship}`)) {
                    taxon.scientificName = (taxon.scientificName + ' ' + authorship);
                }
            } else {
                taxon.scientificNameAuthorship = `(${authorship})`.replace('((', '(').replace('))', ')');
                if (!taxon.scientificName.includes(`${authorship}`)) {
                    taxon.scientificName = (
                        taxon.scientificName + ' ' + '(' + authorship + ')'
                    ).replace('((', '(').replace('))', ')');
                }
            }
        }

        if (taxon.namePublishedInYear === undefined || taxon.namePublishedInYear === '') {
            taxon.namePublishedInYear = null;
        }

        // basic string cleaning
        taxon.acceptedNameUsage = removeSquareBracketsIfNoDigits(taxon.acceptedNameUsage.replace(/\s+/g, ' '));
        taxon.canonicalName = removeSquareBracketsIfNoDigits(taxon.canonicalName.replace(/\s+/g, ' '));
        taxon.scientificName = removeSquareBracketsIfNoDigits(taxon.scientificName.replace(/\s+/g, ' '));
        taxon.scientificNameAuthorship = removeSquareBracketsIfNoDigits(taxon.scientificNameAuthorship.replace(/\s+/g, ' '));

        // if acceptedNameUsage does not have authorship details, add them
        // if (taxon.canonicalName === 'Argyresthia oreadella') {
        //     console.log("taxon:", taxon)
        // }
        if (taxon.valid && !taxon.acceptedNameUsage.includes(taxon.scientificNameAuthorship.replace('(', '').replace(')', ''))) {
            if (taxon.acceptedNameUsage.split(' ')[0] === taxon.originalGenus) {
                taxon.acceptedNameUsage = taxon.acceptedNameUsage + ' ' + taxon.scientificNameAuthorship
            } else {
                taxon.acceptedNameUsage = taxon.acceptedNameUsage + ' (' + taxon.scientificNameAuthorship + ')'
            }
        }
        taxon.acceptedNameUsage = taxon.acceptedNameUsage.replace('?', '')

        // If you see a leading "?", mark explicitSpecificEpithet = false (for the second word).
        // This portion is handled above, but we keep the existing code for extra checks:
        if (taxon.specificEpithet) {
            if (taxon.specificEpithet.startsWith('?')) {
                taxon.explicitSpecificEpithet = false;
                // taxon.specificEpithet = taxon.specificEpithet.replace(/^\?+/, '');
            } else {
                taxon.explicitSpecificEpithet = true;
            }
        }

        // ensure that invalid names that do not reference a valid taxon (e.g., "Unknown or Unplaced Taxa")
        // do not have an acceptedNameUsage nor acceptedNameId
        if (!taxon.valid && taxon.acceptedNameId === taxon.id) {
            taxon.acceptedNameId = ''
            taxon.acceptedNameUsage = ''
        }

        taxa.push(taxon);
        block.parsedBlockData = { ...taxon };
        blocksByName.set(block.blockName, block);
    });

    return taxa;
}

/**
 * Same as before, but now we also retrieve (and return) any subgenus
 * epithet that was inside parentheses and removed from the word list.
 */
function parseNameIntoWords(name: string): { words: string[]; subgenus?: string } {
    const canonical = parseCanonicalName(name);
    let allWords = canonical.split(/\s+/).filter(Boolean);

    // ─────────────────────────────────────────────────────────
    // NEW: filter out any words that begin with "?" if they are NOT the first word
    // ─────────────────────────────────────────────────────────
    allWords = allWords.filter((word, index) => {
        // Keep the first word no matter what.
        if (index === 0 || index === 1) return true;
        // Otherwise, remove it if it begins with "?"
        return !word.startsWith("?");
    });

    let subgenus: string | undefined = undefined;
    if (
        allWords.length > 1 &&
        /^\([^)]*[A-Z][a-z]+[^)]*\)$/.test(allWords[1])
    ) {
        // Capture what's inside the parentheses
        subgenus = allWords[1].replace(/[()]/g, "");
        // Remove that parenthesized subgenus mention from the final words
        allWords.splice(1, 1);
    }

    return { words: allWords, subgenus };
}

/**
 * Now includes infragenericEpithet (e.g. subgenus) if it was found in parentheses.
 * Updated to handle cases where the third word is an infraspecies marker (e.g. "f." or "var.")
 */
function determineSpeciesRankAndType(nameToBaseRankOn: string): {
    taxonRank: string;
    infragenericEpithet?: string;
    infraspeciesType?: InfraspeciesType;
    specificEpithet?: string;
    infraspecificEpithet?: string;
} {
    const { words, subgenus } = parseNameIntoWords(nameToBaseRankOn);
    const wordCount = words.length;
    // if (nameToBaseRankOn.includes("Amata f. sexmacula")) {
    //     console.log("nameToBaseRankOn:", nameToBaseRankOn)
    //     console.log("words:", words)
    //     console.log("subgenus:", subgenus)
    //     console.log("wordCount:", wordCount)
    // }
    const result: {
        taxonRank: string;
        infragenericEpithet?: string;
        infraspeciesType?: InfraspeciesType;
        specificEpithet?: string;
        infraspecificEpithet?: string;
    } = {
        taxonRank: "species",
        infragenericEpithet: subgenus,
        infraspeciesType: undefined,
        specificEpithet: words.length >= 2 ? words[1] : undefined,
        infraspecificEpithet: undefined
    };

    // Utility to check if all words are capitalized (first letter uppercase, rest lower)
    function allWordsAreCapitalized(wds: string[]) {
        return wds.every(
            (w) => /^[A-Z][a-z]*$/.test(w)
        );
    }

    // Exactly 1 word → treat as genus
    if (wordCount === 1) {
        result.taxonRank = "genus";
        result.infraspecificEpithet = undefined;
        return result;
    }

    // Exactly 2 words → if both capitalized, treat as genus + subgenus; else species
    if (wordCount === 2) {
        if (allWordsAreCapitalized(words)) {
            result.taxonRank = "subgenus";
            result.infragenericEpithet = words[1];
            result.specificEpithet = undefined;
            result.infraspecificEpithet = undefined;
            return result;
        } else {
            return result; // species
        }
    }

    // For 3+ words, default to infraspecies logic
    result.taxonRank = "infraspecies";

    const secondWord = words[1]?.replace("?", "").toLowerCase();
    const thirdWord = words[2]?.replace("?", "").toLowerCase();

    // Special check: if the third word repeats the second, treat the third word as the infraspecific epithet
    if (thirdWord === secondWord && wordCount >= 3) {
        if (wordCount === 3) { result.infraspecificEpithet = words[2]; }
        const fourthWord = words[3]?.replace("?", "").toLowerCase() ?? "";

        if (wordCount >= 3) { result.infraspecificEpithet = words.slice(2).join(' '); }

        if (["subsp.", "subsp", "ssp", "ssp."].includes(fourthWord)) {
            result.infraspeciesType = "subspecies";
        } else if (["var", "var.", "variety", "varietas"].includes(fourthWord)) {
            result.infraspeciesType = "variant";
        } else if (["f.", "f", "form", "forma", "f.loc"].includes(fourthWord)) {
            result.infraspeciesType = "form";
        } else if (["ab", "ab.", "abb.", "abb"].includes(fourthWord)) {
            result.infraspeciesType = "aberration";
        } else if (["cv", "cv."].includes(fourthWord)) {
            result.infraspeciesType = "cultivar";
        } else {
            result.infraspeciesType = "subspecies";
        }
        return result;
    }

    // NEW: if the second word is a recognized marker, the third becomes the infraspecific epithet
    // Example: "Amata f. sexmacula-semicaeca" → secondWord === "f."
    if (
        wordCount >= 3 &&
        ["subsp.", "subsp", "ssp", "ssp.", "var", "var.", "variety", "varietas", "f", "f.", "f.loc", "form", "forma", "ab", "ab.", "abb.", "abb", "cv", "cv."].includes(secondWord)
    ) {
        if (["subsp.", "subsp", "ssp", "ssp."].includes(secondWord)) {
            result.infraspeciesType = "subspecies";
        } else if (["var", "var.", "variety", "varietas"].includes(secondWord)) {
            result.infraspeciesType = "variant";
        } else if (["f.", "f", "form", "forma", "f.loc"].includes(secondWord)) {
            result.infraspeciesType = "form";
        } else if (["ab", "ab.", "abb.", "abb"].includes(secondWord)) {
            result.infraspeciesType = "aberration";
        } else if (["cv", "cv."].includes(secondWord)) {
            result.infraspeciesType = "cultivar";
        }
        result.infraspecificEpithet = words[2];
        result.specificEpithet = '(unknown)';

        // if (nameToBaseRankOn.includes("Amata f. sexmacula")) {
        //     console.log("nameToBaseRankOn:", nameToBaseRankOn)
        //     console.log("result:", result)
        // }
        return result;
    }

    // NEW: if the third word is a recognized marker, the fourth becomes the infraspecific epithet
    // Example: "Glyphipterix thrasonella f. struvei" → thirdWord === "f."
    if (
        wordCount >= 4 &&
        ["subsp.", "subsp", "ssp", "ssp.", "var", "var.", "variety", "varietas", "f", "f.", "form", "forma", "ab", "ab.", "abb.", "abb", "cv", "cv."].includes(thirdWord)
    ) {
        if (["subsp.", "subsp", "ssp", "ssp."].includes(thirdWord)) {
            result.infraspeciesType = "subspecies";
        } else if (["var", "var.", "variety", "varietas"].includes(thirdWord)) {
            result.infraspeciesType = "variant";
        } else if (["f.", "f", "form", "forma", "f.loc"].includes(thirdWord)) {
            result.infraspeciesType = "form";
        } else if (["ab", "ab.", "abb.", "abb"].includes(thirdWord)) {
            result.infraspeciesType = "aberration";
        } else if (["cv", "cv."].includes(thirdWord)) {
            result.infraspeciesType = "cultivar";
        }
        result.infraspecificEpithet = words[3];
        return result;
    }

    // If 4+ words, check the 4th for a recognized marker
    if (wordCount >= 4) {
        const fourthWord = words[3]?.replace("?", "").toLowerCase() ?? "";
        if (["subsp.", "subsp", "ssp", "ssp."].includes(fourthWord)) {
            result.infraspeciesType = "subspecies";
            result.infraspecificEpithet = words[2];
        } else if (["var", "var.", "variety", "varietas"].includes(fourthWord)) {
            result.infraspeciesType = "variant";
            result.infraspecificEpithet = words[2];
        } else if (["f.", "f", "form", "forma", "f.loc"].includes(fourthWord)) {
            result.infraspeciesType = "form";
            result.infraspecificEpithet = words[2];
        } else if (["ab", "ab.", "abb.", "abb"].includes(fourthWord)) {
            result.infraspeciesType = "aberration";
            result.infraspecificEpithet = words[2];
        } else if (["cv", "cv."].includes(fourthWord)) {
            result.infraspeciesType = "cultivar";
            result.infraspecificEpithet = words[2];
        } else {
            result.infraspeciesType = "subspecies";
            result.infraspecificEpithet = words.slice(2).join(' ');
        }
        return result;
    }

    // Otherwise, fall back to original checks for 3rd / 4th word as a marker
    if (
        wordCount === 3 ||
        (wordCount === 4 && ["subsp.", "subsp", "ssp", "ssp."].includes(thirdWord))
    ) {
        result.infraspeciesType = "subspecies";
    } else if (wordCount === 4) {
        if (["var", "var.", "variety", "varietas"].includes(thirdWord)) {
            result.infraspeciesType = "variant";
        } else if (["f.", "f", "form", "forma", "f.loc"].includes(thirdWord)) {
            result.infraspeciesType = "form";
        } else if (["ab", "ab.", "abb.", "abb"].includes(thirdWord)) {
            result.infraspeciesType = "aberration";
        } else if (["cv", "cv."].includes(thirdWord)) {
            result.infraspeciesType = "cultivar";
        }
    } else if (wordCount === 5 || wordCount === 6) {
        const fifthWord = words[4]?.replace("?", "").toLowerCase();
        if (["subvar", "subvar.", "subvariety"].includes(fifthWord)) {
            result.infraspeciesType = "subvariety";
        } else if (
            ["f", "f.", "f.loc"].includes(thirdWord) &&
            ["sp.", "sp"].includes(words[3]?.replace("?", "").toLowerCase())
        ) {
            result.infraspeciesType = "forma_specialis";
        }
    }

    // If none of the above returned, the final word is the infraspecific epithet
    // (the user might have a distinctive pattern that isn't matched by "var.", etc.)
    result.infraspecificEpithet = words[wordCount - 1];
    return result;
}

export function injectGenusParent(taxa: TaxonRecord[], genusParentId: string): TaxonRecord[] {
    const injectedTaxa = taxa
    injectedTaxa.forEach(taxon => {
        if (taxon.taxonRank === 'genus' && taxon.parentId === undefined) {
            taxon.parentId = genusParentId
        }
    })

    return injectedTaxa
}

/**
 * Transliterates a string by:
 *  1) Converting German umlauts into vowel + "e" form (e.g. mü -> mue).
 *  2) Removing all other diacritics and non-latin marks (e.g. ñ -> n, é -> e, ø -> o, etc.).
 *
 * In addition, for each character that changes, we record:
 *  - An id (UUID)
 *  - The original character
 *  - Its index in the input string
 *  - The final character(s) it was replaced with
 *
 * Returns an object containing:
 *   epithet: the final, fully transliterated string
 *   changed: boolean indicating if at least one character was changed
 *   transliterated: an array of objects describing every transformation
 */
export function transliterateEpithet(epithet: string): {
    epithet: string;
    changed: boolean;
    transliterated?: {
        id: string;
        letter: string;
        location: number;
        replacementCharacter: string;
    }[];
} {
    const original = epithet;
    const transformations: {
        id: string;
        letter: string;
        location: number;
        replacementCharacter: string;
    }[] = [];

    let finalStr = '';

    for (let i = 0; i < original.length; i++) {
        const char = original[i];
        let replaced = char;

        // Step 1: Expand known conversions before normalization.
        switch (char) {
            // Germanic/Umlauts
            case 'ä':
                replaced = 'ae';
                break;
            case 'ö':
                replaced = 'oe';
                break;
            case 'ü':
                replaced = 'ue';
                break;
            case 'Ä':
                replaced = 'Ae';
                break;
            case 'Ö':
                replaced = 'Oe';
                break;
            case 'Ü':
                replaced = 'Ue';
                break;
            case 'ß':
                replaced = 'ss';
                break;
            // Ligatures
            case 'æ':
                replaced = 'ae';
                break;
            case 'Æ':
                replaced = 'Ae';
                break;
            case 'œ':
                replaced = 'oe';
                break;
            case 'Œ':
                replaced = 'Oe';
                break;

            // Remove punctuation such as apostrophes
            case '\'':
                replaced = '';
                break;

            // For hyphens, only remove if the preceding "word" is NOT a single letter.
            case '-': {
                // Find the substring before this hyphen
                const startAt = original.lastIndexOf(' ', i - 1) + 1;
                const precedingWord = original.substring(startAt, i);

                // If precedingWord is exactly one letter, keep the hyphen. Otherwise remove it.
                if (/^(\?)?[A-Za-z]$/.test(precedingWord)) {
                    replaced = '-';
                }
                // If precedingWord is purely numeric and between 1..13, transliterate to Latin and omit the hyphen.
                else if (/^\d+$/.test(precedingWord)) {
                    const numericVal = parseInt(precedingWord, 10);
                    if (!isNaN(numericVal) && numericVal >= 1 && numericVal <= 13) {
                        // Remove the digits from finalStr that were already appended
                        // (character-by-character building up to now).
                        finalStr = finalStr.slice(0, finalStr.length - precedingWord.length);

                        // Prepare a simple map for 1..13 → Latin equivalents
                        const numberToLatinMap: { [key: number]: string } = {
                            1: "uni",
                            2: "bi",
                            3: "tri",
                            4: "quadri",
                            5: "quinque",
                            6: "sex",
                            7: "septem",
                            8: "octo",
                            9: "novem",
                            10: "decem",
                            11: "undecim",
                            12: "duodecim",
                            13: "tredecim",
                        };

                        // Replace the preceding digits with their Latin form
                        const latinForm = numberToLatinMap[numericVal];
                        finalStr += latinForm;

                        // Record transformations for each digit and the hyphen
                        // (so at least we note they were replaced)
                        for (let digitIndex = 0; digitIndex < precedingWord.length; digitIndex++) {
                            transformations.push({
                                id: uuidv7(),
                                letter: precedingWord[digitIndex],
                                location: startAt + digitIndex,
                                replacementCharacter: (digitIndex === 0 && latinForm) ? latinForm : "",
                            });
                        }
                        transformations.push({
                            id: uuidv7(),
                            letter: '-',
                            location: i,
                            replacementCharacter: "",
                        });

                        // Do not keep the hyphen
                        replaced = '';
                    } else {
                        replaced = '';
                    }
                } else {
                    replaced = '';
                }
                break;
            }

            default:
                // Leave replaced as-is (the normal path).
                break;
        }

        // Step 2: Strip other diacritics via normalization 
        // (e.g. ñ → n, ø → o, é → e, å → a, etc.).
        const norm = replaced
            .normalize('NFD')
            .replace(/\p{M}+/gu, ''); // remove combining marks

        // If final differs from the original, record a transformation.
        if (norm !== char) {
            transformations.push({
                id: uuidv7(),
                letter: char,
                location: i,
                replacementCharacter: norm,
            });
        }

        finalStr += norm;
    }

    return {
        epithet: finalStr,
        changed: transformations.length > 0,
        transliterated: transformations.length > 0 ? transformations : undefined,
    };
}

export function parseCanonicalName(canonicalName: string): string {
    // This regex captures:
    //   1) The first capitalized word. (p1)
    //   2) Optionally, a parenthesized subgenus. (p2)
    //   3) Everything else. (p3)
    //
    // We originally removed capitalized words from p3 unconditionally if p2 didn’t exist.
    // Instead, if no parentheses are found, return all of p3 intact. This way, two capital words
    // (“Simplicala Convercala”) remain as separate words for parseNameIntoWords().
    const cleanedCanonicalName = canonicalName.replace(
        /^([A-Z][a-z]+)(\s*\([^)]*[A-Z][a-z]+[^)]*\))?(.*)/,
        (match, p1, p2, p3) => {
            // If we found parenthesized text right after p1, keep it for subgenus and then
            // remove further capitalized words from p3 as originally.
            if (p2) {
                return (
                    p1 +
                    p2 +
                    p3.replace(/\s*[\(\[]?\??[A-Z][a-z]+[\)\]]?\s*/g, " ")
                );
            } else {
                // If NO parentheses, we simply append p3 unchanged, preserving second capital words.
                return p1 + p3;
            }
        }
    ).trim();

    return cleanedCanonicalName;
}

function removeSquareBracketsIfNoDigits(str: string): string {
    return str.replace(/\[([^\]]+)\]/g, (match, p1) => {
        // If p1 contains any digit, preserve the entire match (i.e., keep the square brackets).
        // Otherwise, remove only the brackets, returning the content inside them.
        return /\d/.test(p1) ? match : p1;
    });
}
