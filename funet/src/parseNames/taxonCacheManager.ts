import { TaxonRecord } from "../types";

const taxonCache: Map<string, TaxonRecord> = new Map();

export function getTaxonFromCache(name: string): TaxonRecord | undefined {
    return taxonCache.get(name.toLowerCase());
}

export function addTaxonToCache(taxon: TaxonRecord): void {
    taxonCache.set(`${taxon.scientificName.toLowerCase()}_${taxon.taxonRank.toLowerCase()}`, taxon);
}

export function clearTaxonCache(): void {
    taxonCache.clear();
}

export function getAllTaxonFromCache(): TaxonRecord[] {
    return Array.from(taxonCache.values());
}

export function getTaxonCacheSize(): number {
    return taxonCache.size;
}
