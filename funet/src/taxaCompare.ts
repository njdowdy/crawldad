export async function compareTaxaRecords(): Promise<void> {
    const fs = await import('fs/promises');
    const path1 = './data/results/taxa.json';
    const path2 = './data/results/taxa2.json';

    // Read file contents
    const [file1Content, file2Content] = await Promise.all([
        fs.readFile(path1, 'utf-8'),
        fs.readFile(path2, 'utf-8'),
    ]);

    // Parse JSON arrays
    const taxa1 = JSON.parse(file1Content);
    const taxa2 = JSON.parse(file2Content);

    // For each taxon in taxa1, look for a matching taxon in taxa2 by scientificName
    for (const taxon1 of taxa1) {
        const { scientificName } = taxon1;
        const matchingTaxon2 = taxa2.find((t: any) => t.scientificName === scientificName);

        // If no match found, log and skip
        if (!matchingTaxon2) {
            console.log(`No match found in taxa2 for scientificName: ${scientificName}`);
            continue;
        }

        // Stringify both matching objects
        let str1 = JSON.stringify(taxon1, null, 2);

        if (!str1.includes("namePublishedIn")) {
            console.log(`No article reference for: ${scientificName}`);
        }

        let str2 = JSON.stringify(matchingTaxon2, null, 2);

        // Remove anything that looks like a UUID from both strings
        const uuidRegex = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/gi;
        str1 = str1.replace(uuidRegex, '');
        str2 = str2.replace(uuidRegex, '');

        // Compare
        if (str1 === str2) {
            // console.log(`No differences found for scientificName: ${scientificName}`);
        } else {
            // console.log(`Differences found for scientificName: ${scientificName}`);
        }
    }
}

/**
 * Main entry point for running this script directly via Node:
 * "node dist/src/taxaCompare.js"
 */
export async function main(): Promise<void> {
    await compareTaxaRecords();
}

if (require.main === module) {
    main().catch((err) => {
        console.error(err);
        process.exit(1);
    });
}