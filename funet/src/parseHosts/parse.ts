import { ParsedBlock } from "../parseBlocks";
import { Interaction, InteractionType, TaxonRecord } from "../types";
import { addHostToCache, getHostFromCache } from "./hostCacheManager";

/**
 * Attempt to expand abbreviated genus names in the form "X. species"
 * by scanning backward for a prior, full genus name that starts with X,
 * provided no other abbreviated entry is in between.
 *
 * Example:
 *    "Q. douglasii" -> "Quercus douglasii"
 *    "C. australis" -> "Celtis australis"
 * but "P. blocki" stays abbreviated if there's an intervening abbreviated name
 * different from "P." (e.g. "C. something") before we can find "Polypogon".
 */
function expandGenusAbbreviations(rawNames: string[]): string[] {
    const result: string[] = [];

    // Utility to see if a name is abbreviated "X. something"
    const isAbbreviated = (name: string) => {
        return /^[A-Z]\.\s+\S+/.test(name.trim());
    };

    // Utility to parse out the "genus" from a multi-word name
    // (simply the first token, e.g. "Quercus" from "Quercus agrifolia")
    const getGenus = (fullName: string) => {
        const [genus] = fullName.trim().split(/\s+/);
        return genus;
    };

    for (let i = 0; i < rawNames.length; i++) {
        const currentName = rawNames[i];

        if (!isAbbreviated(currentName)) {
            // No abbreviation: add directly to results
            result.push(currentName);
            continue;
        }

        // If abbreviated -> look backward to find suitable full genus
        // with the same leading capital letter, ensuring we do not pass
        // another abbreviation in between.
        const abbrevLetter = currentName.trim()[0]; // e.g. 'Q' in "Q. douglasii"
        let foundGenus: string | undefined;

        // Walk backwards until we find a full name that starts with abbrevLetter
        // Stop immediately if we see any other abbreviated name in between.
        for (let j = i - 1; j >= 0; j--) {
            const candidate = result[j];
            if (isAbbreviated(candidate)) {
                // Another abbreviated entry in between => break out, no expansion
                foundGenus = undefined;
                break;
            }
            // If it's a full name, see if it starts with the same letter
            const genus = getGenus(candidate);
            if (genus && genus[0] === abbrevLetter) {
                foundGenus = genus;
                break;
            }
        }

        if (foundGenus) {
            // Expand: "Q. douglasii" -> "Quercus douglasii"
            // quick parse to remove "Q." from the string
            const abbreviatedParts = currentName.split(".");
            const speciesPart = abbreviatedParts[1]?.trim() ?? "";
            result.push(`${foundGenus} ${speciesPart}`);
        } else {
            // Could not expand, keep original
            result.push(currentName);
        }
    }

    return result;
}

export function parseInteraction(element: Element): Interaction | undefined {
    const subjectIds: string[] = [];
    const allHostElements = element.querySelectorAll('i');
    let verb: string | undefined;

    if (element.querySelector('b')?.textContent?.includes('Parasites')) {
        verb = 'parasitized_by';
    }
    if (element.querySelector('b')?.textContent?.includes('Larva')) {
        verb = 'consumes';
    }

    if (!verb) {
        return undefined;
    }

    // Filter out <i> elements that are wrapped in an <a> with an href matching "#[0-9]+"
    const hostElements = Array.from(allHostElements).filter((iEl) => {
        const parentAnchor = iEl.closest('a[href]');
        return !(parentAnchor && /^#[0-9]+$/.test(parentAnchor.getAttribute('href') ?? ''));
    });

    // Gather raw host names
    const rawNames = hostElements
        .map((el) => el.textContent?.trim() ?? "")
        // Filter out trailing '.' except for " sp."
        .filter((name) => name && (!name.endsWith('.') || /\s+sp\.$/i.test(name)));

    // 1) Expand any abbreviated genus references
    const expandedNames = expandGenusAbbreviations(rawNames);

    // 2) Filter out names that are just a single word starting with a lowercase letter
    //    (These likely represent "(as xxxxx)" references too ambiguous to parse usefully)
    const filteredNames = expandedNames.filter((name) => {
        const tokens = name.trim().split(/\s+/);
        // Exclude if single token matches /^[a-z]/ (starts with lowercase)
        return !(tokens.length === 1 && /^[a-z]/.test(tokens[0]));
    });

    // 3) For each remaining name, get/create a cached host object and collect its id
    filteredNames.forEach((hostName) => {
        let cleanedHostName = hostName.replace(/[:;\[].*$/, '').trim()
        cleanedHostName = cleanedHostName.replace(/\bless\b/, '').trim()
        if (!cleanedHostName.includes(";") && cleanedHostName !== '') {
            let hostObj = getHostFromCache(cleanedHostName);
            if (!hostObj) {
                hostObj = addHostToCache(cleanedHostName);
            }
            subjectIds.push(hostObj.id);
        }
    });

    return {
        subjects: subjectIds,
        verb: verb as InteractionType,
    };
}

export function parseInteractions(parsedBlocks: ParsedBlock[], taxa: TaxonRecord[]): TaxonRecord[] {
    // Deep copy of taxa array - all modifications will be made to this copy
    const taxaWithInjectedInteractions = structuredClone(taxa);

    // Create a map of all blocks by their blockName for easy parent lookup
    const blocksByName = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        // console.log(block.parsedBlockData)
        if (block.blockName) {
            blocksByName.set(block.blockName, block);
        }
    });

    // First pass: collect only the blocks we want to process
    const interactionsBlocks = new Map<string, ParsedBlock>();
    parsedBlocks.forEach(block => {
        if (block.blockType === "LarvaeBlock") {
            interactionsBlocks.set(block.blockName, block);
        }
    });

    // Second pass: process only the collected valid blocks
    interactionsBlocks.forEach(interactionsBlock => {
        // Find parent block's id
        // blockType NameBlock and SynonymBlock need to go up 2 levels to get parent info
        let parentData: TaxonRecord | null
        const validParentBlock = blocksByName.get(`${interactionsBlock.parentBlock}_NameBlock_1`);
        // console.log("Parent: ", validParentBlock)
        parentData = validParentBlock?.parsedBlockData

        if (parentData?.id) {
            // Find the matching taxon in our deep copy
            const taxonToUpdate = taxaWithInjectedInteractions.find(taxon => taxon.id === parentData?.id);

            if (taxonToUpdate && interactionsBlock.parsedBlockData) {
                // These modifications are being made to our deep copy
                if (!taxonToUpdate.interactions) {
                    taxonToUpdate.interactions = [];
                }
                taxonToUpdate.interactions.push(interactionsBlock.parsedBlockData);
            }
        }
    });

    // Return the modified deep copy
    return taxaWithInjectedInteractions;
}