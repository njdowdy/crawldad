import { uuidv7 } from "uuidv7";
import { Host } from "../types";

// If you’re using an actual UUID library, import it (e.g. from 'uuidv7')
// Otherwise adapt to any desired UUID generation you have installed.

const hostCache: Map<string, Host> = new Map();

/**
 * Retrieves a Host by a case-insensitive (lowercased) name.
 */
export function getHostFromCache(hostName: string): Host | undefined {
    return hostCache.get(hostName.toLowerCase());
}

/**
 * Adds a new Host to the cache, keyed by hostName.toLowerCase().
 */
export function addHostToCache(name: string): Host {
    const nameKey = name.toLowerCase();
    if (hostCache.has(nameKey)) {
        // Already exists, just return it
        // (In practice you might want custom logic or to throw an error.)
        return hostCache.get(nameKey)!;
    }

    const newHost: Host = {
        id: uuidv7(),
        name
    };
    hostCache.set(nameKey, newHost);
    return newHost;
}

/**
 * Clears all data from the host cache.
 */
export function clearHostCache(): void {
    hostCache.clear();
}

/**
 * Returns all hosts in the cache as an array.
 */
export function getAllHostsFromCache(): Host[] {
    return Array.from(hostCache.values());
}

/**
 * Returns the count of all hosts in the cache.
 */
export function getHostCacheSize(): number {
    return hostCache.size;
}
