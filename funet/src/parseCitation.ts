import { Article, Journal, Page, PageSubelement, PageType, PageSubelementType, Issue, Volume } from './types';

// type Article = {
//     id?: string,
//     title?: string,
//     issue?: Issue,
//     citedAs: string[],
//     tags?: Tag[],
//     pages?: Page[],
//     authors?: Authorship[],
//     funetRefLink?: string,
//     externalLink?: string,
//     doi?: string,
// }

interface IssueMatchResult {
    issueLabel: string;
    externalLink?: string;
    remainingText: string;
}

export function extractIssueInfo(journalText: string, element?: Element): IssueMatchResult {
    // Split text at colon and only look for issue labels in the left part
    const [leftPart] = journalText.split(':');

    const result: IssueMatchResult = {
        issueLabel: '',
        externalLink: undefined,
        remainingText: journalText
    };

    const issueMatch = leftPart.match(/\((.+)\)/);
    if (issueMatch) {
        // Handle cases where issue is in form of (1) (2) - keep the last one (2)
        result.issueLabel = issueMatch[1].includes(')')
            ? issueMatch[1].split(/[()]/g).filter(Boolean).pop()?.trim() || ''
            : issueMatch[1];

        // Remove the issue only from the left part of the text
        const cleanedLeftPart = leftPart.replace(issueMatch[0], '').trim();
        const [, ...rightParts] = journalText.split(':');
        result.remainingText = rightParts.length > 0
            ? `${cleanedLeftPart}: ${rightParts.join(':')}`
            : cleanedLeftPart;

        // Check for external link if element is provided
        if (element) {
            const externalLink = Array.from(element.querySelectorAll('a'))
                .find(a => a.textContent?.trim() === issueMatch[0]);
            result.externalLink = externalLink?.getAttribute('href') || undefined;
        }
    }

    return result;
}

export function parseCitation(element: Element): Article {
    /**
     * Parses citation information from an HTML element.
     * 
     * @param element - The HTML Element containing citation information.
     *                  Expected to have the following structure:
     *                  - An optional <a> tag with an href attribute (FUNET link)
     *                    containing an <i> tag (journal name)
     *                  - An optional <b> tag (volume number)
     *                  - Text content following the pattern: 
     *                    "(issue): pages, figures (notes)"
     * 
     * @returns CitationData object containing:
     *          - funetLink: string (optional)
     *          - journalName: string (optional)
     *          - volume: string (optional)
     *          - issue: string (optional)
     *          - pages: Page[] (array of Page objects)
     *          - notes: string (optional)
     *          - otherData: string (optional)
     *          - archiveLink: string (optional)
     */
    //? example: <div><a href="#21133"><i>Wien. Ent. Monats. </i><b>8</b> (6)</a>: <a href="https://archive.org/stream/wienerentomologi08wien#page/195/mode/1up">195</a>,  pl. <a href="https://archive.org/stream/wienerentomologi08wien#page/n508/mode/1up">5</a>,  f. 13-14</div>

    const articleResult: Article = {
        citedAs: [],
        issue: undefined,
        tags: [],
        pages: [],
        authors: [],
        funetRefLink: undefined,
        externalLink: undefined,
        doi: undefined,
        notes: []
    };
    const issueResult: Issue = {
        label: '',
        volume: {
            label: '',
            journal: {
                title: '',
                abbreviation: ''
            }
        }
    };
    const volumeResult: Volume = {
        label: '',
        journal: {
            title: '',
            abbreviation: ''
        }
    };
    const journalResult: Journal = {
        title: '',
        abbreviation: ''
    };

    // Extract full text content
    const fullText = element.textContent || '';

    const citationText = fullText.split(';')[0]; // Get the text before the first semicolon

    // console.log('initial citationText:', citationText);

    // this is parsed elsewhere
    // const typeInfoText = fullText.split(';').slice(1).join(";").trim(); // Get the text after the first semicolon

    let journalText = citationText.split(":")[0].trim();
    let pageText = citationText.split(":").slice(1).join(":").trim();

    // console.log('initial journalText:', journalText);

    // Extract FUNET link and journal name
    const journalLink = element.querySelector('a'); // citation may or may not be wrapped in an <a> tag
    const italicElement = element.querySelector('i'); // journal name always wrapped in <i> tag

    // Extract funet link
    if (journalLink) {
        articleResult.funetRefLink = journalLink.getAttribute('href') || undefined;
    }

    // Extract "in ... " note
    articleResult.notes = [];
    const inNoteMatch = journalText.match(/^(in\s+.*),/);
    if (inNoteMatch) {
        articleResult.notes.push(inNoteMatch[1]);
        journalText = journalText.replace(inNoteMatch[0], '').trim();
    }

    // Extract journal name
    if (italicElement) {
        journalResult.abbreviation = italicElement.textContent?.trim() || '';
        // remove the <i> tag and its contents from journalText
        journalText = journalText.replace(italicElement.textContent?.trim() || '', '').trim();
    }

    // Extract issue in form of (${issue})
    const { issueLabel, externalLink, remainingText } = extractIssueInfo(journalText, element);
    issueResult.label = issueLabel;
    issueResult.externalLink = externalLink;
    journalText = remainingText;

    // Extract volume
    if (journalText.trim() !== '') {
        volumeResult.label = journalText.replace('.-', '').trim();
    }

    // console.log('final journalText:', journalText);

    // Extract pages, plates, and figures
    articleResult.pages = [];

    // Extract notes
    const notesMatch = pageText.match(/\((.*?)\)$/);
    if (notesMatch) {
        // if the notes are not 'list', add them to the result, otherwise we will parse alongside the page info
        if (notesMatch[1] !== 'list') {
            // replace illegal characters (") from match with "'"
            const cleanedNotes = notesMatch[1].replace(/"/g, "'");
            articleResult.notes.push(cleanedNotes);
            pageText = pageText.replace(notesMatch[0], '').trim();
        }
    }

    // console.log('initial pageText:', pageText);

    // Extract page text info, splitting on commas and periods
    let page_parts = pageText.split(/[\s,]+/);
    let page_parts_cleaned: string[] = [];

    // merge entries on either side of a '-' or 'to' entry
    page_parts.map((part, index) => {
        // if current element is '-' or 'to', do nothing
        if (part === '-' || part === 'to') {
            return;
        }
        // if the previous element is '-' or 'to', do nothing
        if (page_parts[index - 1] === '-' || page_parts[index - 1] === 'to') {
            return;
        }
        // look ahead at the next element
        if (page_parts[index + 1] === '-' || page_parts[index + 1] === 'to') {
            page_parts_cleaned.push(part + ' ' + page_parts[index + 1] + ' ' + page_parts[index + 2]);
            // skip the next 2 elements
        } else if (part !== '') {
            page_parts_cleaned.push(part);
        } else {
            // skip the empty element
        }
    })

    // console.log('page_parts:', page_parts_cleaned);

    // Use a for loop instead of map to allow for skipping iterations
    for (let i = 0; i < page_parts_cleaned.length; i++) {
        const part = page_parts_cleaned[i];

        if (part === 'f.' || part === 'pl.') {
            continue;
        }

        let pageType: PageType = "page" as PageType;
        const subelements: PageSubelement[] = [];

        // look backwards to see if the past element is a 'pl.'
        if (i > 0 && page_parts_cleaned[i - 1] === 'pl.') {
            pageType = "plate" as PageType;
        }

        // look forwards to see if the next element is a 'f.'
        if (i < page_parts_cleaned.length - 1 && page_parts_cleaned[i + 1] === 'f.') {
            // check each subsequent element and append to subelements until either a 'pl.' or 'f.' is encountered
            for (let j = i + 2; j < page_parts_cleaned.length; j++) {
                if (page_parts_cleaned[j] === 'pl.' || page_parts_cleaned[j] === 'f.') {
                    break;
                } else {
                    subelements.push({
                        type: "illustration" as PageSubelementType,
                        label: page_parts_cleaned[j].replace(/\.$/, '')
                    });
                }
                i = j; // Skip processed elements
            }
        }

        // check if next element is '(list)'
        if (i < page_parts_cleaned.length - 1 && page_parts_cleaned[i + 1] === '(list)') {
            pageType = "table" as PageType;
            i++; // Skip the '(list)' element
        }

        const cleanedPageLabel = part.replace(/\.$/, '').
            replace(/#[A-Za-z0-9]+$/, '').
            replace(/^\(/, '').replace(/\)$/, '').
            replace(/^\.\s+/, '')

        const page: Page = {
            type: pageType,
            label: cleanedPageLabel,
            subelements: subelements
        };

        articleResult.pages?.push(page);
    }

    // use parsed page labels to find any corresponding links
    // loop through all links in the element and check if they match the page labels
    // if a match is found, add the href from the link to the page object's link field
    articleResult.pages?.forEach(page => {
        const link = element.querySelector(`a[href*="#page/${page.label}"]`);
        // if no link is found, try searching the textContent of the links
        if (link) {
            page.link ??= link.getAttribute('href') || undefined;
        } else {
            const textLink = Array.from(element.querySelectorAll('a')).find(a => a.textContent?.trim() === page.label);
            if (textLink) {
                page.link ??= textLink.getAttribute('href') || undefined;
            }
        }
    });


    // console.log('Final result:', JSON.stringify(result, null, 2));

    // Extract other data (anything in parentheses after the main citation)
    // const otherDataMatch = fullText.match(/\((.*?)\)$/);
    // if (otherDataMatch) {
    //     result.otherData = otherDataMatch[1].trim();
    // }

    // Extract archive link
    // const pageLinks = Array.from(element.querySelectorAll('a')).slice(1);  // Skip the first link (journal)
    // const archiveLink = pageLinks.find(link => link.href.includes('archive.org'));
    // if (archiveLink) {
    //     result.archiveLink = archiveLink.getAttribute('href') || undefined;
    // }

    // combine issue, volume, and journal into article
    articleResult.issue = issueResult;
    articleResult.issue.volume = volumeResult;
    articleResult.issue.volume.journal = journalResult;

    return articleResult;
}
