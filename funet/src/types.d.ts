type Journal = {
    id?: string,
    title: string,
    abbreviation: string,
}

type Volume = {
    id?: string,
    label: string, // e.g., volume number
    year?: number,
    bracketedYear?: boolean,
    journal: Journal,
}

type Issue = {
    id?: string,
    label: string, // e.g., issue number
    volume: Volume,
    externalLink?: string,
}

type Person = {
    id?: string,
    givenName?: string,
    nicknames?: string[],
    surname?: string,
    birthName?: string,
    middleNames?: {
        position: number,
        middleName: string,
    }[],
    suffix?: string[],
    salutation?: string,
    easternNameOrder: boolean,
    aliases?: string[],
}

type CreditCategories = 'conceptualization' | 'data_curation' | 'formal_analysis' | 'funding_acquisition' | 'investigation' | 'methodology' | 'project_administration' | 'resources' | 'software' | 'supervision' | 'validation' | 'visualization' | 'writing_original_draft' | 'writing_review_editing'
type CreditDegree = 'lead' | 'equal' | 'supporting' | 'other'

type Authorship = {
    position: number,
    author: Person,
    creditCategories?: { role: CreditCategories, degree?: CreditDegree }[],
}

type Tag = {
    id?: string,
    tag: string,
}

type Article = {
    id?: string,
    title?: string,
    issue?: Issue,
    citedAs: string[],
    tags?: Tag[],
    pages?: Page[],
    authors?: Authorship[],
    abbreviation?: string,
    funetRefLink?: string, // ! NOTE THESE ARE NOT UNIQUE TO ARTICLES, ONLY BIBLIOGRAPHY LINE ITEMS (multiple articles)
    externalLink?: string,
    doi?: string | null,
    notes?: string[] | null,
}

type PageType = "page" | "plate"

type PageSubelementType = 'text' | 'photograph' | 'illustration' | 'table' | 'chart' | 'multimedia' | 'other'

type PageSkeleton = {
    id?: string,
    label: string,
    link?: string,
    content?: string,
}

type Page = PageSkeleton & {
    type: PageType,
    subelements?: PageSubelement[],
}

export type PageSubelement = PageSkeleton & {
    type?: PageSubelementType,
    labelText?: string,
}

// Web resources and mentions
export type WebResource = {
    id?: string,
    abbreviation: string,
    resourceName?: string,
    externalLink?: string,
    webSubelement: WebSubelement[],
}

export type WebSubelementType = 'web_page' | 'pdf' | 'image' | 'video' | 'audio' | 'other'

export type WebSubelement = {
    id?: string,
    externalLink?: string,
    subelementType: WebSubelementType,
}

export type DataConnectionType = 'original_description' | 'namePublishedIn' | 'treated_as_type' | 'treated_as_synonym' | 'applied_name' | 'unresolved_name' | 'misidentification' | 'misapplication'
// DwC: "namePublishedIn": A citation of the first publication of the name in its given combination, not the basionym / original name.

// DataConnections / Citations / Evidence / Mentions / Sensu
type BaseDataConnection = {
    id?: string,
    dataConnectionType?: DataConnectionType,
    notes?: string[],
    linkedResources?: LinkedResource[],
}

export type LinkedResource = {
    id?: string,
    description?: string,
    notes?: string[],
    source?: string,
    resource_type?: 'web' | 'literature'
}

// type Alias = BaseDataConnection & {
//     alias: string,
//     claimType: 'alias',
// }

export type Mention = BaseDataConnection & {
    mentionedAs?: string,
}

type SynonymClaim = BaseDataConnection & {
    synonymOfId?: string,
    lastEffectiveDate?: string,
}

export type TypeClaim = BaseDataConnection & {
    typeOfId?: string,
}


type Misapplication = BaseDataConnection & {
    misappliedTo?: string,
    otherMisappliedText?: string,
}


type InteractionType = 'consumes' | 'nectars' | 'pollinates' | 'parasitizes' | 'vectors' | 'disperses' | 'parasitized_by'

export type Interaction = {
    verb: InteractionType,
    subjects: string[]
}

export type Host = {
    id: string,
    name: string
};

export type DataConnection = BaseDataConnection | SynonymClaim | Mention | Misapplication | TypeClaim;

type NameTypes = 'nomen_alternativum' | 'nomen_conservandum' | 'nomen_dubium' | 'nomen_ambiguum' | 'nomen_confusum' | 'nomen_perplexum' | 'nomen_periculosum' | 'nomen_erratum' | 'nomen_illegitimum' | 'nomen_invalidum' | 'nomen_manuscriptum' | 'nomen_monstrositatum' | 'nomen_novum' | 'nomen_nudum' | 'nomen_oblitum' | 'nomen_protectum' | 'nomen_rejiciendum' | 'nomen_suppressum' | 'nomen_vanum' | 'nomina_circumscribentia' | 'species_inquirenda'

type ICZNSynonymTypes = 'accepted' | 'misapplied' | 'junior_synonym' | 'objective_synonym' | 'subjective_synonym' | 'senior_synonym'

type IBCSynonymTypes = 'accepted' | 'misapplied' | 'synonym' | 'homotypic_nomenclatural_synonym' | 'heterotypic_taxonomic_synonym'

export type InfraspeciesType = "subspecies" | "variant" | "form" | "aberration" | "subvariety" | "forma_specialis" | "cultivar"

// export type UnresolvedName = {
//     id?: string,
//     name: string,
//     dataConnection: DataConnection[]
// }

type TaxonRecord = {
    id?: string,
    parentId: string,
    acceptedNameUsage: string, // DwC: The full scientific name, when scientificName is synonym or misapplied: Tamias minimus (valid name for Eutamias minimus)
    acceptedNameId: string, //  id of valid name
    canonicalName: string, // Roptrocerus typographi
    scientificName: string, // DwC: Roptrocerus typographi (Györfi, 1952) // === "Available Name"
    scientificNameAuthorship?: string, // DwC: (Györfi, 1952)
    specificEpithet?: string, // DwC: concolor (for scientificName Puma concolor concolor (Linnaeus, 1771))
    infragenericEpithet?: string,// DwC: Abacetillus (for scientificName Abacetus (Abacetillus) ambiguus)
    infraspecificEpithet?: string, // DwC: concolor (for scientificName Puma concolor concolor (Linnaeus, 1771))
    namePublishedInYear?: string | null, // DwC: 1915
    valid?: boolean,
    explicitHigherTaxon?: boolean, // funet/reference sometimes leaves off the genus name
    explicitSpecificEpithet?: boolean, // funet/reference sometimes leaves off the species name
    taxonRank: string, // DwC: The taxonomic rank of the most specific name in the dwc:scientificName
    taxonomicStatus?: ICZNSynonymTypes | IBCSynonymTypes, // DwC: homotypic synonym
    nomenclaturalStatus?: NameTypes, // DwC: nom. ambig.
    // aliases?: Alias[],
    vernacularNames?: { language: string, vernacularNames: string[], regions?: string[] }[],
    // citation?: Article,
    dataConnections?: Array<DataConnection & Mention>,
    typeTaxonId?: string,
    typeLocality?: string,
    funetRefLink?: string,
    assertedDistribution?: string[],
    infraspeciesType?: InfraspeciesType,
    transliterated?: {
        id?: string,
        letter: string,
        location: number,
        replacementCharacter: string,
    }[],
    originalRank?: string,
    originalGenus?: string,
    originalInfragenericEpithet?: string,
    originalSpecificEpithet?: string,
    originalInfraspecificEpithet?: string,
    originalInfraspeciesType?: string,
    unresolvedChildren?: UnresolvedName[],
    interactions?: Interaction[],
    uncertainStatus?: boolean,
}

export type HigherLevelTaxon = {
    id: string;
    rank: string;
    name: string;
    parentId: string | null;
}

// Export all types
export {
    Journal, Volume, Issue, Person, Authorship, Tag, Article, Page, PageType, PageSubelementType, PageSubelement, PageSkeleton,
    WebResource, WebSubelement, Claim, EvidenceItem, AliasClaim, WebMentionClaim,
    Misapplication, LiteratureMentionClaim, Aliases, TaxonRecord,
    CreditCategories, CreditDegree, WebSubelementType, SynonymClaim,
    InteractionType, NameTypes, ICZNSynonymTypes, IBCSynonymTypes, AvailableName, AliasName, Synonym
}