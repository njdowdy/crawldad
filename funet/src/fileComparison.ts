import * as fs from 'fs';

// Define the paths for the three files
const initialCachePath = './data/initialCache/pageCacheKeys.json';
const loadedCachePath = './data/LoadedPageCacheKeys.json';
const reprocessedCachePath = './data/pageCacheKeys.json';

try {
    // Read the contents of all three files
    const initialCacheContent = fs.readFileSync(initialCachePath, 'utf-8');
    const loadedCacheContent = fs.readFileSync(loadedCachePath, 'utf-8');
    const reprocessedCacheContent = fs.readFileSync(reprocessedCachePath, 'utf-8');

    // Convert file contents to sets of lines
    const linesInitial = new Set(initialCacheContent.split('\n').map(line => line.trim()));
    const linesLoaded = new Set(loadedCacheContent.split('\n').map(line => line.trim()));
    const linesReprocessed = new Set(reprocessedCacheContent.split('\n').map(line => line.trim()));

    console.log(`Initial Cache has ${linesInitial.size} lines.`);
    console.log(`Loaded Cache has ${linesLoaded.size} lines.`);
    console.log(`Reprocessed Cache has ${linesReprocessed.size} lines.`);

    // Find lines that are in initial cache but not in loaded cache
    const uniqueToInitial = Array.from(linesInitial).filter(line => !linesLoaded.has(line));
    console.log(`Found ${uniqueToInitial.length} unique lines in Initial Cache not in Loaded Cache.`);

    // Find lines that are in loaded cache but not in reprocessed cache
    const uniqueToLoaded = Array.from(linesLoaded).filter(line => !linesReprocessed.has(line));
    console.log(`Found ${uniqueToLoaded.length} unique lines in Loaded Cache not in Reprocessed Cache.`);

    // Find lines that are in reprocessed cache but not in initial cache
    const uniqueToReprocessed = Array.from(linesReprocessed).filter(line => !linesInitial.has(line));
    console.log(`Found ${uniqueToReprocessed.length} unique lines in Reprocessed Cache not in Initial Cache.`);

    // Print the unique lines for each comparison
    if (uniqueToInitial.length !== 0) {
        console.log('Unique lines in Initial Cache not in Loaded Cache:');
        uniqueToInitial.forEach(line => {
            if (line) {
                // console.log(line);
            }
        });
    }

    if (uniqueToLoaded.length !== 0) {
        console.log('Unique lines in Loaded Cache not in Reprocessed Cache:');
        uniqueToLoaded.forEach(line => {
            if (line) {
                // console.log(line);
            }
        });
    }

    if (uniqueToReprocessed.length !== 0) {
        console.log('Unique lines in Reprocessed Cache not in Initial Cache:');
        uniqueToReprocessed.forEach(line => {
            if (line) {
                // console.log(line);
            }
        });
    }


} catch (error) {
    console.error('Error reading files:', error);
}