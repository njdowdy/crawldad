// browserManager.ts
import { Browser, BrowserContext, Page, chromium } from 'playwright';

export class BrowserManager {
    private static browser: Browser | null = null;
    private static context: BrowserContext | null = null;
    private static page: Page | null = null;

    static async getPage(): Promise<Page> {
        if (!this.browser) {
            this.browser = await chromium.launch({ headless: true });
        }
        if (!this.context) {
            this.context = await this.browser.newContext({
                viewport: { width: 800, height: 600 },
                // Optionally disable resources you don't need
                // For example, block images and stylesheets
                // permissions: [], // adjust as needed
            });
        }
        if (!this.page) {
            this.page = await this.context.newPage();

            // Optionally block unnecessary resources
            await this.page.route('**/*', (route) => {
                const request = route.request();
                const resourceType = request.resourceType();
                if (['image', 'stylesheet', 'font', 'media'].includes(resourceType)) {
                    route.abort();
                } else {
                    route.continue();
                }
            });
        }
        return this.page;
    }

    static async closeBrowser(): Promise<void> {
        if (this.page) {
            await this.page.close();
            this.page = null;
        }
        if (this.context) {
            await this.context.close();
            this.context = null;
        }
        if (this.browser) {
            await this.browser.close();
            this.browser = null;
        }
    }
}