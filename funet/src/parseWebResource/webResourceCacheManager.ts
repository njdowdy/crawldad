import { uuidv7 } from "uuidv7";
import { WebResource, WebSubelement } from "../types";

const webResourceCache: Map<string, WebResource> = new Map();

export function getWebResourceByFunetRefLink(abbreviation: string): WebResource | undefined {
    const resource = webResourceCache.get(abbreviation);
    // console.log(`Cache lookup - Key: "${abbreviation}", Found: ${!!resource}, Cache keys: [${Array.from(webResourceCache.keys())}]`);
    return resource;
}

export function addOrUpdateWebResource(webResource: WebResource): void {
    if (!webResource.abbreviation) {
        throw new Error('Cannot cache WebResource without abbreviation');
    }
    // console.log(`Cache update - Key: "${webResource.abbreviation}", Cache keys before update: [${Array.from(webResourceCache.keys())}]`);
    webResourceCache.set(webResource.abbreviation, webResource);
}

/**
 * Upsert a subelement (like a detailed link) for an existing WebResource. 
 * @param abbreviation The resource’s cache key
 * @param externalLink The link (e.g. http://yutaka.it-n.jp/pie/20110001.html)
 * @param subelementType For example: "web_page"
 * @returns The subelement’s id (for use in a LinkedResource)
 */
export function upsertWebResourceSubelement(
    abbreviation: string,
    externalLink: string,
    subelementType: string = "web_page"
): string | undefined {
    const resource = webResourceCache.get(abbreviation);
    if (!resource) {
        // Could either throw or return undefined if the resource doesn’t exist
        return undefined;
    }


    // Initialize webSubelement array if none
    if (!resource.webSubelement) {
        resource.webSubelement = [];
    }

    // Check if subelement with the same external link already exists
    let sub = resource.webSubelement.find(s =>
        encodeURI((s.externalLink || '').trim()) === externalLink
    );
    if (!sub) {
        // Create a new subelement
        sub = {
            id: uuidv7(),
            externalLink: encodeURI(externalLink.trim()),
            subelementType
        } as WebSubelement;
        resource.webSubelement.push(sub);
    }

    // Put the resource back into the cache with the updated subelement
    webResourceCache.set(abbreviation, resource);

    return sub.id;
}

export function clearWebResourceCache(): void {
    webResourceCache.clear();
}

export function getWebResourceCacheSize(): number {
    return webResourceCache.size;
}

export function getAllWebResourcesFromCache(): WebResource[] {
    return Array.from(webResourceCache.values());
}

export function getTotalWebSubelementCount(): number {
    return Array.from(webResourceCache.values())
        .reduce((total, resource) =>
            total + (resource.webSubelement?.length || 0), 0);
}

export function getWebResourceById(id: string): WebResource | undefined {
    return Array.from(webResourceCache.values()).find(resource => resource.id === id);
}