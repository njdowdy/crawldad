import { uuidv7 } from "uuidv7";
import { LinkedResource, WebResource, WebSubelement } from "../types";
import {
    getWebResourceByFunetRefLink,
    addOrUpdateWebResource
} from "./webResourceCacheManager";

export function parseWebResources(element: Element): WebResource {
    let externalLink = element.querySelector('a')?.getAttribute('href') || undefined;
    if (!externalLink?.startsWith('http')) { externalLink = undefined }
    const resourceNameText = element.textContent?.trim() || '';


    const abbreviation = resourceNameText.match(/\[([^\]]*?)\]/)?.[1]?.trim() || 'ERROR!';

    const resourceName = resourceNameText.includes('\n')
        ? resourceNameText.split('\n')[1].split(';')[0].trim()
        : abbreviation;

    // Create new resource if not found
    const webResource: WebResource = {
        id: uuidv7(),
        abbreviation,
        resourceName,
        externalLink,
        webSubelement: []
    };

    // console.log("RL Item: ", JSON.stringify(webResource, null, 2))

    // Try to get existing resource first
    const existingResource = getWebResourceByFunetRefLink(abbreviation);
    if (existingResource) {
        // console.log("abbreviation:", abbreviation)
        // console.log("existingResource:", existingResource)
        // console.log("webResource:", webResource)

        // Update external link if it exists and different
        if (externalLink && existingResource.externalLink !== externalLink) {
            existingResource.externalLink = encodeURI(externalLink.trim());
            addOrUpdateWebResource(existingResource);
        }
        return existingResource;
    }

    addOrUpdateWebResource(webResource);
    return webResource;
}