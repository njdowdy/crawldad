import { createReadStream, existsSync } from 'fs';
import { Article, HigherLevelTaxon, Host, Issue, Journal, Person, TaxonRecord, Volume, WebResource } from './types';
// @ts-ignore
import JSONStream from 'JSONStream';
import { getTaxonCacheSize } from './parseNames/taxonCacheManager';
import { getHigherTaxaCacheSize } from './parseHigherLevelTaxa/higherTaxonCacheManager';
import { getWebResourceCacheSize } from './parseWebResource/webResourceCacheManager';
import { getPersonCacheSize } from './parseRelatedLiterature/personCacheManager';
import { getJournalCacheSize } from './parseRelatedLiterature/journalCacheManager';
import { getVolumeCacheSize } from './parseRelatedLiterature/volumeCacheManager';
import { getIssueCacheSize } from './parseRelatedLiterature/issueCacheManager';
import { getArticleCacheSize } from './parseRelatedLiterature/articleCacheManager';
import { getHostCacheSize } from './parseHosts/hostCacheManager';
import { getPageCacheSize } from './parseRelatedLiterature/pageCacheManager';
import { Distribution, getDistributionCacheSize } from './parseDistributions/assertedDistributionCacheManager';
import { getLocalityCacheSize, Locality } from './parseDistributions/typeLocalityCacheManager';

/**
 * Example: load a large JSON file via streaming
 * @param {string} path
 * @returns {Promise<any[]>} - Array of parsed JSON objects
 */
export async function loadJsonFile(path: string): Promise<any[]> {
    if (!existsSync(path)) {
        console.log(`No existing cache found for: ${path}`);
        return [];
    }
    return new Promise((resolve, reject) => {
        const data: any[] = [];
        try {
            const readStream = createReadStream(path, { encoding: 'utf8' });

            // JSONStream.parse('*') will parse each element of a top-level array
            // If your JSON is an object with nested arrays, you'll need to adjust the path accordingly (e.g. 'nestedArray.*')
            const parseStream = JSONStream.parse('*');

            parseStream.on('data', (obj: any) => {
                data.push(obj);
            });

            parseStream.on('end', () => {
                console.log(`Loaded ${data.length} items from ${path} via streaming.`);
                resolve(data);
            });

            // @ts-ignore
            parseStream.on('error', (err) => {
                reject(new Error(`Error streaming JSON from ${path}: ${err}`));
            });

            readStream.pipe(parseStream);
        } catch (err) {
            reject(new Error(`Could not create stream from ${path}: ${err}`));
        }
    });
}

export async function loadCacheData(): Promise<void> {
    try {
        console.log('\n-------------------');
        console.log('Checking for existing cache data...');
        // Helper function to load and parse JSON file
        // const loadJsonFile = async (path: string) => {
        //     try {
        //         const data = await fs.readFile(path, 'utf8');
        //         console.log(`Found existing cache: ${path}`);
        //         return JSON.parse(data);
        //     } catch (error) {
        //         console.log(`No existing cache found for: ${path}`);
        //         return [];
        //     }
        // };

        // Load all cache files
        const [
            articleCache,
            webCache,
            higherTaxaData,
            personCache,
            journalCache,
            volumeCache,
            issueCache,
            taxonCache,
            hostCache,
            assertedDistributionCache,
            localityCache,
        ]: [Article[], WebResource[], HigherLevelTaxon[], Person[], Journal[], Volume[], Issue[], TaxonRecord[], Host[], Distribution[], Locality[]] = await Promise.all([
            loadJsonFile('./data/caches/articleCache.json'),
            loadJsonFile('./data/caches/webCache.json'),
            loadJsonFile('./data/caches/higherTaxaData.json'),
            loadJsonFile('./data/caches/personCache.json'),
            loadJsonFile('./data/caches/journalCache.json'),
            loadJsonFile('./data/caches/volumeCache.json'),
            loadJsonFile('./data/caches/issueCache.json'),
            loadJsonFile('./data/caches/taxonCache.json'),
            loadJsonFile('./data/caches/hostCache.json'),
            loadJsonFile('./data/caches/assertedDistributionCache.json'),
            loadJsonFile('./data/caches/localityCache.json'),

        ]);

        // Import the necessary functions to populate caches
        const {
            populateArticleCache,
            populateWebResourceCache,
            populateHigherTaxonCache,
            populatePersonCache,
            populateJournalCache,
            populateVolumeCache,
            populateIssueCache,
            populateTaxonCache,
            populateHostCache,
            populateAssertedDistributionCache,
            populateLocalityCache,
        } = await import('./cachePopulator');

        // Populate all caches with existing data
        await Promise.all([
            populateArticleCache(articleCache),
            populateWebResourceCache(webCache),
            populateHigherTaxonCache(higherTaxaData),
            populatePersonCache(personCache),
            populateJournalCache(journalCache),
            populateVolumeCache(volumeCache),
            populateIssueCache(issueCache),
            populateTaxonCache(taxonCache),
            populateHostCache(hostCache),
            populateAssertedDistributionCache(assertedDistributionCache),
            populateLocalityCache(localityCache)
        ]);

        console.log(`Loaded ${getTaxonCacheSize()} taxa`);
        console.log(`Loaded ${getHigherTaxaCacheSize()} higher level taxa`);
        console.log(`Loaded ${getWebResourceCacheSize()} web resources`);
        console.log(`Loaded ${getPersonCacheSize()} people`);
        console.log(`Loaded ${getJournalCacheSize()} journals`);
        console.log(`Loaded ${getVolumeCacheSize()} volumes`);
        console.log(`Loaded ${getIssueCacheSize()} issues`);
        console.log(`Loaded ${getArticleCacheSize()} articles`);
        console.log(`Loaded ${getPageCacheSize()} pages`);
        console.log(`Loaded ${getHostCacheSize()} hosts`);
        console.log(`Loaded ${getDistributionCacheSize()} distribution locations`);
        console.log(`Loaded ${getLocalityCacheSize()} type localities`);

        console.log('-------------------');

        // await Promise.all([
        //     writeResultsToFile(getAllArticleCacheKeys(), `./data/caches/LoadedArticleCacheKeys.json`),
        //     writeResultsToFile(getAllPageCacheKeys(), `./data/caches/LoadedPageCacheKeys.json`),
        //     writeResultsToFile(getAllIssueCacheKeys(), `./data/caches/LoadedIssueCacheKeys.json`),
        //     writeResultsToFile(getAllVolumeCacheKeys(), `./data/caches/LoadedVolumeCacheKeys.json`),
        //     writeResultsToFile(getAllJournalCacheKeys(), `./data/caches/LoadedJournalCacheKeys.json`),
        //     writeResultsToFile(getAllPersonCacheKeys(), `./data/caches/LoadedPersonCacheKeys.json`)
        // ])

    } catch (error) {
        console.error('Error loading cache data:', error);
        throw error;
    }
}