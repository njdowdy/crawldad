import { extractIssueInfo } from "./parseCitation";
import { JSDOM } from 'jsdom';
import { Article, Authorship, DataConnection, DataConnectionType, LinkedResource, NameTypes, Page, PageSubelement, PageSubelementType, SynonymClaim, TaxonRecord, TypeClaim } from "./types";
import { uuidv7 } from "uuidv7";
import { getArticleByFunetRefLink, getArticleById, getArticleFromCache, getAllArticlesFromCache, findBestArticleMatch, upsertArticle, generateArticleCacheKey } from "./parseRelatedLiterature/articleCacheManager";
import { parseAuthorshipString } from "./parseRelatedLiterature/authorshipUtilities";
import { composeArticleObject, composeIssueObject } from "./parseRelatedLiterature/publicationUtilities";
import { getOrCreateLocality } from "./parseDistributions/typeLocalityCacheManager";
import { transliterateEpithet } from "./parseNames/parse";


export function parseNameAndAuthorship(text: string): {
    name: string;
    authorship: string;
    remainingText: string;
    parentheticalAuthorship: boolean;
} {
    // Split by semicolon and take the first part
    const [firstPart, ...rest] = text.split(';');
    const remainingText = rest.join(';').trim();

    // Split the first part into words
    const words = firstPart.trim().split(' ');
    // If the first entry starts with a capital letter, assume it's the genus
    let startsWithGenus = /^(\?)?[A-Z]/.test(words[0]);

    let name = '';
    let authorStartIndex = 0;

    // Try to find a year-like pattern
    let yearIndex = words.findIndex(word =>
        /\[?\d{4}\]?|\[?\d{3}[xX]\]?|\[?\d{2}[xX][xX]\]?|\[?\d{4}\?\]?/.test(word)
    );

    // Find first obviously lowercase word that's likely part of the species (beyond the first word)
    let firstLowerCaseIndex = words.findIndex((word, index) => {
        if (index === 0) return false; // skip the first word (genus)
        // skip typical "von / van / de / etc." lowercased connectors
        if (/^(da|de|del|della|den|der|des|di|du|la|le|los|ten|ter|van|von|zu)$/i.test(word)) return false;
        // skip weird partial uppercase words like "sambawnaSmith"
        if (/^[a-z].*[A-Z]/.test(word)) return false;
        return /^[a-z]/.test(word);
    });

    // If no year found, treat it as if year is at the end
    if (yearIndex === -1) {
        yearIndex = words.length;
    }

    // If no straightforward "first lower case" is found, attempt to place it before the year
    // if (firstLowerCaseIndex === -1) {
    //     firstLowerCaseIndex = yearIndex - 2;
    // }
    if (yearIndex === -1 && firstLowerCaseIndex === -1) {
        firstLowerCaseIndex = yearIndex - 1;
    }
    // if (text.includes('lineata Heath')) {
    //     console.log("text:", text)
    //     console.log("name1:", name)
    //     console.log("startsWithGenus:", startsWithGenus)
    // }
    if (startsWithGenus) {
        // console.log("words1:", words)
        // Check for subgenus pattern: Genus (Subgenus) species
        if (
            words.length >= 3 &&
            words[1].startsWith('(') &&
            words[1].endsWith(')') &&
            /^(\?)?[A-Z]/.test(words[1].slice(1, -1))
        ) {
            // This is a subgenus case
            const genus = words[0];
            const subgenus = words[1]; // Keep parentheses for subgenus
            const remainingWords = words.slice(2); // I'm not sure why we do this, but I don't want to break it at the moment
            // adjust the previously calculated indexes
            const tempFirstLowerCaseIndex = firstLowerCaseIndex !== -1 ? firstLowerCaseIndex - 2 : firstLowerCaseIndex
            const tempYearIndex = yearIndex !== -1 ? yearIndex - 2 : yearIndex
            const tempStartsWithGenus = false

            // Look for authorship after the species name
            authorStartIndex = determineAuthorStartIndex(remainingWords, tempFirstLowerCaseIndex, tempYearIndex, tempStartsWithGenus);

            if (authorStartIndex === -1) {
                // No authorship found => entire remainder is the name
                name = `${genus} ${subgenus} ${remainingWords.join(' ')}`.trim();
                authorStartIndex = words.length; // Set to end so authorship is empty
            } else {
                name = `${genus} ${subgenus} ${remainingWords.slice(0, authorStartIndex).join(' ')}`.trim();
                // Adjust for the positions we sliced: "genus subgenus" => 2 words already used
                authorStartIndex += 2;
            }
            // if (text.includes('Eriocephala Curtis, 1839')) {
            //     console.log("firstPart:", firstPart)
            //     console.log("remainingText:", remainingText)
            //     console.log("words:", words)
            //     console.log("startsWithGenus:", startsWithGenus)
            //     console.log("yearIndex:", yearIndex)
            //     console.log("firstLowerCaseIndex:", firstLowerCaseIndex)
            //     console.log("genus:", genus)
            //     console.log("subgenus:", subgenus)
            //     console.log("remainingWords:", remainingWords)
            //     console.log("authorStartIndex:", authorStartIndex)
            //     console.log("name:", name)
            // }
        } else {
            // console.log("words2:", words)
            // Original genus-species handling
            authorStartIndex = determineAuthorStartIndex(words, firstLowerCaseIndex, yearIndex, startsWithGenus);
            // If still no authorship marker found, shift to the end
            if (authorStartIndex === -1) {
                authorStartIndex = words.length;
            }

            const genus = words[0];
            let species = '';

            // If the second token is lowercase or starts with "?", treat it as a species portion.
            // This ensures that something like "?pergaea" is included rather than skipped.
            if (words[1] && (words[1].startsWith('?') || /^[a-z]/.test(words[1]))) {
                species = ' ' + words.slice(1, authorStartIndex).join(' ').trim();
            }
            name = `${genus}${species}`;
            // if (text.includes('Eriocephala Curtis, 1839')) {
            //     console.log("firstPart:", firstPart)
            //     console.log("remainingText:", remainingText)
            //     console.log("words:", words)
            //     console.log("startsWithGenus:", startsWithGenus)
            //     console.log("yearIndex:", yearIndex)
            //     console.log("firstLowerCaseIndex:", firstLowerCaseIndex)
            //     console.log("genus:", genus)
            //     console.log("authorStartIndex:", authorStartIndex)
            //     console.log("name:", name)
            // }
        }
    } else {
        // If the first word doesn't appear to be a genus, just grab the first as the name
        authorStartIndex = determineAuthorStartIndex(words, firstLowerCaseIndex, yearIndex, startsWithGenus);
        // console.log("words3:", words)

        // If not found, shift to the end
        if (authorStartIndex === -1) {
            authorStartIndex = words.length;
        }

        name = words[0];
    }

    // Slice out the author / year portion, if any
    const authorWords = words.slice(authorStartIndex, yearIndex + 1);

    const parentheticalAuthorship = authorWords
        .join(' ')
        .replace(/,(?=\s*$)/g, '')
        .includes('(');

    // Clean up parentheses, trailing commas, etc.
    const authorship = authorWords
        .join(' ')
        .replace(/,(?=\s*$)/g, '')
        .replace(/[()]/g, '')
        .replace(/\n\n=.*$/g, '')
        .trim();

    return {
        name,
        authorship,
        remainingText,
        parentheticalAuthorship
    };
}

function determineAuthorStartIndex(words: string[], firstLowerCaseIndex: number, yearIndex: number, startsWithGenus: boolean): number {
    // Recognized infraspecific rank abbreviations or indicators
    const infraspecificRanks = /^(var\.|subsp\.|ssp\.|forma|f\.|variety|race|cv\.)$/i;

    // Recognized short, lowercase connectors we skip (like "van", "von", etc.; see your original code).
    const connectors = /^(da|de|del|della|den|der|des|di|du|la|le|los|ten|ter|van|von|zu)$/i;

    // Potential word indicating authorship: capitalized, starts with '(' (possible "(Author," or "(Davis,"),
    // or partially uppercase/lowercase mix that might be a capital name like "Gentili-Poole".
    function isLikelyAuthorshipStart(word: string): boolean {
        // console.log("word:", word)
        // console.log("test_word_result:", /^[A-Z]/.test(word))
        // console.log("starts_with_result:", word.startsWith('('))

        return (
            /^[A-Z]/.test(word) ||
            word.startsWith('(') ||
            connectors.test(word) ||
            /^[a-z].*[A-Z]/.test(word) // e.g. "Gentili-Poole"
        );
    }

    // If we never found a lower-case species epithet, we assume "species" token is absent,
    // so we begin searching from the second token (index=1) because the first was the genus. 
    const startIndex = startsWithGenus ? 1 : 0
    let startSearchIndex = firstLowerCaseIndex !== -1 ? firstLowerCaseIndex : startIndex;

    // We traverse from startSearchIndex up to yearIndex (or words.length if no year found).
    // We'll skip known infraspecific rank words or obviously-lowercase words (the species or infraspecific epithet),
    // stopping when we see something that might be part of the authorship.
    let authorStartIndex = -1;
    const upperBound = yearIndex === -1 ? words.length : yearIndex;

    for (let i = startSearchIndex; i < upperBound; i++) {
        const word = words[i];
        // If it looks like an infraspecific rank (e.g. "var.", "ssp.", etc.) or purely lowercase,
        // skip it. They're part of the species name. 
        // But once we see something that might be authorship, we break.
        if (
            infraspecificRanks.test(word) ||
            (!isLikelyAuthorshipStart(word) && word.toLowerCase() === word) // purely lowercase => part of epithet
        ) {
            continue;
        } else {
            authorStartIndex = i;
            break;
        }
    }

    // If not found, set it to the end of the words array.
    return authorStartIndex === -1 ? words.length : authorStartIndex;
}

export function extractCitationContent(element: Element): string[] {
    const components: string[] = [];
    let currentComponent = '';

    function nodeToString(node: Node): string {
        if (node.nodeType === 3) { // Text node
            return node.textContent ?? '';
        }
        return (node as Element).outerHTML;
    }

    function addComponent() {
        if (currentComponent.trim()) {
            components.push(`<div>${currentComponent.trim()}</div>`);
        }
        currentComponent = '';
    }

    let child: ChildNode | null = element.firstChild;

    while (child) {
        // Skip <ul class="SENSU"> and following siblings
        if (child.nodeType === 1 && (child as Element).tagName === 'UL' && (child as Element).classList.contains('SENSU')) {
            break;
        }

        const nodeContent = nodeToString(child);

        if (nodeContent.includes(';')) {
            const parts = nodeContent.split(';');
            for (let i = 0; i < parts.length; i++) {
                currentComponent += parts[i];
                if (i < parts.length - 1) {
                    addComponent();
                }
            }
        } else {
            currentComponent += nodeContent;
        }

        child = child.nextSibling;
    }

    addComponent(); // Add the last component

    return components;
}

export function getCitationElement(dom: JSDOM): Element {
    const body = dom.window.document.body;
    const container = dom.window.document.createElement('div');

    let currentNode: ChildNode | null = body.firstChild;

    while (currentNode) {
        // Clone the current node (whether it's an Element or Text node)
        container.appendChild(currentNode.cloneNode(true));
        currentNode = currentNode.nextSibling;
    }

    return container;
}

function processNumberRanges(text?: string, type: PageSubelementType = 'illustration'): PageSubelement[] {
    const subelements: PageSubelement[] = [];
    if (!text) return subelements;

    text.split(',').forEach(item => {
        let itemTemp = item.trim();
        const itemNotesMatcher = itemTemp.match(/^\S+\s+(.*)$/);
        const itemNotes = itemNotesMatcher?.[1]?.trim();
        itemTemp = itemTemp.replace(`${itemNotes ?? ''}`, '').trim();

        const itemRange = itemTemp.split('-');
        if (itemRange.length === 2) {
            const start = parseInt(itemRange[0]);
            const end = parseInt(itemRange[1]);
            const range = Array.from({ length: end - start + 1 }, (_, i) => (start + i).toString());
            range.forEach(num => subelements.push({ id: uuidv7(), label: num, type }));
        } else {
            subelements.push({ id: uuidv7(), label: itemRange[0], type });
        }
    });

    return subelements;
}

type ParsedPageDetails = {
    referenceNotes?: string,
    reference: Page
}[]

/**
 * Gather all anchors in the element, returning a mapping of
 *   anchorText -> href
 * so we can later match "pl. [122]" => anchorMap["122"] => <href>.
 */
function gatherAnchorMap(element: Element): Record<string, string> {
    const map: Record<string, string> = {};
    const anchors = element.querySelectorAll('a');
    anchors.forEach(a => {
        const text = a.textContent?.trim();
        if (text) {
            map[text] = a.href;
        }
    });
    return map;
}

/**
 * Update extractPageSubelements to accept an optional anchorMap
 * and handle bracketed plate references (pl. [xxx]).
 */
export function extractPageSubelements(pageDetails: string, anchorMap: Record<string, string> = {}): ParsedPageDetails | null {
    if (pageDetails.trim() === '') { return null }
    if (/\(\d+-\d+\)/.test(pageDetails.trim())) { return null }

    let pageText = pageDetails.replace(/\s+/g, ' ').trim();
    pageText = pageText.replace(/(pl\..*?)\s*(\.\s*f\.)/g, '$1$2').replace(/\.\s*f\./g, ', f.');

    const noteMatch = pageText.match(/\((.*?)\)$/);
    let referenceNotes = noteMatch ? noteMatch[1].trim().replace(/"/g, "'") : undefined;
    let hasList = false;
    let remainingText = pageText;

    if (referenceNotes === 'list') {
        hasList = true;
        referenceNotes = undefined;
    } else {
        remainingText = noteMatch
            ? pageText.slice(0, pageText.lastIndexOf('(')).trim()
            : pageText.trim();
    }

    // Helper to expand a Roman-numeral range (e.g. iv-x => iv, v, vi, ... x).
    // If you want more robust coverage of large Roman numerals like "cvii," 
    // you could either store them verbatim (as a single chunk) or implement 
    // a bigger roman-to-integer converter here.
    function expandRomanRange(start: string, end: string): string[] {
        // A small helper for up to 20; you might expand for bigger numbers:
        const romanMap: Record<string, number> = {
            i: 1, ii: 2, iii: 3, iv: 4, v: 5,
            vi: 6, vii: 7, viii: 8, ix: 9, x: 10,
            xi: 11, xii: 12, xiii: 13, xiv: 14, xv: 15,
            xvi: 16, xvii: 17, xviii: 18, xix: 19, xx: 20
        };

        // Convert a Roman up to "xx" if recognized, otherwise store verbatim:
        function toNumber(r: string) {
            const lower = r.toLowerCase();
            return romanMap[lower] ?? 0;
        }
        function toRoman(n: number) {
            // For illustration, just return keys from romanMap if found:
            const entry = Object.entries(romanMap).find(([, num]) => num === n);
            return entry ? entry[0] : String(n); // fallback to digit
        }

        const startNum = toNumber(start) || 0;
        const endNum = toNumber(end) || 0;
        // If we can't parse them as known Roman numerals, just return as is:
        if (!startNum || !endNum) {
            return [start + '-' + end];
        }
        const results: string[] = [];
        for (let i = startNum; i <= endNum; i++) {
            results.push(toRoman(i));
        }
        return results;
    }

    const pages: ParsedPageDetails = [];

    function splitIntoComponents(text: string): string[] {
        const components = text
            .split(/(?<=^|,)\s*(?=pl\.)/)
            .map(component => component.replace(/,\s*$/, ''));

        if (components[0].trim().startsWith('pl.')) {
            return components.map(c => c.trim());
        }
        const finalComponents = [
            ...components[0].split(',').map(s => s.trim()).filter(Boolean),
            ...components.slice(1).map(s => s.trim())
        ];
        return finalComponents;
    }

    const finalComponents = splitIntoComponents(remainingText);

    if (finalComponents.length === 0) {
        pages.push({
            referenceNotes: undefined,
            reference: {
                type: 'page',
                label: ''
            }
        });
    }

    let currentComponent = 0;
    while (currentComponent < finalComponents.length) {
        const component = finalComponents[currentComponent];

        // Handle "pl. [123]" or "pl. XIV, f. 1" etc.
        if (component.includes('pl.')) {
            // Updated regex to allow digits or Roman numerals in plate label:
            const plateFigMatch = /pl\.\s*\[?([\dABEFGHJKNOPQRSTIVXLCDMivxlcdm]+)\]?(?:\s*,?\s*f\.\s*([^(pl\.]+))?/.exec(component);
            if (plateFigMatch) {
                const [_, plateNum, figureList] = plateFigMatch;

                pages.push({
                    referenceNotes,
                    reference: {
                        type: 'plate',
                        label: plateNum,
                        link: anchorMap[plateNum] ?? undefined,
                        subelements: []
                    }
                });

                // Handle any figures
                if (figureList) {
                    const figures = figureList.split(',')
                        .map(f => f.trim())
                        .flatMap(figure => {
                            if (figure.includes('-')) {
                                const [start, end] = figure.split('-').map(f => f.trim());
                                return expandFigureRange(start, end);
                            }
                            return [figure];
                        });

                    const subelements: PageSubelement[] = figures.map(figure => {
                        const [label, ...labelTextParts] = figure.split(' ');
                        return {
                            type: 'illustration',
                            label,
                            labelText: labelTextParts.length > 0
                                ? labelTextParts.join(' ')
                                : referenceNotes
                        };
                    });

                    const targetRef = pages[pages.length - 1];
                    if (subelements.length !== 0) {
                        targetRef.reference.subelements = subelements;
                    }
                }
            }
        }
        else if (component.includes("index")) {
            pages.push({
                referenceNotes,
                reference: {
                    type: 'page',
                    label: 'index'
                }
            });
        }
        // Handle purely numeric pages with or without (list)
        else if (/^\d+(?:\s*\(list\))?$/.test(component)) {
            const pageNumbers = component.includes(',')
                ? component.split(',').map(num => num.trim())
                : [component];

            pageNumbers.forEach(pageNumber => {
                if (component.includes('list')) {
                    const noteMatch = component.match(/\((.*?)\)$/);
                    let referenceNotes = noteMatch ? noteMatch[1].trim().replace(/"/g, "'") : undefined;
                    const label = noteMatch
                        ? component.slice(0, component.lastIndexOf('(')).trim()
                        : component.trim();
                    referenceNotes = undefined;
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label,
                            subelements: [{
                                type: 'table' as PageSubelementType,
                                label: referenceNotes ?? '',
                            }]
                        }
                    });
                } else {
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label: pageNumber
                        }
                    });
                }
            });
        }
        else if (component.includes('f.')) {
            const figureMatch = /f\.\s*([^(pl\.]+)/.exec(component);
            if (figureMatch) {
                const figureList = figureMatch[1];
                const lastReference = pages[pages.length - 1];

                const figures = figureList.split(',')
                    .map(f => f.trim())
                    .flatMap(figure => {
                        if (figure.includes('-')) {
                            const [start, end] = figure.split('-').map(f => f.trim());
                            return expandFigureRange(start, end);
                        }
                        return [figure];
                    });

                const subelements: PageSubelement[] = figures.map(figure => {
                    const [label, ...labelTextParts] = figure.split(' ');
                    return {
                        type: 'illustration',
                        label,
                        labelText: labelTextParts.length > 0 ? labelTextParts.join(' ') : referenceNotes
                    };
                });

                if (lastReference) {
                    lastReference.reference.subelements = [
                        ...(lastReference.reference.subelements || []),
                        ...subelements
                    ];
                } else {
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label: '',
                            subelements
                        }
                    });
                }
            }
        }
        // This block handles purely numeric pages (with possible commas or lists).
        else if (/^\d+/.test(component)) {
            const pageNumbers = component.includes(',')
                ? component.split(',').map(num => num.trim())
                : [component];

            pageNumbers.forEach(pageNumber => {
                if (component.includes('list')) {
                    const noteMatch = component.match(/\((.*?)\)$/);
                    let referenceNotes = noteMatch ? noteMatch[1].trim().replace(/"/g, "'") : undefined;
                    const label = noteMatch
                        ? component.slice(0, component.lastIndexOf('(')).trim()
                        : component.trim();
                    referenceNotes = undefined;
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label,
                            subelements: [{
                                type: 'table' as PageSubelementType,
                                label: referenceNotes ?? '',
                            }]
                        }
                    });
                } else {
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label: pageNumber
                        }
                    });
                }
            });
        }
        // NEW BLOCK for Roman numerals (with optional range, optional (list))
        else if (/^[ivxlcdmIVXLCDM]+(?:-[ivxlcdmIVXLCDM]+)?(\s*\(list\))?$/.test(component)) {
            // e.g. "iv-x", "cvii", "iv (list)"
            const hasListMarker = component.includes('(list)');
            // If there's a list marker, handle referenceNotes similarly:
            let romanRefNotes = referenceNotes;
            let label = component;
            if (hasListMarker) {
                const noteMatch = component.match(/\((.*?)\)$/);
                if (noteMatch) {
                    romanRefNotes = noteMatch[1].trim().replace(/"/g, "'");
                }
                // strip out the (list)
                label = component.slice(0, component.lastIndexOf('(')).trim();
            }
            // Check if there's a dash => range:
            if (label.includes('-')) {
                const [start, end] = label.split('-').map(x => x.trim());
                const expansions = expandRomanRange(start, end);
                // If for some reason we can't parse, expansions will be 1 element [start-end].
                expansions.forEach(pageLabel => {
                    // If (list) is present, treat it like the numeric "(list)" case:
                    if (hasListMarker) {
                        pages.push({
                            referenceNotes,
                            reference: {
                                type: 'page',
                                label: pageLabel,
                                subelements: [
                                    {
                                        type: 'table',
                                        label: romanRefNotes ?? '',
                                    }
                                ]
                            }
                        });
                    } else {
                        pages.push({
                            referenceNotes,
                            reference: {
                                type: 'page',
                                label: pageLabel
                            }
                        });
                    }
                });
            } else {
                // Single Roman numeral e.g. "cvii"
                if (hasListMarker) {
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label: label,
                            subelements: [
                                {
                                    type: 'table',
                                    label: romanRefNotes ?? '',
                                }
                            ]
                        }
                    });
                } else {
                    pages.push({
                        referenceNotes,
                        reference: {
                            type: 'page',
                            label
                        }
                    });
                }
            }
        }

        currentComponent++;
    }

    return pages;
}

export function expandFigureRange(start: string, end: string): string[] {
    // Extract numeric and alphabetic parts
    const startMatch = start.match(/(\d+)([a-z])?/i);
    const endMatch = end.match(/(\d+)([a-z])?/i);

    if (!startMatch || !endMatch) return [start, end];

    const [_, startNum, startLetter] = startMatch;
    const [__, endNum, endLetter] = endMatch;

    const figures: string[] = [];

    // If both have letters, generate range with letters
    if (startLetter && endLetter) {
        const startCode = startLetter.charCodeAt(0);
        const endCode = endLetter.charCodeAt(0);
        for (let num = parseInt(startNum); num <= parseInt(endNum); num++) {
            for (let code = startCode; code <= endCode; code++) {
                figures.push(`${num}${String.fromCharCode(code)}`);
            }
        }
    } else {
        // Just generate numeric range
        for (let num = parseInt(startNum); num <= parseInt(endNum); num++) {
            figures.push(num.toString());
        }
    }

    return figures;
}

function findPageNumberPattern(url: string, label: string): string | undefined {
    // Common URL patterns for page numbers
    const patterns = [
        `/page/${label}/`,    // /page/123/
        `/page${label}/`,     // /page123/
        `page=${label}`,      // page=123
        `/#page/${label}/`,   // /#page/123/
        `/p${label}/`,        // /p123/
        `/${label}/`          // /123/
    ];

    for (const pattern of patterns) {
        if (url.includes(pattern)) {
            return url.replace(pattern, pattern.replace(label, '{label}'));
        }
    }
    return undefined;
}

export function upsertArticlePages(articleId: string, subelements: ParsedPageDetails): string[] {
    const subelementIds: string[] = [];
    // Get article using article ID
    const article = getArticleById(articleId);
    if (!article) {
        console.warn(`No article found for reference: ${articleId}`);
        return subelementIds;
    }

    subelements.forEach(({ reference, referenceNotes }) => {
        let existingReference = article.pages?.find((p: Page) =>
            p.type === reference.type &&
            p.label === reference.label
        );

        if (!existingReference) {
            existingReference = {
                ...reference,
                id: uuidv7(),
            };
            if (!article.pages) {
                article.pages = [];
            }
            article.pages.push(existingReference);
        }

        // Handle subelements
        if (reference.subelements?.length) {
            if (!existingReference!.subelements) {
                existingReference!.subelements = [];
            }

            reference.subelements.forEach(subelement => {
                let existingSubelement = existingReference!.subelements?.find((s: PageSubelement) =>
                    s.type === subelement.type && s.label === subelement.label
                );

                if (!existingSubelement) {
                    existingSubelement = {
                        ...subelement,
                        id: uuidv7()
                    };
                    existingReference!.subelements?.push(existingSubelement);
                } else if (!existingSubelement.id) {
                    // Ensure existing subelements have IDs
                    existingSubelement.id = uuidv7();
                }

                // Update existing subelement properties
                existingSubelement.labelText = subelement.labelText ?? existingSubelement.labelText;
                existingSubelement.type = subelement.type;

                // Only push if ID exists
                if (existingSubelement.id) {
                    subelementIds.push(existingSubelement.id);
                }
            });
        } else {
            if (existingReference!.id) {
                subelementIds.push(existingReference!.id);
            }
        }
    });

    // Update cache using id
    upsertArticle(article, generateArticleCacheKey(article));
    return subelementIds;
}

export const findSiblingLink = (element: Element, text: string): HTMLAnchorElement | null => {
    const parent = element.parentElement
    const anchors = parent?.querySelectorAll('ul.SENSU > li > a');
    // Compare .textContent of each anchor (case-insensitive)
    if (anchors) {
        for (const anchor of anchors) {
            if (anchor.textContent?.toLowerCase().includes(text.toLowerCase())) {
                return anchor as HTMLAnchorElement;
            }
        }
    }
    return null;
};

export function parseNameAndSynonymBlock(element: Element, typeOverride?: DataConnectionType): Partial<TaxonRecord> | void {
    const result: Partial<TaxonRecord> = {};
    result.id = uuidv7();

    // extract the full text content first
    let fullText = element.textContent ?? '';
    let originalText = fullText;
    if (originalText === '') return

    // extract name
    const nameElement = element.querySelector('i');

    if (nameElement?.textContent) {
        // Get the raw scientific name from the element
        result.scientificName = nameElement.textContent.trim();
    }
    // ─────────────────────────────────────────────────────────────────
    //   NEW: Check for "(= …)" portion in the name or in the full text
    //   Use a regex like in parseMisapplicationBlock:
    // ─────────────────────────────────────────────────────────────────
    const eqPattern = /\(\s*=(.*?)\)\s*;\s*/;
    let eqMatch = fullText.match(eqPattern);
    if (!eqMatch) {
        // If not in name, try in fullText
        eqMatch = fullText.match(eqPattern);
    }
    if (eqMatch) {
        // Store it in, e.g., result.otherMisappliedText
        // Remove it from both name and fullText to avoid confusion
        fullText = fullText.replace(eqPattern, '').replace(/,\s\s+/, '; ').trim();
    }

    // Check if there's an accepted name usage in the text
    // removed because we remove these now because they are ambiguous and cause parsing problems
    const acceptedNameMatch = fullText?.match(/=\s*(.*?)\s*;/);
    if (acceptedNameMatch) {
        // If there's an accepted name usage, use it to determine the canonical name
        const acceptedName = acceptedNameMatch[1].trim();
        result.acceptedNameUsage = acceptedName;

        // Use the accepted name to construct the canonical name
        // This ensures names like "D. a. guava" are properly expanded to "Delias alberti guava"
        const nameParts = acceptedName.split(' ');
        if (nameParts.length >= 3) {
            result.canonicalName = acceptedName;
        }
    } else {
        result.acceptedNameUsage = ''
        result.canonicalName = result.scientificName;
    }

    // ─────────────────────────────────────────────────────────────────
    //   If there's no authorship pattern, remove trailing [.* and
    //   treat the entire string as the name with no authorship.
    //   This specifically handles cases like "?Chalodeta candiope [BCR2], ♀"
    //   which lack e.g. "Smith, 1924"
    // ─────────────────────────────────────────────────────────────────
    let name: string;
    let authorship: string;
    let remainingText: string;
    let parentheticalAuthorship: boolean | undefined;
    let textBeforeSemicolon = fullText.split(';')[0].trim()
    // Check for something like "Smith, 1924" or "Smith, ?1924"
    // If not present, assume the authorship data is invalid.
    if (!/,?\s*\(?\[?\??\[?\d{4}\]?\)?/.test(textBeforeSemicolon)) {
        // First, replace "[Author, 1924]" style text with a placeholder
        textBeforeSemicolon = textBeforeSemicolon.replace(/\[.*$/, '').trim();
        const { updatedName, updatedAuthorship } = extractTrailingAuthorshipNoYear(textBeforeSemicolon);
        name = updatedName.replace("??", "?");
        authorship = updatedAuthorship;
        remainingText = '';
        parentheticalAuthorship = undefined;
        fullText = textBeforeSemicolon;
    } else {
        // Otherwise, do our normal name+authorship parse
        const parsed = parseNameAndAuthorship(textBeforeSemicolon);
        name = parsed.name.replace("??", "?");
        authorship = parsed.authorship;
        remainingText = parsed.remainingText;
        parentheticalAuthorship = parsed.parentheticalAuthorship;
    }
    let author: {
        year: number | undefined;
        bracketedYear: boolean;
        authorship: Authorship[];
    } = { year: undefined, bracketedYear: false, authorship: [] };

    if (authorship) {
        author = parseAuthorshipString(authorship);
    }

    // Mark that by default, the genus and specific epithet are explicit
    result.explicitHigherTaxon = true;
    result.explicitSpecificEpithet = true;

    // Check for "?" in specific epithet
    let nameParts = name.trim().split(/\s+/);
    if (nameParts.length >= 2 && nameParts[1].startsWith('?')) {
        result.explicitSpecificEpithet = false;
        nameParts[1] = nameParts[1].replace(/^\?\?+/, '');
        name = nameParts.join(' ');
    }

    result.canonicalName = name.replace(/(\s+ssp\.)([0-9]+)/g, '$1 $2').replace('??', '?').trim()
    result.scientificName = parentheticalAuthorship ? `${name} (${authorship})` : `${name} ${authorship}`;
    result.scientificName = result.scientificName.replace(/(\s+ssp\.)([0-9]+)/g, '$1 $2').trim();
    result.scientificNameAuthorship = parentheticalAuthorship ? `(${authorship})` : authorship;



    result.namePublishedInYear = author.year?.toString()
    result.explicitHigherTaxon = true
    // check for parenthetical genus name; this means the genus was probably not explicitly stated, but implied in the original text
    // basing on textBeforeSemicolon instead of fullText to ensure we don't include citation data
    const parentheticalGenus = textBeforeSemicolon.replace(result.scientificName, '').replace(remainingText, '').replace(';', '').trim();

    // First, see if our 'name' variable is just one single capitalized word (e.g. "Shipherorta")
    const nameParts2 = name.trim().split(/\s+/);
    const isSingleCapitalizedWord = (
        nameParts2.length === 1 &&
        /^[A-Z][a-z]*$/.test(nameParts2[0])
    );

    if (!isSingleCapitalizedWord && parentheticalGenus.startsWith('(') && parentheticalGenus.endsWith(')')) {
        const genus = parentheticalGenus.slice(1, -1);
        if (!(genus.endsWith('idae') || genus.endsWith('inae'))) {
            const cleanedCanonicalName = result.canonicalName.replace(
                /\?[A-Z][\p{L}]+\s+/u,
                ' ').trim()
            const cleanedScientificNameAuthorship = result.scientificNameAuthorship.replace('(', '').replace(')', '').trim()
            result.canonicalName = `${genus} ${cleanedCanonicalName}`.trim();
            result.scientificName = `${genus} ${cleanedCanonicalName} ${cleanedScientificNameAuthorship}`.trim();
            result.scientificNameAuthorship = cleanedScientificNameAuthorship
            result.explicitHigherTaxon = false;
        }
    }
    fullText = remainingText;
    // check for NameTypes in name and remove matched text
    const nameTypePatterns: Record<NameTypes, RegExp> = {
        nomen_alternativum: /(?:nom\.|nomen)\s+(?:altern\.|alternativum)/i,
        nomen_conservandum: /(?:nom\.|nomen)\s+(?:cons\.|conservandum)/i,
        nomen_dubium: /(?:nom\.|nomen)\s+(?:dub\.|dubium)/i,
        nomen_ambiguum: /(?:nom\.|nomen)\s+(?:ambig\.|ambiguum)/i,
        nomen_confusum: /(?:nom\.|nomen)\s+(?:conf\.|confusum)/i,
        nomen_perplexum: /(?:nom\.|nomen)\s+(?:perpl\.|perplexum)/i,
        nomen_periculosum: /(?:nom\.|nomen)\s+(?:peric\.|periculosum)/i,
        nomen_erratum: /(?:nom\.|nomen)\s+(?:err\.|erratum)/i,
        nomen_illegitimum: /(?:nom\.|nomen)\s+(?:illeg\.|illegitimum)/i,
        nomen_invalidum: /(?:nom\.|nomen)\s+(?:inval\.|invalidum)/i,
        nomen_manuscriptum: /(?:nom\.|nomen)\s+(?:ms\.|manuscriptum)/i,
        nomen_monstrositatum: /(?:nom\.|nomen)\s+(?:monstr\.|monstrositatum)/i,
        nomen_novum: /(?:nom\.|nomen)\s+(?:nov\.|novum)/i,
        nomen_nudum: /(?:nom\.|nomen)\s+(?:nud\.|nudum)/i,
        nomen_oblitum: /(?:nom\.|nomen)\s+(?:obl\.|oblitum)/i,
        nomen_protectum: /(?:nom\.|nomen)\s+(?:prot\.|protectum)/i,
        nomen_rejiciendum: /(?:nom\.|nomen)\s+(?:rejic\.|rejiciendum)/i,
        nomen_suppressum: /(?:nom\.|nomen)\s+(?:suppr\.|suppressum)/i,
        nomen_vanum: /(?:nom\.|nomen)\s+(?:van\.|vanum)/i,
        nomina_circumscribentia: /(?:nom\.|nomina)\s+(?:circ\.|circumscribentia)/i,
        species_inquirenda: /(?:sp\.|species)\s+(?:inq\.|inquirenda)/i
    };

    for (const [nameType, pattern] of Object.entries(nameTypePatterns)) {
        const match = fullText.match(pattern);
        if (match) {
            result.nomenclaturalStatus = nameType as NameTypes;
            fullText = fullText.replace(match[0], '').trim();
            break;
        }
    }

    // Extract taxonomic notes in the format "(xxx. text)"
    let taxonomyNotes: string[] | undefined
    const taxonomyNotesMatch = fullText.match(/\(([a-z]+\.)\s+[^)]+\)/i);
    if (taxonomyNotesMatch) {
        taxonomyNotes = [taxonomyNotesMatch[0].replace(/^\(|\)$/g, '').trim()]; // Remove parentheses
        fullText = fullText.replace(taxonomyNotesMatch[0], '').trim(); // Remove the note from fullText
    }

    // remove mention text
    fullText = fullText.split('\n')[0]

    // extract type locality text
    const tlMatch = fullText.split(/;\s*TL:/);
    if (tlMatch.length > 1) {
        const tl = tlMatch[1].split(';')[0].trim();
        if (!tl.includes("REF#")) {
            result.typeLocality = tl
            // convert typeLocality to id
            if (result.typeLocality) {
                // If not found, add it
                let locItem = getOrCreateLocality(result.typeLocality.trim());
                result.typeLocality = locItem.id
            }
        }
    }
    fullText = tlMatch[0];

    const tsMatch = fullText.split(/;\s*TS:/);
    if (tsMatch.length > 1) {
        let rawTs = tsMatch[1].split(';')[0].trim();
        // Look specifically for "[= ... ]" inside that text
        const bracketRegex = /\[=\s*(.*?)\]/;
        const bracketMatch = rawTs.match(bracketRegex);
        if (bracketMatch) {
            // If found, use the bracketed content
            result.typeTaxonId = bracketMatch[1].trim();
        } else {
            // Otherwise, keep the whole raw text
            result.typeTaxonId = rawTs;
        }
    }
    fullText = tsMatch[0];

    // extract issue and remove it from fullText
    const { issueLabel, externalLink } = extractIssueInfo(fullText, element)

    // Remove issue label with surrounding text patterns:
    // 1. (other text) (issueLabel)
    // 2. (issueLabel) (other text)
    // 3. (issueLabel)
    // Split text at colon to only process issue labels in the left part
    const [leftPart, rightPart] = fullText.split(':');
    const cleanedLeftPart = leftPart
        .replace(new RegExp(`\\(.*\\)\\s+\\(${issueLabel}\\)`, 'g'), '')
        .replace(new RegExp(`\\(${issueLabel}\\)\\s+\\(.*\\)`, 'g'), '')
        .replace(new RegExp(`\\(${issueLabel}\\)`, 'g'), '')
        .trim();
    fullText = rightPart ? `${cleanedLeftPart}: ${rightPart}` : cleanedLeftPart;

    // extract volume and remove it from fullText
    const volumeElement = element.querySelector('b');
    const volume = volumeElement?.textContent?.trim();
    const isValidVolume = volume && volume !== 'TL' && /^[0-9A-Za-z]+$/.test(volume);
    let cleanedVolume = volume?.replace('.-', '')
    const volumeValue = isValidVolume ? cleanedVolume : undefined;
    fullText = fullText.replace(`${volume}`, '').trim()

    fullText = originalText
    if (fullText.includes(':')) {
        const match = fullText.match(/:([^;\n:]*$)/);
        const pageDetails = match?.[1].trim();

        if (pageDetails) {
            fullText = fullText.replace(`${match?.[0]}`, '').trim();

            // Extract "in ... " note
            let note
            const noteMatch = fullText.match(/^in\s+.*?,/);
            if (noteMatch) {
                note = noteMatch[0].slice(0, -1).trim(); // Remove trailing comma
                fullText = fullText.replace(noteMatch[0], '').trim(); // Remove the note from fullText
            }

            // Extract journal name
            let journalName = '';
            const journalNameMatch = fullText.match(/^[^,]+/);
            if (journalNameMatch) {
                journalName = journalNameMatch[0].trim();
                fullText = fullText.replace(journalName, '').trim(); // Remove the journal name from fullText
            }

            // Determine if journalName is an abbreviation or title
            let journalAbbreviation
            let journalTitle
            if (journalName?.includes('.')) {
                journalAbbreviation = journalName;
            } else {
                journalTitle = journalName;
            }

            // Gather anchor text => href
            const anchorMap = gatherAnchorMap(element);

            // Pass anchorMap here:
            const subelements = extractPageSubelements(pageDetails, anchorMap);

            // extract Related Literature List reference ID
            let funetRefLink: string | undefined = undefined;
            let link = element.querySelector('a');
            let manualOverride = false
            if (link && originalText.includes('\n\n=')) {
                // ! somehow SENSU items are leaking into some references
                // this will override so we can create the articles and pages
                const citation1 = originalText.split('\n')[0]
                if (!citation1.includes(link.textContent || '')) {
                    manualOverride = true
                }
            }
            let articleId: string | undefined = undefined;

            if (!manualOverride && (link?.href.startsWith("about:blank#") || link?.href.startsWith("#"))) {
                // in this block, the reference should be in the cache
                funetRefLink = link.href.replace("about:blank#", '');
                funetRefLink = funetRefLink.replace("#", '');
                const article = getArticleByFunetRefLink(funetRefLink, issueLabel)
                articleId = article?.id
            } else {
                const newIssue = composeIssueObject(
                    issueLabel,
                    volumeValue ?? '',
                    journalTitle ?? '',
                    journalAbbreviation ?? '',
                    author.bracketedYear,
                    author.year,
                    externalLink,
                    true
                );

                const newPagesInitial: Page[] = []
                const newPagesFinal: Page[] = []
                subelements?.forEach(x => {
                    const newPage: Page = {
                        ...x.reference,
                    }
                    newPagesInitial.push(newPage)
                })

                newPagesInitial.forEach(page => {
                    if (page.label.includes('-')) {
                        // Split the range and create individual pages
                        page.label = page.label.replace('(', '').replace(')', '')
                        const [start, end] = page.label.split('-').map(Number);
                        if (!isNaN(start) && !isNaN(end)) {
                            // Create a page for each number in the range
                            for (let i = start; i <= end; i++) {
                                newPagesFinal.push({
                                    type: page.type,
                                    label: i.toString().replace('[', '').replace(']', '').
                                        replace('-.', '').
                                        replace(/^.*(#[A-Za-z0-9]+)$/, '').
                                        replace(/^\(/, '').replace(/\)$/, '').
                                        replace(/^\.\s+/, '').trim()
                                });
                            }
                        } else {
                            // If range is invalid, keep original page
                            newPagesFinal.push(page);
                        }
                    } else {
                        newPagesFinal.push(page);
                    }
                });

                // Try to find existing article first
                let cacheArticle = findBestArticleMatch({
                    authors: author.authorship,
                    year: author.year?.toLocaleString(),
                    journalTitle: journalTitle ?? '',
                    journalAbbreviation: journalAbbreviation ?? '',
                    volume: volumeValue ?? '',
                    issue: issueLabel
                });

                if (cacheArticle) {
                    articleId = cacheArticle.id;
                } else {
                    // Create new article if not found
                    const article = composeArticleObject(
                        [authorship],
                        '', // title
                        newIssue,
                        undefined,
                        newPagesFinal,
                        author.authorship,
                        undefined,
                        undefined,
                        externalLink ?? undefined,
                        undefined,
                        note ? [note] : undefined,
                    );

                    if (article) {
                        articleId = article.id;
                        if (!articleId) {
                            console.error("Article created but no ID was generated");
                            return result;
                        }
                    } else {
                        console.error("Failed to create article");
                        return result;
                    }
                }
            }

            if (articleId) {
                if (subelements) {
                    const subelementIds = upsertArticlePages(articleId, subelements);
                    const dataConnections: DataConnection[] = []
                    if (subelementIds.length === 0) {
                    } else {
                        const linkedResources: LinkedResource[] = []
                        subelementIds.forEach(x => {
                            linkedResources.push({
                                id: uuidv7(),
                                source: x,
                                resource_type: 'literature'
                            })
                        })
                        dataConnections.push({
                            id: uuidv7(),
                            dataConnectionType: "namePublishedIn",
                            notes: taxonomyNotes,
                            linkedResources: linkedResources
                        })
                    }
                    result.dataConnections = dataConnections
                } else {
                    const dataConnections: DataConnection[] = []
                    dataConnections.push({
                        id: uuidv7(),
                        dataConnectionType: "namePublishedIn",
                        notes: taxonomyNotes,
                        linkedResources: [{
                            id: uuidv7(),
                            source: articleId,
                            resource_type: 'literature'
                        }]
                    })
                    result.dataConnections = dataConnections
                }
            }

            const synonymRecords: SynonymClaim[] = []
            if (originalText.split('\n\n').length > 2) {
                const applicationText = originalText.split('\n\n')[1]
                const applicationList = applicationText.split('\n')
                applicationList.forEach(application => {
                    let nameUsage = application.match(/^=\s*(.*?)\s*;/)?.[1]?.trim() || result.id!;
                    const synonymDetailList: LinkedResource[] = []
                    application.split(';').slice(1).map((x) => {

                        const synonymDetails = x.trim()
                        if (synonymDetails !== '') {
                            const colonIndex = synonymDetails.indexOf(':');
                            const textBeforeColon = colonIndex !== -1 ?
                                synonymDetails.substring(0, colonIndex) :
                                synonymDetails;
                            const siblingText = textBeforeColon.split(',').slice(0, -1).join(',').trim();
                            let pagesText: string | undefined
                            if (colonIndex !== -1) {
                                pagesText = synonymDetails.substring(colonIndex + 1).trim()
                            } else {
                                pagesText = ''
                                const commaSepPages = synonymDetails.match(/,\s*([0-9]+-?[0-9]+?)$/)
                                if (commaSepPages) { pagesText = commaSepPages[1].trim() }
                            }

                            const linkElement = findSiblingLink(element, siblingText.trim());
                            let funetId
                            const { issueLabel: issueLabelX, externalLink } = extractIssueInfo(x, element)
                            if (linkElement?.href) {
                                funetId = linkElement.href.replace("about:blank#", '')
                                const article = getArticleByFunetRefLink(funetId, issueLabelX)

                                pagesText.split(',').forEach(pageRef => {

                                    const listAnnotation = pageRef.match(/\((list)\)/i);
                                    const pageNumber = pageRef.replace(/\(.*?\)/g, '').trim().split(/\s+/)[0];

                                    if (!/^\d+$/.test(pageNumber)) return;

                                    const page = article?.pages?.find(p => p.label === pageNumber);

                                    if (listAnnotation) {
                                        if (page?.subelements?.some(s => s.type === 'table')) {
                                            const tableSubelements = page.subelements.filter(s => s.type === 'table');
                                            tableSubelements.forEach(sub => {
                                                if (sub.id) {
                                                    const linkedResource: LinkedResource = {
                                                        id: uuidv7(),
                                                        source: sub.id,
                                                        resource_type: "literature"
                                                    }
                                                    synonymDetailList.push(linkedResource);
                                                }
                                            });
                                        } else if (page?.id) {
                                            const linkedResource: LinkedResource = {
                                                id: uuidv7(),
                                                source: page.id,
                                                resource_type: "literature"
                                            }
                                            synonymDetailList.push(linkedResource);
                                        } else if (article?.id) {
                                            const linkedResource: LinkedResource = {
                                                id: uuidv7(),
                                                source: article.id,
                                                resource_type: "literature"
                                            }
                                            synonymDetailList.push(linkedResource);
                                        }
                                    } else if (page?.id) {
                                        const linkedResource: LinkedResource = {
                                            id: uuidv7(),
                                            source: page.id,
                                            resource_type: "literature"
                                        }
                                        synonymDetailList.push(linkedResource);
                                    } else if (article?.id) {
                                        const linkedResource: LinkedResource = {
                                            id: uuidv7(),
                                            source: article.id,
                                            resource_type: "literature"
                                        }
                                        synonymDetailList.push(linkedResource);
                                    }
                                })
                            }
                        }
                    })
                    const lastEffectiveDate = synonymDetailList
                        .map(item => getArticleYearFromId(item.source as string))
                        .filter((year): year is number => year !== undefined)
                        .reduce((latest, year) => Math.max(year, latest), 0)
                        ?.toString() || undefined;

                    let dataConnectionType = 'treated_as_synonym' as DataConnectionType
                    if (typeOverride) {
                        dataConnectionType = typeOverride as DataConnectionType
                    }

                    synonymRecords.push({
                        id: uuidv7(),
                        dataConnectionType,
                        synonymOfId: nameUsage,
                        linkedResources: synonymDetailList,
                        lastEffectiveDate: lastEffectiveDate,

                    })
                })
                synonymRecords.map((synonym) => {
                })
            }
            if (synonymRecords.length > 0) {
                if (!result.dataConnections) {
                    result.dataConnections = [];
                }
                result.dataConnections.push(...synonymRecords);
            }
        }
    }

    return result
}

function getArticleYearFromId(id: string): number | undefined {
    // First try to get article directly
    let article = getArticleById(id);

    if (!article) {
        // If not found, check if this is a page or subelement ID
        const allArticles: Article[] = getAllArticlesFromCache();
        for (const cachedArticle of allArticles) {
            // Check subelements
            const hasMatchingSubelement = cachedArticle.pages?.some(p =>
                p.subelements?.some(s => s.id === id)
            );
            if (hasMatchingSubelement) {
                article = cachedArticle;
                break;
            }

            // Check pages
            const matchingPage = cachedArticle.pages?.find(p => p.id === id);

            if (matchingPage) {
                article = cachedArticle;
                break;
            }
        }
    }

    // Navigate through the nested structure to get the year
    if (!article?.issue?.volume?.year) {
        return undefined;
    }

    return article.issue.volume.year;
}

export function resolveSynonymReferences(taxa: TaxonRecord[], genusName: string): TaxonRecord[] {
    // console.log('=== Starting Synonym Resolution ===');
    // console.log(`Processing ${taxa.length} taxa records`);

    // Create a stable mapping with both original and normalized genus names
    const nameToIdEntries = taxa
        .flatMap(t => {
            const entries: [string, string][] = [];

            // Original canonical name
            if (t.canonicalName && t.id) {
                entries.push([t.canonicalName, t.id]);
            }

            // Name with normalized genus
            if (t.canonicalName && t.id) {
                const nameParts = t.canonicalName.split(' ');
                if (nameParts.length > 1) {
                    const normalizedName = `${genusName} ${nameParts.slice(1).join(' ')}`;
                    entries.push([normalizedName, t.id]);
                }
            }

            return entries;
        })
        .sort(([nameA], [nameB]) => nameA.localeCompare(nameB));

    const nameToId = new Map(nameToIdEntries);

    // Create a deep copy with structured clone for better object handling
    const resolvedTaxa = structuredClone(taxa);

    resolvedTaxa.forEach((taxon, index) => {
        if (!taxon.dataConnections) return;

        const synonymConnections = taxon.dataConnections as SynonymClaim[]

        synonymConnections.forEach((connection) => {
            if (connection.dataConnectionType !== 'treated_as_synonym') return;
            if (!connection.synonymOfId) return;

            // Skip if already a UUID
            if (/^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i.test(connection.synonymOfId)) {
                return;
            }

            // Find matches in a deterministic way
            const matches = nameToIdEntries
                .filter(([name]) => name === connection.synonymOfId)
                .map(([_, id]) => id);

            if (matches.length > 0) {
                connection.synonymOfId = matches[0];
            } else {
                // Try nominate subspecies match
                const nominateSubspeciesName = `${connection.synonymOfId} ${connection.synonymOfId.split(' ').pop()}`;
                const nominateMatches = nameToIdEntries
                    .filter(([name]) => name === nominateSubspeciesName)
                    .map(([_, id]) => id);

                if (nominateMatches.length > 0) {
                    connection.synonymOfId = nominateMatches[0];
                } else {
                    // Try matching with the provided genus name
                    const nameParts = connection.synonymOfId.split(' ');
                    if (nameParts.length > 1) {
                        const normalizedName = `${genusName} ${nameParts.slice(1).join(' ')}`;
                        const match = nameToId.get(normalizedName);
                        if (match) {
                            connection.synonymOfId = match;
                        }
                    }
                }
            }
        });
    });

    // Only check for duplicates in original (non-normalized) names
    // console.log('Duplicate name check:',
    //     nameToIdEntries
    //         .filter(([name]) => !name.startsWith(genusName)) // Only check original names
    //         .filter(([name], i, arr) =>
    //             arr.filter(([n]) => !n.startsWith(genusName)) // Compare with other original names
    //                 .findIndex(([n]) => n === name) !== i
    // );

    return resolvedTaxa;
}

export function resolveAcceptedNameUsageIds(taxa: TaxonRecord[]): TaxonRecord[] {
    // Regex that checks for a UUID version 7 (time-ordered) string
    // Example: xxxxxxxx-xxxx-7xxx-yxxx-xxxxxxxxxxxx
    // where x is [0-9a-f], y is from [89ab].
    const uuidV7Regex = /^[0-9a-f]{8}-[0-9a-f]{4}-7[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

    const isUuidV7 = (value: string | undefined): boolean => {
        return typeof value === 'string' && uuidV7Regex.test(value);
    };

    // Create a quick lookup to find taxa by ID
    const taxaById: Map<string, TaxonRecord> = new Map();
    for (const taxon of taxa) {
        if (taxon.id) {
            taxaById.set(taxon.id, taxon);
        }
    }

    // For each taxon, if the acceptedNameUsage is a UUIDv7,
    // find the corresponding taxon by that ID, and replace
    // the current taxon's acceptedNameUsage with the matched taxon's
    // own acceptedNameUsage string.
    for (const taxon of taxa) {
        if (isUuidV7(taxon.acceptedNameUsage)) {
            const matchedTaxon = taxaById.get(taxon.acceptedNameUsage);
            if (matchedTaxon?.acceptedNameUsage) {
                taxon.acceptedNameUsage = matchedTaxon.acceptedNameUsage;
            }
        }
    }

    return taxa;
}

function removeParentheses(str: string): string {
    // Removes any text inside parentheses (including the parentheses themselves), then collapses any extra spaces.
    return str.replace(/\([^)]*\)/g, '').replace(/\s+/g, ' ').trim();
}

/**
 * Resolves the "typeTaxonId" field in each TaxonRecord, cross-referencing the "scientificName" of taxa:
 * 1. If exactly one match is found (ignoring trailing year info in scientificName),
 *    - replaces the referencing taxon's .typeTaxonId with the matched taxon's .id
 *    - creates a new DataConnection of type "treated_as_type" within the matched taxon
 *      containing notes/linkedResources copied from any "namePublishedIn" dataConnection(s)
 *      in the referencing taxon. If none found, it creates an empty "treated_as_type" DataConnection.
 * 2. If multiple matches are found:
 *    - attempt to pick the "valid" one (for example, the one whose acceptedNameUsage is itself, or which does
 *      not appear to be a synonym). If more than one "valid" match is found, pick the oldest
 *      by checking whichever has the earliest year. If exactly one oldest remains, use that;
 *      otherwise append " (ambiguous)" to the referencing taxon's typeTaxonId.
 * 3. If no matches are found, leave the typeTaxonId as-is.
 */
export function resolveTypeTaxonIds(taxa: TaxonRecord[]): TaxonRecord[] {
    // Make a deep copy so we don't mutate the original data
    const resolvedTaxa = structuredClone(taxa);

    // Helper to strip trailing year info from a name like "Papilio nigrina Fabricius, 1775"
    // to get "Papilio nigrina Fabricius"
    function stripYearInfo(name: string): string {
        // This regex removes a comma (optionally) followed by space(s) and a year-like token (4 digits, possibly in brackets).
        // e.g. "Papilio nigrina Fabricius, 1775" => "Papilio nigrina Fabricius"
        //      "Papilio nigrina Fabricius, [1775]" => "Papilio nigrina Fabricius"
        //      "Papilio nigrina Fabricius, 1775, extra" => "Papilio nigrina Fabricius"
        return name.replace(/,?\s*\[?\d{4}\]?.*$/, '').trim();
    }

    // Example heuristic to decide if a taxon is "valid":
    // (1) If it has no acceptedNameUsage or (acceptedNameUsage === scientificName), treat it as valid
    // (2) If it looks like it is "treated_as_synonym" of another, skip it
    // You can refine these conditions as needed in your data model.
    function isValidTaxon(t: TaxonRecord): boolean {
        const isAcceptedItself =
            !t.acceptedNameUsage || t.acceptedNameUsage === t.scientificName;

        const notSynonymizedElsewhere = !t.dataConnections?.some(dc =>
            dc.dataConnectionType === 'treated_as_synonym'
        );

        return isAcceptedItself && notSynonymizedElsewhere;
    }

    // Try to glean a "year" from the taxon's authorship or namePublishedInYear
    // for tie-breaking. If there's no direct year, return Infinity as fallback.
    function getYearForTaxon(t: TaxonRecord): number {
        if (t.namePublishedInYear) {
            const parsed = parseInt(t.namePublishedInYear, 10);
            if (!isNaN(parsed)) return parsed;
        }
        // If no known year, push it to the end in sorting
        return Number.POSITIVE_INFINITY;
    }

    // Build a lookup from stripped scientificName => array of taxon IDs.
    // Additionally, if the stripped name contains parentheses (subgenus),
    // also store a version with that parenthetical text removed.
    const nameToTaxonIds: Map<string, string[]> = new Map();
    resolvedTaxa.forEach(taxon => {
        if (!taxon.scientificName || !taxon.id) return;

        // 1) The main key = original name (minus trailing year info)
        // console.log("taxon.scientificName:", taxon.scientificName)
        let strippedName = stripYearInfo(taxon.scientificName);
        // console.log("strippedName:", strippedName)
        strippedName = strippedName.replace('[', '').replace(']', '').trim() // remove square brackets around authornames
        if (strippedName === 'Noctua coniuncta Esper') {
            // ! special cases that need manual override of values
            strippedName = 'Noctua conjuncta Esper'
        }
        if (!nameToTaxonIds.has(strippedName)) {
            nameToTaxonIds.set(strippedName, []);
        }
        nameToTaxonIds.get(strippedName)!.push(taxon.id);

        // 2) A second key that removes parentheses, in case typeTaxonId is missing subgenus parentheses.
        const noParenName = removeParentheses(strippedName);
        if (noParenName !== strippedName) {
            if (!nameToTaxonIds.has(noParenName)) {
                nameToTaxonIds.set(noParenName, []);
            }
            nameToTaxonIds.get(noParenName)!.push(taxon.id);
        }
    });

    // For each taxon, check its typeTaxonId against that lookup
    resolvedTaxa.forEach(referencingTaxon => {
        if (!referencingTaxon.typeTaxonId) return;

        // Example: "Phalaena calthella Linnaeus" => strip year => "Phalaena calthella Linnaeus"
        let strippedTypeName = stripYearInfo(referencingTaxon.typeTaxonId);
        const namePart = strippedTypeName.split(' ').slice(0, -1).join(' ')
        const authorPart = strippedTypeName.split(' ').slice(-1).join(' ')
        const { epithet } = transliterateEpithet(namePart)
        const finalName = epithet + ' ' + authorPart
        let matchedIds = nameToTaxonIds.get(finalName) || [];

        // for (const [key, value] of nameToTaxonIds.entries()) {
        //     if (key.includes('promissa')) {
        //         console.log(`${key}: ${value}`);
        //     }
        //     // console.log(`${key}: ${value}`);
        // }

        // if (referencingTaxon.typeTaxonId.includes("promissa")) {
        //     console.log("strippedTypeName:", strippedTypeName)
        //     console.log("namePart:", namePart)
        //     console.log("authorPart:", authorPart)
        //     console.log("finalName:", finalName)
        //     console.log("matchedIds:", matchedIds)
        // }
        // If we didn't find anything, try removing parentheses from the stripped name
        if (matchedIds.length === 0) {
            const noParenTypeName = removeParentheses(strippedTypeName);
            if (noParenTypeName !== strippedTypeName) {
                matchedIds = nameToTaxonIds.get(noParenTypeName) || [];
            }
        }

        // If nothing or exactly one match, handle as before:
        if (matchedIds.length === 0) {
            // No matches => do nothing.
            return;
        } else if (matchedIds.length === 1) {
            // Exactly one match => link
            referencingTaxon.typeTaxonId = matchedIds[0];
            linkForTreatedAsType(referencingTaxon, resolvedTaxa, matchedIds[0]);
        } else {
            // More than one match:
            // 1. Filter for "valid" taxa
            const matchedTaxa = matchedIds
                .map(id => resolvedTaxa.find(x => x.id === id))
                .filter(Boolean) as TaxonRecord[];

            const validTaxa = matchedTaxa.filter(isValidTaxon);

            if (validTaxa.length === 0) {
                // If no valid taxa, mark ambiguous
                referencingTaxon.typeTaxonId = `${referencingTaxon.typeTaxonId} (ambiguous)`;
                return;
            } else if (validTaxa.length === 1) {
                // If exactly one valid, pick that
                if (validTaxa[0].id) {
                    referencingTaxon.typeTaxonId = validTaxa[0].id;
                    linkForTreatedAsType(referencingTaxon, resolvedTaxa, validTaxa[0].id);
                }
            } else {
                // More than one valid => pick the earliest year
                validTaxa.sort((a, b) => getYearForTaxon(a) - getYearForTaxon(b));
                const firstYear = getYearForTaxon(validTaxa[0]);
                const secondYear = getYearForTaxon(validTaxa[1]);

                if (firstYear === secondYear) {
                    // If tie, mark ambiguous
                    referencingTaxon.typeTaxonId = `${referencingTaxon.typeTaxonId} (ambiguous)`;
                } else {
                    // Use the one with the earliest year
                    if (validTaxa[0].id) {
                        referencingTaxon.typeTaxonId = validTaxa[0].id;
                        linkForTreatedAsType(referencingTaxon, resolvedTaxa, validTaxa[0].id);
                    }
                }
            }
        }
    });

    return resolvedTaxa;
}
/**
 * For a referencingTaxon that points typeTaxonId => matchedId, create or update
 * a "treated_as_type" DataConnection in the matched taxon, copying over
 * "namePublishedIn" notes from referencingTaxon if present.
 */
function linkForTreatedAsType(
    referencingTaxon: TaxonRecord,
    allTaxa: TaxonRecord[],
    matchedId: string
): void {
    const matchedTaxon = allTaxa.find(t => t.id === matchedId);
    if (!matchedTaxon) return;

    if (!matchedTaxon.dataConnections) {
        matchedTaxon.dataConnections = [];
    }

    // Copy "namePublishedIn" data from referencing taxon
    const referencingNamePublishedIn = (referencingTaxon.dataConnections || [])
        .filter(dc => dc.dataConnectionType === 'namePublishedIn');

    // For each "namePublishedIn" connection, create a corresponding "treated_as_type" connection
    if (referencingNamePublishedIn.length > 0) {
        referencingNamePublishedIn.forEach(origDC => {
            const newConnection: TypeClaim = {
                ...origDC,
                id: uuidv7(),
                dataConnectionType: 'treated_as_type',
                typeOfId: referencingTaxon.id
            };
            matchedTaxon.dataConnections?.push(newConnection);
        });
    } else {
        // If none found, create a minimal "treated_as_type" connection
        const emptyConnection: TypeClaim = {
            id: uuidv7(),
            dataConnectionType: 'treated_as_type',
            typeOfId: referencingTaxon.id,
            linkedResources: []
        };
        matchedTaxon.dataConnections.push(emptyConnection);
    }
}

/**
 * Attempt to detect a trailing authorship (no year) within a string. 
 *   For example: 
 *     "?Calephelis cerea Hübner" => (name="?Calephelis cerea", authorship="Hübner")
 *     "?Calephelis cerea Hübner & de Jannis" => (name="?Calephelis cerea", authorship="Hübner & de Jannis")
 */
function extractTrailingAuthorshipNoYear(raw: string): { updatedName: string; updatedAuthorship: string } {
    const text = raw.trim();
    if (!text) return { updatedName: '', updatedAuthorship: '' };

    // Split the text into tokens
    const tokens = text.split(/\s+/);
    // We'll walk backwards to see which tokens look like they belong to authorship
    // E.g. capitalized tokens, possibly joined by & / and / et / de, etc.
    let i = tokens.length - 1;
    const authorStack: string[] = [];

    // A small helper to detect if a token might be part of an author's name
    // We'll allow capital letter start or certain connectors.
    function couldBeAuthorPart(tok: string) {
        // Skip tokens that start with question marks - they're likely part of the name
        if (tok.startsWith('?')) return false;

        // Skip lowercase words (likely species epithets) unless they're known connectors
        if (/^[a-z]/.test(tok) && !/(and|&|et|de|del|van|von)$/i.test(tok)) return false;

        // Allow uppercase starts or known connectors
        if (/^[A-ZÁÉÍÓÚÑÜÄÖÆØÅ]|[St\.?\s]/.test(tok)) return true;
        if (/^(and|&|et|de|del|van|von|MS)$/i.test(tok)) return true;

        // Also allow comma-separated parts like "Heath, MS"
        if (/^[A-Z][a-z]+,/.test(tok)) return true;

        return false;
    }

    while (i >= 1) {
        const w = tokens[i];
        if (couldBeAuthorPart(w)) {
            // This token is presumably part of the authorship
            authorStack.push(w);
            i--;
        } else {
            // Stop once we no longer see an author-like token
            break;
        }
    }

    if (authorStack.length === 0) {
        // No trailing authorship found
        return { updatedName: text, updatedAuthorship: '' };
    }

    // Reverse the stack to get the authorship in the correct order
    authorStack.reverse();
    const authorship = authorStack.join(' ');
    const updatedName = tokens.slice(0, i + 1).join(' ');

    return {
        updatedName,
        updatedAuthorship: authorship
    };
}