import { getDocument, parseBlocks, getNextLink, getMainNavigationElement, getPageHTML, ParsedBlock } from './parseBlocks';
import fs from 'fs/promises';
// import path from 'path';
import { extractTaxonomicHierarchy } from './parseNavbarBlock';

// Flag to control local file fallback
const allow_local_file: boolean = process.env.ALLOW_LOCAL_FILE === 'true';

async function writeResultsToFile(data: any, filename: string): Promise<void> {
    // const outputPath = path.join(__dirname, filename);
    const outputPath = filename;
    await fs.writeFile(outputPath, JSON.stringify(data, null, 2));
    console.log(`Results written to ${outputPath}`);
}

async function main(): Promise<void> {
    const url: string = "https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/arctiinae/grammia/";
    const localFilePath: string = '/data/grammia.html';
    let htmlString: string | null = null;

    try {
        htmlString = await getPageHTML(url);
        if (!htmlString) {
            throw new Error('Failed to fetch HTML from URL');
        }
    } catch (error) {
        if (allow_local_file) {
            console.warn(`Failed to fetch from URL. Falling back to local file.`);
            try {
                htmlString = await fs.readFile(localFilePath, 'utf-8');
            } catch (fileError) {
                console.error(`Failed to read local file.`);
                return;
            }
        } else {
            console.error(`Failed to fetch from URL. Local file fallback is disabled.`);
            return;
        }
    }

    if (htmlString) {
        const document: Document = await getDocument(htmlString);
        const parsedBlocks: ParsedBlock[] = parseBlocks(document);
        const nextLink: string | null = getNextLink(document);
        const mainNavigation: Element | null = getMainNavigationElement(document);

        // Prepare results object
        const results = {
            parsedBlocks: parsedBlocks.map(block => ({
                blockName: block.blockName,
                blockType: block.blockType,
                parentBlock: block.parentBlock,
                blockData: block.blockData,
                blockDataText: block.blockData.textContent,
                parsedBlockData: block.parsedBlockData || null,
                // We're not including blockData as it's a DOM element and can't be easily serialized
            })),
            nextLink,
            mainNavigation: mainNavigation ? {
                tagName: mainNavigation.tagName,
                className: mainNavigation.className,
                id: mainNavigation.id,
            } : null,
        };

        // TODO: loop over blocks, parsing data into structured format
        results.parsedBlocks.forEach(block => {
            if (block.blockType === 'NAVBAR') {
                const higherTaxonomy = extractTaxonomicHierarchy(block.blockData);
                const directHigherTaxonParent = higherTaxonomy[higherTaxonomy.length - 1].id;
            }
            if (block.blockType === 'GenusBlock') {
                // Extract genus name
            }
            // TODO: for each "typeSpecies" field, loop through and replace with coresponding taxon ID
        });

        // Write results to file
        await writeResultsToFile(results, './data/parsing_results.json');

        console.log('Parsed Blocks:', parsedBlocks.length);
        console.log('Next Link:', nextLink);
        console.log('Main Navigation Element:', mainNavigation ? 'Found' : 'Not Found');
    } else {
        console.error('Failed to obtain HTML content');
    }
}

main().catch(console.error);