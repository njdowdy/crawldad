import { parseBlocksFromSource, ParsedBlock, getPageHTML } from './parseBlocks';

describe('Parse HTML from source', () => {
  let parsedBlocks: ParsedBlock[];

  beforeAll(async () => {
    const url = 'https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/arctiinae/grammia/';
    const localFilePath = '/Users/njdowdy/code/crawldad/funet/data/grammia.html';

    try {
      // First, try to fetch from URL
      const htmlContent = await getPageHTML(url);
      if (htmlContent) {
        parsedBlocks = await parseBlocksFromSource(url, false);
      } else {
        throw new Error('Failed to fetch from URL');
      }
    } catch (error: unknown) {
      let errorMessage = 'An unknown error occurred';
      if (error instanceof Error) {
        // errorMessage = error.message;
      } else if (typeof error === 'string') {
        // errorMessage = error;
      }
      console.warn(`Failed to fetch from URL: ${errorMessage}. Falling back to local file.`);
      // If URL fetch fails, use local file
      parsedBlocks = await parseBlocksFromSource(localFilePath, true);
    }
  }, 60000); // Increase timeout to 60 seconds for the network request

  test('Parsed block structure matches expected structure', () => {
    const expectedStructure = [
      { blockType: 'NAVBAR', count: 1 },
      { blockType: 'GenusBlock', count: 1 },
      { blockType: 'SubgenusBlock', count: 2 },
      { blockType: 'SpeciesBlock', count: 38 },
      { blockType: 'SubspeciesBlock', count: 21 },
      { blockType: 'MentionsBlock', count: 61 }, // Grammia quenseli saura is missing mentions
      { blockType: 'NameBlock', count: 59 },  // G. v. virgo, G. s. speciosa, G. n. nevadensis are missing name blocks
      { blockType: 'SynonymBlock', count: 58 },  // 
      { blockType: 'CommonNameBlock', count: 16 },
      { blockType: 'LarvaeBlock', count: 1 },
      { blockType: 'Last update date', count: 1 },
      { blockType: 'Web References', count: 1 },
      { blockType: 'Literature References', count: 1 }
    ];

    // Count the occurrences of each block type
    const actualCounts: { [key: string]: number } = {};
    parsedBlocks.forEach(block => {
      actualCounts[block.blockType] = (actualCounts[block.blockType] || 0) + 1;
    });

    // Check if the counts match the expected structure
    expectedStructure.forEach(expected => {
      expect(actualCounts[expected.blockType]).toBe(expected.count);
    });

    // Check if there are any unexpected block types
    const expectedBlockTypes = expectedStructure.map(e => e.blockType);
    const unexpectedBlockTypes = Object.keys(actualCounts).filter(type => !expectedBlockTypes.includes(type));
    expect(unexpectedBlockTypes).toEqual([]);

    // Check if the total number of blocks matches
    const expectedTotal = expectedStructure.reduce((sum, item) => sum + item.count, 0);
    expect(parsedBlocks.length).toBe(expectedTotal);
  });

  test('Web References block has 23 subitems', () => {
    const webReferencesBlock = parsedBlocks.find(block => block.blockType === 'Web References');
    expect(webReferencesBlock).toBeDefined();
    const subitems = webReferencesBlock!.blockData.querySelectorAll('li');
    expect(subitems.length).toBe(23);
  });

  test('Literature References block has 86 subitems', () => {
    const literatureReferencesBlock = parsedBlocks.find(block => block.blockType === 'Literature References');
    expect(literatureReferencesBlock).toBeDefined();
    const subitems = literatureReferencesBlock!.blockData.querySelectorAll('li');
    expect(subitems.length).toBe(86);
  });

  test('All blocks have unique names', () => {
    const blockNames = parsedBlocks.map(block => block.blockName);
    const uniqueBlockNames = new Set(blockNames);
    expect(blockNames.length).toBe(uniqueBlockNames.size);
  });

  test('GenusBlock has correct children', () => {
    const genusBlock = parsedBlocks.find(block => block.blockName === "genusblock_block_1");
    expect(genusBlock).toBeDefined();

    const children = parsedBlocks.filter(block => block.parentBlock === "genusblock_block_1");

    expect(children.filter(child => child.blockType === "NameBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "SynonymBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "MentionsBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "SpeciesBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "SubgenusBlock").length).toBe(2);

    expect(children.length).toBe(6); // Total number of children
  });

  test('First SubgenusBlock has correct children', () => {
    const subgenusBlock = parsedBlocks.find(block => block.blockName === "subgenusblock_block_1");
    expect(subgenusBlock).toBeDefined();

    const children = parsedBlocks.filter(block => block.parentBlock === "subgenusblock_block_1");

    expect(children.filter(child => child.blockType === "NameBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "MentionsBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "SpeciesBlock").length).toBe(35);

    expect(children.length).toBe(37); // Total number of children
  });

  test('Second SubgenusBlock has correct children', () => {
    const subgenusBlock = parsedBlocks.find(block => block.blockName === "subgenusblock_block_2");
    expect(subgenusBlock).toBeDefined();

    const children = parsedBlocks.filter(block => block.parentBlock === "subgenusblock_block_2");

    expect(children.filter(child => child.blockType === "NameBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "MentionsBlock").length).toBe(1);
    expect(children.filter(child => child.blockType === "SpeciesBlock").length).toBe(2);

    expect(children.length).toBe(4); // Total number of children
  });

  describe('SpeciesBlock children tests', () => {
    test('SpeciesBlocks with 2 children (NameBlock and MentionsBlock)', () => {
      const speciesBlockNumbers = [1, 7, 8, 9, 10, 11, 12, 14, 15, 16, 17, 18, 19, 20, 21, 22, 25, 27, 29, 33, 35, 36];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 2, ['NameBlock', 'MentionsBlock']);
    });

    test('SpeciesBlocks with 3 children (NameBlock, MentionsBlock, CommonNameBlock)', () => {
      const speciesBlockNumbers = [5, 6, 23, 26, 28, 30, 37];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 3, ['NameBlock', 'MentionsBlock', 'CommonNameBlock']);
    });

    test('SpeciesBlocks with 4 children (NameBlock, MentionsBlock, CommonNameBlock, 1 SubspeciesBlock)', () => {
      const speciesBlockNumbers = [3];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 4, ['NameBlock', 'MentionsBlock', 'CommonNameBlock', 'SubspeciesBlock']);
    });

    test('SpeciesBlocks with 5 children (NameBlock, MentionsBlock, CommonNameBlock, 2 SubspeciesBlock)', () => {
      const speciesBlockNumbers = [24, 31, 32, 38];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 5, ['NameBlock', 'MentionsBlock', 'CommonNameBlock', 'SubspeciesBlock']);
    });

    test('SpeciesBlocks with 8 children (NameBlock, MentionsBlock, CommonNameBlock, 5 SubspeciesBlock)', () => {
      const speciesBlockNumbers = [13];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 8, ['NameBlock', 'MentionsBlock', 'CommonNameBlock', 'SubspeciesBlock']);
    });

    test('SpeciesBlocks with 9 children (NameBlock, MentionsBlock, LarvaeBlock, CommonNameBlock, 5 SubspeciesBlock)', () => {
      const speciesBlockNumbers = [34];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 9, ['NameBlock', 'MentionsBlock', 'LarvaeBlock', 'CommonNameBlock', 'SubspeciesBlock']);
    });

    test('SpeciesBlocks with 4 children (NameBlock, MentionsBlock, 2 SubspeciesBlock)', () => {
      const speciesBlockNumbers = [4];
      testSpeciesBlocks(parsedBlocks, speciesBlockNumbers, 4, ['NameBlock', 'MentionsBlock', 'SubspeciesBlock']);
    });
  });

  describe('SubspeciesBlock children tests', () => {
    test('SubspeciesBlocks with 2 children (NameBlock and MentionsBlock)', () => {
      // const subspeciesBlockNumbers = [1, 3, 5, 6, 7, 8, 9, 10, 11, 14, 15, 16, 17, 18, 21];
      const subspeciesBlockNumbers = [1, 3, 5, 6, 7, 8, 9, 14, 16, 18, 21];
      testSubspeciesBlocks(parsedBlocks, subspeciesBlockNumbers, 2, ['NameBlock', 'MentionsBlock']);
    });

    test('SubspeciesBlocks with 2 children and synonyms (NameBlock, SynonymBlock, MentionsBlock)', () => {
      const subspeciesBlockNumbers = [10, 11, 15, 16, 17];
      testSubspeciesBlocks(parsedBlocks, subspeciesBlockNumbers, 3, ['NameBlock', 'SynonymBlock', 'MentionsBlock']);
    });

    test('SubspeciesBlocks with 3 children (NameBlock, MentionsBlock, CommonNameBlock)', () => {
      const subspeciesBlockNumbers = [12, 20];
      testSubspeciesBlocks(parsedBlocks, subspeciesBlockNumbers, 3, ['NameBlock', 'MentionsBlock', 'CommonNameBlock']);
    });

    test('SubspeciesBlocks with 1 child (NameBlock)', () => {
      const subspeciesBlockNumbers = [19];
      testSubspeciesBlocks(parsedBlocks, subspeciesBlockNumbers, 1, ['NameBlock']);
    });

    test('SubspeciesBlocks with 1 child (MentionsBlock)', () => {
      const subspeciesBlockNumbers = [2, 4, 13];
      testSubspeciesBlocks(parsedBlocks, subspeciesBlockNumbers, 1, ['MentionsBlock']);
    });
  });

});

function testSpeciesBlocks(parsedBlocks: ParsedBlock[], blockNumbers: number[], expectedChildCount: number, expectedChildTypes: string[]) {
  blockNumbers.forEach(num => {
    const speciesBlock = parsedBlocks.find(block => block.blockName === `speciesblock_block_${num}`);
    expect(speciesBlock).toBeDefined();

    const children = parsedBlocks.filter(block => block.parentBlock === `speciesblock_block_${num}`);
    expect(children.length).toBe(expectedChildCount);

    expectedChildTypes.forEach(childType => {
      expect(children.some(child => child.blockType === childType)).toBe(true);
    });

    if (expectedChildTypes.includes('SubspeciesBlock')) {
      const subspeciesCount = children.filter(child => child.blockType === 'SubspeciesBlock').length;
      expect(subspeciesCount).toBe(expectedChildCount - (expectedChildTypes.length - 1));
    }
  });
}

function testSubspeciesBlocks(parsedBlocks: ParsedBlock[], blockNumbers: number[], expectedChildCount: number, expectedChildTypes: string[]) {
  blockNumbers.forEach(num => {
    const subspeciesBlock = parsedBlocks.find(block => block.blockName === `subspeciesblock_block_${num}`);
    expect(subspeciesBlock).toBeDefined();

    const children = parsedBlocks.filter(block => block.parentBlock === `subspeciesblock_block_${num}`);
    expect(children.length).toBe(expectedChildCount);

    expectedChildTypes.forEach(childType => {
      expect(children.some(child => child.blockType === childType)).toBe(true);
    });
  });
}