import { parseRelatedLiterature } from './parse';
import { Article } from '../types';

describe('parseRelatedLiterature', () => {
    const mockElement = {
        id: 'mock-id',
        textContent: '',
        getElementsByTagName: jest.fn().mockReturnValue([]),
    } as unknown as Element;

    const testCases: Array<{
        description: string;
        blockDataText: string;
        expected: Article[] | undefined;
    }> = [
            {
                description: 'Test 1',
                blockDataText: 'Watson & Goodger, 1986\nCatalogue of the Neotropical Tigermoths\nOcc. papers on syst. entomology 1\n: 1-71\n',
                expected: [
                    {
                        "citedAs": [
                            "Watson & Goodger, 1986"
                        ],
                        "title": "Catalogue of the Neotropical Tigermoths",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "1",
                                "year": 1986,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Occ. papers on syst. entomology",
                                    "id": "019276a6-e797-7e6f-af8e-d3923fbe1f0e"
                                },
                                "id": "019276a6-e797-7e6f-af8e-d391877f2d5e"
                            },
                            "id": "019276a6-e797-7e6f-af8e-d390c2013a8f"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e797-7e6f-af8e-d38ec7c6cd67",
                                    "easternNameOrder": false,
                                    "surname": "Watson"
                                },
                                "position": 0
                            },
                            {
                                "author": {
                                    "id": "019276a6-e797-7e6f-af8e-d38f524534be",
                                    "easternNameOrder": false,
                                    "surname": "Goodger"
                                },
                                "position": 1
                            }
                        ],
                        "funetRefLink": "33043",
                        "id": "019276a6-e797-7e6f-af8e-d39347635cfc"
                    }
                ]
            },
            {
                description: 'Test 2',
                blockDataText: 'Barnes & McDunnough, 1910\nNew species and varietes of North American Lepidoptera\nCan. Ent. 42\n (6): 208-213, \n (7): 246-252\n',
                expected: [
                    {
                        "citedAs": [
                            "Barnes & McDunnough, 1910"
                        ],
                        "title": "New species and varietes of North American Lepidoptera",
                        "issue": {
                            "label": "6",
                            "volume": {
                                "label": "42",
                                "year": 1910,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Can. Ent.",
                                    "id": "019276a6-e786-7b98-b0bd-dd66c77a9b45"
                                },
                                "id": "019276a6-e786-7b98-b0bd-dd651f9348af"
                            },
                            "id": "019276a6-e786-7b98-b0bd-dd64a351dcb2"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e786-7b98-b0bd-dd626410b70d",
                                    "easternNameOrder": false,
                                    "surname": "Barnes"
                                },
                                "position": 0
                            },
                            {
                                "author": {
                                    "id": "019276a6-e786-7b98-b0bd-dd633afe9f9c",
                                    "easternNameOrder": false,
                                    "surname": "McDunnough"
                                },
                                "position": 1
                            }
                        ],
                        "funetRefLink": "1361",
                        "id": "019276a6-e786-7b98-b0bd-dd67f6de5934"
                    },
                    {
                        "citedAs": [
                            "Barnes & McDunnough, 1910"
                        ],
                        "title": "New species and varietes of North American Lepidoptera",
                        "issue": {
                            "label": "7",
                            "volume": {
                                "label": "42",
                                "year": 1910,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Can. Ent.",
                                    "id": "019276a6-e786-7b98-b0bd-dd6a44756fd9"
                                },
                                "id": "019276a6-e786-7b98-b0bd-dd698400e2cd"
                            },
                            "id": "019276a6-e786-7b98-b0bd-dd68855866b5"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e786-7b98-b0bd-dd626410b70d",
                                    "easternNameOrder": false,
                                    "surname": "Barnes"
                                },
                                "position": 0
                            },
                            {
                                "author": {
                                    "id": "019276a6-e786-7b98-b0bd-dd633afe9f9c",
                                    "easternNameOrder": false,
                                    "surname": "McDunnough"
                                },
                                "position": 1
                            }
                        ],
                        "funetRefLink": "1361",
                        "id": "019276a6-e786-7b98-b0bd-dd6b31b8721e"
                    }
                ]
            },
            {
                description: 'Test 3',
                blockDataText: 'Strecker, 1878\nLepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations\nLep. Rhopal. Het.\n (1): 1-7,  pl. 1 (1872), \n (2): 9-15,  pl. 2 (1873), \n (3): 17-24,  pl. 3 (1873), \n (4): 25-32,  pl. 4 (1873), \n (5): 33-43,  pl. 5 (1873), \n (6): 45-50,  pl. 6 (1873), \n (7): 51-60,  pl. 7 (1873), \n (8): 61-70,  pl. 8 (1874), \n (9): 71-80,  pl. 9 (1874), \n (10): 81-94,  pl. 10 (1874), \n (11): 95-100,  pl. 11 (1874), \n (12): 101-108,  pl. 12 (1875), \n (13): 109-123,  pl. 13 (1876), \n (14): 125-134,  pl. 14 (1878), \n (15): 135-143,  pl. 15 (1878)\n',
                expected: [
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "1",
                            "volume": {
                                "label": "",
                                "year": 1872,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce25090ec4c6"
                                },
                                "id": "019276a6-e795-7e07-9103-ce24ec2732ae"
                            },
                            "id": "019276a6-e795-7e07-9103-ce236108ad74"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce264978eec0"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "2",
                            "volume": {
                                "label": "",
                                "year": 1873,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce2968ff8c59"
                                },
                                "id": "019276a6-e795-7e07-9103-ce2833d28606"
                            },
                            "id": "019276a6-e795-7e07-9103-ce27fe48dcb6"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce2a749d277a"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "3",
                            "volume": {
                                "label": "",
                                "year": 1873,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce2de01e204d"
                                },
                                "id": "019276a6-e795-7e07-9103-ce2ce2fe5ab5"
                            },
                            "id": "019276a6-e795-7e07-9103-ce2bfd1dba1a"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce2ef3aa335a"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "4",
                            "volume": {
                                "label": "",
                                "year": 1873,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce31983dcb5f"
                                },
                                "id": "019276a6-e795-7e07-9103-ce3041bdcea0"
                            },
                            "id": "019276a6-e795-7e07-9103-ce2fab9197d5"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce323bcc2886"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "5",
                            "volume": {
                                "label": "",
                                "year": 1873,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce35d2b18fae"
                                },
                                "id": "019276a6-e795-7e07-9103-ce34e54916e3"
                            },
                            "id": "019276a6-e795-7e07-9103-ce330ab4af36"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce36d4916079"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "6",
                            "volume": {
                                "label": "",
                                "year": 1873,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce39939d343d"
                                },
                                "id": "019276a6-e795-7e07-9103-ce38af982678"
                            },
                            "id": "019276a6-e795-7e07-9103-ce3785b010ad"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce3ab03aa405"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "7",
                            "volume": {
                                "label": "",
                                "year": 1873,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce3d55282f19"
                                },
                                "id": "019276a6-e795-7e07-9103-ce3c06b3f2d8"
                            },
                            "id": "019276a6-e795-7e07-9103-ce3b11affe9e"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce3ef687b63b"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "8",
                            "volume": {
                                "label": "",
                                "year": 1874,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce418b32af33"
                                },
                                "id": "019276a6-e795-7e07-9103-ce408f0d7db4"
                            },
                            "id": "019276a6-e795-7e07-9103-ce3f8018a303"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce42739cf51e"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "9",
                            "volume": {
                                "label": "",
                                "year": 1874,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce45c9ae0b52"
                                },
                                "id": "019276a6-e795-7e07-9103-ce443f0abace"
                            },
                            "id": "019276a6-e795-7e07-9103-ce43341602b3"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce46a746ff94"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "10",
                            "volume": {
                                "label": "",
                                "year": 1874,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce49e501921f"
                                },
                                "id": "019276a6-e795-7e07-9103-ce4878864d74"
                            },
                            "id": "019276a6-e795-7e07-9103-ce47313adced"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce4aa5f980f3"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "11",
                            "volume": {
                                "label": "",
                                "year": 1874,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce4d1af2b228"
                                },
                                "id": "019276a6-e795-7e07-9103-ce4c44a4d70f"
                            },
                            "id": "019276a6-e795-7e07-9103-ce4b295fc788"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce4e0223bb8a"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "12",
                            "volume": {
                                "label": "",
                                "year": 1875,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce5169cce76c"
                                },
                                "id": "019276a6-e795-7e07-9103-ce5053a61c79"
                            },
                            "id": "019276a6-e795-7e07-9103-ce4f3a162f13"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce524087f0b0"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "13",
                            "volume": {
                                "label": "",
                                "year": 1876,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e795-7e07-9103-ce554e324d99"
                                },
                                "id": "019276a6-e795-7e07-9103-ce54e9191d44"
                            },
                            "id": "019276a6-e795-7e07-9103-ce530ff5ac28"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e795-7e07-9103-ce56d811b5eb"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "14",
                            "volume": {
                                "label": "",
                                "year": 1878,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e796-7ae4-95ee-f84da1c11ab0"
                                },
                                "id": "019276a6-e796-7ae4-95ee-f84c858317d9"
                            },
                            "id": "019276a6-e796-7ae4-95ee-f84bb76b4b5f"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e796-7ae4-95ee-f84ec002adcf"
                    },
                    {
                        "citedAs": [
                            "Strecker, 1878"
                        ],
                        "title": "Lepidoptera, Rhopaloceres and Heteroceres, indigenous and exotic, with descriptions and colored illustrations",
                        "issue": {
                            "label": "15",
                            "volume": {
                                "label": "",
                                "year": 1878,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "Lep. Rhopal. Het.",
                                    "id": "019276a6-e796-7ae4-95ee-f851f3f33a67"
                                },
                                "id": "019276a6-e796-7ae4-95ee-f85040a3ee61"
                            },
                            "id": "019276a6-e796-7ae4-95ee-f84f332d22e0"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e795-7e07-9103-ce22d285865b",
                                    "easternNameOrder": false,
                                    "surname": "Strecker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "29602",
                        "id": "019276a6-e796-7ae4-95ee-f8524225d3ea"
                    }
                ]
            },
            {
                description: 'Test 4',
                blockDataText: 'Walker, [1865]\nList of the Specimens of Lepidopterous Insects in the Collection of the British Museum\nList Spec. Lepid. Insects Colln Br. Mus. \n 31: 1-322 ([1865]), \n 32: 323-706 (1865), \n 33: 707-1120 (1865), \n 34: 1121-1534 ([1866]), \n 35: 1535-2040 (1866)\n',
                expected: [
                    {
                        "citedAs": [
                            "Walker, [1865]"
                        ],
                        "title": "List of the Specimens of Lepidopterous Insects in the Collection of the British Museum",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "31",
                                "year": 1865,
                                "bracketedYear": true,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "List Spec. Lepid. Insects Colln Br. Mus.",
                                    "id": "019276a6-e797-7e6f-af8e-d36a53246f68"
                                },
                                "id": "019276a6-e797-7e6f-af8e-d36908759dc6"
                            },
                            "id": "019276a6-e797-7e6f-af8e-d3686c8786cc"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e796-7ae4-95ee-f8779f71697e",
                                    "easternNameOrder": false,
                                    "surname": "Walker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "32549",
                        "id": "019276a6-e797-7e6f-af8e-d36b553867cb"
                    },
                    {
                        "citedAs": [
                            "Walker, [1865]"
                        ],
                        "title": "List of the Specimens of Lepidopterous Insects in the Collection of the British Museum",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "32",
                                "year": 1865,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "List Spec. Lepid. Insects Colln Br. Mus.",
                                    "id": "019276a6-e797-7e6f-af8e-d36ea5228048"
                                },
                                "id": "019276a6-e797-7e6f-af8e-d36d9b1ae2b8"
                            },
                            "id": "019276a6-e797-7e6f-af8e-d36c4eda80f2"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e796-7ae4-95ee-f8779f71697e",
                                    "easternNameOrder": false,
                                    "surname": "Walker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "32549",
                        "id": "019276a6-e797-7e6f-af8e-d36ff0d25727"
                    },
                    {
                        "citedAs": [
                            "Walker, [1865]"
                        ],
                        "title": "List of the Specimens of Lepidopterous Insects in the Collection of the British Museum",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "33",
                                "year": 1865,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "List Spec. Lepid. Insects Colln Br. Mus.",
                                    "id": "019276a6-e797-7e6f-af8e-d37262d2ab57"
                                },
                                "id": "019276a6-e797-7e6f-af8e-d371dbceeda5"
                            },
                            "id": "019276a6-e797-7e6f-af8e-d370b8ea54d8"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e796-7ae4-95ee-f8779f71697e",
                                    "easternNameOrder": false,
                                    "surname": "Walker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "32549",
                        "id": "019276a6-e797-7e6f-af8e-d373502ff1f7"
                    },
                    {
                        "citedAs": [
                            "Walker, [1865]"
                        ],
                        "title": "List of the Specimens of Lepidopterous Insects in the Collection of the British Museum",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "34",
                                "year": 1866,
                                "bracketedYear": true,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "List Spec. Lepid. Insects Colln Br. Mus.",
                                    "id": "019276a6-e797-7e6f-af8e-d376db2aef24"
                                },
                                "id": "019276a6-e797-7e6f-af8e-d3758a0237ed"
                            },
                            "id": "019276a6-e797-7e6f-af8e-d374b271f630"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e796-7ae4-95ee-f8779f71697e",
                                    "easternNameOrder": false,
                                    "surname": "Walker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "32549",
                        "id": "019276a6-e797-7e6f-af8e-d377ac3ba25f"
                    },
                    {
                        "citedAs": [
                            "Walker, [1865]"
                        ],
                        "title": "List of the Specimens of Lepidopterous Insects in the Collection of the British Museum",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "35",
                                "year": 1866,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "",
                                    "abbreviation": "List Spec. Lepid. Insects Colln Br. Mus.",
                                    "id": "019276a6-e797-7e6f-af8e-d37a24988862"
                                },
                                "id": "019276a6-e797-7e6f-af8e-d37983abe19e"
                            },
                            "id": "019276a6-e797-7e6f-af8e-d378caddbf1a"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "019276a6-e796-7ae4-95ee-f8779f71697e",
                                    "easternNameOrder": false,
                                    "surname": "Walker"
                                },
                                "position": 0
                            }
                        ],
                        "funetRefLink": "32549",
                        "id": "019276a6-e797-7e6f-af8e-d37b14f5324b"
                    }
                ]
            },
            {
                description: 'Test 5',
                blockDataText: '[SPRK]; Seppänen, 1970\nSuomen suurperhostoukkien ravintokasvit\nAnimalia Fennica 14\n',
                expected: [
                    {
                        "citedAs": [
                            "Seppänen, 1970"
                        ],
                        "title": "Suomen suurperhostoukkien ravintokasvit",
                        "issue": {
                            "label": "",
                            "volume": {
                                "label": "14",
                                "year": 1970,
                                "bracketedYear": false,
                                "journal": {
                                    "title": "Animalia Fennica",
                                    "abbreviation": "",
                                    "id": "01927761-a9f1-70a0-be9f-0d3bb5371516"
                                },
                                "id": "01927761-a9f1-70a0-be9f-0d3a6e462aaa"
                            },
                            "id": "01927761-a9f1-70a0-be9f-0d398e8bd4ad"
                        },
                        "tags": [],
                        "authors": [
                            {
                                "author": {
                                    "id": "01927761-a9f1-70a0-be9f-0d38f78b4348",
                                    "easternNameOrder": false,
                                    "surname": "Seppänen"
                                },
                                "position": 0
                            }
                        ],
                        "abbreviation": "SPRK",
                        "funetRefLink": "27302",
                        "id": "01927761-a9f1-70a0-be9f-0d3c7002e25a"
                    }
                ]
            },
            // Add more test cases here
        ];

    testCases.forEach(({ description, blockDataText, expected }) => {
        it(description, () => {
            mockElement.textContent = blockDataText;
            const result = parseRelatedLiterature(mockElement);

            console.log('Test case:', description);
            console.log('Result:', JSON.stringify(result, null, 2));
            console.log('Expected:', JSON.stringify(expected, null, 2));

            if (result && expected) {
                result.forEach((article, index) => {
                    expect(article).toMatchObject({
                        id: expect.any(String),
                        funetRefLink: mockElement.id,
                        citedAs: expected[index].citedAs,
                        title: expected[index].title,
                        issue: expect.objectContaining({
                            id: expect.any(String),
                            label: expected[index].issue?.label,
                            volume: expect.objectContaining({
                                id: expect.any(String),
                                label: expect.any(String),
                                year: expected[index].issue?.volume?.year,
                                bracketedYear: expected[index].issue?.volume?.bracketedYear,
                                journal: expect.objectContaining({
                                    id: expect.any(String),
                                    abbreviation: expected[index].issue?.volume?.journal?.title || "",
                                    title: expected[index].issue?.volume?.journal?.abbreviation || "",
                                }),
                            }),
                        }),
                        authors: expect.arrayContaining(
                            expected[index].authors?.map(author => expect.objectContaining({
                                author: expect.objectContaining({
                                    id: expect.any(String),
                                    surname: author.author.surname,
                                }),
                                position: author.position,
                            })) || []
                        ),
                        abbreviation: expected[index].abbreviation,
                    });
                });
            } else {
                fail('Result or expected is undefined');
            }
        });
    });
});