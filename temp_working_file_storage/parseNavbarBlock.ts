import fs from 'fs';
import path from 'path';
import { uuidv7 } from 'uuidv7';
import { JSDOM } from 'jsdom';

interface TaxonItem {
    id: string;
    name: string;
    rank: string;
    parentId: string | null;
}

const TAXONOMY_FILE = './data/taxonomy_data.json';

function loadExistingTaxonomy(): TaxonItem[] {
    try {
        const data = fs.readFileSync(TAXONOMY_FILE, 'utf8');
        return JSON.parse(data) as TaxonItem[];
    } catch (error) {
        return [];
    }
}

function saveTaxonomyToFile(taxonomy: TaxonItem[]): void {
    const data = JSON.stringify(taxonomy, null, 2);
    fs.writeFileSync(TAXONOMY_FILE, data, 'utf8');
}

export function extractTaxonomicHierarchy(element: Element): TaxonItem[] {
    const existingTaxonomy = loadExistingTaxonomy();
    const result: TaxonItem[] = [];

    function determineRank(name: string): string {
        const rankMap: { [key: string]: string } = {
            'Insecta': 'class',
            'Mammalia': 'class',
            'Aves': 'class',
            'Reptilia': 'class',
            'Batrachia': 'class',
            'Pisces': 'clade',
            'Chilopoda': 'class',
            'Diplopoda': 'class',
            'Crustacea': 'class',
            'Arachnida': 'class',
            'Monotremata': 'order',
            'Marsupialia': 'order',
            'Insectivora': 'order',
            'Chiroptera': 'order',
            'Primates': 'order',
            'Lagomorpha': 'order',
            'Rodentia': 'order',
            'Hystricognathi': 'order',
            'Carnivora': 'order',
            'Proboscidea': 'order',
            'Hyracoidea': 'order',
            'Sirenia': 'order',
            'Perissodactyla': 'order',
            'Artiodactyla': 'order',
            'Pholidota': 'order',
            'Cetacea': 'order',
            'formes': 'order',
            'Ophidia': 'order',
            'Sauria': 'order',
            'Testudines': 'order',
            'Anura': 'order',
            'Urodela': 'order',
            'Myxini': 'class',
            'Cephalaspidomorphi': 'clade',
            'Chondrichthyes': 'clade',
            'terygii': 'class',
            'optera': 'order',
            'oidea': 'superfamily',
            'idae': 'family',
            'inae': 'subfamily',
            'ini': 'tribe',
            'ina': 'subtribe'
        };

        for (const [key, value] of Object.entries(rankMap)) {
            if (name.includes(key)) {
                return value;
            }
        }

        return 'clade';
    }

    const upSpans = element.querySelectorAll('span.up');

    upSpans.forEach((span) => {
        const nameElement = span.querySelector('a');
        if (!nameElement) return;

        const name = nameElement.textContent?.trim();
        if (!name || name === 'Life') return;

        let taxonItem = existingTaxonomy.find(item => item.name === name);

        if (taxonItem) {
            // Check for hierarchical mismatch
            const expectedParentId = result.length > 0 ? result[result.length - 1].id : null;
            if (taxonItem.parentId !== expectedParentId) {
                const existingParent = existingTaxonomy.find(item => item.id === taxonItem!.parentId);
                const expectedParent = result[result.length - 1];
                throw new Error(`Hierarchical mismatch detected for "${name}". ` +
                    `Existing parent: "${existingParent?.name}" (ID: ${taxonItem.parentId}). ` +
                    `Expected parent based on current parsing: "${expectedParent?.name}" (ID: ${expectedParentId}).`);
            }
        } else {
            const rank = determineRank(name);
            taxonItem = {
                id: uuidv7(),
                name: name,
                rank: rank,
                parentId: result.length > 0 ? result[result.length - 1].id : null
            };
            existingTaxonomy.push(taxonItem);
        }

        result.push(taxonItem);
    });

    saveTaxonomyToFile(existingTaxonomy);

    return result;
}