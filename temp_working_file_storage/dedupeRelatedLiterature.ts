import fs from 'fs/promises';
import path from 'path';
import { Article } from './types';

interface ParsedBlock {
    blockType: string;
    parsedBlockData: any;
}

async function dedupeRelatedLiterature() {
    try {
        const parsingResultsPath = path.join(__dirname, '..', '..', 'data', 'parsing_results.json');
        const rawData = await fs.readFile(parsingResultsPath, 'utf-8');
        console.log('Raw data:', rawData.substring(0, 100) + '...'); // Log the first 100 characters

        const parsedData = JSON.parse(rawData);

        if (!parsedData.parsedBlocks || !Array.isArray(parsedData.parsedBlocks)) {
            console.error('Error: parsing_results.json does not contain a parsedBlocks array');
            console.log('Content of parsing_results.json:', JSON.stringify(parsedData, null, 2));
            return;
        }

        const relatedLiteratureBlocks = parsedData.parsedBlocks.filter(
            (item: { blockType: string; parsedBlockData: any; }) => item.blockType === 'RelatedLiteratureBlock' && item.parsedBlockData
        );

        const dedupedResults: ParsedBlock[] = [];
        let addedCount = 0;
        let updatedCount = 0;
        let skippedCount = 0;

        for (let i = 0; i < relatedLiteratureBlocks.length; i++) {
            const item = relatedLiteratureBlocks[i];
            const { id, title, doi, url, ...comparableData } = item.parsedBlockData;

            console.log(`\nProcessing item ${i + 1}:`);
            console.log(`  Title: ${title || 'undefined'}`);
            console.log(`  DOI: ${doi || 'undefined'}`);
            console.log(`  URL: ${url || 'undefined'}`);
            console.log(`  Other fields: ${Object.keys(comparableData).join(', ')}`);

            const existingIndex = dedupedResults.findIndex(existing => {
                const { id: existingId, title: existingTitle, doi: existingDoi, url: existingUrl, ...existingComparableData } = existing.parsedBlockData;
                return JSON.stringify(comparableData) === JSON.stringify(existingComparableData);
            });

            if (existingIndex === -1) {
                // No overlap
                console.log(`  Record not found... storing.`);
                dedupedResults.push(item);
                addedCount++;
            } else {
                // Partial or complete overlap
                const existing = dedupedResults[existingIndex];
                const [merged, updatedFields] = mergeItems(existing, item);

                if (updatedFields.length > 0) {
                    // Partial overlap
                    console.log(`  Similar record found... updating.`);
                    console.log(`  Fields added/updated: ${updatedFields.join(", ")}`);
                    dedupedResults[existingIndex] = merged;
                    updatedCount++;
                } else {
                    // Complete overlap
                    console.log(`  Exact record found... skipping.`);
                    skippedCount++;
                }
            }
        }

        // Write the deduped results back to a file
        const outputPath = path.join(__dirname, '..', '..', 'data', 'db_relatedLiterature.json');
        await fs.writeFile(outputPath, JSON.stringify(dedupedResults, null, 2));
        console.log(`Deduplication complete. Results written to ${outputPath}`);

        // Print summary
        console.log("\nDeduplication Summary:");
        console.log(`Total records processed: ${relatedLiteratureBlocks.length}`);
        console.log(`Records added: ${addedCount}`);
        console.log(`Records updated: ${updatedCount}`);
        console.log(`Records skipped: ${skippedCount}`);

    } catch (error) {
        console.error('Error during deduplication:', error);
        if (error instanceof SyntaxError) {
            console.error('JSON parsing failed. The file might be empty or contain invalid JSON.');
        }
        console.log('Current directory:', __dirname);
        console.log('Attempted file path:', path.join(__dirname, '..', '..', 'data', 'parsing_results.json'));
    }
}

function mergeItems(existing: ParsedBlock, newItem: ParsedBlock): [ParsedBlock, string[]] {
    const merged = { ...existing };
    merged.parsedBlockData = { ...existing.parsedBlockData };
    const updatedFields: string[] = [];

    for (const [key, value] of Object.entries(newItem.parsedBlockData)) {
        if (key !== 'id') {
            if (merged.parsedBlockData[key] === undefined) {
                merged.parsedBlockData[key] = value;
                updatedFields.push(`${key}: ${value}`);
            } else if (merged.parsedBlockData[key] !== value) {
                console.log(`  Conflict in merging: ${key}`);
                console.log(`    Existing value: ${merged.parsedBlockData[key]}`);
                console.log(`    New value: ${value}`);
                // Skip updating this field
            }
        }
    }

    return [merged, updatedFields];
}

dedupeRelatedLiterature();
