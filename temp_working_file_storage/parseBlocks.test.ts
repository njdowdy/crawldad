// parseFromURL.test.ts

import { parseBlocksFromSource, ParsedBlock, getPageHTML } from './parseBlocks';

describe('Parse HTML from Source', () => {
    let parsedBlocks: ParsedBlock[];
    beforeAll(async () => {
        const url = 'https://www.nic.funet.fi/pub/sci/bio/life/insecta/lepidoptera/ditrysia/noctuoidea/arctiidae/arctiinae/grammia/';
        const localFilePath = '/Users/njdowdy/code/crawldad/funet/data/grammia.html';

        try {
            // First, try to fetch from URL
            const htmlContent = await getPageHTML(url);
            if (htmlContent) {
                parsedBlocks = await parseBlocksFromSource(url, false);
            } else {
                throw new Error('Failed to fetch from URL');
            }
        } catch (error: unknown) {
            let errorMessage = 'An unknown error occurred';
            if (error instanceof Error) {
                errorMessage = error.message;
            } else if (typeof error === 'string') {
                errorMessage = error;
            }
            console.warn(`Failed to fetch from URL: ${errorMessage}. Falling back to local file.`);
            // If URL fetch fails, use local file
            parsedBlocks = await parseBlocksFromSource(localFilePath, true);
        }
    }, 60000); // Increase timeout to 60 seconds for the network request

    test('Blocks are parsed successfully', () => {
        expect(parsedBlocks.length).toBeGreaterThan(0);
    });
    // test('Parsed block structure matches expected structure', () => {
    //     const expectedStructure = [
    //         { blockType: 'NAVBAR', count: 1 },
    //         { blockType: 'GenusBlock', count: 1 },
    //         { blockType: 'NameBlock', parentType: 'GenusBlock', count: 1 },
    //         { blockType: 'MentionsBlock', parentType: 'GenusBlock', count: 1 },
    //         { blockType: 'NameBlock', parentType: 'GenusBlock', count: 2 },

    //         { blockType: 'Last update date', count: 1 },
    //         { blockType: 'Web References', count: 1 },
    //         { blockType: 'Literature References', count: 1 }
    //     ];

    //     let blockIndex = 0;
    //     expectedStructure.forEach(expectedBlock => {
    //         for (let i = 0; i < expectedBlock.count; i++) {
    //             const block = parsedBlocks[blockIndex];
    //             expect(block.blockType).toBe(expectedBlock.blockType);

    //             if (expectedBlock.parentType) {
    //                 const parent = parsedBlocks.find(b => b.blockType === expectedBlock.parentType);
    //                 expect(block.parentBlock).toBe(parent?.blockName);
    //             } else {
    //                 expect(block.parentBlock).toBeNull();
    //             }

    //             blockIndex++;
    //         }
    //     });

    //     // Check total number of blocks
    //     expect(parsedBlocks.length).toBe(expectedStructure.reduce((sum, block) => sum + block.count, 0));
    // });

    test('Web References block has 23 subitems', () => {
        const webReferencesBlock = parsedBlocks.find(block => block.blockType === 'Web References');
        expect(webReferencesBlock).toBeDefined();
        const subitems = webReferencesBlock!.blockData.querySelectorAll('li');
        expect(subitems.length).toBe(23);
    });

    test('Literature References block has 86 subitems', () => {
        const literatureReferencesBlock = parsedBlocks.find(block => block.blockType === 'Literature References');
        expect(literatureReferencesBlock).toBeDefined();
        const subitems = literatureReferencesBlock!.blockData.querySelectorAll('li');
        expect(subitems.length).toBe(86);
    });

    test('All blocks have unique names', () => {
        const blockNames = parsedBlocks.map(block => block.blockName);
        const uniqueBlockNames = new Set(blockNames);
        expect(blockNames.length).toBe(uniqueBlockNames.size);
    });
});